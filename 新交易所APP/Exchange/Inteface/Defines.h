//
//  Defines.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/9/15.
//  Copyright © 2017年 Mac. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

//自定义数据库名字
#define FMDBName_Collection @"Collection"
#define FMDBName_Symbol @"Symbol"


/*APP风格*/
#define WHITE_COLOR @"WHITECOLOR"
#define BLACK_COLOR @"BLACKCOLOR"

//单例模式宏模板
#define DEFINE_SINGLETON_FOR_HEADER(className) \
\
+ (className *)shared##className;

#define DEFINE_SINGLETON_FOR_CLASS(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}
#define C_UserDefaults_Set(key, value) NSUserDefaults *ud = [[NSUserDefaults standardUserDefaults] init]; [ud setObject:value forKey:key]; [ud synchronize];
#define C_UserDefaults_Get(key) [[NSUserDefaults standardUserDefaults] objectForKey:key];
//系统
#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
#define IS_IOS11 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 11.0)
//获取设备唯一ID
#define DEVICEID [[UIDevice currentDevice].identifierForVendor UUIDString]

//Value
#define RealValue_W(value) ((value)/2.0/375.0f*[UIScreen mainScreen].bounds.size.width)
#define RealValue_H(value) ((value)/2.0/375.0f*[UIScreen mainScreen].bounds.size.width)
//#define RealValue_H(value) ((value)/2.0/667.0f* [UIScreen mainScreen].bounds.size.height)

//Frame
#define SCREEN_SIZE [[UIScreen mainScreen]bounds].size
#define SCREEN_FRAME [[UIScreen mainScreen]bounds]
#define PassworldLastlength  5
#define Passworldhighlength  17
#define codelength  6
#define UIScreenWidth SCREEN_SIZE.width
#define UIScreenHeight  SCREEN_SIZE.height
#define SCREEN_CENTER_X SCREEN_SIZE.width/2
#define SCREEN_CENTER_Y SCREEN_SIZE.height/2
#define NAVI_HEIGHT ([EXUnit deviceIsIPhoneX] ? 64 + 24 : 64)
#define TABBAR_HEIGHT1 ((IS_IPhoneX_All == YES) ? 50 + 24 : 50)
#define ContentViewHeight UIScreenHeight - NAVI_HEIGHT - TABBAR_HEIGHT

#define IS_IPhoneX_All ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896)
#define IPhoneTop ((IS_IPhoneX_All == YES) ? 24 : 0.0)
#define kStatusBarAndNavigationBarHeight ((IS_IPhoneX_All == YES) ? 88.0 : 64.0)
#define IPhoneBottom ((IS_IPhoneX_All == YES) ? 16 : 0.0)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH MAX(UIScreenWidth,UIScreenHeight)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)


//自定义颜色
#define RGBA(R/*红*/, G/*绿*/, B/*蓝*/, A/*透明*/) \
[UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]

//系统颜色
#define WHITECOLOR [UIColor whiteColor]
#define CLEARCOLOR [UIColor clearColor]
#define BLACKCOLOR [UIColor blackColor]
#define ORANGECOLOR [UIColor orangeColor]

#define DEFAULTBACKGROUNDCOLOR RGBA(240,240,240,1)
#define NAVTITLECOLOR RGBA(31,84,139,1)
#define TABNORMALCOLOR RGBA(111, 111, 111, 1)
#define maintitleLCOLOR RGBA(114, 145, 161, 1)
#define colorrefColor(R, G, B, A)  CGColorCreate(colorSpace,(CGFloat[]){ R/255.f, G/255.f, B/255.f, A });
#define ColorStr(a) [EXUnit colorWithHexString: (NSString *)a]

//ZG
//
#if TARGET_OS_IPHONE 

#else

#endif
#define MainBGCOLOR [ZBLocalized sharedInstance].mainBgViewColor
#define TRABSACTIONColor [ZBLocalized sharedInstance].popBgViewColor
#define MAINCOLOR   [ZBLocalized sharedInstance].mainBgColor
#define textFieldColor   [ZBLocalized sharedInstance].textfieldBgColor
#define CELLCOLOR   [ZBLocalized sharedInstance].cellBgColor
#define TABLEVIEWLCOLOR  [ZBLocalized sharedInstance].tableViewBgColor
#define MAINBLACKCOLOR [ZBLocalized sharedInstance].mainBlockBgColor
#define textFieldPCOLOR [ZBLocalized sharedInstance].textFieldPBgColor
#define SEGMENTBgViewColor [ZBLocalized sharedInstance].segmentBgViewColor
//主题颜色
#define TABBARBGCOLOR [ZBLocalized sharedInstance].tabbarBgColor
//tabbar所有按钮颜色
#define TABTITLECOLOR [ZBLocalized sharedInstance].tabtitleColor
//tabbar字颜色和涨幅颜色
#define ZHANGCOLOR [ZBLocalized sharedInstance].zhangTitleColor
//跌字体颜色
#define DIEECOLOR [ZBLocalized sharedInstance].dieTitleColor
//k线图背景颜色
#define KMAINCOLOR [ZBLocalized sharedInstance].kmainBgColor

#define MAINTITLECOLOR1 [ZBLocalized sharedInstance].maintitleColor1
#define MAINTITLECOLOR [ZBLocalized sharedInstance].maintitleColor


//系统字体
#define SystemFont(size) [UIFont systemFontOfSize:(size)]
#define SystemBlodFont(font)  [UIFont fontWithName:@"Helvetica-Bold"size:font]
//* 适配字体  *//
#define AutoFont(size)     [UIFont systemFontOfSize:size * UIScreenWidth/(375.0f)]

#define AutoBoldFont(font)   [UIFont fontWithName:@"Helvetica-Bold"size:font * UIScreenWidth/(375.0f)]

/// View 圆角
#define KViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

///  View加边框
#define KViewBorder(View, BorderColor, BorderWidth )\
\
View.layer.borderColor = BorderColor.CGColor;\
View.layer.borderWidth = BorderWidth;
//cell 价格颜色
#define kCellLineColor      ColorStr(@"f0f0f1")

//防止block里引用self造成循环引用
#define WeakObject(obj/*对象*/)  __weak __typeof(obj)weak##obj = obj;

//weakself
#define WeakSelf typeof(self) __weak weakSelf = self;

//strongself
#define StrongSelf __strong __typeof(self) strongSelf = weakSelf;

//沙盒路径
#define LibraryPath [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject]
#define DocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
#define CachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]

#define ChatBookStr @"QuakeVideo-ChatBook"

#define ChatBookPath [NSString stringWithFormat:@"%@/%@",DocumentPath,ChatBookStr]


//window
#define  kWindow   [[[UIApplication sharedApplication] delegate] window]
//* 站位图  *//
#define PlaceHolderImage   [UIImage imageNamed:@"morentouxiang"]
#define UnreadMessageKey  @"UnreadMessage"

#define UnreadNoticeKey   @"UnreadNoticeKey"


/*k 线图的*/
#define kRGBColor(R,G,B)   [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1.0]

#define gCommonBackgroundColor UIColorFromHex(0xffffff)
#define gMainColor UIColorFromHex(0x4A90E2)
#define gThickLineColor UIColorFromHex(0xf2f2f2)
#define gThinLineColor UIColorFromHex(0xeeeeee)

#define gTextColorMain UIColorFromHex(0x4a4a4a)
#define gTextColorSub UIColorFromHex(0x999999)
#define gTextColor333 UIColorFromHex(0x333333)
#define gTextBuySub UIColorFromHex(0x00be66)
#define gTextSellSub UIColorFromHex(0xea573c)
#define gTextNoChangeSub UIColorFromHex(0x999999)
#define gTextColorSpecial UIColorFromHex(0xca0051)
#define gTextColorDetail UIColorFromHex(0xc8c8c8)
#define gBorderColorDetail UIColorFromHex(0xe8e8e8)

#define gFontSelected18 [UIFont systemFontOfSize:18.0]
#define gFontMain15 [UIFont systemFontOfSize:15.0]
#define gFontSub12 [UIFont systemFontOfSize:13.0]
#define gFontDetail10 [UIFont systemFontOfSize:10.5]
#define UIColorFromHexWithAlpha(hexValue,a) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:a]

#define UIColorFromHex(hexValue)            UIColorFromHexWithAlpha(hexValue,1.0)
#define UIColorFromRGBA(r,g,b,a)            [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define UIColorFromRGB(r,g,b)               UIColorFromRGBA(r,g,b,1.0)

//iOS 11头部下拉刷新控件适配
#define AdjustsScrollViewInsetNever(controller,view) if(@available(iOS 11.0, *)) {view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;} else if([controller isKindOfClass:[UIViewController class]]) {controller.automaticallyAdjustsScrollViewInsets = false;}

#define ifIsiPhoneX ([UIScreen mainScreen].bounds.size.height > 736 ? YES:NO)

//适配字体及屏幕宽高基于的宽度(6/6s)
#define FitScreenSize(size) ([CFBaseTool smartSizeCalculate:size])
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define PFRFontWithSize(s) (IOS_VERSION >= 9.0?[UIFont fontWithName:@"PingFangSC-Regular" size:(s)]:SysFontWithSize(s))
#define PFMFontWithSize(s) (IOS_VERSION >= 9.0?[UIFont fontWithName:@"PingFangSC-Medium" size:(s)]:SysFontWithSize(s))
#define PFSFontWithSize(s) (IOS_VERSION >= 9.0?[UIFont fontWithName:@"PingFangSC-Semibold" size:(s)]:SysFontWithSize(s))
#define SysFontWithSize(s)    [UIFont systemFontOfSize:(s)]
#define CF_IMAGE(name) [UIImage imageNamed:(name)]
#define CF_IMAGEURL(imgUrlStr) [NSURL URLWithString:(imgUrlStr)]
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define APPDELEGATE ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define AUTOLANGUAGE(a) [LocalizationManager getStringByKey:a]
//我的界面宽度
#define MINE_VC_WIDTH 300

//沙盒中Preferences文件夹的路径
#define PREFERENCES_PATH [NSString stringWithFormat:@"%@/Preferences",NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0]]

#endif /* Defines_h */
