//
//  CalculatedHead.pch
//  Calculated
//
//  Created by 张庆勇 on 2018/3/20.
//  Copyright © 2018年 张庆勇. All rights reserved.

#import "ZBLocalized.h"
#import "HomeViewController.h"
#import "QuotationViewController.h"
#import "PurchaseViewController.h"
#import "WalletViewController.h"
#import "PersonalCenterViewController.h"
@implementation ZBLocalized
+ (ZBLocalized *)sharedInstance{
    static ZBLocalized *language=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        language = [[ZBLocalized alloc] init];
    });
    return language;
}
//设置皮肤
- (void)setAppbBGColor:(NSString *)Color;
{
    [[NSUserDefaults standardUserDefaults] setObject:Color forKey:appBGColor];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)AppbBGColor{
    NSString *language=[[NSUserDefaults standardUserDefaults]objectForKey:appBGColor];
    return language;
}
- (void)initAppbBGColor{
    NSString *language=[self AppbBGColor];
    
    if ([language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        [self setAppbBGColor:BLACK_COLOR];
        
    }else if ([NSString isEmptyString:language] ||[language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        [self setAppbBGColor:WHITE_COLOR];
    }else
    {
        [self setAppbBGColor:WHITE_COLOR];
    }
}
- (void)initLanguage{
    NSString *language=[self currentLanguage];
    if ([[EXUserManager getLanguage] isEqualToString:@"1"])
    {
        NSArray *languages = [NSLocale preferredLanguages];
        NSString *currentLanguage = [languages objectAtIndex:0];
        [self setLanguage:currentLanguage];
    }else
    {
        if ([NSString isEmptyString:language]) {
            NSArray *languages = [NSLocale preferredLanguages];
            NSString *currentLanguage = [languages objectAtIndex:0];
            [self setLanguage:currentLanguage];
            
        }
    }
    
    [self systemLanguage];
    
    
    
}
- (NSString *)currentLanguage{
    NSString *language=[[NSUserDefaults standardUserDefaults]objectForKey:appLanguage];
    return language;
}
- (void)setLanguage:(NSString *)language{
    [[NSUserDefaults standardUserDefaults] setObject:language forKey:appLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)systemLanguage{
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] objectForKey:appLanguage];
    NSLog(@"系统语言:%@",languageCode);
    if([languageCode hasPrefix:@"zh-Hant"]){
        languageCode = @"zh-Hant";//繁体中文
    }else if([languageCode hasPrefix:@"zh-Hans"]){
        languageCode = @"zh-Hans";//简体中文
    }else if([languageCode hasPrefix:@"pt"]){
        languageCode = @"pt";//葡萄牙语
    }else if([languageCode hasPrefix:@"es"]){
        languageCode = @"es";//西班牙语
    }else if([languageCode hasPrefix:@"th"]){
        languageCode = @"th";//泰语
    }else if([languageCode hasPrefix:@"hi"]){
        languageCode = @"hi";//印地语
    }else if([languageCode hasPrefix:@"ru"]){
        languageCode = @"ru";//俄语
    }else if([languageCode hasPrefix:@"ja"]){
        languageCode = @"ja";//日语
    }else if([languageCode hasPrefix:@"en"]){
        languageCode = @"en";//英语
    }else{
        languageCode = @"zh-Hans";//英语
    }
    [self setLanguage:languageCode];
}
/*  升级ios9之后，使得原本支持中英文的app出现闪退，中英文混乱的问题！大家不要慌，原因是升级之后中英文目录名字改了。在真机上，中文资源目录名由zh-Hans---->zh-Hans-CN，英文资源目录名由en---->en-CN，ios9模拟器上面的中英文资源目录名和真机上面的不一样，分别是zh-Hans-US，en-US。
 */

- (UIColor *)mainBgColor;
{
    NSString *language=[self AppbBGColor];
    
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return RGBA(32, 32, 46, 1);
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return WHITECOLOR;
    }
    return RGBA(20, 20, 67, 1);
}
- (UIColor *)textfieldBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#242433");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#FAFAFC");
    }
    return ColorStr(@"#221f61");
}
- (UIColor *)cellBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#2e2e4c");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#F0F0F0");
    }
    return RGBA(39, 251, 248, 0.1);
}
- (UIColor *)buttonNormalStatus;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#00BFA6");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#F0F0F0");
    }
    return ColorStr(@"#F0F0F0");
}
- (UIColor *)tableViewBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return  ColorStr(@"#20202E");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return [UIColor whiteColor];
    }
    return  ColorStr(@"#18184c");
}
- (UIColor *)mainBlockBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return  RGBA(32, 32, 46, 1);
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return [UIColor whiteColor];
    }
    return  ColorStr(@"#2D2D3D");
}
- (UIColor *)maintitleColor1;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return   ColorStr(@"#6E6E8A");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#AAAAAA");
    }
    return   RGBA(114, 145, 161, 1);
}
- (UIColor *)textFieldPBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return   ColorStr(@"#6E6E8A");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#CCCCCC");
    }
    return   ColorStr(@"#42428d");
}
- (UIColor *)tabbarBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return   ColorStr(@"#2E2E42");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return [UIColor whiteColor];
    }
    return   RGBA(24, 24, 76, 1);
}
- (UIColor *)tabtitleColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#00BFA6");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#00BFA6");
    }
    return  ColorStr(@"#00BFA6");
}

- (UIColor *)dieTitleColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#FA4A61");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#FA4A61");
    }
    return  RGBA(250, 74, 97, 1);
}
- (UIColor *)zhangTitleColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#3EC28F");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#38B183");
    }
    return  ColorStr(@"#03ecc1");
}
- (UIColor *)kmainBgColor;
{
    
    return  [self mainBlockBgColor];
}
- (UIColor *)segmentLayerBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#525266");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#CCCCCC");
    }
    return  ColorStr(@"#CCCCCC");;
}
- (UIColor *)maintitleColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#DDDEF0");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#333333");
    }
    return  WHITECOLOR;
}
- (UIColor *)popBgViewColor
{
    {
        NSString *language=[self AppbBGColor];
        if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
        {
            return RGBA(95, 95, 121, 0.6);
            
        }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
        {
            return [UIColor colorWithWhite:0 alpha:0.6];
        }
        return  WHITECOLOR;
    }
}
- (UIColor *)mainBgViewColor
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return ColorStr(@"#20202E");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return RGBA(240, 240, 240, 1);
    }
    return  WHITECOLOR;
}
- (UIColor *)segmentBgViewColor
{
    {
        NSString *language=[self AppbBGColor];
        if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
        {
            return ColorStr(@"#8591B8");
            
        }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
        {
            return TABTITLECOLOR;
        }
        return  ColorStr(@"#8591B8");
    }
}
- (UIColor *)segmentBaColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return   ColorStr(@"#DDDEF0");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#2D2D3D");
    }
    return   RGBA(114, 145, 161, 1);
}
- (UIColor *)TabBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return   ColorStr(@"#008F7C");
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return ColorStr(@"#00BFA6");
    }
    return   RGBA(114, 145, 161, 1);
}
- (UIColor *)HomeBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return WHITECOLOR;
        
    }else
    {
        
        return  RGBA(32, 32, 46, 1);
    }
    return WHITECOLOR;
}
- (UIColor *)BgViewLightColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#F7F7FC");
        
    }else
    {
        
        return  ColorStr(@"#14141F");
    }
    return WHITECOLOR;
}
- (UIColor *)C2CTitleTextColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#666666");
        
    }else
    {
        
        return  ColorStr(@"#9292AD");
    }
    return WHITECOLOR;
}
- (UIColor *)C2CboxBgColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#F7F7FC");
        
    }else
    {
        
        return  ColorStr(@"#272738");
    }
    return WHITECOLOR;
}
- (UIColor *)tab2chooseBGColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return RGBA(248, 248, 248, 1);
        
    }else
    {
        
        return  RGBA(32, 32, 46, 1);
    }
    return WHITECOLOR;
}
- (UIColor *)CustomBgViewColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#FFFFFF");
        
    }else
    {
        
        return  ColorStr(@"#272738");
    }
    return WHITECOLOR;
}
- (NSArray *)tabSelectImages
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"] || [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
        {
            return (@[@"tabicon_home01",\
                      @"tabicon_stats01",\
                      @"tabicon_bit01",\
                      @"tabicon_avatar01"]);
        }
        return (@[@"tabicon_home01",\
                  @"tabicon_stats01",\
                  @"tabicon_bit01",\
                  @"tabicon_fabi01",\
                  @"tabicon_avatar01"]);
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        
        if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
        {
            return (@[@"tabicon_home01_1",\
                      @"tabicon_stats01_1",\
                      @"tabicon_bit01_1",\
                      @"tabicon_avatar01_1"]);
        }
        return (@[@"tabicon_home01_1",\
                  @"tabicon_stats01_1",\
                  @"tabicon_bit01_1",\
                  @"tabicon_fabi01_1",\
                  @"tabicon_avatar01_1"]);
    }
    return nil;
}
- (NSArray *)tabNormalImages;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
        {
            return (@[@"tabicon_home02",\
                      @"tabicon_stats02",\
                      @"tabicon_bit02",\
                      @"tabicon_avatar02"]);
        }
        return (@[@"tabicon_home02",\
                  @"tabicon_stats02",\
                  @"tabicon_bit02",\
                  @"tabicon_fabi02",\
                  @"tabicon_avatar02"]);
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
        {
            return (@[@"tabicon_home02_1",\
                      @"tabicon_stats02_1",\
                      @"tabicon_bit02_1",\
                      @"tabicon_avatar02_1"]);
        }
        return (@[@"tabicon_home02_1",\
                  @"tabicon_stats02_1",\
                  @"tabicon_bit02_1",\
                  @"tabicon_fabi02_1",\
                  @"tabicon_avatar02_1"]);
    }
    return nil;
}
- (NSArray *)tabVC
{
    if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
    {
        return (@[[HomeViewController alloc],\
                  [QuotationViewController alloc],\
                  [PurchaseViewController alloc],\
                  [PersonalCenterViewController alloc]]);
    }
    return (@[[HomeViewController alloc],\
              [QuotationViewController alloc],\
              [PurchaseViewController alloc],\
              [WalletViewController alloc],\
              [PersonalCenterViewController alloc]]);
    return nil;
}
- (NSArray *)tabTitle
{
    if ([[EXUnit getSite] isEqualToString:@"BitAladdin"]||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
    {
        return (@[Localized(@"main_home"),Localized(@"main_quotation"),Localized(@"main_transaction"),Localized(@"main_me")]);
    }
    return (@[Localized(@"main_home"),Localized(@"main_quotation"),Localized(@"main_transaction"),Localized(@"main_Legal_currency"),Localized(@"main_me")]);
    return nil;
}
- (NSInteger)tabNum
{
    
    if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] ||[[EXUnit getSite] isEqualToString:@"Coinbetter"]|| [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
    {
        return 4;
    }
    return 5;
}
- (NSString *)backicon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"K_goback";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"w_goback";
    }
    return @"";
}
- (NSString *)loginbackicon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"title_back1";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"title_back";
    }
    return @"";
}
- (BOOL)StatusBarStyle;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return NO;
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return YES;
    }
    return NO;
}
- (NSString *)bitAladdinLaunchImg
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"w_bitAladdin_launchImg";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"bitAladdin_launchImg";
    }
    return nil;
}
- (NSString *)symbolhighicon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"home_high_1";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"home_high";
    }
    return nil;
}
- (NSString *)symboldieicon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"home_die_1";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"home_die";
    }
    return nil;
}
- (NSString *)Selected1Image;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"Selected1_block";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"Selected1";
    }
    return nil;
}
- (NSString *)SelectedImage;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"Selected_block";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"Selected";
    }
    return nil;
}
- (NSString *)NormalImage;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"Normal_block";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"Normal";
    }
    return nil;
}
- (NSString *)symbolrighticon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"title_Quotes_b";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"title_Quotes_w";
    }
    return nil;
}
- (NSString *)symbolLefticon;
{
    NSString *language=[self AppbBGColor];
    if ([NSString isEmptyString:language] ||[language isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        return @"title-species_b";
        
    }else if ([language isEqualToString:WHITE_COLOR])//app风格为白色
    {
        return @"title-species_w";
    }
    return nil;
}
- (UIColor *)selectedbuttonBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#F2F2F5");
        
    }else
    {
        
        return  ColorStr(@"#36364D");
    }
    return ColorStr(@"#36364D");
    
}
- (UIColor *)sliderbgColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#EEEEEE");
        
    }else
    {
        
        return  ColorStr(@"#49495C");
    }
    return ColorStr(@"#49495C");
}
- (UIColor *)sliderTitleColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#CCCCCC");
        
    }else
    {
        
        return  ColorStr(@"#6E6E8A");
    }
    return ColorStr(@"#49495C");
}
- (UIColor *)C2CTextPColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#EEEEEE");
        
    }else
    {
        
        return  ColorStr(@"#2E2E42");
    }
    return ColorStr(@"#2E2E42");
}
- (UIColor *)helpClassColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#666666");
        
    }else
    {
        
        return  ColorStr(@"#DDDEF0");
    }
    return ColorStr(@"#2E2E42");
}
- (UIColor *)helpNavColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return TABTITLECOLOR;
        
    }else
    {
        
        return  ColorStr(@"#2E2E42");
    }
    return ColorStr(@"#2E2E42");
}
- (UIColor *)SVPBgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return RGBA(0, 0, 0, 0.5);
        
    }else
    {
        
        return  RGBA(89, 89, 128, 0.5);
    }
    return ColorStr(@"#2E2E42");
}
- (UIColor *)newsTitleColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#999999");
        
    }else
    {
        
        return  ColorStr(@"#6E6E8A");
    }
    return ColorStr(@"#2E2E42");
}
- (NSString *)openNormalImage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return @"icon-show";
        
    }else
    {
        
        return  @"b_icon-show";
    }
}
- (NSString *)closeNormalImage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return @"icon-hide";
        
    }else
    {
        
        return  @"b_icon-hide";
    }
}
- (NSString *)assetsHeadBgImage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return @"assetsBgviewImage";
        
    }else
    {
        
        return  @"b_assetsBgviewImage";
    }
}
- (NSString *)chooseOpenNormalImage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return @"icon_selected";
        
    }else
    {
        
        return  @"b_icon_selected";
    }
}
- (NSString *)choosecloseNormalImage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return @"icon_dis_selected";
        
    }else
    {
        
        return  @"b_icon_dis_selected";
    }
}
- (UIColor *)assetsHeadBGColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return MAINBLACKCOLOR;
        
    }else
    {
        
        return  ColorStr(@"#20202E");
    }
}
- (UIColor *)assetsTitleColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return RGBA(255, 255, 255, 0.7);
        
    }else
    {
        
        return  ColorStr(@"#9292AD");
    }
}
- (UIColor *)ALLTotalTitleColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return RGBA(255, 255, 255, 1);
        
    }else
    {
        
        return  ColorStr(@"#DDDEF0");
    }
}
- (UIColor *)assetsCellColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return [self BgViewLightColor];
        
    }else
    {
        
        return  RGBA(32, 32, 46, 1);
    }
}
- (UIColor *)assetsCellviewbgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return ColorStr(@"#FFFFFF");
        
    }else
    {
        
        return  ColorStr(@"#2E2E42");
    }
}
- (UIColor *)assetsSegmentbgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        
        return RGBA(247, 247, 252, 1);
        
    }else
    {
        
        return  RGBA(32, 32, 46, 1);
    }
}
- (UIColor *)assetsSegmentSelectbgColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return WHITECOLOR;
        
    }else
    {
        
        return  ColorStr(@"#2E2E42");
    }
}
- (UIColor *)AnimationColor;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return RGBA(40, 120, 255, 1);
    }else
    {
        return RGBA(255, 255, 255, 1);
    }
}
- (UIColor *)paymentimageViewColor
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return RGBA(247, 247, 252, 1);
    }else
    {
        return RGBA(41, 41, 57, 0.5);
    }
}
- (NSString *)paymentimage;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return @"sf_bai_add";
    }else
    {
        return @"sf_hei_add";
    }
}
- (NSString *)paymenticonDowm;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return @"4otc_icon_xia_bai";
    }else
    {
        return @"4otc_icon_xia_hei";
    }
}
- (NSString *)CoinbetterLoginIcon;
{
    NSString *language=[self AppbBGColor];
    if ([language isEqualToString:WHITE_COLOR]) //app风格为默认色
    {
        return @"coinbetter_launchImg_w";
    }else
    {
        return @"coinbetter_launchImg_b";
    }
}
@end
