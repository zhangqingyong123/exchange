//
//  QuakeVideoAPI.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/9/15.
//  Copyright © 2017年 Mac. All rights reserved.
//

#ifndef ExchangeAPI_h
#define ExchangeAPI_h
static NSString * const WYQY_APPKEY = @"5936a18701bff20c11c79104a1183161";
static NSString * const ZG_UM_APPKEY = @"5bed394ff1f5560acc000174";
static NSString * const ZT_UM_APPKEY = @"5bc450b5f1f5568688000048";
//记住账号和密码
static NSString * const KEY_USERNAME_PASSWORD = @"com.company.app.usernamepassword";
static NSString * const KEY_USERNAME = @"com.company.app.username";
// AES
static NSString * const AES_KEY = @"r3FzZAU59c1NRdl0XIHnGs8qSCi2ekbt";

/*ZG 测试域名 */
//static NSString * const HOST_IP = @"http://47.110.129.33:8883/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"ws://47.110.129.33:8090";
//static NSString * const HOSTAPI = @"47.110.129.33:3000";
//ZG正式域名
//static NSString * const HOST_IP = @"https://www.zg.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.zg.com/ws";
//static NSString * const HOSTAPI = @"www.zg.com";

/*58正式域名*/
//static NSString * const HOST_IP = @"https://www.58.vip/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.58.vip/ws";
//static NSString * const HOSTAPI = @"www.58.vip";

/*人人币正式域名*/
//static NSString * const HOST_IP = @"https://www.rr.vip/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.rr.vip/ws";
//static NSString * const HOSTAPI = @"www.rr.vip";

/*Kcoin正式*/
//static NSString * const HOST_IP = @"https://www.k-coin.io/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.k-coin.io/ws";
//static NSString * const HOSTAPI = @"www.k-coin.io";

/*ZT域名 测试*/
//static NSString * const HOST_IP = @"http://47.97.206.151:8883/api/v1/";
////websocket地址
//static NSString * const WBSCOKETHOST_IP = @"ws://47.97.206.151:8090";
////测试环境区分子站
//static NSString * const HOSTAPI = @"47.97.206.151:3000";

//ZT正式
//static NSString * const HOST_IP = @"https://www.zt.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.zt.com/ws";
////正式环境区分子站
//static NSString * const HOSTAPI = @"www.zt.com";

/*比特日本站*/
//static NSString * const HOST_IP = @"http://www.bitaladdin.com/api/v1/";
////static NSString * const HOST_IP = @"https://www.bitaladdin.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"ws://www.bitaladdin.com/ws";
////static NSString * const WBSCOKETHOST_IP = @"wss://www.bitaladdin.com/ws";
////正式环境区分子站
//static NSString * const HOSTAPI = @"www.bitaladdin.com";

/*zgk正式*/
//static NSString * const HOST_IP = @"https://www.zgk.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.zgk.com/ws";
//static NSString * const HOSTAPI = @"www.zgk.com";

/*GEMEX正式*/
//static NSString * const HOST_IP = @"https://www.gemex.pro/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.gemex.pro/ws";
//static NSString * const HOSTAPI = @"www.gemex.pro";

/*gitbtc正式*/
//static NSString * const HOST_IP = @"https://www.gitbtc.net/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.gitbtc.net/ws";
//static NSString * const HOSTAPI = @"www.gitbtc.net";


/*ACE 正式*/
//static NSString * const HOST_IP = @"https://www.aucep.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.aucep.com/ws";
//static NSString * const HOSTAPI = @"www.aucep.com";

/*Rockex 正式*/
//static NSString * const HOST_IP = @"https://www.rockex.top/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.rockex.top/ws";
//static NSString * const HOSTAPI = @"www.rockex.top";

/*Coinbetter 正式*/
//static NSString * const HOST_IP = @"https://www.coinbetter.com/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.coinbetter.com/ws";
//static NSString * const HOSTAPI = @"www.coinbetter.com";

/*Comex 正式*/
//static NSString * const HOST_IP = @"https://www.comex.vip/api/v1/";
//static NSString * const WBSCOKETHOST_IP = @"wss://www.comex.vip/ws";
//static NSString * const HOSTAPI = @"www.comex.vip";
/*Ex.pizza 正式*/
static NSString * const HOST_IP = @"https://www.ex.pizza/api/v1/";
static NSString * const WBSCOKETHOST_IP = @"wss://www.ex.pizza/ws";
static NSString * const HOSTAPI = @"www.ex.pizza";
/*
  注册登录
 */
//登录
static NSString * const loginHttp = @"login";
//注册
static NSString * const registerHttp = @"register";
//获取国家代码
static NSString * const countryHttp = @"country";
//忘记密码
static NSString * const forgetPasswordHttp = @"forgetPassword";
//忘记密码
static NSString * const captchaHttp = @"captcha/id";
//发送验证码
static NSString * const SMSHttp = @"SMS";
/*
 首页
 */

//配置接口
static NSString * const site = @"site";
//获取banner列表

static NSString * const bannerList = @"banner";
//获取文章列表
static NSString * const articleList = @"articleList";
//市场价
static NSString * const symbolHttp = @"symbol";
//获取文章详情
static NSString * const article = @"article";

//用户下限价单
static NSString * const tradelimit = @"user/trade/limit";
//用户下市价单
static NSString * const trademarket = @"user/trade/market";
//用户下市价单
static NSString * const cancelOrder = @"user/trade/cancel";
//获取用户信息
static NSString * const getUser = @"user";
//创建身份认证
static NSString * const identity = @"user/identity";
//获取用户资产
static NSString * const assets = @"user/assets";
//获取用户充值地址
static NSString * const rechargeAddress = @"user/rechargeAddress";
//获取用户充值资金记录
static NSString * const rechargeMoneyLog = @"user/rechargeMoneyLog";
//获取用户提现记录
static NSString * const withdrawMoneyLog = @"user/withdrawMoneyLog";
//获取用户资产
static NSString * const Userassets = @"user/assets";
//用户重置密码
static NSString * const resetPassword = @"user/resetPassword";
//获取用户验证方式
static NSString * const security = @"user/security";
//登录用户发送验证码
static NSString * const userSMS = @"user/SMS";
//用户创建资金密码
static NSString * const createWithdrawPassword = @"user/createWithdrawPassword";
//用户重置提现密码
static NSString * const resetWithdrawPassword = @"user/resetWithdrawPassword";
//重新开启验证
static NSString * const userReopen = @"user/reopen";
//关闭短信验证
static NSString * const closePhone = @"user/closePhone";
//关闭邮件验证
static NSString * const closeEmail = @"user/closeEmail";
//关闭两步验证
static NSString * const closeTwoStep = @"user/closeTwoStep";
//获取用户两步验证密钥
static NSString * const twoStepAuthKey = @"user/twoStepAuthKey";
//用户创建两步验证
static NSString * const twoStep = @"user/twoStep";
//绑定邮箱
static NSString * const bindEmail = @"user/bindEmail";
//绑定手机号
static NSString * const bindPhone = @"user/bindPhone";
//获取用户提现地址
static NSString * const withdrawAddress = @"user/withdrawAddress";
//创建地址
static NSString * const BuildwithdrawAddress = @"user/withdrawAddress";
//用户提现
static NSString * const userwithdraw = @"user/withdraw";
//获取用户绑定的银行卡信息
static NSString * const userbank = @"user/bank";
//删除用户银行卡
static NSString * const deleteBank = @"user/deleteBank";
//用户绑定银行卡
static NSString * const Binduserbank = @"user/bank";
//获取c2c信息
static NSString * const c2cPrice = @"c2cPrice";
//用户绑定银行卡
static NSString * const c2ctrade = @"user/c2c/trade";
//获取c2c交易列表
static NSString * const gettrade = @"user/c2c/trade";
//更改c2c交易状态
static NSString * const updateTrade = @"user/c2c/updateTrade";
//获取关于我们详情
static NSString * const about = @"about";
//根据订单编号查询成交订单
static NSString * const orderDeals = @"user/order/deals";
//删除用户提现地址
static NSString * const deleteWithdrawAddress = @"user/deleteWithdrawAddress";
//获取用户推荐信息
static NSString * const recommend = @"user/recommend";
//获取推荐记录
static NSString * const recommendlog = @"user/recommend/log";
//获取app升级信息接口
static NSString * const upgrade = @"app/upgrade";

//安全登录接口
static NSString * const safeLogin = @"user/safeLogin";

//更新身份认证信息
static NSString * const updateIdentity = @"user/updateIdentity";
//上传身份证图片
static NSString * const uploadImage = @"user/uploadImage";
//用户确认身份认证信息
static NSString * const confirmIdentity = @"user/confirmIdentity";

//获取oss访问临时授权
static NSString * const ossAuth = @"user/oss/auth";
//获取币种资料详情
static NSString * const assetIntro = @"assetIntro";
//获取交易对板块
static NSString * const symbolClass = @"symbolClass";
//获取标签配置
static NSString * const articleClass = @"articleClass";

//获取标签配置
static NSString * const labelconfig = @"label/config";

//汇率接口
static NSString * const currency = @"currency";
//otc获取用户支付宝、微信、银行
static NSString * const mobilePay = @"user/otc/mobilePay";
//otc用户添加支付宝、微信
static NSString * const addMobilePay = @"user/otc/addMobilePay";
//删除用户的支付宝或微信账户
static NSString * const deletePay = @"user/otc/deletePay";
//otc买入卖出列表
static NSString * const getMerchant = @"otc/getMerchant";
//otc用户订单获取
static NSString * const otc_trade = @"user/otc/trade";
//otc获取商家收款方式 买入的时候
static NSString * const merchantAccount = @"user/otc/merchantAccount";
//otc获取订单详情
static NSString * const otc_detail = @"user/otc/detail";
//otc确认订单
static NSString * const otc_update = @"user/otc/update";
#endif 
