//
//  CalculatedHead.pch
//  Calculated
//
//  Created by 张庆勇 on 2018/3/20.
//  Copyright © 2018年 张庆勇. All rights reserved.

#import <Foundation/Foundation.h>

//当前语言
static NSString *const appLanguage = @"appLanguage";
//当前app背景色
static NSString *const appBGColor = @"appBGColor";
#define Localized(key)  [[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"]] ofType:@"lproj"]] localizedStringForKey:(key) value:nil table:@"Localizable"]
@interface ZBLocalized : NSObject

+ (ZBLocalized *)sharedInstance;

//初始化多语言功能
- (void)initLanguage;

//当前语言
- (NSString *)currentLanguage;

//设置要转换的语言
- (void)setLanguage:(NSString *)language;

//设置为系统语言
- (void)systemLanguage;



#pragma color

//设置皮肤
- (void)setAppbBGColor:(NSString *)Color;
- (NSString *)AppbBGColor;
- (void)initAppbBGColor;
/*背景色*/
- (UIColor *)mainBgColor;
- (UIColor *)SVPBgColor;
/*textfield背景色*/
- (UIColor *)textfieldBgColor;
/*分割线背景色*/
- (UIColor *)cellBgColor;
/*tablview背景色*/
- (UIColor *)tableViewBgColor;
/*主题分割背景色*/
- (UIColor *)mainBlockBgColor;
/*textfieldplaceb字体背景色*/
- (UIColor *)textFieldPBgColor;
/*tabbar背景色*/
- (UIColor *)tabbarBgColor;
/*tabbar字体背景色*/
- (UIColor *)tabtitleColor;
/*跌字体背景色*/
- (UIColor *)dieTitleColor;
/*涨字体背景色*/
- (UIColor *)zhangTitleColor;
/*k线图背景色*/
- (UIColor *)kmainBgColor;
/*主1字体颜色*/
- (UIColor *)maintitleColor1;
/*主字体颜色*/
- (UIColor *)maintitleColor;
- (UIColor *)CustomBgViewColor;
- (UIColor *)popBgViewColor;
- (UIColor *)mainBgViewColor;
- (UIColor *)segmentBgViewColor;
- (UIColor *)BgViewLightColor;
- (BOOL)StatusBarStyle;
- (NSString *)bitAladdinLaunchImg;
- (UIColor *)segmentBaColor;
- (UIColor *)TabBgColor;
#pragma login
- (UIColor *)buttonNormalStatus;
- (NSString *)loginbackicon;
#pragma home
- (UIColor *)newsTitleColor;
- (NSString *)symbolhighicon;
- (NSString *)symboldieicon;
- (UIColor *)HomeBgColor;
#pragma 交易
- (NSString *)symbolrighticon;
- (NSString *)symbolLefticon;
- (UIColor *)sliderbgColor;
- (UIColor *)sliderTitleColor;
#pragma 行情
- (UIColor *)segmentLayerBgColor;
- (UIColor *)selectedbuttonBgColor;
#pragma C2C
- (UIColor *)C2CTextPColor;
- (UIColor *)C2CTitleTextColor;
- (UIColor *)C2CboxBgColor;
#pragma 个人中心
- (UIColor *)helpClassColor;
- (UIColor *)helpNavColor;
#pragma icon
/*tabbare背景图片*/
- (NSArray *)tabSelectImages;
/*tabbare未选中图片*/
- (NSArray *)tabNormalImages;
/*根据站点配置tabbar*/
- (NSArray *)tabVC;
/*根据站点tabbar个数*/
- (NSInteger)tabNum;
/*tabbar标题*/
- (NSArray *)tabTitle;
/*全部返回照片*/
- (NSString *)backicon;

- (UIColor *)tab2chooseBGColor;
- (NSString *)Selected1Image;
- (NSString *)SelectedImage;
- (NSString *)NormalImage;


#pragma 我的资产
- (NSString *)openNormalImage;
- (NSString *)closeNormalImage;
- (NSString *)assetsHeadBgImage;
- (NSString *)chooseOpenNormalImage;
- (NSString *)choosecloseNormalImage;
- (UIColor *)assetsHeadBGColor;
- (UIColor *)assetsTitleColor;
- (UIColor *)ALLTotalTitleColor;
- (UIColor *)assetsCellColor;
- (UIColor *)assetsCellviewbgColor;
- (UIColor *)assetsSegmentbgColor;
- (UIColor *)assetsSegmentSelectbgColor;
- (UIColor *)AnimationColor;

#pragma 新增资产
- (UIColor *)paymentimageViewColor;
- (NSString *)paymentimage;
- (NSString *)paymenticonDowm;
- (NSString *)CoinbetterLoginIcon;
@end
