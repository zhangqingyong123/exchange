//
//  NoNetworkView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/14.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoNetworkView : UIView
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,copy) void(^buttonclickeBlock)(UIButton *button);
#pragma mark - 构造方法
/**
 构造方法
 @param frame 占位图的frame
 @param NoNetwork 如果有网络说明没有按钮
 */
- (instancetype)initWithFrame:(CGRect)frame
                    NoNetwork:(BOOL )NoNetwork;
//隐藏
- (void)dissmiss;
@end
