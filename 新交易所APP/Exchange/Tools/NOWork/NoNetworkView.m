//
//  NoNetworkView.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/14.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "NoNetworkView.h"
@interface NoNetworkView()
{
    BOOL   _NoNetwork;
}
@end
@implementation NoNetworkView

/**
 构造方法
 @param frame 占位图的frame
 @param NoNetwork 如果有网络说明没有按钮
 */
- (instancetype)initWithFrame:(CGRect)frame
                    NoNetwork:(BOOL )NoNetwork
{
    if (self = [super initWithFrame:frame])
    {
        
        _NoNetwork = NoNetwork;
        [self setUI];
    }
    return self;
}
- (void)dissmiss
{
    [self removeFromSuperview];
}
- (void)buttonClick:(UIButton *)button
{
    if (self.buttonclickeBlock) {
        self.buttonclickeBlock(button);
    }
}
#pragma mark - UI搭建
- (void)setUI
{
    UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(self.width/2 - RealValue_W(70), self.height/2 -  RealValue_W(100), RealValue_W(140), RealValue_W(140))];
    _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(self.width/2 - 120, button.bottom, 240, 22)];
    _titlelable.adjustsFontSizeToFitWidth = YES;
    _titlelable.font = AutoFont(13);
    _titlelable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titlelable];
    
    NSString * str = [ZBLocalized sharedInstance].AppbBGColor;
    if ([NSString isEmptyString:str] ||[str isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        _titlelable.textColor = MAINTITLECOLOR;
        if (_NoNetwork) {
            
            [button setImage:[UIImage imageNamed:@"NOwork"] forState:UIControlStateNormal];
            _titlelable.text = Localized(@"No_network");
            
        }else
        {
            [button setImage:[UIImage imageNamed:@"icon_zanwujilu"] forState:UIControlStateNormal];
            _titlelable.text = Localized(@"No_data");
        }
        
    }else if ([str isEqualToString:WHITE_COLOR])//app风格为白色
    {
        _titlelable.textColor = MAINTITLECOLOR1;
        if (_NoNetwork) {
            
            [button setImage:[UIImage imageNamed:@"NOwork_1"] forState:UIControlStateNormal];
            _titlelable.text = Localized(@"No_network");
            
        }else
        {
            [button setImage:[UIImage imageNamed:@"icon_zanwujilu_1"] forState:UIControlStateNormal];
            _titlelable.text = Localized(@"No_data");
        }
    }
   
    [button centerVerticallyImageAndText];
//    [button setTitleColor:RGBA(49, 76, 90, 1) forState:UIControlStateNormal];
    [button setTitleColor:MAINTITLECOLOR forState:0];
    button.titleLabel.font = AutoFont(13);
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:button];
  
    
}
@end
