//
//  YWUnlockView.m
//  YWUnlock
//
//  Created by dyw on 2017/2/24.
//  Copyright © 2017年 dyw. All rights reserved.
//

#import "YWUnlockView.h"
#import "YWUnlockMacros.h"
#import "YWUnlockPreviewView.h"
#import "YWGesturesUnlockView.h"
#import "LoginViewController.h"
#define GesturesPassword @"GesturesPassword"

@interface YWUnlockView ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
/** 头像 */
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
/** 绘制密码的状态label */
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
/** 重新绘制按钮 */
@property (weak, nonatomic) IBOutlet UIButton *resetGesturesPasswordButton;

/** 手势密码绘制视图 */
@property (weak, nonatomic) IBOutlet YWGesturesUnlockView *gesturesUnlockView;
/** 当前创建的手势密码 */
@property (nonatomic, copy) NSString *curentGesturePassword;
/** 当前处理密码类型 (默认是创建密码) */
@property (nonatomic, assign) YWUnlockViewType type;


@end

@implementation YWUnlockView

#pragma mark - life cycle
- (void)awakeFromNib{
    [super awakeFromNib];
    self.type = YWUnlockViewCreate;//默认是创建密码
    _gesturesUnlockView.backgroundColor = TABLEVIEWLCOLOR;
    _bgView.backgroundColor = TABLEVIEWLCOLOR;
    self.backgroundColor = TABLEVIEWLCOLOR;
    [_returnbutton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].backicon] forState:0];
    self.statusLabel.textColor = MAINTITLECOLOR1;
    self.lableTitle.textColor = MAINTITLECOLOR;
    self.statusLabel.text = Localized(@"Security_gesture_code");
}

#pragma mark - private methods
/** 根据不同的类型处理 */
- (void)handleWithType:(YWUnlockViewType)type password:(NSString *)gesturePassword{
    switch (type) {
        case YWUnlockViewCreate://创建手势密码
            [self createGesturesPassword:gesturePassword];
            break;
        case YWUnlockViewUnlock://解锁手势密码
            [self validateGesturesPassword:gesturePassword];
            break;
    }
}

//创建手势密码
- (void)createGesturesPassword:(NSString *)gesturesPassword {
    if (self.curentGesturePassword.length == 0) {
        if (gesturesPassword.length <6) {
            self.statusLabel.text = Localized(@"gesture_password_re-enter");
            [self shakeAnimationForView:self.statusLabel];
            return;
        }
        if (self.resetGesturesPasswordButton.hidden == YES) {
            self.resetGesturesPasswordButton.hidden = NO;
        }
        self.curentGesturePassword = gesturesPassword;
       
        self.statusLabel.text = Localized(@"gesture_password_again");
        return;
    }
    if ([self.curentGesturePassword isEqualToString:gesturesPassword]) {//绘制成功
        //保存手势密码
        [YWUnlockView saveGesturesPassword:gesturesPassword];
        !self.block?:self.block(YES);
        [self hide];
    }else {
        self.statusLabel.text = Localized(@"Security_password_error");
        [self shakeAnimationForView:self.statusLabel];
    }
}

//验证手势密码
- (void)validateGesturesPassword:(NSString *)gesturesPassword {
    static NSInteger errorCount = 5;
    if ([gesturesPassword isEqualToString:[YWUnlockView getGesturesPassword]]) {
        errorCount = 5;
        !self.block?:self.block(YES);
        [self hide];
    }else {
        if (errorCount - 1 == 0) {//你已经输错五次了！ 退出登陆！
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:Localized(@"Security_password_expired") message:Localized(@"Security_re_login") delegate:self cancelButtonTitle:nil otherButtonTitles:Localized(@"Re_landing"), nil];
            [alertView show];
            errorCount = 5;
            return;
        }
        self.statusLabel.text = [NSString stringWithFormat:Localized(@"Security_password_wrong"),--errorCount];
        [self shakeAnimationForView:self.statusLabel];
    }
}

//抖动动画
- (void)shakeAnimationForView:(UIView *)view{
    CALayer *viewLayer = view.layer;
    CGPoint position = viewLayer.position;
    CGPoint left = CGPointMake(position.x - 10, position.y);
    CGPoint right = CGPointMake(position.x + 10, position.y);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:left]];
    [animation setToValue:[NSValue valueWithCGPoint:right]];
    [animation setAutoreverses:YES]; // 平滑结束
    [animation setDuration:0.08];
    [animation setRepeatCount:3];
    [viewLayer addAnimation:animation forKey:nil];
}

#pragma mark - public methods
/** 展示 手势密码视图 */
+ (void)showUnlockViewWithType:(YWUnlockViewType)type callBack:(CallBackBlock)callBack{
    
    if(type == YWUnlockViewUnlock && ![YWUnlockView getGesturesPassword].length) return;
    YWUnlockView *unlockView = [YWUnlock_BD loadNibNamed:@"YWUnlockView" owner:nil options:nil].lastObject;
    unlockView.frame = CGRectMake(0, YWUnlock_SH, YWUnlock_SW, YWUnlock_SH);
    unlockView.backgroundColor = TABLEVIEWLCOLOR;
    [YWUnlock_KW addSubview:unlockView];
    unlockView.block = [callBack copy];
    unlockView.type = type;
    [UIView animateWithDuration:0.25 animations:^{
        unlockView.frame = CGRectMake(0, 0, YWUnlock_SW, YWUnlock_SH);
    }];
}

/** 隐藏视图 */
- (void)hide{
    [UIView animateWithDuration:0.25 animations:^{
        self.frame = CGRectMake(0, YWUnlock_SH, YWUnlock_SW, YWUnlock_SH);
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    !self.block?:self.block(NO);
 
    [self hide];

    
}


#pragma mark - event response
//返回按钮点击
- (IBAction)backButtonClick:(UIButton *)sender {

    !self.block?:self.block(NO);
    [self hide];
}

//点击重新绘制按钮
- (IBAction)resetGesturePassword:(id)sender {
    self.curentGesturePassword = nil;
    self.statusLabel.text = Localized(@"Security_gesture_code");
    self.resetGesturesPasswordButton.hidden = YES;
   
}

#pragma mark - getters and setters
/** 设置密码的操作类型 */
- (void)setType:(YWUnlockViewType)type{
    _type = type;
    if (_type == YWUnlockViewCreate) {
        self.lableTitle.text = Localized(@"Security_gesture_cipher");
    }else
    {
        self.lableTitle.text = ![NSString isEmptyString:[EXUserManager userInfo].phone]?[EXUserManager userInfo].phone:[EXUserManager userInfo].email;
    }
  
    YWUnlock_WKSELF;
    [self.gesturesUnlockView setDrawRectFinishedBlock:^(NSString *gesturePassword) {
        [weakSelf handleWithType:type password:gesturePassword];
    }];
}

#pragma mark - other methods
/** 是否已经创建过手势密码 */
+ (BOOL)haveGesturePassword{
    return [YWUnlockView getGesturesPassword].length?YES:NO;
}

/** 删除手势密码 */
+ (void)deleteGesturesPassword{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:GesturesPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/** 保存手势密码 */
+ (void)saveGesturesPassword:(NSString *)gesturesPassword {
    [[NSUserDefaults standardUserDefaults] setObject:gesturesPassword forKey:GesturesPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/** 获取手势密码 */
+ (NSString *)getGesturesPassword{
    return [[NSUserDefaults standardUserDefaults] objectForKey:GesturesPassword];
}

@end
