//
//  CHKeychain.h
//  GCD
//
//  Created by admin on 14-3-3.
//  Copyright (c) 2014年 ldns. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHKeychain : NSObject

+ (void)save:(NSString *)service
        data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;

@end
