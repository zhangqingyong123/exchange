//
//  InvitationWebViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/18.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface InvitationWebViewController : BaseViewViewController
@property (nonatomic, copy) NSString *urlStr;
/**是否支持web下拉刷新 default is NO*/
@property (nonatomic, assign) BOOL isPullRefresh;
@end


