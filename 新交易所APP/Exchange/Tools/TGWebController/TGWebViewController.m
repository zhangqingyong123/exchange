//
//  TGWebViewController.m
//  TGWebViewController
//
//  Created by 赵群涛 on 2017/9/15.
//  Copyright © 2017年 QR. All rights reserved.
//

#import "TGWebViewController.h"
#import "TGWebProgressLayer.h"
#import <WebKit/WebKit.h>
@interface TGWebViewController ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>

@property (nonatomic, strong)WKWebView *tgWebView;

@property (nonatomic, strong)TGWebProgressLayer *webProgressLayer;


@end

@implementation TGWebViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
   [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = self.webTitle;
    [self setUpUI];

}

- (void)setUpUI {
    self.view.backgroundColor = WHITECOLOR;
//    UIButton *leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 43, 43)];
//    leftBtn.titleLabel.font = AutoFont(16);
//    [leftBtn setTitle:Localized(@"Agreement_text_Return") forState:(UIControlStateNormal)];
//    [leftBtn setImage:[UIImage imageNamed:@"icon_back_blue"] forState:UIControlStateNormal];
//    [leftBtn addTarget:self action:@selector(popClick) forControlEvents:UIControlEventTouchUpInside];
//    [leftBtn setTitleColor:RGBA(50, 127, 255, 1) forState:UIControlStateNormal];
//    leftBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [leftBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -RealValue_W(10), 0, RealValue_W(0))];
//    [leftBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -RealValue_W(18), 0, RealValue_W(0))];
//    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    //    // 创建配置
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    WKUserContentController* userContent = [[WKUserContentController alloc] init];
    [config.userContentController addScriptMessageHandler:self name:@"collectSendKey"];
    config.userContentController = userContent;
    self.tgWebView = [[WKWebView alloc] initWithFrame:CGRectMake(20, 0,UIScreenWidth -40,  UIScreenHeight -64) configuration:config];
    self.tgWebView.navigationDelegate =self;
    self.tgWebView.UIDelegate = self;
//    NSURL *url = [NSURL URLWithString:self.url];
//    [self.tgWebView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.tgWebView loadHTMLString:self.url baseURL:nil];
   
    [self.view addSubview:self.tgWebView];
    self.webProgressLayer = [[TGWebProgressLayer alloc] init];
    self.webProgressLayer.frame = CGRectMake(0, 48, UIScreenWidth, 2);
    self.webProgressLayer.strokeColor =  MAINCOLOR.CGColor;
    [self.navigationController.navigationBar.layer addSublayer:self.webProgressLayer];

}
- (void)popClick {
//    [self.navigationController popViewControllerAnimated:YES];
  
    if (self.tgWebView.goBack) {
        [self.tgWebView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        [self.webProgressLayer removeFromSuperlayer];
    }
}
#pragma mark - UIWebViewDelegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [self.webProgressLayer tg_startLoad];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    //修改字体大小 300%
    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '200%'"completionHandler:nil];
    
    //修改字体颜色  #9098b8
//    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#222222'"completionHandler:nil];
 
    
    [self.webProgressLayer tg_finishedLoadWithError:nil];
}

//- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
//    [self.webProgressLayer tg_finishedLoadWithError:error];
//
//}
//
////实现js注入方法的协议方法
//- (void)userContentController:(WKUserContentController *)userContentController
//      didReceiveScriptMessage:(WKScriptMessage *)message {
//    //找到对应js端的方法名,获取messge.body
//    if ([message.name isEqualToString:@"collectSendKey"]) {
//
//        NSLog(@"%@", message.body);
//
//    }
//}





- (void)dealloc {
    [self.webProgressLayer tg_closeTimer];
    [_webProgressLayer removeFromSuperlayer];
    _webProgressLayer = nil;
}




@end
