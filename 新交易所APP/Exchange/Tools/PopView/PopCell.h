//
//  PopCell.h
//  Calculated
//
//  Created by 张庆勇 on 2018/3/29.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopCell : UITableViewCell
@property (nonatomic,strong)UIButton * icon;
@property (nonatomic,strong)UILabel  * leftlable;
@end
