//
//  WebSocket.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/26.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketRocket.h"
extern NSString * const kNeedPayOrderNote1;
extern NSString * const kWebSocketDidOpenNote1;
extern NSString * const kWebSocketDidCloseNote1;
extern NSString * const kWebSocketdidReceiveMessageNote1;
@interface WebSocket : NSObject
@property (nonatomic,strong) SRWebSocket *socket;
// 获取连接状态
@property (nonatomic,assign,readonly) SRReadyState socketReadyState;

+ (SocketRocketUtility *)instance;

-(void)SRWebSocketOpenWithURLString:(NSString *)urlString;//开启连接
-(void)SRWebSocketClose;//关闭连接
- (void)sendData:(id)data;//发送数据
@end
