//  LoginViewController.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/21.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "EXUnit.h"
#import <AdSupport/AdSupport.h>
#import "sys/utsname.h"

#import <LocalAuthentication/LocalAuthentication.h>
@implementation EXUnit
- (instancetype)init
{
    self = [super init];
    if (self) {
        /*
         *  hhhhhh
         *
         */
    }
    return self;
}
+ (void)showMessage:(NSString *)message
{
    static BOOL isShow = NO;
    if (isShow) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        isShow = YES;
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = AutoFont(14);
        style.verticalPadding =10;
        style.cornerRadius = 0;
        [kWindow makeToast:message
                  duration:1.5
                  position:CSToastPositionCenter
                     style:style];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShow = NO;
        });
    });
}
+ (void)showSVProgressHUD:(NSString *)title
{
    [SVProgressHUD showWithStatus:title];
    [SVProgressHUD showImage:[UIImage imageNamed:@"success"] status:title];
    //设置HUD的Style
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    //设置HUD和文本的颜色
    [SVProgressHUD setForegroundColor:ColorStr(@"#DDDEF0")];
    //设置HUD背景颜色
    [SVProgressHUD setBackgroundColor:[ZBLocalized sharedInstance].SVPBgColor];
    /**
     *  设置HUD背景图层的样式
     *
     *  SVProgressHUDMaskTypeNone：默认图层样式，当HUD显示的时候，允许用户交互。
     *
     *  SVProgressHUDMaskTypeClear：当HUD显示的时候，不允许用户交互。
     *
     *  SVProgressHUDMaskTypeBlack：当HUD显示的时候，不允许用户交互，且显示黑色背景图层。
     *
     *  SVProgressHUDMaskTypeGradient：当HUD显示的时候，不允许用户交互，且显示渐变的背景图层。
     *
     *  SVProgressHUDMaskTypeCustom：当HUD显示的时候，不允许用户交互，且显示背景图层自定义的颜色。
     */
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    //设置多少秒后隐藏
    [SVProgressHUD dismissWithDelay:1];
    
}
/*
 iphoneX
 */
+ (BOOL)deviceIsIPhoneX {
    struct utsname systemInfo;
    
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    if ([platform isEqualToString:@"i386"] || [platform isEqualToString:@"x86_64"]) {
        // 模拟器下采用屏幕的高度来判断
        return [UIScreen mainScreen].bounds.size.height == 812;
    }
    // iPhone10,6是美版iPhoneX 感谢hegelsu指出：https://github.com/banchichen/TZImagePickerController/issues/635
    BOOL isIPhoneX = [platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"];
    return isIPhoneX;
}

// 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
+ (UIColor *)colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // 判断前缀并剪切掉
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}
+ (NSMutableAttributedString *)finderattributedString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)finderattributedLinesString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    NSMutableParagraphStyle *paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:8];
    [muattributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)getcontent:(NSString *)str image:(NSString *)imageStr index:(NSInteger)index imageframe:(CGRect)imageframe
{
    CGFloat y = imageframe.origin.y;
    if (y == 0) {
        y = RealValue_H(1);
    }
    NSMutableAttributedString *attri       = [[NSMutableAttributedString alloc] initWithString:str];
    NSTextAttachment *attch                = [[NSTextAttachment alloc] init];
    attch.image                            = [UIImage imageNamed:imageStr];
    attch.bounds                           = CGRectMake(0,y, imageframe.size.width,imageframe.size.height);
    
    NSAttributedString *string             = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:string atIndex:index];
    return attri;
}
+ (BOOL)judgePassWordLegal:(NSString *)pass{
    BOOL result = false;
    if ([pass length] >= PassworldLastlength){
        // 判断长度大于6位后再接着判断是否同时包含数字和字符
        NSString * regex = @"^(?=.*[0-9])(?=.*[a-zA-Z])(.{6,16})$";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        result = [pred evaluateWithObject:pass];
        if (result ==NO) {
            return NO;
        }
    }
    NSRange range = [pass rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }else {
        
        return YES; //反之
    }
    
    return result;
    
}
+ (BOOL)judgebankNumberLegal:(NSString *)bankNumber{
    BOOL result = false;
    if ([bankNumber length] >= 9){
        // 判断长度大于6位后再接着判断是否同时包含数字和字符
        NSString * regex = @"^[0-9]{6,25}$";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        result = [pred evaluateWithObject:bankNumber];
        if (result ==NO) {
            return NO;
        }
    }
    NSRange range = [bankNumber rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }
    
    return result;
    
}
+ (BOOL)isSpaceWithstr:(NSString *)str
{
    NSRange range = [str rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO;
    }else {
        return YES; //反之
    }
}
/*去除首尾空格：*/
+ (NSString *)removeSpecialWithText:(NSString *)text
{
    NSString * content = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return content;
}

//获取当前时间戳  （以毫秒为单位）

+(NSString *)getNowTimeTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    return timeSp;
    
}
//获取当前时间
+ (NSString*)getCurrentTimes{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    [formatter setDateFormat:@"YYYY.MM.dd"];
    
    //现在时间,你可以输出来看下是什么格式
    
    NSDate *datenow = [NSDate date];
    
    //----------将nsdate按formatter格式转成nsstring
    
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
    
}
+ (NSString*)getNowTimeymd{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //现在时间,你可以输出来看下是什么格式
    
    NSDate *datenow = [NSDate date];
    
    //----------将nsdate按formatter格式转成nsstring
    
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
    
}
//获取当前时间戳有两种方法(以秒为单位)

+ (NSString *)getNowTimeTimestamp1{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    return timeSp;
    
}
+ (NSMutableArray *)quickSortArray:(NSMutableArray *)array
                     withLeftIndex:(NSInteger)leftIndex
                     andRightIndex:(NSInteger)rightIndex

{
    if (leftIndex >= rightIndex) {//如果数组长度为0或1时返回
        return array;
    }
    
    NSInteger i = leftIndex;
    NSInteger j = rightIndex;
    //记录比较基准数
    HomeModle * modle = array[i];
    CGFloat key = [modle.IncreaseDegree floatValue];
    
    while (i < j) {
        /**** 首先从右边j开始查找比基准数小的值 ***/
        HomeModle * modle = array[i];
        while (i < j && [modle.IncreaseDegree fastestEncoding] >= key) {//如果比基准数大，继续查找
            j--;
        }
        //如果比基准数小，则将查找到的小值调换到i的位置
        array[i] = array[j];
        
        /**** 当在右边查找到一个比基准数小的值时，就从i开始往后找比基准数大的值 ***/
        while (i < j && modle.IncreaseDegree.floatValue <= key) {//如果比基准数小，继续查找
            i++;
        }
        //如果比基准数大，则将查找到的大值调换到j的位置
        array[j] = array[i];
        
    }
    
    //将基准数放到正确位置
    array[i] = modle;
    
    //    /**** 递归排序 ***/
    //    //排序基准数左边的
    //    [self quickSortArray:array withLeftIndex:leftIndex andRightIndex:i - 1];
    //    //排序基准数右边的
    //    [self quickSortArray:array withLeftIndex:i + 1 andRightIndex:rightIndex];
    
    return array;
}
//正则匹配用户身份证号15或18位
+ (BOOL)validateIDCardNumber:(NSString *)value {
    
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSInteger length =0;
    if (!value) {
        return NO;
    }else {
        length = value.length;
        //不满足15位和18位，即身份证错误
        if (length !=15 && length !=18) {
            return NO;
        }
    }
    // 省份代码
    NSArray *areasArray = @[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41", @"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
    
    // 检测省份身份行政区代码
    NSString *valueStart2 = [value substringToIndex:2];
    BOOL areaFlag =NO; //标识省份代码是否正确
    for (NSString *areaCode in areasArray) {
        if ([areaCode isEqualToString:valueStart2]) {
            areaFlag =YES;
            break;
        }
    }
    
    if (!areaFlag) {
        return NO;
    }
    
    NSRegularExpression *regularExpression;
    NSUInteger numberofMatch;
    
    int year =0;
    //分为15位、18位身份证进行校验
    switch (length) {
        case 15:
            //获取年份对应的数字
            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
            
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                //创建正则表达式 NSRegularExpressionCaseInsensitive：不区分字母大小写的模式
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive error:nil];//测试出生日期的合法性
            }
            //使用正则表达式匹配字符串 NSMatchingReportProgress:找到最长的匹配字符串后调用block回调
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            if(numberofMatch >0) {
                return YES;
            }else {
                return NO;
            }
        case 18:
            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$" options:NSRegularExpressionCaseInsensitive error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$" options:NSRegularExpressionCaseInsensitive error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            
            if(numberofMatch >0) {
                //1：校验码的计算方法 身份证号码17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7－9－10－5－8－4－2－1－6－3－7－9－10－5－8－4－2。将这17位数字和系数相乘的结果相加。
                
                int S = [value substringWithRange:NSMakeRange(0,1)].intValue*7 + [value substringWithRange:NSMakeRange(10,1)].intValue *7 + [value substringWithRange:NSMakeRange(1,1)].intValue*9 + [value substringWithRange:NSMakeRange(11,1)].intValue *9 + [value substringWithRange:NSMakeRange(2,1)].intValue*10 + [value substringWithRange:NSMakeRange(12,1)].intValue *10 + [value substringWithRange:NSMakeRange(3,1)].intValue*5 + [value substringWithRange:NSMakeRange(13,1)].intValue *5 + [value substringWithRange:NSMakeRange(4,1)].intValue*8 + [value substringWithRange:NSMakeRange(14,1)].intValue *8 + [value substringWithRange:NSMakeRange(5,1)].intValue*4 + [value substringWithRange:NSMakeRange(15,1)].intValue *4 + [value substringWithRange:NSMakeRange(6,1)].intValue*2 + [value substringWithRange:NSMakeRange(16,1)].intValue *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
                
                //2：用加出来和除以11，看余数是多少？余数只可能有0－1－2－3－4－5－6－7－8－9－10这11个数字
                int Y = S %11;
                NSString *M =@"F";
                NSString *JYM =@"10X98765432";
                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 3：获取校验位
                //4：检测ID的校验位
                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
                    return YES;
                }else {
                    return NO;
                }
                
            }else {
                return NO;
            }
        default:
            return NO;
    }
}
//获取Window当前显示的ViewController
+ (UIViewController*)currentViewController{
    AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *vc  =  appdele.window.rootViewController;
    
    //    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    //    UIViewController* vc = (UIViewController *)window.rootViewController;
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
        vc = ((UITabBarController*)vc).selectedViewController;
    }
    
    if ([vc isKindOfClass:[UINavigationController class]]) {
        vc = ((UINavigationController*)vc).visibleViewController;
    }
    
    if (vc.presentedViewController) {
        vc = vc.presentedViewController;
    }
    
    return vc;
}
//将本地日期字符串转为UTC日期字符串
//本地日期格式:2013-08-03 12:53:51
//可自行指定输入输出格式
+ (NSString *)getUTCFormateLocalDate:(NSString *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //输入格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *dateFormatted = [dateFormatter dateFromString:localDate];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    //输出格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *dateString = [dateFormatter stringFromDate:dateFormatted];
    return dateString;
}

//将UTC日期字符串转为本地时间字符串
//输入的UTC日期格式2013-08-03T04:53:51+0000
+ (NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //输入格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDate];
    //输出格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:dateFormatted];
    return dateString;
}
+ (NSString *)getTimeFromTimestamp:(NSString *)time{
    
    
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:time.integerValue];
    
    //设置时间格式
    
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //将时间转换为字符串
    
    NSString *timeStr=[formatter stringFromDate:myDate];
    
    return timeStr;
}
+ (NSString *)getTimeFromNOYearTimestamp:(NSString *)time{
    
    
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:time.integerValue];
    
    //设置时间格式
    
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"HH:mm:ss"];
    
    //将时间转换为字符串
    
    NSString *timeStr=[formatter stringFromDate:myDate];
    
    return timeStr;
}
+ (NSData *)NSJSONSerializationWithmethod:(NSString *)method
                                parameter:(NSArray *)parameter
                                       id:(NSInteger)id{
    NSData *data= [NSJSONSerialization dataWithJSONObject:@{@"method":method,@"params":parameter,@"id":@(id)}
                                                  options:NSJSONWritingPrettyPrinted error:nil];
    return data;
    
}
+ (void)saveCollectionDataUserDefaultsWith:(NSString *)market;
{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:market];
    NSMutableArray *likeArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"Collection"] mutableCopy];
    if (!likeArray) {
        likeArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [likeArray addObject:data];
    NSArray *saveArray = [NSArray arrayWithArray:likeArray];
    [[NSUserDefaults standardUserDefaults]setObject:saveArray forKey:@"Collection"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
+ (NSMutableArray *)getCollectionData
{
    //取出
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *array  = [userDefaults  objectForKey:@"Collection"];
    NSMutableArray * collectionArray = [[NSMutableArray alloc] init];
    for (NSData* data in array) {
        NSString * str = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (![NSString isEmptyString:str]) {
            [collectionArray addObject:str];
        }
        
    }
    
    return collectionArray;
}
+ (void)removoveWithmarket:(NSString *)market;
{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:market];
    NSMutableArray *likeArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"Collection"] mutableCopy];
    if (!likeArray) {
        likeArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [likeArray removeObject:data];
    NSArray *saveArray = [NSArray arrayWithArray:likeArray];
    [[NSUserDefaults standardUserDefaults]setObject:saveArray forKey:@"Collection"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
+ (void)deletaCollectionData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"Collection"];
    
    //    NSString *appDomainStr = [[NSBundle mainBundle] bundleIdentifier];
    //    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomainStr];
    
}

//身份绑定
+ (BOOL)isPhoneBind;
{
    NSString * str = [EXUserManager phone];
    return [str isEqualToString:@"1"]?YES:NO;
}
+ (BOOL)isEmailBind
{
    NSString * str = [EXUserManager email];
    return [str isEqualToString:@"1"]?YES:NO;
}
+ (BOOL)isGooleBind;
{
    NSString * str = [EXUserManager goole];
    return [str isEqualToString:@"1"]?YES:NO;
}
+ (BOOL)isRealName
{
    if ([EXUserManager authenticatedName].integerValue ==1)
    {
        return YES;
    }
    NSString * str = [NSString stringWithFormat:@"%@",[EXUserManager RealName]];
    if ([str isEqualToString:@"0"] || [str isEqualToString:@"1"] ||[str isEqualToString:@"3"]) {
        return NO;
    }else if ([str isEqualToString:@"2"])
    {
        return YES;
    }
    return NO;
}
+ (BOOL)isOpenTouchid;
{
    NSString * str = [EXUserManager Touchid];
    return [str isEqualToString:@"0"]?NO:YES;
}
+ (BOOL)isOpenGesture;
{
    NSString * str = [EXUserManager Gesture];
    return [str isEqualToString:@"0"]?NO:YES;
}
/*
 * @return 币种价格
 @param name 币种名字
 @param Price 币种价格
 */
+ (NSString *)getBTBPriceWithName:(NSString *)name Price:(NSString *)Price
{
    
    
    if ([[self getSite] isEqualToString:@"KCoin"])
    {
        if ([name isEqualToString:[AppDelegate shareAppdelegate].DICKEY])
        {
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"≈ ¥ %.4f",Price.floatValue];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"≈ $ %.4f",Price.floatValue/[EXUserManager currency].USD_CNY.floatValue];
            }
            
            
        }else if ([name isEqualToString:@"USDT"])
        {
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"≈ ¥ %.4f",Price.floatValue * [EXUserManager currency].USD_CNY.floatValue];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"≈ $ %.4f",Price.floatValue];
            }
        }else
        {
            NSDictionary * dic = [AppDelegate shareAppdelegate].dictionary;
            NSString * valuestr = @"0";
            
            for (NSString *key in dic) {
                NSString *value = dic[key];
                if ([name isEqualToString:key] )
                {
                    valuestr = value;
                    break;
                }
                
            }
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"≈ ¥ %.4f",Price.floatValue*valuestr.floatValue*1];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"≈ $ %.4f",Price.floatValue*valuestr.floatValue*1 / [EXUserManager currency].USD_CNY.floatValue];
            }
            
        }
    }
    
    NSString *newPrice =nil;
    BOOL isfloat = false;
    BOOL isdian= false;
    int index = 0;
    Price = [NSString stringWithFormat:@"%@",@(Price.floatValue)];
    if ([name isEqualToString:[AppDelegate shareAppdelegate].DICKEY] || [name isEqualToString:@"USDT"])
    {
        for(int i =0; i < [Price length]; i++)
        {
            
            NSString * temp = [Price substringWithRange:NSMakeRange(i, 1)];
            if ([temp isEqualToString:@"."]) {
                index=0;
                isdian = YES;
            }
            if (![self isPureInt:Price]) {
                
                if (isdian) {
                    if ([temp isEqualToString:@"0"])
                    {
                        isfloat = YES;
                        if ([name isEqualToString:@"USDT"])
                        {
                            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                            {
                                NSString * p = [NSString stringWithFormat:@"%@",@(Price.floatValue * [EXUserManager currency].USD_CNY.floatValue)];
                                newPrice = [NSString stringWithFormat:@"≈ ¥ %@",[self formatternumber: index + 2 assess:p]];
                                
                            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                            {
                                newPrice = [NSString stringWithFormat:@"≈ $ %@",[self formatternumber: index + 2 assess:Price]];
                            }
                        }else
                        {
                            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                            {
                                newPrice = [NSString stringWithFormat:@"≈ ¥ %@",[self formatternumber: index + 2 assess:Price]];
                            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                            {
                                NSString * p = [NSString stringWithFormat:@"%@",@(Price.floatValue / [EXUserManager currency].USD_CNY.floatValue)];
                                newPrice = [NSString stringWithFormat:@"≈ $ %@",[self formatternumber: index + 2 assess:p]];
                            }
                        }
                        
                        
                        
                    }
                }
                if (index>3)
                {
                    if ([self formatternumber: 4 assess:Price].floatValue >0.00)
                    {
                        if ([name isEqualToString:@"USDT"])
                        {
                            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                            {
                                NSString * p = [NSString stringWithFormat:@"%@",@(Price.floatValue * [EXUserManager currency].USD_CNY.floatValue)];
                                return  [NSString stringWithFormat:@"≈ ¥ %@",[self formatternumber: 4 assess:p]];
                                
                            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                            {
                                
                                return  [NSString stringWithFormat:@"≈ $ %@",[NSString stringWithFormat:@"%@",@([self formatternumber: 4 assess:Price].floatValue)]];
                            }
                            
                        }else
                        {
                            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                            {
                                return  [NSString stringWithFormat:@"≈ ¥ %@",[NSString stringWithFormat:@"%@",@([self formatternumber: 4 assess:Price].floatValue)]];
                            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                            {
                                NSString * p = [NSString stringWithFormat:@"%@",@(Price.floatValue / [EXUserManager currency].USD_CNY.floatValue)];
                                return  [NSString stringWithFormat:@"≈ $ %@",[self formatternumber: 4 assess:p]];
                                
                            }
                        }
                        
                        
                        
                    }else
                    {
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            return  @"< ¥ 0.00";
                        }else
                        {
                            return  @"< $ 0.00";
                        }
                    }
                    
                }
            }else
            {
                if ([self formatternumber: 4 assess:Price].floatValue >0.00)
                {
                    if ([name isEqualToString:@"USDT"])
                    {
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            
                            return  [NSString stringWithFormat:@"≈ ¥ %.2f",Price.floatValue * [EXUserManager currency].USD_CNY.floatValue];
                            
                        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                        {
                            
                            return  [NSString stringWithFormat:@"≈ $ %.2f",Price.floatValue];
                        }
                        
                    }else
                    {
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            return  [NSString stringWithFormat:@"≈ ¥ %.2f",Price.floatValue];
                        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                        {
                            
                            return  [NSString stringWithFormat:@"≈ $ %.2f",Price.floatValue / [EXUserManager currency].USD_CNY.floatValue];
                        }
                    }
                    
                    
                    
                    
                }else
                {
                    if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                    {
                        return  @"< ¥ 0.00";
                    }else
                    {
                        return  @"< $ 0.00";
                    }
                    
                }
                
                
            }
            
            index ++;
        }
        if (!isfloat) {
            
            if ([name isEqualToString:@"USDT"])
            {
                if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                {
                    return [NSString stringWithFormat:@"≈ ¥ %.2f",Price.floatValue *  [EXUserManager currency].USD_CNY.floatValue];
                }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                {
                    return [NSString stringWithFormat:@"≈ $ %.2f",Price.floatValue ];
                }
                
            }else
            {
                if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                {
                    return [NSString stringWithFormat:@"≈ ¥ %.2f",Price.floatValue];
                }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                {
                    return [NSString stringWithFormat:@"≈ $ %.2f",Price.floatValue / [EXUserManager currency].USD_CNY.floatValue];
                }
            }
            
            
        }
        
        return  [NSString stringWithFormat:@"%@",newPrice];
        
    }else
    {
        NSDictionary * dic = [AppDelegate shareAppdelegate].dictionary;
        NSString * valuestr = @"0";
        
        for (NSString *key in dic) {
            NSString *value = dic[key];
            if ([name isEqualToString:key] )
            {
                valuestr = value;
                break;
            }
            
            
        }
        
        NSString * newStr = [NSString stringWithFormat:@"%@",@(Price.floatValue*valuestr.floatValue*1)];
        for(int i =0; i < [newStr length]; i++)
        {
            NSString * temp = [newStr substringWithRange:NSMakeRange(i, 1)];
            if ([temp isEqualToString:@"."]) {
                index=0;
                isdian = YES;
            }
            if (![self isPureInt:newStr]) {
                
                if (isdian) {
                    if ([temp isEqualToString:@"0"])
                    {
                        isfloat = YES;
                        
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            newPrice = [NSString stringWithFormat:@"≈ ¥ %@",[self formatternumber: index + 2 assess:newStr]];
                        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                        {
                            NSString * p = [NSString stringWithFormat:@"%@",@(newStr.floatValue / [EXUserManager currency].USD_CNY.floatValue)];
                            newPrice = [NSString stringWithFormat:@"≈ $ %f",[self formatternumber: index + 2 assess:p].floatValue];
                        }
                    }
                    
                }
                if (index>3)
                {
                    if ([self formatternumber: 4 assess:newStr].floatValue >0.00)
                    {
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            return  [NSString stringWithFormat:@"≈ ¥ %@",[NSString stringWithFormat:@"%@",@([self formatternumber: 4 assess:newStr].floatValue)]];
                        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                        {
                            NSString * p = [NSString stringWithFormat:@"%@",@(newStr.floatValue / [EXUserManager currency].USD_CNY.floatValue)];
                            
                            return  [NSString stringWithFormat:@"≈ $ %@",[NSString stringWithFormat:@"%@",@([self formatternumber: 4 assess:p].floatValue)]];
                        }
                        
                        
                    }else
                    {
                        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                        {
                            return  @"< ¥ 0.00";
                        }else
                        {
                            return  @"< $ 0.00";
                        }
                    }
                    
                }
                
            }else
            {
                if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
                {
                    return [NSString stringWithFormat:@"≈ ¥ %.2f",newStr.floatValue];
                }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
                {
                    return [NSString stringWithFormat:@"≈ $ %.2f",newStr.floatValue / [EXUserManager currency].USD_CNY.floatValue];
                }
                
            }
            
            
            index ++;
        }
        if (!isfloat) {
            
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"≈ ¥ %.2f",newStr.floatValue];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"≈ $ %.2f",newStr.floatValue / [EXUserManager currency].USD_CNY.floatValue];
            }
        }
        
        return  [NSString stringWithFormat:@"%@",newPrice];
        
    }
    
    
}
/*判断是否为整形*/
+ (BOOL)isPureInt:(NSString*)string{
    
    NSScanner* scan = [NSScanner scannerWithString:string];
    
    int val;
    
    return[scan scanInt:&val] && [scan isAtEnd];
    
}
/*
 * @return 资金筛选
 @param name 币种名字
 @param Price 币种价格
 */
+ (NSString *)getCapitalPriceNoUnitWithName:(NSString *)name Price:(NSString *)Price
{
    
    
    if ([[self getSite] isEqualToString:@"KCoin"])
    {
        
        if ([name isEqualToString:[AppDelegate shareAppdelegate].DICKEY])
        {
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue/[EXUserManager currency].USD_CNY.floatValue];
            }
            
            
        }else if ([name isEqualToString:@"USDT"])
        {
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue * [EXUserManager currency].USD_CNY.floatValue];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue];
            }
        }else
        {
            NSDictionary * dic = [AppDelegate shareAppdelegate].dictionary;
            NSString * valuestr = @"0";
            
            for (NSString *key in dic) {
                NSString *value = dic[key];
                if ([name isEqualToString:key] )
                {
                    valuestr = value;
                    break;
                }
                
            }
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue*valuestr.floatValue*1];
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"%.4f",Price.floatValue*valuestr.floatValue*1 / [EXUserManager currency].USD_CNY.floatValue];
            }
            
        }
        
    }
    
    
    if ([name isEqualToString:[AppDelegate shareAppdelegate].DICKEY])
    {
        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue];
        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue/[EXUserManager currency].USD_CNY.floatValue];
        }else
        {
            return @"";
        }
        
        
    }else if ([name isEqualToString:@"USDT"])
    {
        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue * [EXUserManager currency].USD_CNY.floatValue];
        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue];
        }
    }else
    {
        NSDictionary * dic = [AppDelegate shareAppdelegate].dictionary;
        NSString * valuestr = @"0";
        BOOL ishide = NO;
        for (NSString *key in dic) {
            
            if ([name isEqualToString:key] )
            {
                NSString *value = dic[key];
                valuestr = value;
                ishide = YES;
                break;
            }
            
        }
        if (!ishide)
        {
            for (HomeModle * hModle in [AppDelegate shareAppdelegate].dataArray)
            {
                if ([hModle.name isEqualToString:[NSString stringWithFormat:@"%@_%@",name,@"USDT"]])
                {
                    valuestr =  hModle.last;
                    
                }
            }
            if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
            {
                return [NSString stringWithFormat:@"%.2f",Price.floatValue*valuestr.floatValue*[EXUserManager currency].USD_CNY.floatValue];
                
            }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
            {
                return [NSString stringWithFormat:@"%.2f",Price.floatValue*valuestr.floatValue*1];
            }
            
        }
        if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue*valuestr.floatValue*1];
            
        }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
        {
            return [NSString stringWithFormat:@"%.2f",Price.floatValue*valuestr.floatValue*1 / [EXUserManager currency].USD_CNY.floatValue];
        }
        
    }
    return @"";
}
+ (NSString *)iphoneType {
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    
    return platform;
    
}
//获取应用当前版本号
+ (NSString *)getVersion
{
    //此获取的版本号对应bundle，打印出来对应为12345这样的数字
    //    NSNumber *number = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
    
    //此获取的版本号对应version，打印出来对应为1.2.3.4.5这样的字符串
    NSString *string = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return string;
}
/**
 *    不四舍五入截取索引位置
 */
+ (NSString *)formatternumber:(int)i assess:(NSString *)assess
{
    
    //    NSString* format = [NSString stringWithFormat:@"%%.%df",i];
    //    NSString * str = [NSString stringWithFormat:format,assess.doubleValue];
    //    return str;
    
    
    
    //    NSDecimalNumberHandler * roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:i raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    //    NSDecimalNumber*ouncesDecimal = [[NSDecimalNumber alloc]initWithFloat:assess.floatValue];
    //    NSDecimalNumber*roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:i
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:assess];
    NSDecimalNumber*roundedOunces = [a decimalNumberByRoundingAccordingToBehavior:roundUp];
    return [self asksOrbadsformatternumber:i assess:[NSString stringWithFormat:@"%@",roundedOunces]];
    
}

/*!
 @brief 修正浮点型精度丢失
 @param str 传入接口取到的数据
 @return 修正精度后的数据
 */
+(NSString *)reviseString:(NSString *)str
{
    //直接传入精度丢失有问题的Double类型
    double conversionValue = [str doubleValue];
    NSString *doubleString = [NSString stringWithFormat:@"%lf", conversionValue];
    NSDecimalNumber *decNumber = [NSDecimalNumber decimalNumberWithString:doubleString];
    return [decNumber stringValue];
}
/**
 * 当精度丢失时 保留两位有效数字（字符串转浮点型 可能精度丢失）
 */
+ (NSString *)asksOrbadsformatternumber:(int)i assess:(NSString *)assess
{
    NSString* format = [NSString stringWithFormat:@"%%.%df",i];
    NSString * str = [NSString stringWithFormat:format,assess.doubleValue];
    return str;
}

/*
 去掉字符串里的空格和换行
 */
+ (NSString *)removeCharacter:(NSString *)str;
{
    //1. 去掉首尾空格和换行符
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //2. 去掉所有空格和换行符
    
    str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return str;
}
/*
 去掉字符串空格和换行
 */
+(NSString *)removeStrCharacter:(NSString *)str;
{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return str;
}
+ (NSMutableArray *)getNewbidsArrayWithData:(NSArray *)Array withArray:(NSMutableArray *)newArray model:(HomeModle *)model
{
    NSMutableArray * newbids = [[NSMutableArray alloc] init];
    NSMutableArray * newAry = [NSMutableArray arrayWithArray:Array];
    if (Array.count==0||Array==nil) {
        return newArray;
    }
    const float EPSINON = 0.00001;
    int index;
    if (newArray.count==0)
    {
        for (int i=0; i<newAry.count; i++)
        {
            NSArray * arr = newAry[i];
            if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
            {
                
            }else
            {
                
                [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
            }
        }
        return newbids;
    }
    //找到arr2中有,arr1中没有的数据
    for (int i=0; i<newArray.count; i++)
    {
        BOOL isHave = NO;
        NSArray * asks = newArray[i];
        index = i;
        NSString * price =  [EXUnit asksOrbadsformatternumber:model.quote_asset_precision.intValue assess:asks[0]];
        
        for (int i=0; i<newAry.count; i++)
        {
            NSArray * arr = newAry[i];
            NSString * newprice = [EXUnit asksOrbadsformatternumber:model.quote_asset_precision.intValue assess:arr[0]];
            if ([price isEqualToString:newprice])
            {
                if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
                {
                    
                }else
                {
                    
                    [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
                    [newAry removeObject:arr];
                }
                isHave = YES;
                
            }
            
            if (index == newArray.count-1)
            {
                if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
                {
                    
                }else
                {
                    
                    [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
                    
                }
            }
            
        }
        
        if (!isHave) {
            [newbids addObject:asks];
        }
        
    }
    return newbids;
}
+ (NSMutableArray *)getNewAsksArrayWithData:(NSArray *)Array withArray:(NSMutableArray *)newArray model:(HomeModle *)model
{
    NSMutableArray * asksArray = [[NSMutableArray alloc] init];
    NSMutableArray * newbids = [[NSMutableArray alloc] init];
    NSMutableArray * newAry = [NSMutableArray arrayWithArray:Array];
    if (Array.count==0||Array==nil) {
        return newArray;
    }
    const float EPSINON = 0.00001;
    
    int index;
    if (newArray.count==0)
    {
        for (int i=0; i<newAry.count; i++)
        {
            NSArray * arr = newAry[i];
            if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
            {
                
            }else
            {
                
                [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
            }
        }
        return newbids;
    }
    //找到arr2中有,arr1中没有的数据
    for (int i=0; i<newArray.count; i++)
    {
        BOOL isHave = NO;
        NSArray * asks = newArray[i];
        index = i;
        NSString * price =  [EXUnit asksOrbadsformatternumber:model.quote_asset_precision.intValue assess:asks[0]];
        
        for (int i=0; i<newAry.count; i++)
        {
            NSArray * arr = newAry[i];
            NSString * newprice = [EXUnit asksOrbadsformatternumber:model.quote_asset_precision.intValue assess:arr[0]];
            if ([price isEqualToString:newprice])
            {
                if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
                {
                    [newAry removeObject:arr];
                }else
                {
                    
                    [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
                    [newAry removeObject:arr];
                }
                isHave = YES;
                
            }
            
            if (index == newArray.count-1)
            {
                if (([arr[1] floatValue] >= - EPSINON) && ([arr[1] floatValue] <= EPSINON))
                {
                    [newAry removeObject:arr];
                    
                }else
                {
                    
                    [newbids addObject:arr];//如果有相同的价格添加最新推送的价格
                    
                }
            }
            
        }
        
        if (!isHave) {
            [newbids addObject:asks];
        }
        
    }
    NSArray * newsasks = [self get5listAskAry:newbids];
    NSArray * asks;
    if (newsasks.count>10) {
        asks = [newsasks subarrayWithRange:NSMakeRange(0, 10)];
    }else
    {
        asks = newsasks;
    }
    asksArray = [NSMutableArray arrayWithArray:asks];
    return asksArray;
}
//升序
+ (NSArray *)get5listAskAry:(NSArray *)asksArray
{
    if (asksArray.count>1) {
        //对数组进行排序
        NSArray  *ary = [asksArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:obj1[0]];
            NSDecimalNumber *number1 = [NSDecimalNumber decimalNumberWithString:obj2[0]];
            NSComparisonResult result_clearrate_float = [number compare:number1];
            if (result_clearrate_float==NSOrderedDescending)
            {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedAscending;
            }
        }];
        return ary;
    }else
    {
        return asksArray;
    }
    
}
+ (NSString *)getSite;
{
    if ([HOST_IP isEqualToString:@"http://47.110.129.33:8883/api/v1/"]||[HOST_IP isEqualToString:@"https://www.zg.com/api/v1/"]) {
        
        return @"ZG";
        
    }else if ([HOST_IP isEqualToString:@"https://www.58.vip/api/v1/"])
    {
        return @"58";
    }else if ([HOST_IP isEqualToString:@"https://www.rr.vip/api/v1/"])
    {
        return @"人人币";
    }else if ([HOST_IP isEqualToString:@"https://www.k-coin.io/api/v1/"])
    {
        return @"KCoin";
    }else if ([HOST_IP isEqualToString:@"http://47.97.206.151:8883/api/v1/"] ||[HOST_IP isEqualToString:@"https://www.zt.com/api/v1/"])
    {
        return @"ZT";
    }else if ([HOST_IP isEqualToString:@"http://www.bitaladdin.com/api/v1/"] ||[HOST_IP isEqualToString:@"https://www.bitaladdin.com/api/v1/"])
    {
        return @"BitAladdin";
    }else if ([HOST_IP isEqualToString:@"https://www.zgk.com/api/v1/"])
    {
        return @"ZGK";
    }else if ([HOST_IP isEqualToString:@"https://www.gemex.pro/api/v1/"])
    {
        return @"GEMEX";
    }else if ([HOST_IP isEqualToString:@"https://www.gitbtc.net/api/v1/"])
    {
        return @"GitBtc";
    }else if ([HOST_IP isEqualToString:@"https://www.aucep.com/api/v1/"])
    {
        return @"ACE";
    }else if ([HOST_IP isEqualToString:@"https://www.rockex.top/api/v1/"])
    {
        return @"Rockex";
    }else if ([HOST_IP isEqualToString:@"https://www.coinbetter.com/api/v1/"])
    {
        return @"Coinbetter";
    }else if ([HOST_IP isEqualToString:@"https://www.comex.vip/api/v1/"])
    {
        return @"Comex";
    }else if ([HOST_IP isEqualToString:@"https://www.ex.pizza/api/v1/"])
    {
        return @"Ex.pizza";
    }
    
    return nil;
}
/**
 *  设置字体大小或类型
 *
 *  @param name 类型
 *  @param size 内容
 *
 *  @return 普通字符串
 */
+ (UIFont *)setFontName:(NSString *)name size:(CGFloat)size;
{
    return [UIFont fontWithName:name size:size * UIScreenWidth/(375.0f)];
}
+ (NSString *)isLocalizable
{
    return [ZBLocalized sharedInstance].currentLanguage;
}
/**
 *  提币地址限制
 *
 *  @param address 提币地址
 *  @return 是否合法
 */
+ (BOOL)judgeAddressLegal:(NSString *)address{
    BOOL result = NO;
    if ([address length] > 0 && [address length]<=50)
    {
        NSString *str =@"^[A-Za-z0-9\\u9fa5]+$";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", str];
        if (![emailTest evaluateWithObject:address])
        {
            return NO;
        }
    }else
    {
        return NO;
    }
    NSRange range = [address rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }else {
        
        return YES; //反之
    }
    
    return result;
    
}
/**
 * 开始到结束的时间差是否在五天之类
 */
+ (NSString *)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = start - end;
    int second = (int)value %60;//秒
    int minute = (int)value /60%60;
    int house = (int)value / (24 *3600)%3600;
    int day = (int)value / (24 *3600);
    NSString *str;
    if (day != 0) {
        str = [NSString stringWithFormat:@"%d天 %d:%d:%d",day,house,minute,second];
    }else if (day==0 && house !=0) {
        str = [NSString stringWithFormat:@"%d:%d:%d",house,minute,second];
    }else if (day==0 && house==0 && minute!=0) {
        
        str = [NSString stringWithFormat:@"00:%d:%d",minute,second];
        
    }else{
        str = [NSString stringWithFormat:@"00:00:%d",second];
    }
    if (second <= 0)
    {
        str = @"已过期";
    }
    return str;
}
/**
 *  KCion 必须是500的倍数切不能低于500
 *
 *  @param Price 购买的价格
 *  @return 是否成立
 */
+ (BOOL)KCionC2C500WithPrice:(NSString *)Price;
{
    if(Price.floatValue>0)
    {
        return YES;
        
    }else
    {
        return NO;
    }
}
//获取其拼音
+ (NSString *)huoqushouzimuWithString:(NSString *)string{
    NSMutableString *ms = [[NSMutableString alloc]initWithString:string];
    CFStringTransform((__bridge CFMutableStringRef)ms, 0,kCFStringTransformStripDiacritics, NO);
    NSString *bigStr = [ms uppercaseString];
    NSString *cha = [bigStr substringToIndex:1];
    return cha;
}

//根据拼音的字母排序  ps：排序适用于所有类型
+ (NSMutableArray *)paixuWith:(NSMutableArray *)array{
    [array sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        NSString *string1 = [self huoqushouzimuWithString:modle1.name];
        NSString *string2 = [self huoqushouzimuWithString:modle2.name];;
        return [string1 compare:string2];
    }];
    
    return array;
}
//根据拼音的字母排序  ps：排序适用于所有类型
+ (NSMutableArray *)jiangPxuWith:(NSMutableArray *)array{
    [array sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        NSString *string1 = [self huoqushouzimuWithString:modle1.name];
        NSString *string2 = [self huoqushouzimuWithString:modle2.name];;
        return [string2 compare:string1];
    }];
    
    return array;
}

// 降序排序
+ (NSMutableArray *)jiangxuWith:(NSMutableArray *)array{
    
    NSArray * result = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        if ([modle1.IncreaseDegree floatValue] > [modle2.IncreaseDegree floatValue])
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
        
    }];
    NSMutableArray * ary = [NSMutableArray arrayWithArray:result];
    return ary;
}
// 升序排序
+ (NSMutableArray *)shengxuWith:(NSMutableArray *)array{
    NSArray * result = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        if ([modle1.IncreaseDegree floatValue] < [modle2.IncreaseDegree floatValue])
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
        
    }];
    NSMutableArray * ary = [NSMutableArray arrayWithArray:result];
    return ary;
}

// 价格降序排序
+ (NSMutableArray *)pricejiangxuWith:(NSMutableArray *)array{
    
    NSArray * result = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        NSString *  price1 = modle1.last;
        NSString *  price2 = modle2.last;
        if ([price1 floatValue] > [price2 floatValue])
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
        
    }];
    NSMutableArray * ary = [NSMutableArray arrayWithArray:result];
    return ary;
}
// 升序排序
+ (NSMutableArray *)priceshengxuWith:(NSMutableArray *)array{
    NSArray * result = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        HomeModle * modle1 = obj1;
        HomeModle * modle2 = obj2;
        NSString *  price1 = modle1.last;
        NSString *  price2 = modle2.last;
        if ([price1 floatValue] < [price2 floatValue])
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
        
    }];
    NSMutableArray * ary = [NSMutableArray arrayWithArray:result];
    return ary;
}
+ (CAShapeLayer *)ShapeLayerWithViewCGRect:(UIView *)view cornerRadius:(CGFloat)cornerRadius;
{
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight |UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(cornerRadius, cornerRadius)].CGPath;
    return maskLayer;
}
//绘制渐变色颜色的方法
+ (CAGradientLayer *)setGradualChangingColor:(UIView *)view fromColor:(NSString *)fromHexColorStr toColor:(NSString *)toHexColorStr{
    
    //    CAGradientLayer类对其绘制渐变背景颜色、填充层的形状(包括圆角)
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = view.bounds;
    
    //  创建渐变色数组，需要转换为CGColor颜色
    gradientLayer.colors = @[(__bridge id)[self colorWithHex:fromHexColorStr].CGColor,(__bridge id)[self colorWithHex:toHexColorStr].CGColor];
    
    //  设置渐变颜色方向，左上点为(0,0), 右下点为(1,1)
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    
    //  设置颜色变化点，取值范围 0.0~1.0
    gradientLayer.locations = @[@0,@1];
    
    return gradientLayer;
}
//获取16进制颜色的方法
+ (UIColor *)colorWithHex:(NSString *)hexColor {
    hexColor = [hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([hexColor length] < 6) {
        return nil;
    }
    if ([hexColor hasPrefix:@"#"]) {
        hexColor = [hexColor substringFromIndex:1];
    }
    NSRange range;
    range.length = 2;
    range.location = 0;
    NSString *rs = [hexColor substringWithRange:range];
    range.location = 2;
    NSString *gs = [hexColor substringWithRange:range];
    range.location = 4;
    NSString *bs = [hexColor substringWithRange:range];
    unsigned int r, g, b, a;
    [[NSScanner scannerWithString:rs] scanHexInt:&r];
    [[NSScanner scannerWithString:gs] scanHexInt:&g];
    [[NSScanner scannerWithString:bs] scanHexInt:&b];
    if ([hexColor length] == 8) {
        range.location = 4;
        NSString *as = [hexColor substringWithRange:range];
        [[NSScanner scannerWithString:as] scanHexInt:&a];
    } else {
        a = 255;
    }
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:((float)a / 255.0f)];
}
+ (BOOL)CodeissSixplace:(NSString *)code;
{
    if (code.length == codelength)
    {
        return YES;
    }
    return NO;
}

+ (BOOL)array:(NSArray *)array1 isEqualTo:(NSArray *)array2 {
    if (array1.count != array2.count) {
        return NO;
    }
    for (NSString *str in array2) {
        if (![array1 containsObject:str]) {
            return NO;
        }
    }
    return YES;
}
+ (NSString *)getPriceCurrencyWith:(HomeModle *)modle;
{
    if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"]) //如果是cny 计价
    {
        if ([modle.quote_asset isEqualToString:[AppDelegate shareAppdelegate].DICKEY])//判断值是交易对值 直接取出折合这里的已经换上好了
        {
            return modle.currency;
        }else
        {
            //这里为什么要直接获取数据dataArray 而不是从数据库获取，因为循环次数过大获取表的效率降低，app不流畅，主线程堵塞的原因可能和插件有关
            
            for (HomeModle * hModle in [AppDelegate shareAppdelegate].dataArray) //如果不是先判断是否有交易对的值如果有直接取 （注意 如果这时候市场价为0没有销售量 直接取曾经算过的为0的值）
            {
                if ([[NSString stringWithFormat:@"%@_%@",modle.base_asset,[AppDelegate shareAppdelegate].DICKEY] isEqualToString:hModle.name]) //如果有交易对为usdt的直接取出之前有的
                {
                    if (modle.last.floatValue >0) {
                        
                        return hModle.currency;
                    }else
                    {
                        return modle.currency;
                    }
                    
                }
            }
            return modle.currency;
        }
        
    }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
    {
        if ([modle.quote_asset isEqualToString:@"USDT"]) //美元直接取c值
        {
            return modle.currency;
            
        }else
        {
            for (HomeModle * allModle in [AppDelegate shareAppdelegate].dataArray)
            {
                if ([allModle.name isEqualToString: [NSString stringWithFormat:@"%@_%@",modle.base_asset,@"USDT"]]) //如果有sudt交易对优秀取usdt的市场价
                {
                    return  allModle.currency;
                    
                }
            }
            
            for (HomeModle * allModle in [AppDelegate shareAppdelegate].dataArray)
            {
                if ([[NSString stringWithFormat:@"%@_%@",modle.base_asset,[AppDelegate shareAppdelegate].DICKEY] isEqualToString:allModle.name])
                {
                    if (modle.last.floatValue >0) {
                        
                        return allModle.currency;
                    }else
                    {
                        return modle.currency;
                    }
                }
            }
            return  modle.currency;
            
        }
    }
    return @"";
}
// 是否隐藏折合价格
+ (BOOL)isHideCurrencyPrice
{
    //     NSArray * array = [NSArray bg_arrayWithName:FMDBName_Symbol];
    for (HomeModle * modle in [AppDelegate shareAppdelegate].dataArray)
    {
        if ([modle.quote_asset isEqualToString:[AppDelegate shareAppdelegate].DICKEY] || [modle.quote_asset isEqualToString:@"USDT"])
        {
            return NO;
        }
    }
    return YES;
}
/*获取zt 价格单位 其他位¥*/
+ (NSString *)getZTPriceUnit;
{
    if ([[self getSite] isEqualToString:@"ZT"])
    {
        return @"NT$";
    }else
    {
        return @"¥";
    }
}
@end
