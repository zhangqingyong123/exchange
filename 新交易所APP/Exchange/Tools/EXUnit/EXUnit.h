//  LoginViewController.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/21.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeModle.h"
@interface EXUnit : NSObject
+ (void)showMessage:(NSString *)message;
//SVP
+ (void)showSVProgressHUD:(NSString *)title;
/*
 iphoneX
 */
+ (BOOL)deviceIsIPhoneX;
/*
  获取手机系统
 */
+ (NSString *)iphoneType;

/**
 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
 
 @param color #开头字符串色值
 @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)color;
//*   *//
/**
 查找指定字符串的富文本
 
 @param str str
 @param attributedString attributedString
 @param color color
 @param font font
 @return 富文本
 */
+ (NSMutableAttributedString *)finderattributedString:(NSString *)str
                                     attributedString:(NSString *)attributedString
                                                color:(UIColor *)color
                                                 font:(UIFont*)font;
//设置行间距
+ (NSMutableAttributedString *)finderattributedLinesString:(NSString *)str
                                          attributedString:(NSString *)attributedString
                                                     color:(UIColor *)color
                                                      font:(UIFont *)font;

/**
 自定义图片大小·
 字符串与图片拼接为富文本字符串
 
 @param str 原始文字
 @param imageStr image名称
 @param index 位置
 @param imageframe  imageframe y轴偏移量 传0为默认(向上偏移约0.5),>0向上偏移，<0向下偏移
 @return 富文本
 */
+ (NSMutableAttributedString *)getcontent:(NSString *)str image:(NSString *)imageStr index:(NSInteger)index imageframe:(CGRect)imageframe;
/*
 *  判断用户输入的密码是否符合规范，符合规范的密码要求：
 1. 长度大于8位
 2. 密码中必须同时包含数字和字母
 */  
+ (BOOL)judgePassWordLegal:(NSString *)pass;

+ (NSString*)getNowTimeymd;

/*获取当前时间戳  （以毫秒为单位）
 * @return 字符串时间
 */

+(NSString *)getNowTimeTimestamp;
/* 快排
 * @return 新的数组
 @param array 要排到数组
 @param leftIndex 要排到数组
 */
+ (NSMutableArray *)quickSortArray:(NSMutableArray *)array
                     withLeftIndex:(NSInteger)leftIndex
                     andRightIndex:(NSInteger)rightIndex;
/*
 * @return 时间戳转时间
 @param time 时间戳
 @param
 */
+ (NSString *)getTimeFromTimestamp:(NSString *)time;
+ (NSString *)getTimeFromNOYearTimestamp:(NSString *)time;
//将本地日期字符串转为UTC日期字符串
//本地日期格式:2013-08-03 12:53:51
//可自行指定输入输出格式
+ (NSString *)getUTCFormateLocalDate:(NSString *)localDate;
//将UTC日期字符串转为本地时间字符串
//输入的UTC日期格式2013-08-03T04:53:51+0000
+ (NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate;

/**
 获取Window当前显示的ViewController
 
 @return Window当前显示的ViewController
 */
+ (UIViewController*)currentViewController;
/*
 * @return jsondata
 @param method 请求接口
 @param parameter 请求参数已数组形式
 @param id;
 @return data
 */
+ (NSData *)NSJSONSerializationWithmethod:(NSString *)method
                                parameter:(NSArray *)parameter
                                       id:(NSInteger)id;


/*
 @param 正则匹配用户身份证号15或18位
 @param value 输入的身份证号
 @return 是否为身份证号
 */
+ (BOOL)validateIDCardNumber:(NSString *)value;

/*
 * @return 储存可变数组 收藏
 @param method 请求接口
 @param getCollectionData 获取收藏
 @param deletaCollectionData 删除
 */
+ (void)saveCollectionDataUserDefaultsWith:(NSString *)market;
/*
 * @return 币种价格
 @param name 币种名字
 @param Price 币种价格
 */
+ (NSString *)getBTBPriceWithName:(NSString *)name Price:(NSString *)Price;
// 判断价格是否小于0.01
+ (NSString *)getBTBPriceWithPriceStr:(NSString *)Price;
//获取收藏
+ (NSMutableArray *)getCollectionData;
+ (NSString *)getNowTimeTimestamp1;
//取消收藏
+ (void)removoveWithmarket:(NSString *)market;
+ (void)deletaCollectionData;
//获取应用当前版本号
+ (NSString *)getVersion;
//身份绑定
+ (BOOL)isPhoneBind;
+ (BOOL)isEmailBind;
+ (BOOL)isGooleBind;
+ (BOOL)isRealName;
//是否开启指纹验证
+ (BOOL)isOpenTouchid;
// 是否开启手势验证
+ (BOOL)isOpenGesture;

/*!
 @brief 修正浮点型精度丢失
 @param str 传入接口取到的数据
 @return 修正精度后的数据
 */
+(NSString *)reviseString:(NSString *)str;
/**
 * 当精度丢失时 保留两位有效数字（字符串转浮点型 可能精度丢失）
 */
+ (NSString *)asksOrbadsformatternumber:(int)i assess:(NSString *)assess;
/**
 * 当精度丢失时 保留两位有效数字（字符串转浮点型 可能精度丢失）
 */
+(NSString *)formatternumber:(int)i assess:(NSString *)assess;
/*
  去掉字符串首位空格和换行
 */
+(NSString *)removeCharacter:(NSString *)str;
/*
 去掉字符串空格和换行
 */
+(NSString *)removeStrCharacter:(NSString *)str;
+ (NSString *)getCapitalPriceNoUnitWithName:(NSString *)name Price:(NSString *)Price;

/*
 @param 获取新数组进行排序，如果数组里有价格相同则移除这个数据或者是添加
 @param Array 获取的s新数据
 @param newArray 更新后的数组
 @return newArray
 */
+ (NSMutableArray *)getNewbidsArrayWithData:(NSArray *)Array withArray:(NSMutableArray *)newArray model:(HomeModle *)model;

+ (NSMutableArray *)getNewAsksArrayWithData:(NSArray *)Array withArray:(NSMutableArray *)newArray model:(HomeModle *)model;
/*
  通过站点获取app名称
 */
+ (NSString *)getSite;
/*
 银行卡格式
 */
+ (BOOL)judgebankNumberLegal:(NSString *)bankNumber;
/*
 @param 判断字符串中是否包含g空格
 @param str 需要传人的字符串
 @return yes or no
 */
+ (BOOL)isSpaceWithstr:(NSString *)str;

//获取当前时间
+ (NSString*)getCurrentTimes;

/**
 *  设置字体大小或类型
 *
 *  @param name 类型
 *  @param size 内容
 *
 *  @return 普通字符串
 */
+ (UIFont *)setFontName:(NSString *)name size:(CGFloat)size;
/*
 * 判断是什么语言
 */
+ (NSString *)isLocalizable;

/**
 *  提币地址限制
 *
 *  @param address 提币地址
 *  @return 是否合法
 */
+ (BOOL)judgeAddressLegal:(NSString *)address;

/**
 * 开始到结束的时间差是否在五天之类
 */
+ (NSString *)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime;

/**
 *  KCion 不能低于500
 *
 *  @param Price 购买的价格
 *  @return 是否成立
 */
+ (BOOL)KCionC2C500WithPrice:(NSString *)Price;

//获取其拼音
+ (NSString *)huoqushouzimuWithString:(NSString *)string;

//根据拼音的字母排序  ps：排序适用于所有类型
+ (NSMutableArray *)paixuWith:(NSMutableArray *)array;
// 降序
+ (NSMutableArray *)jiangPxuWith:(NSMutableArray *)array;
// 升序排序
+ (NSMutableArray *)shengxuWith:(NSMutableArray *)array;
// 降序排序
+ (NSMutableArray *)jiangxuWith:(NSMutableArray *)array;

//价格降序
+ (NSMutableArray *)pricejiangxuWith:(NSMutableArray *)array;
//价格升序
+ (NSMutableArray *)priceshengxuWith:(NSMutableArray *)array;

/**
 *  真确设置控件圆角
 *
 *  @param view 控件
 *  @param cornerRadius 圆角幅度
 *  @return 是否成立
 */
+ (CAShapeLayer *)ShapeLayerWithViewCGRect:(UIView *)view cornerRadius:(CGFloat)cornerRadius;

/*
 colors  渐变的颜色
 locations 颜色变化位置的取值范围
 startPoint  颜色渐变的起始位置:取值范围在(0,0)~(1,1)之间
 endPoint  颜色渐变的终点位置,取值范围也是在(0,0)~(1,1)之间
 补充下:startPoint & endPoint设置为(0,0)(1.0,0)代表水平方向渐变,(0,0)(0,1.0)代表竖直方向渐变
 
 */
+ (CAGradientLayer *)setGradualChangingColor:(UIView *)view fromColor:(NSString *)fromHexColorStr toColor:(NSString *)toHexColorStr;

+ (BOOL)CodeissSixplace:(NSString *)code;

/*判断两个数组是否有相同的元素*/
+ (BOOL)array:(NSArray *)array1 isEqualTo:(NSArray *)array2;


/**
 *  美元和人民币查看是否有相同的
 *
 *  @param modle modle
 *  @return 价格
 */
+ (NSString *)getPriceCurrencyWith:(HomeModle *)modle;

// 是否隐藏折合价格
+ (BOOL)isHideCurrencyPrice;

/*去除首尾空格：*/
+ (NSString *)removeSpecialWithText:(NSString *)text;


/*获取zt 价格单位 其他位¥*/
+ (NSString *)getZTPriceUnit;
@end
