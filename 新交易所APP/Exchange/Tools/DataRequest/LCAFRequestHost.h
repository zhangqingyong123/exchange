//
//  LoginViewController.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/21.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LCAFHttpRequestProtocol.h"

NS_ASSUME_NONNULL_BEGIN
/**
 *  @brief 基于AF的网络请求
 */
@interface LCAFRequestHost : NSObject

@property (nonatomic,strong) AFHTTPSessionManager *requestManager;

@property (nonatomic,strong) NSString *baseUrlStr;

DEFINE_SINGLETON_FOR_HEADER(LCAFRequestHost)

- (NSString *)baseUrl;

- (void)afRequestWithUrlString:(NSString *)urlString
                        method:(NSString *)method
                    parameters:(NSDictionary *)param
                         cache:(BOOL)isCache
                  successBlock:(LCSuccessBlock)success
                     failBlock:(LCFailBlock)fail;

- (void)enableCacheWithDirectory:(NSString *)cacheDirectoryPath
                    inMemoryCost:(NSUInteger)inMemoryCost;


@end

NS_ASSUME_NONNULL_END
