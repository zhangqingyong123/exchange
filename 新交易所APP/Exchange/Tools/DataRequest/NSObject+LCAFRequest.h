//
//  LoginViewController.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/21.
//  Copyright © 2018年 张庆勇. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LCAFHttpRequestProtocol.h"

NS_ASSUME_NONNULL_BEGIN
@interface NSObject (LCAFRequest)

- (void)POSTWithHost:(NSString *)host
                 path:(NSString *)path
                param:(NSDictionary *)param
                cache:(BOOL)isCache
            completed:(LCSuccessBlock)completed
                error:(LCFailBlock)errorBlock;

- (void)GETWithHost:(NSString *)host
               path:(NSString *)path
              param:(NSDictionary *)param
              cache:(BOOL)isCache
          completed:(LCSuccessBlock)completed
              error:(LCFailBlock)errorBlock;



//上传图片
- (void)upload:(NSString *)path parameters:(NSDictionary *)dic constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress success:(LCSuccessBlock)success failure:(LCFailBlock)failure;
-(void)UpLoad:(NSString*)url Data:(id)data dict:(id)dict succeed:(void (^)(NSInteger status,id data))succeed failure:(void (^)(NSError *error))failure show:(BOOL)showProgress;

//下载
- (void)downloadWithUrl:(NSString *)url
            destination:(nullable NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
      completionHandler:(nullable void (^)(NSURLResponse *response, NSURL * _Nullable filePath, NSError * _Nullable error))completionHandler;
@end
NS_ASSUME_NONNULL_END

