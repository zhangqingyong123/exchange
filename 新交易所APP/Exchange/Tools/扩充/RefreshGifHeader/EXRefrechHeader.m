//
//  EXRefrechHeader.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "EXRefrechHeader.h"

@implementation EXRefrechHeader
- (instancetype)init
{
    if (self = [super init]) {
        
        NSString * color = [[ZBLocalized sharedInstance]AppbBGColor];
        if (![color isEqualToString:WHITE_COLOR]) //app风格为默认色
        {
            self.stateLabel.textColor = [UIColor whiteColor];
            self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        }
        [self setTitle:Localized(@"MJRefreshHeaderIdleText") forState:MJRefreshStateIdle];
        [self setTitle:Localized(@"MJRefreshHeaderPullingText") forState:MJRefreshStatePulling];
        [self setTitle:Localized(@"MJRefreshHeaderRefreshingText") forState:MJRefreshStateRefreshing];
        self.lastUpdatedTimeLabel.hidden = YES;
       
    }
    return self;
}
- (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}
@end
@implementation EXRefrechFootview
- (instancetype)init
{
    if (self = [super init]) {
        NSString * color = [[ZBLocalized sharedInstance]AppbBGColor];
        if (![color isEqualToString:WHITE_COLOR]) //app风格为默认色
        {
            self.stateLabel.textColor = [UIColor whiteColor];
            
            self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
            
        }
        // 初始化文字
        [self setTitle:Localized(@"MJRefreshBackFooterIdleText") forState:MJRefreshStateIdle];
        [self setTitle:Localized(@"MJRefreshBackFooterPullingText") forState:MJRefreshStatePulling];
        [self setTitle:Localized(@"MJRefreshBackFooterRefreshingText") forState:MJRefreshStateRefreshing];
        [self setTitle:Localized(@"MJRefreshBackFooterNoMoreDataText") forState:MJRefreshStateNoMoreData];
      
    }
    return self;
}

@end
