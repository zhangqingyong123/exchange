//
//  WLRefreshGifHeader.m
//  SingleChat
//
//  Created by 李鹏飞 on 17/3/9.
//  Copyright © 2017年 weijieMac. All rights reserved.
//

#import "WLRefreshGifHeader.h"
#import "UIImage+GIF.h"
#import <ImageIO/ImageIO.h>     // 图像的输入输出文件
#import <MobileCoreServices/MobileCoreServices.h>

@implementation WLRefreshGifHeader

//- (instancetype)init
//{
//    if (self = [super init]) {
//        self.lastUpdatedTimeLabel.hidden = YES;
//        self.stateLabel.hidden  =   YES;
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"header" ofType:@"gif"];
//        NSData *data = [NSData dataWithContentsOfFile:path];
//        UIImage *image = [UIImage sd_animatedGIFWithData:data];
////        [self setImages:@[image] forState:MJRefreshStateIdle];
////        [self setImages:@[image] forState:MJRefreshStateRefreshing];
//
//        CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)data, NULL);
//
//        //2. 将gif分解为一帧帧
//        size_t count = CGImageSourceGetCount(source);
//        NSLog(@"%zu",count);
//
//        NSMutableArray * tmpArray = [NSMutableArray arrayWithCapacity:0];
//        for (int i = 0; i < count; i ++) {
//            CGImageRef imageRef = CGImageSourceCreateImageAtIndex(source, i, NULL);
//
//            //3. 将单帧数据转为UIImage
//            UIImage * image = [UIImage imageWithCGImage:imageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
//            [tmpArray addObject:image];
////#warning CG类型的对象 不能用ARC自动释放内存.需要手动释放
//            CGImageRelease(imageRef);
//        }
//        CFRelease(source);
//        // 创建一个数组来存放普通状态下的gif图片（就是在下拉过程中，还没有达到松手即刷新界限的时刻）
//        NSMutableArray *normalImages = [NSMutableArray array];
//        for (NSUInteger i = 1; i<= 1; i++) {
////            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Refresh－%ld", i]];
//            [normalImages addObject:image];
//        }
//
////        // 创建一个数组来存放将要刷新时需要展示的gif图片数组（就是松手即刷新的时刻）
////        NSMutableArray *refreshingImages = [NSMutableArray array];
////        for (NSUInteger i = 1; i<=2; i++) {
////            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Refresh－%ld", i]];
////            [refreshingImages addObject:image];
////        }
//
//        // 设置下拉过程中的图片数组
//        [self setImages:normalImages forState:MJRefreshStateIdle];
//        // 设置松手即刷新时的图片数组
//        [self setImages:normalImages forState:MJRefreshStatePulling];
//
//        // 设置正在刷新状态时的动画图片
////        [self setImages:tmpArray forState:MJRefreshStateRefreshing];
//        [self setImages:tmpArray duration:1 forState:MJRefreshStateRefreshing];
//
//    }
//    return self;
//}
#pragma mark - 重写父类的方法
- (void)prepare{

    [super prepare];

    // 创建一个数组来存放普通状态下的gif图片（就是在下拉过程中，还没有达到松手即刷新界限的时刻）
    NSMutableArray *normalImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<= 27; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"refresh_%ld", i]];
        [normalImages addObject:image];
    }

    // 创建一个数组来存放将要刷新时需要展示的gif图片数组（就是松手即刷新的时刻）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=27; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"refresh_%ld", i]];
        [refreshingImages addObject:image];
    }

    // 设置下拉过程中的图片数组
    [self setImages:normalImages forState:MJRefreshStateIdle];
    // 设置松手即刷新时的图片数组
    [self setImages:normalImages forState:MJRefreshStatePulling];
    // 设置正在刷新状态时的动画图片
    [self setImages:refreshingImages forState:MJRefreshStateRefreshing];
    
    

    //隐藏时间文字
    self.lastUpdatedTimeLabel.hidden = YES;
    //隐藏状态文字
    self.stateLabel.hidden = YES;
}
@end
