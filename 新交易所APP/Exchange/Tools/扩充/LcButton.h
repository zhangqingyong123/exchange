//
//  LcButton.h
//  ExampleObjective-C
//
//  Created by weijieMac on 2017/2/27.
//  Copyright © 2017年 ouy.Aberi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LcButton : UIButton

@property (strong, nonatomic) UIColor *originalBgColor;
//动画时间，默认为1秒
@property (nonatomic,assign) NSTimeInterval AnimationDuration;
//动画颜色
@property (nonatomic,strong) UIColor        *highlightedColor;
- (void)stopAnimation;

@end
