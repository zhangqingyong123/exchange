//
//  OTCTimeLable.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface OTCTimeLable : UILabel
@property (nonatomic,assign)NSString * expire_time;
@property (nonatomic,assign)NSInteger second;
@property (nonatomic,assign)NSInteger minute;
@property (nonatomic,assign)NSInteger hour;
@property (nonatomic,assign)NSInteger day;
@end

