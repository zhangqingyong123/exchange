//
//  TimeLable.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/4.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TimeLable : UILabel
@property (nonatomic,assign)NSInteger second;
@property (nonatomic,assign)NSInteger minute;
@property (nonatomic,assign)NSInteger hour;
@property (nonatomic,assign)NSInteger day;
@end

