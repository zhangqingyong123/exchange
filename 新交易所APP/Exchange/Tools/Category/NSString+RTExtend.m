//
//  NSString+RTExtend.m
//  MLearningUniversal_3.0
//
//  Created by Sharon_Ouyang on 16/1/7.
//  Copyright © 2016年 OuyangRenshuang. All rights reserved.
//

#import "NSString+RTExtend.h"

@implementation NSString (RTExtend)

@end

@implementation NSString (RTJudgement)
// 如果字符穿是空的使用@""作为占位符
+(NSString *)stanceString:(NSString *)string
{
    if ([NSString isEmptyString:string]) {
        return @"";
    }else
    {
        return string;
    
    }
  
}
+ (BOOL)isEmptyString:(NSString *)string
{
    if ([string isKindOfClass:[NSNull class]] ) {
        return YES;
    }
    if ([string isKindOfClass:[NSNumber class]]) {
        return NO;
    }
    if (!string || string.length == 0) {
        return YES;
    }
    if ([string isEqualToString:@" "]) {
        return YES;
    }

    if (string.length>1 && [[string substringToIndex:0] isEqualToString:@" "]) {
        return YES;
    }
    NSArray *array = [string componentsSeparatedByString:@" "];
    NSInteger lenghth = 0;
    for (NSString *substr in array) {
        lenghth+=substr.length;
        if (lenghth) {
            return NO;
        }
    }
    return !lenghth;
    return NO;
}

+ (BOOL)isEmailString:(NSString *)emailString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}

+ (BOOL)isPhoneNumberString:(NSString *)string
{
    NSString *aaa = @"^\\s*\\+?\\s*(\\(\\s*\\d+\\s*\\)|\\d+)(\\s*-?\\s*(\\(\\s*\\d+\\s*\\)|\\s*\\d+\\s*))*\\s*$";
    
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", aaa];
    if (([regextestct evaluateWithObject:string]
         )) {
        return YES;
    }
    return NO;
}

+ (NSString *)deleteNewline:(NSString *)string
{
    NSString *resultString = @"";
    for (int i = 0;i<string.length;i++){
        unichar c = [string characterAtIndex:i];
        if (c!='\n') {
            resultString = [resultString stringByAppendingString:[NSString stringWithFormat:@"%C",c]];
        }
    }
    
    return resultString;
}

+ (NSString *)deleteNewlineAndLineSpace:(NSString *)string {
    NSString *resultString = @"";
    for (int i = 0;i<string.length;i++){
        unichar c = [string characterAtIndex:i];
        if (c!='\n' && c!=' ') {
            resultString = [resultString stringByAppendingString:[NSString stringWithFormat:@"%C",c]];
        }
    }
    
    return resultString;
}

+ (NSString *)getImageUrlStrWithImageName:(NSString *)imageName
{
    NSString *str = [@"http://img.chatting365.cn/video/img/user/" stringByAppendingString:imageName];
    return str;
}
+(NSString *)TimeStamp:(NSString *)strTime

{
    
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"shanghai"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy年MM月dd日 HH:mm"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[strTime doubleValue]/ 1000.0];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
    
  
    
}

@end
