//
//  NSString+RTExtend.h
//  MLearningUniversal_3.0
//
//  Created by Sharon_Ouyang on 16/1/7.
//  Copyright © 2016年 OuyangRenshuang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RTExtend)

@end

@interface NSString (RTJudgement)
// 如果字符穿是空的使用@""作为占位符
+(NSString *)stanceString:(NSString *)string;
//判定是否为空
+ (BOOL)isEmptyString:(NSString *)string;

//判定是否为邮箱地址
+ (BOOL)isEmailString:(NSString *)emailString;

//判定是否为电话号码
+ (BOOL)isPhoneNumberString:(NSString *)string;

// 返回去除换行符的字符串
+ (NSString *)deleteNewline:(NSString *)string;

// 删除空格和换行
+ (NSString *)deleteNewlineAndLineSpace:(NSString *)string;

//根据文件名获取下载地址
+ (NSString *)getImageUrlStrWithImageName:(NSString *)imageName;
//转成时间戳
+ (NSString *)TimeStamp:(NSString *)strTime;
@end
