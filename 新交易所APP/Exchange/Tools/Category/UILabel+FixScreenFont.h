//
//  UILabel+FixScreenFont.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FixScreenFont)
@property (nonatomic)IBInspectable float fixWidthScreenFont;
@end
