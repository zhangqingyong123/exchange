//
//  UIButton+myFont.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "UIButton+myFont.h"
#define  C_WIDTH(WIDTH) WIDTH * [UIScreen mainScreen].bounds.size.width/375.0
@implementation UIButton (myFont)

- (void)setFixWidthScreenFont:(float)fixWidthScreenFont{
    
    if (fixWidthScreenFont > 0 ) {
        self.titleLabel.font = [UIFont systemFontOfSize:C_WIDTH(fixWidthScreenFont)];
    }else{
         self.titleLabel.font = self.titleLabel.font;
    }
}

- (float )fixWidthScreenFont{
    return self.fixWidthScreenFont;
}
@end
