//
//  OTCTimeLable.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTCTimeLable.h"
@interface OTCTimeLable ()
@property (nonatomic, strong)NSTimer *timer;
@end
@implementation OTCTimeLable
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSLog(@"initWithCoder");
        //        self.textAlignment = NSTextAlignmentCenter;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
    }
    return self;
}
- (void)timeHeadle{
    self.second--;
    if (self.second==-1) {
        self.second=59;
        self.minute--;
        if (self.minute==-1) {
            self.minute=59;
            self.hour--;
        }
    }
    if (self.day>0)
    {
        NSString * str = [NSString stringWithFormat:@"%ld天 %.2ld时%.2ld分%.2ld秒",(long)self.day,(long)self.hour,(long)self.minute,(long)self.second];
        self.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:Localized(@"OTC_pay_seller_within"),str] attributedString:str  color:[UIColor redColor] font:AutoFont(12)];
    }else
    {
        if (self.hour>0) {
             NSString * str = [NSString stringWithFormat:@"%.2ld时%.2ld分%.2ld秒",(long)self.hour,(long)self.minute,(long)self.second];
            self.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:Localized(@"OTC_pay_seller_within"),str] attributedString:str  color:[UIColor redColor] font:AutoFont(12)];
        }else if (self.hour==0) {
            NSString * str = [NSString stringWithFormat:@"%.2ld分%.2ld秒",(long)self.minute,(long)self.second];
            self.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:Localized(@"OTC_pay_seller_within"),str] attributedString:str  color:[UIColor redColor] font:AutoFont(12)];
        }else if (self.minute==0)
        {
            NSString * str = [NSString stringWithFormat:@"%.2ld秒",(long)self.second];
            self.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:Localized(@"OTC_pay_seller_within"),str] attributedString:str  color:[UIColor redColor] font:AutoFont(12)];
        }
    }
    
    
    if (self.second==0 && self.minute==0 && self.hour==0) {
        [self.timer invalidate];
        self.text = [NSString stringWithFormat:Localized(@"OTC_Order_overdue_time"),_expire_time];
        self.timer = nil;
    }
}
@end
