//
//  TimeLable.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/4.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "TimeLable.h"
@interface TimeLable ()
@property (nonatomic, strong)NSTimer *timer;
@end
@implementation TimeLable

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.textAlignment = NSTextAlignmentCenter;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSLog(@"initWithCoder");
//        self.textAlignment = NSTextAlignmentCenter;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeHeadle) userInfo:nil repeats:YES];
    }
    return self;
}
- (void)timeHeadle{
    
    self.second--;
    if (self.second==-1) {
        self.second=59;
        self.minute--;
        if (self.minute==-1) {
            self.minute=59;
            self.hour--;
        }
    }
    if (self.day>0)
    {
         self.text = [NSString stringWithFormat:@"%ld天 %.2ld时%.2ld分%.2ld秒",(long)self.day,(long)self.hour,(long)self.minute,(long)self.second];
    }else
    {
        if (self.hour>0) {
            self.text = [NSString stringWithFormat:@"%.2ld时%.2ld分%.2ld秒",(long)self.hour,(long)self.minute,(long)self.second];
        }else if (self.hour==0) {
            self.text = [NSString stringWithFormat:@"%.2ld分%.2ld秒",(long)self.minute,(long)self.second];
        }else if (self.minute==0)
        {
            self.text = [NSString stringWithFormat:@"%.2ld秒",(long)self.second];
        }
    }
   
    
    if (self.second==0 && self.minute==0 && self.hour==0) {
        [self.timer invalidate];
        self.text = Localized(@"Current_elegationendTime");
        self.timer = nil;
    }
}
@end
