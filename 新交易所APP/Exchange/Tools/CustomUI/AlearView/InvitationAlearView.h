//
//  InvitationAlearView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger , LXAShowAnimationStyle) {
    LXASAnimationDefault    = 0,
    LXASAnimationLeftShake  ,
    LXASAnimationTopShake   ,
    LXASAnimationNO         ,
};
typedef void(^SaveBlock)(void);
@interface InvitationAlearView : UIView
@property (nonatomic,assign)LXAShowAnimationStyle animationStyle;
@property (nonatomic,copy)SaveBlock saveBlock;

/**
 保存相片到本地
 @param imageurl 跳转的URL

 **/
- (instancetype)initWithimageurl:(NSString *)imageurl
                        titleStr:(NSString *)titleStr
                    QRcodWithUrl:(NSString *)QRcodWithUrl
                       SaveBlock:(SaveBlock)saveBlock;
-(void)showLXAlertView;
@end
