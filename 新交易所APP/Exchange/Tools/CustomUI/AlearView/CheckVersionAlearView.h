//
//  CheckVersionAlearView.h
//  Calculated
//
//  Created by 张庆勇 on 2018/5/21.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger , LXAShowAnimationStyle) {
    LXASAnimationDefault    = 0,
    LXASAnimationLeftShake  ,
    LXASAnimationTopShake   ,
    LXASAnimationNO         ,
};
typedef void(^RechargeBlock)(void);
@interface CheckVersionAlearView : UIView
@property (nonatomic,strong)NSString * qrcodeUrl;
@property (nonatomic,assign)LXAShowAnimationStyle animationStyle;
@property (nonatomic,copy)RechargeBlock recharBlock;
/**
 自定义检查版本更新
 @param message 内容
 @param url 跳转的URL
 @param confirm 确定
 @param cancel 取消
 @param state 1,强制更新 2，介意更新 3，不需更新
**/
- (instancetype)initWithmessage:(NSString *)message
                       titleStr:(NSString *)titleStr
                        openUrl:(NSString *)url
                        confirm:(NSString *)confirm
                         cancel:(NSString *)cancel
                          state:(NSInteger )state
                  RechargeBlock:(RechargeBlock)block;
-(void)getViewHeight;
-(void)showLXAlertView;
-(void)cancelclick;
@end
