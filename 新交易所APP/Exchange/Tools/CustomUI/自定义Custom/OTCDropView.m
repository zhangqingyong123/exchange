//
//  OTCDropView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTCDropView.h"
@interface OTCDropView ()<UITextFieldDelegate>
@end
@implementation OTCDropView
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.capitalField resignFirstResponder];
    
    [UIView animateWithDuration:0.325 animations:^{
        
        self.frame = CGRectMake(0,  UIScreenHeight - self.height, self.width, self.height);
        
        
    }];
}
- (instancetype)init
{
    if (self = [super init]) {
        self.backgroundColor =[ZBLocalized sharedInstance].CustomBgViewColor;
        self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(466), UIScreenWidth, RealValue_W(466));
        
        [self initCustonWithTitile7:@[Localized(@"capital_password_message")] numbers:@[Localized(@"Security_Capital_cipher")] buttons:@[@""]];
        RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
            return @(value.length>5);
        }];
        
        WeakSelf
        [capital subscribeNext:^(NSNumber* x) {
            if ([x boolValue]) {
                weakSelf.okButton.enabled=YES;
                weakSelf.okButton.alpha = 1;
                weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
            }else{
                weakSelf.okButton.enabled=NO;
                weakSelf.okButton.alpha = 1;
                weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
            }
        }];
        [self addSubview:self.titlelable];
        [self addSubview:self.cancelButton];
        [self addSubview:self.okButton];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
        
    }
    return self;
}

- (void)initCustonWithTitile7:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+22), 120, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        
        
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        
        if (i ==0) {
            _capitalField = textField;
            _capitalField.secureTextEntry = YES;
        }
       
        [self addSubview:textField];
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        [self addSubview:bgView];
    }
    
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(0, RealValue_W(38), 200, RealValue_W(34))];
        _titlelable.text = Localized(@"Security_verification");
        _titlelable.font = AutoBoldFont(16);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.textAlignment = NSTextAlignmentCenter;
        _titlelable.centerX = self.centerX;
    }
    return _titlelable;
}
- (UIButton *)cancelButton
{
    WeakSelf
    if (!_cancelButton) {
        _cancelButton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(self.width - 10 -60, 10, 60, 40) title:Localized(@"transaction_cancel") backgroundColor:CLEARCOLOR titleColor:MAINTITLECOLOR tapAction:^(UIButton *button) {
            if (weakSelf.cancelBlock) {
                weakSelf.cancelBlock();
            }
        }];
        _cancelButton.titleLabel.font = AutoFont(14);
        _cancelButton.centerY = _titlelable.centerY;
    }
    return _cancelButton;
}
- (LcButton *)okButton
{
    
    if (!_okButton) {
        _okButton = [[LcButton alloc] initWithFrame:CGRectMake(RealValue_W(60), _capitalField.bottom + RealValue_H(56), self.width -RealValue_W(120), RealValue_W(80))];
        [_okButton setTitle:Localized(@"transaction_OK") forState:0];
        _okButton.backgroundColor = TABTITLECOLOR;
        [_okButton setTitleColor:WHITECOLOR forState:0];
        KViewRadius(_okButton, 2);
    }
    return _okButton;
}
@end
