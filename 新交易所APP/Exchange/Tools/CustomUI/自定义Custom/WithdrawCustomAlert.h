//
//  WithdrawCustomAlert.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
/*  重置提现密码 =11  */

#import "QVCustomAlertContentView.h"

@interface WithdrawCustomAlert : QVCustomAlertContentView
@property(nonatomic,strong)UIButton *cancelButton;
@property(nonatomic,strong)LcButton *okButton;
@property(nonatomic,strong)UITextField *capitalField;
@property(nonatomic,strong)UITextField *phoneField;
@property(nonatomic,strong)UITextField *emailField;
@property(nonatomic,strong)UITextField *gooldField;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)NSString *type;//验证码用途
@property(copy,nonatomic)void(^cancelBlock)(void); //退出
@end
