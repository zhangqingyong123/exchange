//
//  BindCustonAlert.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
/*
 
 0 |
 [int]
 |
 默认值 |    注册
 1 |
 [int]
 |
 |    登录
 2 |
 [int]
 |
 |    忘记密码
 3 |
 [int]
 |
 |    创建两步验证
 4 |
 [int]
 |
 |    绑定手机
 5 |
 [int]
 |
 |    绑定邮箱
 6 |
 [int]
 |
 |    创建提现密码
 7 |
 [int]
 |
 |    提现
 8 |
 [int]
 |
 |    关闭手机验证
 9 |
 [int]
 |
 |    关闭邮箱验证
 10 |
 [int]
 |
 |    关闭两步验证
 11 |
 [int]
 |
 |    重置提现密码
 12 |
 [int]
 |
 |    绑定银行卡
 13 |
 [int]
 |
 |    重新绑定银行卡

 */

#import "QVCustomAlertContentView.h"
#import "LcButton.h"
@interface BindCustonAlert : QVCustomAlertContentView
@property(nonatomic,strong)UIButton *cancelButton;
@property(nonatomic,strong)LcButton *okButton;
@property(nonatomic,strong)UITextField *phoneField;
@property(nonatomic,strong)UITextField *emailField;
@property(nonatomic,strong)UITextField *gooldField;
@property(nonatomic,strong)UIView *lineView;
@property(nonatomic,strong)NSString *type;//验证码用途
@property(nonatomic,assign)BOOL isBindGoole;//验证码用途
@property(copy,nonatomic)void(^cancelBlock)(void); //退出

@end
