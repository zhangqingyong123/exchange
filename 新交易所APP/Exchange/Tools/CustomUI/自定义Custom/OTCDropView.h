//
//  OTCDropView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QVCustomAlertContentView.h"
@interface OTCDropView : QVCustomAlertContentView
@property(nonatomic,strong)UIButton *cancelButton;
@property(nonatomic,strong)LcButton *okButton;
@property(nonatomic,strong)UITextField *capitalField;
@property(nonatomic,strong)UILabel *titlelable;
@property(copy,nonatomic)void(^cancelBlock)(void); //退出
@end
