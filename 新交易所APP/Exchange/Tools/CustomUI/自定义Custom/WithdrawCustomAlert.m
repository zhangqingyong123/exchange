//
//  WithdrawCustomAlert.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "WithdrawCustomAlert.h"
#import "QCCountdownButton.h"
@interface WithdrawCustomAlert ()<UITextFieldDelegate>
@property(nonatomic,strong)NSArray *titles;
@property(nonatomic,strong)UILabel *titlelable;
@property (nonatomic,retain)QCCountdownButton * VerificationCodebtn;

@end
@implementation WithdrawCustomAlert
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneField resignFirstResponder];
    
    [UIView animateWithDuration:0.325 animations:^{
        
        self.frame = CGRectMake(0,  UIScreenHeight - self.height, self.width, self.height);
    }];
}
- (instancetype)init
{
    if (self = [super init]) {
        self.backgroundColor =[ZBLocalized sharedInstance].CustomBgViewColor;
     
        if ([EXUserManager personalData].withdraw_password_set && [EXUnit isEmailBind] && [EXUnit isPhoneBind] && [EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(989), UIScreenWidth, RealValue_W(989));
            _titles = @[Localized(@"capital_password_message"),Localized(@"Security_phone_code_message"),Localized(@"mailbox_verification_code"),Localized(@"Security_Google_code")];
            
            [self initCustonWithTitile:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].phone,[EXUserManager personalData].email,Localized(@"Security_Google_code_message")] buttons:@[@"",Localized(@"register_phone_verification_code"),Localized(@"register_get_Mailbox_verification_code"),@""]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*phone=[_phoneField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*email=[_emailField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*goole=[_gooldField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,phone,email,goole] reduce:^id(NSNumber*capitalField,NSNumber*oldPWField,NSNumber*newsPWSignal,NSNumber*angeinPWSignal){
                return @([capitalField boolValue]&&[oldPWField boolValue]&&[newsPWSignal boolValue] &&[angeinPWSignal boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
            
        }else if ([EXUserManager personalData].withdraw_password_set &&[EXUnit isEmailBind] && [EXUnit isPhoneBind] && ![EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(862), UIScreenWidth, RealValue_W(862));
            _titles = @[Localized(@"capital_password_message"),Localized(@"Security_phone_code_message"),Localized(@"mailbox_verification_code")];
            [self initCustonWithTitile1:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].phone,[EXUserManager personalData].email] buttons:@[@"",Localized(@"register_phone_verification_code"),Localized(@"register_get_Mailbox_verification_code")]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*phone=[_phoneField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*email=[_emailField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,phone,email] reduce:^id(NSNumber*capitalField,NSNumber*oldPWField,NSNumber*newsPWSignal){
                return @([capitalField boolValue]&&[oldPWField boolValue]&&[newsPWSignal boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set &&[EXUnit isEmailBind]&& ![EXUnit isPhoneBind]  && [EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(862), UIScreenWidth, RealValue_W(862));
            _titles = @[Localized(@"capital_password_message"),Localized(@"mailbox_verification_code"),Localized(@"Security_Google_code")];
            
            [self initCustonWithTitile2:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].email,Localized(@"Security_Google_code_message")] buttons:@[@"",Localized(@"register_get_Mailbox_verification_code"),@""]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*email=[_emailField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*goole=[_gooldField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,email,goole] reduce:^id(NSNumber*capitalField,NSNumber*emailVale,NSNumber*goolevale){
                return @([capitalField boolValue]&&[emailVale boolValue]&&[goolevale boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set &&![EXUnit isEmailBind] && [EXUnit isPhoneBind] && [EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(862), UIScreenWidth, RealValue_W(862));
            _titles = @[Localized(@"capital_password_message"),Localized(@"Security_phone_code_message"),Localized(@"Security_Google_code")];
            [self initCustonWithTitile3:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].phone,Localized(@"Security_Google_code_message")] buttons:@[@"",Localized(@"register_phone_verification_code"),@""]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*phone=[_phoneField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*goole=[_gooldField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>3);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,phone,goole] reduce:^id(NSNumber*Vale,NSNumber*Vale1,NSNumber*vale2){
                return @([Vale boolValue]&&[Vale1 boolValue]&&[vale2 boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set &&![EXUnit isEmailBind] && ![EXUnit isPhoneBind] && [EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(652), UIScreenWidth, RealValue_W(652));
            _titles = @[Localized(@"capital_password_message"),Localized(@"Security_Google_code")];
            [self initCustonWithTitile4:_titles numbers:@[Localized(@"Security_Capital_cipher"),Localized(@"Security_Google_code_message")] buttons:@[@"",@""]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*goole=[_gooldField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,goole] reduce:^id(NSNumber*capitalField,NSNumber*goolevale){
                return @([capitalField boolValue]&&[goolevale boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set &&![EXUnit isEmailBind] && [EXUnit isPhoneBind] && ![EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(652-40), UIScreenWidth, RealValue_W(652)-40);
            _titles = @[Localized(@"capital_password_message"),Localized(@"Security_phone_code_message")];
            [self initCustonWithTitile5:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].phone] buttons:@[@"",Localized(@"register_phone_verification_code")]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*phone=[_phoneField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,phone] reduce:^id(NSNumber*capitalField,NSNumber*phoneevale){
                return @([capitalField boolValue]&&[phoneevale boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set &&[EXUnit isEmailBind] && ![EXUnit isPhoneBind] && ![EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(652), UIScreenWidth, RealValue_W(652));
            _titles = @[Localized(@"capital_password_message"),Localized(@"mailbox_verification_code")];
            [self initCustonWithTitile6:_titles numbers:@[Localized(@"Security_Capital_cipher"),[EXUserManager personalData].email] buttons:@[@"",Localized(@"register_get_Mailbox_verification_code")]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*email=[_emailField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital,email] reduce:^id(NSNumber*capitalField,NSNumber*emailvale){
                return @([capitalField boolValue]&&[emailvale boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }else if ([EXUserManager personalData].withdraw_password_set && ![EXUnit isEmailBind] && ![EXUnit isPhoneBind] && ![EXUnit isGooleBind])
        {
            self.frame = CGRectMake(0, UIScreenHeight - RealValue_H(466-40), UIScreenWidth, RealValue_W(466-40));
            _titles = @[Localized(@"capital_password_message")];
            [self initCustonWithTitile7:_titles numbers:@[Localized(@"Security_Capital_cipher")] buttons:@[@""]];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            
            WeakSelf
            [capital subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.okButton.enabled=YES;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.okButton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.okButton.enabled=NO;
                    weakSelf.okButton.alpha = 1;
                    weakSelf.okButton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.okButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }
        
        [self addSubview:self.titlelable];
        [self addSubview:self.cancelButton];
        [self addSubview:self.okButton];
        
        NSMutableArray * array = [[NSMutableArray alloc] init];
        if ([EXUnit isEmailBind]) {
            [array addObject:Localized(@"Google_email_message")];
        }
        if ([EXUnit isGooleBind])
        {
            [array addObject:[AppDelegate shareAppdelegate].emailSite];
        }
        CGFloat gooldlable_H;
        if ([[EXUnit  isLocalizable] isEqualToString:@"ja"]) {
            gooldlable_H = 20;
        }else
        {
            gooldlable_H = RealValue_W(28);
        }
        gooldlable_H =  array.count * gooldlable_H;
        self.height = self.height + gooldlable_H;
        for (int i =0; i<array.count; i++)
        {
            
            UILabel * gooldlable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60) +10, _okButton.bottom +RealValue_H(30) + i * gooldlable_H, UIScreenWidth - RealValue_W(120) -10, gooldlable_H -2)];
            gooldlable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
            gooldlable.text = array[i];
            gooldlable.numberOfLines = 0;
            gooldlable.font = AutoFont(10);
            [self addSubview:gooldlable];
            UIView * view = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), 0, RealValue_H(10), RealValue_H(10))];
            view.backgroundColor = TABTITLECOLOR;
            view.centerY = gooldlable.centerY;
            KViewRadius(view, RealValue_H(10)/2);
            [self addSubview:view];
        }
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
    }
    return self;
}
- (void)initCustonWithTitile:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W((60)), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
    
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
     
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(236),
                                                lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
       
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
            VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        VerificationCodebtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        [VerificationCodebtn addTarget:self action:@selector(startTime:) forControlEvents:UIControlEventTouchUpInside];
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
     
        [self addSubview:VerificationCodebtn];
        
        
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else if (i ==1)
        {
            _phoneField = textField;
             VerificationCodebtn.hidden = NO;
        }else if (i==2)
        {
            _emailField = textField;
             VerificationCodebtn.hidden = NO;
        }else if (i ==3)
        {
            _gooldField = textField;
             VerificationCodebtn.hidden = YES;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
        
    }
}
- (void)initCustonWithTitile1:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        
    
        
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
       
        
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(226),
                                               lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
       
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
             VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
             VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        VerificationCodebtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        [VerificationCodebtn addTarget:self action:@selector(startTime:) forControlEvents:UIControlEventTouchUpInside];
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
        [self addSubview:VerificationCodebtn];
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else if (i ==1)
        {
            _phoneField = textField;
            VerificationCodebtn.hidden = YES;
        }else if (i==2)
        {
            _emailField = textField;
            VerificationCodebtn.hidden = NO;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
        
    }
}
- (void)initCustonWithTitile2:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
     
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(226),
                                               lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
        
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
           VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
            VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        [VerificationCodebtn addTarget:self action:@selector(startTime1:) forControlEvents:UIControlEventTouchUpInside];
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
        [self addSubview:VerificationCodebtn];
        
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else if (i ==1)
        {
            _emailField = textField;
             VerificationCodebtn.hidden = NO;
        }else if (i ==2)
        {
            _gooldField = textField;
             VerificationCodebtn.hidden = YES;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
        
    }
}
- (void)initCustonWithTitile3:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
      
       
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(226),
                                               lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
        
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
            VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        VerificationCodebtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        [VerificationCodebtn addTarget:self action:@selector(startTime2:) forControlEvents:UIControlEventTouchUpInside];
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
        [self addSubview:VerificationCodebtn];
        
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else if (i ==1)
        {
            _phoneField = textField;
             VerificationCodebtn.hidden = NO;
        }else
        {
            _gooldField = textField;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
        
    }
}
- (void)initCustonWithTitile4:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 120, RealValue_W(28))];
        lable.textColor = MAINTITLECOLOR1;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                                lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
        if (i ==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
        }else
        {
            _gooldField = textField;
        }
        [self addSubview:textField];
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
    }
}
- (void)initCustonWithTitile5:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                                lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
       
        
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(226),
                                                lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
        
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
           VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        VerificationCodebtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        [VerificationCodebtn addTarget:self action:@selector(startTime2:) forControlEvents:UIControlEventTouchUpInside];
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
 
        [self addSubview:VerificationCodebtn];
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(self.width - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else
        {
            _phoneField = textField;
            VerificationCodebtn.hidden = NO;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
        
    }
}
- (void)initCustonWithTitile6:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+10), 160, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        

        
        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
        [self addSubview:textField];
        
        
        QCCountdownButton * VerificationCodebtn = [QCCountdownButton countdownButton];
        VerificationCodebtn.title = buttons[i];
        VerificationCodebtn.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        VerificationCodebtn.frame = CGRectMake(UIScreenWidth - RealValue_W(60) -  RealValue_W(226),
                                               lable.bottom+RealValue_W(40),
                                               RealValue_W(220),
                                               RealValue_H(60));
        
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
             VerificationCodebtn.titleLabel.font = AutoFont(11);
        }else
        {
            VerificationCodebtn.titleLabel.font = AutoFont(13);
        }
        [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
        VerificationCodebtn.tag = i;
        // 倒计时的时长
        VerificationCodebtn.totalSecond = 60;
        VerificationCodebtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [VerificationCodebtn addTarget:self action:@selector(startTime1:) forControlEvents:UIControlEventTouchUpInside];
        //进度b
        [VerificationCodebtn processBlock:^(NSUInteger second)
         {
             [VerificationCodebtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
             VerificationCodebtn.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
         } onFinishedBlock:^{  // 倒计时完毕
             [VerificationCodebtn setTitleColor:TABTITLECOLOR forState:UIControlStateNormal];
             VerificationCodebtn.title =Localized(@"Regain_validation_code");
         }];
  
        [self addSubview:VerificationCodebtn];
        
        if (i==0) {
            _capitalField = textField;
            _capitalField.keyboardType = UIKeyboardTypeNamePhonePad;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(self.width - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            
            button.centerY = textField.centerY;
            [self addSubview:button];
            VerificationCodebtn.hidden = YES;
        }else
        {
            _emailField = textField;
            VerificationCodebtn.hidden = NO;
        }
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
        
    }
}
- (void)initCustonWithTitile7:(NSArray *)titles numbers:(NSArray *)numbers buttons:(NSArray *)buttons
{
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130) + i*RealValue_W(161+22), 120, RealValue_W(28))];
        lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        lable.text = numbers[i];
        lable.font = AutoFont(13);
        [self addSubview:lable];
        

        
        UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               lable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        textField.textAlignment=NSTextAlignmentLeft;
        textField.backgroundColor = [UIColor clearColor];
        textField.placeholder = titles[i];
        textField.delegate = self;
        textField.tintColor = TABTITLECOLOR;
        [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [textField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(11);
        }else
        {
            [textField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            textField.font = AutoBoldFont(13);
        }
        textField.textColor = MAINTITLECOLOR;
        textField.keyboardType = UIKeyboardTypeNumberPad;
       
        if (i ==0) {
            _capitalField = textField;
            _capitalField.secureTextEntry = YES;
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(60) -40, 0, 40, 40)];
            [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
            [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
            [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = i;
            button.centerY = textField.centerY;
            [self addSubview:button];
        }
        [self addSubview:textField];
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), textField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        _lineView = bgView;
        [self addSubview:bgView];
    }
}
- (void)openPassword:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.capitalField.secureTextEntry = !self.capitalField.secureTextEntry;
    
    
}
- (void)startTime:(QCCountdownButton *)VerificationCodebtn
{
    if (VerificationCodebtn.tag ==1) {//手机验证码
    
        [self sendSMSWithusertype:@"0" button:VerificationCodebtn];
    }else if (VerificationCodebtn.tag ==2) //邮箱验证码
    {
       

        [self sendSMSWithusertype:@"1" button:VerificationCodebtn];
    }
}
- (void)startTime1:(QCCountdownButton *)VerificationCodebtn
{
    if (VerificationCodebtn.tag ==1) {//邮箱验证码
        
       
        [self sendSMSWithusertype:@"1" button:VerificationCodebtn];
    }
}
- (void)startTime2:(QCCountdownButton *)VerificationCodebtn
{
    if (VerificationCodebtn.tag ==1) {//手机验证码
        
       
        [self sendSMSWithusertype:@"0" button:VerificationCodebtn];
    }
}
- (void)sendSMSWithusertype:(NSString *)usertype button:(QCCountdownButton *)button
{
     [button startTime];
    [self POSTWithHost:@"" path:userSMS param:@{@"type":usertype,@"use_type":_type,@"phone":@"",@"email":@"",@"country_id":@""} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
        }else
        {
            
             [EXUnit showMessage:responseObject[@"message"]];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(0, RealValue_W(38), 200, RealValue_W(34))];
        _titlelable.text = Localized(@"Security_verification");
        _titlelable.font = AutoBoldFont(16);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.textAlignment = NSTextAlignmentCenter;
        _titlelable.centerX = self.centerX;
    }
    return _titlelable;
}
- (UIButton *)cancelButton
{
    WeakSelf
    if (!_cancelButton) {
        _cancelButton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(self.width - 10 -60, 0, 60, 40) title:Localized(@"transaction_cancel") backgroundColor:CLEARCOLOR titleColor:MAINTITLECOLOR tapAction:^(UIButton *button) {
            if (weakSelf.cancelBlock) {
                weakSelf.cancelBlock();
            }
        }];
        _cancelButton.titleLabel.font = AutoFont(14);
        _cancelButton.centerY = _titlelable.centerY;
    }
    return _cancelButton;
}
- (LcButton *)okButton
{
    
    if (!_okButton) {
        _okButton = [[LcButton alloc] initWithFrame:CGRectMake(RealValue_W(60), _lineView.bottom + RealValue_H(46), self.width -RealValue_W(120), RealValue_W(80))];
        [_okButton setTitle:Localized(@"transaction_OK") forState:0];
        _okButton.backgroundColor = TABTITLECOLOR;
        [_okButton setTitleColor:WHITECOLOR forState:0];
        KViewRadius(_okButton, 2);
    }
    return _okButton;
}

@end
