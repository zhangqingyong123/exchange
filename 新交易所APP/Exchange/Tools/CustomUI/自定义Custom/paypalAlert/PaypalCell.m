//
//  CountryCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//
#import "PaypalCell.h"
@implementation PaypalCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
        [self.contentView addSubview:self.hideButton];
        [self.contentView addSubview:self.icon];
        [self.contentView addSubview:self.titlelable];
    }
    return self;
}
- (void)setModle:(PaypalModel *)modle
{
    _icon.image = [UIImage imageNamed:modle.icon];
    _titlelable.text = modle.title;
}
- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(70, 50/2 - RealValue_W(38)/2, RealValue_W(38), RealValue_W(38))];
    }
    return _icon;
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(_icon.right +10, 0, 240, 50)];
        _titlelable.font = AutoBoldFont(15);
        _titlelable.textColor = MAINTITLECOLOR;
    }
    return _titlelable;
}
- (UIButton *)hideButton
{
    if (!_hideButton) {
        _hideButton = [[UIButton alloc] initWithFrame:CGRectMake(30, 10, 30, 30)];
        [_hideButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].choosecloseNormalImage] forState:UIControlStateNormal];
        [_hideButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].chooseOpenNormalImage] forState:UIControlStateSelected];
    }
    return _hideButton;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
