//
//  CountryCustomAlert.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "PaypalCustomAlert.h"
#import "PaypalCell.h"
#import "PaypalModel.h"
@interface PaypalCustomAlert ()<UITextFieldDelegate>
@property (nonatomic,strong)UILabel * titleLable;
@property (nonatomic,strong)UIButton * cancelbutton;
@property (nonatomic,strong)UILabel * capitaltitleLable;

@property (nonatomic,strong)UILabel * choosePaypalLable;
@property (nonatomic,strong)UITableView * tableview;

@property (nonatomic,assign)BOOL isSell;
@property (nonatomic,strong)NSArray * dataArray;

@end
@implementation PaypalCustomAlert
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
  
    [UIView animateWithDuration:0.325 animations:^{
        
        self.frame = CGRectMake(0,  UIScreenHeight - self.height, self.width, self.height);
    }];
}

- (instancetype)initWithCustomContentDataArray:(NSMutableArray *)dataArray isSell:(BOOL)isSell
{
    if (self = [super init]) {
        _dataArray = dataArray;
        _isSell = isSell;
        CGFloat viewHeight;
        if (_isSell) {
            
            viewHeight = RealValue_H(850)- (3 - dataArray.count)*50;
        }else
        {
            viewHeight = RealValue_H(850)- (3 - dataArray.count)*50 - RealValue_H(200);
        }
        self.frame = CGRectMake(0, UIScreenHeight - viewHeight, UIScreenWidth, viewHeight);
        self.backgroundColor =[ZBLocalized sharedInstance].CustomBgViewColor;
        [self addSubview:self.titlelable];
        [self addSubview:self.cancelbutton];
        [self addSubview:self.OKbutton];
        
        if (_isSell) {
            [self addSubview:self.capitaltitleLable];
            [self addSubview:self.capitalField];
            RACSignal*capital=[_capitalField.rac_textSignal map:^id(NSString* value) {
                return @(value.length>5);
            }];
            RACSignal*loginSignal=[RACSignal combineLatest:@[capital] reduce:^id(NSNumber*capitalSignal){
                return @([capitalSignal boolValue]);
            }];
            WeakSelf
            [loginSignal subscribeNext:^(NSNumber* x) {
                if ([x boolValue]) {
                    weakSelf.OKbutton.enabled=YES;
                    weakSelf.OKbutton.alpha = 1;
                    weakSelf.OKbutton.backgroundColor = TABTITLECOLOR;
                    [weakSelf.OKbutton setTitleColor:[UIColor whiteColor] forState:0];
                }else{
                    weakSelf.OKbutton.enabled=NO;
                    weakSelf.OKbutton.alpha = 1;
                    weakSelf.OKbutton.backgroundColor = [ZBLocalized sharedInstance].buttonNormalStatus;
                    [weakSelf.OKbutton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
                }
            }];
        }
        [self addSubview:self.tableview];
        
       
       
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
        
       
    }
    return self;
}
- (UILabel *)titlelable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, RealValue_W(38), 200, RealValue_W(34))];
        if (_isSell) {
             _titleLable.text = Localized(@"Security_Password_funds");
        }else
        {
            _titleLable.text = Localized(@"C2C_Payment_method");
        }
       
        _titleLable.font = AutoBoldFont(16);
        _titleLable.textColor = MAINTITLECOLOR;
        _titleLable.textAlignment = NSTextAlignmentCenter;
        _titleLable.centerX = self.centerX;
    }
    return _titleLable;
}
- (UIButton *)cancelbutton
{
    WeakSelf
    if (!_cancelbutton) {
        _cancelbutton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(self.width - 10 -60, 0, 60, 40) title:Localized(@"transaction_cancel") backgroundColor:CLEARCOLOR titleColor:MAINTITLECOLOR1 tapAction:^(UIButton *button) {
            
            if (weakSelf.cancelBlock) {
                weakSelf.cancelBlock();
            }
        }];
        _cancelbutton.titleLabel.font = AutoFont(14);
        _cancelbutton.centerY = _titleLable.centerY;
    }
    return _cancelbutton;
}
- (UILabel *)capitaltitleLable
{
    if (!_capitaltitleLable) {
        _capitaltitleLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), RealValue_H(130), 120, RealValue_W(28))];
        _capitaltitleLable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
        _capitaltitleLable.text = Localized(@"Security_Capital_cipher");
        _capitaltitleLable.font = AutoFont(13);
    }
    return  _capitaltitleLable;
}
- (UITextField *)capitalField
{
    if (!_capitalField) {
        _capitalField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(60),
                                                                               _capitaltitleLable.bottom+RealValue_W(40),
                                                                               RealValue_W(500),
                                                                               RealValue_W(60))];
        _capitalField.textAlignment=NSTextAlignmentLeft;
        _capitalField.backgroundColor = [UIColor clearColor];
        _capitalField.placeholder = Localized(@"capital_password_message");
        _capitalField.delegate = self;
        _capitalField.tintColor = TABTITLECOLOR;
        [_capitalField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
        {
            [_capitalField setValue:AutoFont(11)forKeyPath:@"_placeholderLabel.font"];
            _capitalField.font = AutoBoldFont(11);
        }else
        {
            [_capitalField setValue:AutoFont(13)forKeyPath:@"_placeholderLabel.font"];
            _capitalField.font = AutoBoldFont(13);
        }
        _capitalField.textColor = MAINTITLECOLOR;
        _capitalField.secureTextEntry = YES;
        [self addSubview:_capitalField];
        UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(60), _capitalField.bottom+1, UIScreenWidth - RealValue_W(120), 0.4f)];
        bgView.backgroundColor = CELLCOLOR;
        [self addSubview:bgView];
    }
    return _capitalField;
}
- (UITableView *)tableview
{
    if (!_tableview) {
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, _capitalField.bottom + RealValue_H(48), UIScreenWidth, RealValue_H(370)- (3 - self.dataArray.count)*50)];
        if (!_isSell) {
             _tableview.mj_y = RealValue_H(130);
        }
        _tableview.tableFooterView = [UIView new];
        _tableview.separatorColor = [UIColor clearColor];
        _tableview.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
        _tableview.delegate = self;
        _tableview.dataSource=self;
        _tableview.showsVerticalScrollIndicator = NO;
        [_tableview registerClass:[PaypalCell class] forCellReuseIdentifier:@"PaypalCell"];
 
    }
    return _tableview;
}
- (LcButton *)OKbutton
{
    if (!_OKbutton) {
        _OKbutton = [[LcButton alloc] initWithFrame:CGRectMake(RealValue_W(60), self.height - RealValue_H(62) - RealValue_H(78), self.width -RealValue_W(120), RealValue_H(78))];
        [_OKbutton setTitle:Localized(@"transaction_OK") forState:0];
        _OKbutton.backgroundColor = TABTITLECOLOR;
        [_OKbutton setTitleColor:WHITECOLOR forState:0];
        KViewRadius(_OKbutton, 2);
        [_OKbutton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _OKbutton;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_H(70);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaypalCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"PaypalCell"];
    cell.modle = _dataArray[indexPath.row];
    if (indexPath.row ==_index) {
        cell.hideButton.selected = YES;
    }else
    {
        cell.hideButton.selected = NO;
    }
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(70))];
    view.backgroundColor = [ZBLocalized sharedInstance].CustomBgViewColor;
    UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), 0, 200, 25)];
    lable.textColor = [ZBLocalized sharedInstance].C2CTitleTextColor;
    lable.text = Localized(@"OTC_choose_paypal");
    lable.font = AutoFont(13);
    [view addSubview:lable];
    return view;
}
- (void)buttonClick:(LcButton *)button
{
    if (self.selectIndexPathRow) {
        self.selectIndexPathRow(_index, _capitalField.text);
    }

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _index = indexPath.row;
    [_tableview reloadData];
}
@end
