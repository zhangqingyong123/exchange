//
//  CountryCustomAlert.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "QVCustomAlertContentView.h"
@interface PaypalCustomAlert : QVCustomAlertContentView<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)LcButton * OKbutton;
@property (nonatomic,strong)UITextField * capitalField;
@property (nonatomic,assign)NSInteger  index;
@property(copy,nonatomic)void(^cancelBlock)(void); //退出
@property(copy,nonatomic)void(^selectIndexPathRow)(NSInteger index,NSString * capital); //退出
- (instancetype)initWithCustomContentDataArray:(NSMutableArray *)dataArray isSell:(BOOL)isSell;
@end

