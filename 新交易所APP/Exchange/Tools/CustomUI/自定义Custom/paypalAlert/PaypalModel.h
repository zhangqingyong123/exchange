//
//  PaypalModel.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaypalModel : NSObject
@property (nonatomic,strong)NSString * icon;
@property (nonatomic,strong)NSString * title;
@property (nonatomic,strong)NSString * id;
@end
@interface PModel : NSObject
@property (nonatomic,strong)NSString * pay_method;
@property (nonatomic,strong)NSString * id;
@property (nonatomic,strong)NSString * pay_method_name;
@property (nonatomic,strong)NSString * pay_method_no;
@property (nonatomic,strong)NSString * pay_method_sub_name;
@end
NS_ASSUME_NONNULL_END
