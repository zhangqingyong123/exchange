//
//  CountryCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaypalModel.h"
@interface PaypalCell : UITableViewCell
@property (nonatomic,strong)UIImageView * icon;
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UIButton * hideButton;
@property (nonatomic,strong)PaypalModel * modle;
@end

