//
//  PasswordView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ZJInputPasswordCompletionBlock)(NSString *password);
@interface PasswordView : UIView
@property (nonatomic,copy)ZJInputPasswordCompletionBlock completionBlock;
@property (nonatomic, strong) UITextField *textField;

/** 更新输入框数据 */
- (void)updateLabelBoxWithText:(NSString *)text;

/** 抖动输入框 */
- (void)startShakeViewAnimation;

- (void)didInputPasswordError;
@end
