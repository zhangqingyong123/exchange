//
//  SC_ActionSheet.h
//  SingleChat
//
//  Created by weijieMac on 2017/3/13.
//  Copyright © 2017年 weijieMac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SC_ActionSheet : UIView

@property (copy, nonatomic) void(^selectedSth)(NSInteger tag);

- (instancetype)initWithFirstTitle:(NSString *)firstTitle secondTitle:(NSString *)secondTitle;

- (void)show;
-(void)cancleColor:(UIColor *)color titleColor:(UIColor *)titleColor;
-(void)secondBtnColor:(UIColor *)color titleColor:(UIColor *)titleColor;
-(void)firstBtnColor:(UIColor *)color titleColor:(UIColor *)titleCol;
@end
