//
//  CustomAlertVC.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QVCustomAlertDelegate.h"
typedef NS_ENUM(NSUInteger, AleartShowFromDirection) {
    NoAnimation,
    FromLeft,
    FromTop,
    FromRight,
    FromBottom,
    FromCenter,
};

@interface CustomAlertVC : UIViewController
/**
 *  自定义的 view;
 */
@property (strong, nonatomic) UIView   *contentView;


/**
 是否已经显现
 */
@property (assign, nonatomic) BOOL isShowing;

/**
 动画持续时间
 */
@property (assign, nonatomic) NSTimeInterval animationDuraction;

/**
 动画方向
 */
@property (assign, nonatomic) AleartShowFromDirection direction;

/**
 初始化

 @param contentViewClass 内容view的class名称
 @param delegate view上的点击事件回调的接受者
 @return 实例
 */
- (instancetype)initWithCustomContentViewClass:(NSString *)contentViewClass delegate:(id<QVCustomAlertDelegate>)delegate;

- (void)show;

- (void)dismiss;


@end
