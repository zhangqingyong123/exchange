//
//  MDFlipCollectionView.h
//  MDMultipleSegment
//
//  Created by 梁宪松 on 2017/8/29.
//  Copyright © 2017年 Madao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrencyMarketViewController.h"
@protocol MDFlipCollectionViewDelegate <NSObject>
/**
 滑动回调
 @param index 对应的下标（从1开始）
 */
- (void)flipToIndex:(NSInteger)index;
@end


@interface MDFlipCollectionView : UIView

/**
 存放对应的内容控制器
 */
@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *sourceArray;
@property (assign, nonatomic) NSInteger      index; //这个是自定义默认选择哪一项等视图初始化成功后有效
@property (assign, nonatomic) NSInteger      ID; //板块ID
@property (assign, nonatomic) BOOL      isQuotation; //板块ID
@property (nonatomic,assign)  BOOL  isSymbolClass;
/**
 delegate
 */
@property (nonatomic, weak) id<MDFlipCollectionViewDelegate> delegate;

/**
 初始化方法

 @param frame frame
 @param contentArray 视图控制器数组
 @return instancetype
 */
-(instancetype)initWithFrame:(CGRect)frame withArray:(NSArray <UIViewController *>*)contentArray;
/**
 手动选中某个页面
 
 @param index 默认为1（即从1开始）
 */
-(void)selectIndex:(NSInteger)index;


@end
