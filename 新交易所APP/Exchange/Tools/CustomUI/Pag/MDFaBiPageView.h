//
//  FaBiPageView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/26.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MDFlipCollectionViewDelegate <NSObject>
/**
 滑动回调
 @param index 对应的下标（从1开始）
 */
- (void)flipToIndex:(NSInteger)index;
@end

@interface MDFaBiPageView : UIView
/**
 存放对应的内容控制器
 */
@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *sourceArray;
/**
 delegate
 */
@property (nonatomic, weak) id<MDFlipCollectionViewDelegate> delegate;

/**
 初始化方法
 
 @param frame frame
 @param contentArray 视图控制器数组
 @return instancetype
 */
-(instancetype)initWithFrame:(CGRect)frame withArray:(NSArray <UIViewController *>*)contentArray;
/**
 手动选中某个页面
 
 @param index 默认为1（即从1开始）
 */
-(void)selectIndex:(NSInteger)index;

@end

