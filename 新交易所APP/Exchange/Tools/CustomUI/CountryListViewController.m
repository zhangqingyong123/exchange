//
//  CountryListViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CountryListViewController.h"
#import "PaypalCustomAlert.h"
typedef void(^animationFinishHandle)(BOOL finished);
@interface CountryListViewController ()
@property (nonatomic,strong)PaypalCustomAlert * PaypalView;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,assign) BOOL keyBoardlsVisible;
@property (weak, nonatomic) id<QVCustomAlertDelegate> delegate;
@property (strong, nonatomic) NSString *className;
@end

@implementation CountryListViewController
- (instancetype)initWithCustomContentViewClass:(NSString *)contentViewClass delegate:(id<QVCustomAlertDelegate>)delegate
{
    if (self = [super init]) {
        self.animationDuraction = 0.3;
        self.delegate = delegate;
        self.className = contentViewClass;
    }
    return self;
}
#pragma mark --- UI
- (void)configViewWith:(NSMutableArray *)dataArray wihtISSell:(BOOL)isSell;
{
    self.contentView = [[PaypalCustomAlert alloc] initWithCustomContentDataArray:dataArray isSell:isSell];
    NSAssert(self.contentView, @"contentView初始化失败");
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wundeclared-selector"
    NSAssert([self.contentView respondsToSelector:@selector(setQvDelegate:)], @"请将contentview继承自QVCustomAlertContentView，以获取属性qvDelegate");
#pragma clang diagnostic pop
    
    [self.contentView setValue:self.delegate forKeyPath:@"qvDelegate"];
    self.view.backgroundColor = RGBA(0, 0, 0, 0.3);
    [self.view addSubview:self.contentView];
}
- (void)showWith:(NSMutableArray *)dataArray wihtISSell:(BOOL)isSell;
{
    [self configViewWith:dataArray wihtISSell:isSell];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = self;
    self.window.hidden = NO;
    [self switchAnimationisEnd:NO completion:nil];
    self.isShowing = YES;
    [kWindow addSubview:self.window];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardWillHideNotification object:nil];
    _keyBoardlsVisible = NO;

}
//  键盘弹出触发该方法
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    NSLog(@"键盘弹出");
    _keyBoardlsVisible =YES;
    //创建自带来获取穿过来的对象的info配置信息
    
    NSDictionary *userInfo = [aNotification userInfo];
    
    //创建value来获取 userinfo里的键盘frame大小
    
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    //创建cgrect 来获取键盘的值
    
    CGRect keyboardRect = [aValue CGRectValue];
    
    //最后获取高度 宽度也是同理可以获取
    
    int height = keyboardRect.size.height;
    [UIView animateWithDuration:0.225 animations:^{
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, UIScreenHeight - self.contentView.height - height, self.contentView.frame.size.width, self.contentView.frame.size.height);
    }];
}
//  键盘隐藏触发该方法
- (void)keyboardDidHide
{
    NSLog(@"键盘隐藏");
    _keyBoardlsVisible =NO;
    [UIView animateWithDuration:0.325 animations:^{
        
        self.contentView.frame = CGRectMake(0,  UIScreenHeight - self.contentView.height, self.contentView.width, self.contentView.height);
        
    }];
}
- (void)switchAnimationisEnd:(BOOL)isEnd completion:(animationFinishHandle)completion
{
    switch (self.direction) {
        case FromTop:
        {
            //TODO:添加contentview从上而下的动画
        }
            break;
        case FromLeft:
        {
            //TODO:添加contentview从左到右的动画
        }
            break;
        case FromRight:
        {
            //TODO:添加contentview从右到左的动画
        }
            break;
        case FromBottom:
        {
            [self showFromBottomisEnd:isEnd completion:completion];
        }
            break;
        case FromCenter:
        {
            //TODO:添加contentview从中间弹出的动画
            [self showFromCenterisEnd:isEnd completion:completion];
        }
            break;
        case NoAnimation:
        {
            
        }
            break;
        default:
            break;
    }
}

- (void)dismiss
{
    WeakSelf
    [self switchAnimationisEnd:YES completion:^(BOOL finished) {
        weakSelf.window.rootViewController = nil;
        [weakSelf.contentView removeFromSuperview];
        weakSelf.isShowing = NO;
        [weakSelf.window removeFromSuperview];
        weakSelf.window.hidden = YES;
        weakSelf.window = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }];
}
- (void)showFromCenterisEnd:(BOOL)isEndAnimation completion:(animationFinishHandle)completion
{
    self.contentView.center = self.view.center;
    [UIView animateWithDuration:0.0 animations:^{
        
    }completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
    
    
}

- (void)showFromBottomisEnd:(BOOL)isEndAnimation completion:(animationFinishHandle)completion
{
    WeakSelf
    CGRect frame = self.contentView.frame;
    if (!isEndAnimation) {
        self.contentView.frame = CGRectMake(frame.origin.x, self.view.height, frame.size.width, frame.size.height);
    }
    [UIView animateWithDuration:self.animationDuraction animations:^{
        if (isEndAnimation) {
            weakSelf.contentView.top += weakSelf.contentView.height;
        }else {
            weakSelf.contentView.top -= weakSelf.contentView.height;
        }
    }completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}
#pragma mark --- Action
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [UIView animateWithDuration:0.225 animations:^{
        [self dismiss];
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
