//
//  AssetsSegmentView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "AssetsSegmentView.h"
#define segmentHeaderHight 44
@implementation AssetsSegmentView
- (instancetype)initWithFrame:(CGRect)frame controllers:(NSArray *)controllers titleArray:(NSArray *)titleArray ParentController:(UIViewController *)parentC  lineWidth:(float)lineW lineHeight:(float)lineH
{
    if ( self=[super initWithFrame:frame  ])
    {
        CGFloat  btnw = 90;
        self.btnMutableArray = [[NSMutableArray alloc]init]; //将button放到数组里面
        self.controllers=controllers;
        self.nameArray=titleArray;
        
        self.segmentView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, segmentHeaderHight)];
        self.segmentView.tag=50;
        [self addSubview:self.segmentView];
        self.segmentScrollV=[[UIScrollView alloc]initWithFrame:CGRectMake(0, segmentHeaderHight, frame.size.width, UIScreenHeight-segmentHeaderHight)];
        self.segmentScrollV.contentSize=CGSizeMake(frame.size.width*self.controllers.count, 0);
        self.segmentScrollV.delegate=self;
        self.segmentScrollV.showsHorizontalScrollIndicator=NO;
        self.segmentScrollV.pagingEnabled=YES;
        self.segmentScrollV.bounces=YES;
        self.segmentScrollV.pagingEnabled = YES;
        self.segmentScrollV.backgroundColor = [UIColor backgroundColor];
        self.segmentScrollV.scrollEnabled = NO;
        [self addSubview:self.segmentScrollV];
        self.backgroundColor = [UIColor whiteColor];
        for (int i=0;i<self.controllers.count;i++)
        {
            
            if ([[EXUnit isLocalizable]isEqualToString:@"en"]) {
                
                btnw = 130;
            }else
            {
                btnw = 90;
            }
            UIButton * btn=[ UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(RealValue_W(30)+i*btnw, RealValue_W(10), btnw, segmentHeaderHight);
            btn.tag=i;
            [btn setTitle:self.nameArray[i] forState:(UIControlStateNormal)];
            [btn setTitleColor:MAINTITLECOLOR1 forState:(UIControlStateNormal)];
            [btn setTitleColor:MAINTITLECOLOR forState:(UIControlStateSelected)];
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btn addTarget:self action:@selector(Click:) forControlEvents:(UIControlEventTouchUpInside)];
            btn.titleLabel.font=AutoFont(15);
            [self.segmentView addSubview:btn];
            [self.btnMutableArray addObject:btn];
            if (i==0) {
                self.seleBtn =btn;
                self.seleBtn.titleLabel.font = AutoBoldFont(15);
            }
        }
        ((UIButton *)[self.btnMutableArray objectAtIndex:0]).selected=YES;  // 关键是这里，设置 数组的第一个button为选中状态
        
        for (int i=0;i<self.controllers.count;i++)
        {
            UIViewController * contr=self.controllers[i];
            [self.segmentScrollV addSubview:contr.view];
            contr.view.frame=CGRectMake(i*frame.size.width, 0, frame.size.width,UIScreenHeight-segmentHeaderHight);
            [parentC addChildViewController:contr];
            
            [contr didMoveToParentViewController:parentC];
        }
    }
    
    
    return self;
}
- (void)Click:(UIButton*)sender
{
    self.seleBtn.selected = NO;
    self.seleBtn.titleLabel.font= AutoFont(15);
    if (sender == self.seleBtn) {
        return;
    }
    sender.selected = YES;
    self.seleBtn = sender;
    self.seleBtn.titleLabel.font= AutoBoldFont(15);
    
    UIViewController * currentVC = self.controllers[self.seleBtn.tag];
    if (!currentVC.isViewLoaded)
    {
        currentVC.view.frame = CGRectMake(UIScreenWidth * self.seleBtn.tag, 0, UIScreenWidth, self.segmentScrollV.bounds.size.height);
        [self.segmentScrollV addSubview:currentVC.view];
        
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.lineimage.centerX=self.seleBtn.centerX;
        
    }];
    [self.segmentScrollV setContentOffset:CGPointMake((sender.tag)*self.frame.size.width, 0) animated:YES ];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectVC" object:sender userInfo:nil];
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    UIButton * btn=(UIButton*)[self.segmentView viewWithTag:(self.segmentScrollV.contentOffset.x/self.frame.size.width)];
    self.seleBtn.selected=NO;
    self.seleBtn=btn;
    self.seleBtn.selected=YES;
    self.seleBtn.titleLabel.font= AutoBoldFont(15);
    [UIView animateWithDuration:0.2 animations:^{
        
        self.lineimage.centerX=self.seleBtn.centerX;
    }];
    
    
}


@end
