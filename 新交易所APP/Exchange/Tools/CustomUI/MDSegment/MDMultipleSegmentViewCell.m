//
//  MDMultipleSegmentViewCell.m
//  MDMultipleSegment
//
//  Created by 梁宪松 on 2017/8/29.
//  Copyright © 2017年 Madao. All rights reserved.
//

#import "MDMultipleSegmentViewCell.h"

@interface MDMultipleSegmentViewCell()

@property (nonatomic, strong) UIColor *tintColor;

@end

@implementation MDMultipleSegmentViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _bottomLineWidth = 1.0f;
        _tintColor = [UIColor redColor];
        [self addSubview:self.titleLabel];
        self.titleLabel.frame = CGRectMake(0,
                                           0,
                                           CGRectGetWidth(frame),
                                           CGRectGetHeight(frame));
        [self addSubview:self.lineLabel];
        
    }
    return self;
}

- (void)setIsSeleted:(BOOL)isSeleted
{
    _isSeleted = isSeleted;
    if (_isSeleted) {
        _titleLabel.textColor = _tintColor;
         _lineLabel.backgroundColor = [ZBLocalized sharedInstance].segmentBaColor;
        
    }else
    {
        _titleLabel.textColor = MAINTITLECOLOR1;
         _lineLabel.backgroundColor = CLEARCOLOR;
    }
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = _tintColor;
        _titleLabel.font = AutoBoldFont(15);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}
- (UILabel *)lineLabel
{
    if (!_lineLabel) {
        _lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _titleLabel.bottom -5, 20, 3)];
        _lineLabel.backgroundColor = [ZBLocalized sharedInstance].segmentBaColor;
        _lineLabel.centerX = _titleLabel.centerX;
    }
    return _lineLabel;
}

@end
