//
//  QVMagicStickersView.m
//  QuakeVideo
//
//  Created by weijieMac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

#import "QVMagicStickersView.h"


@implementation QVMagicStickersView

- (instancetype)init
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, UIScreenWidth, 200);
    }
    return self;
}

- (void)configView
{
    self.backgroundColor = [UIColor redColor];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(100, 80, 40, 40)];
    btn.backgroundColor = WHITECOLOR;
    btn.tag = 5678;
    [btn addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
    if (self.qvDelegate && [self.qvDelegate respondsToSelector:@selector(infoForView)]) {
        NSString *name = [[self.qvDelegate infoForView] objectForKey:@"name"];
        [btn setTitle:name forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    [self addSubview:btn];
}

- (void)clicked:(UIButton *)sender
{
    if (self.qvDelegate && [self.qvDelegate respondsToSelector:@selector(selectedItemWithItemInfo:)]) {
        NSDictionary *dic = @{};
        if (self.qvDelegate && [self.qvDelegate respondsToSelector:@selector(infoForView)]) {
            dic = [self.qvDelegate infoForView];
        }
        [self.qvDelegate selectedItemWithItemInfo:@{@"tag":@(sender.tag),@"info":dic}];
    }
}

- (void)setQvDelegate:(id<QVCustomAlertDelegate>)qvDelegate
{
    super.qvDelegate = qvDelegate;
    [self configView];
}

- (void)dealloc
{
    
}

@end
