//
//  QVCustomAlertContentView.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QVCustomAlertDelegate.h"
@interface QVCustomAlertContentView : UIView

@property (weak, nonatomic) id<QVCustomAlertDelegate> qvDelegate;

@end
