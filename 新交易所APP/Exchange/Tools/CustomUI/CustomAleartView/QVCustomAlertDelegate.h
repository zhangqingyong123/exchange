//
//  QVCustomAlertDelegate.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QVCustomAlertDelegate <NSObject>

- (void)selectedItemWithItemInfo:(NSDictionary *)itemInfo;

@optional

- (NSDictionary *)infoForView;

@end
