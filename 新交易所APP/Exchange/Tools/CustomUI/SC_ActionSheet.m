//
//  SC_ActionSheet.m
//  SingleChat
//
//  Created by weijieMac on 2017/3/13.
//  Copyright © 2017年 weijieMac. All rights reserved.
//

#import "SC_ActionSheet.h"

@interface SC_ActionSheet()
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIButton *cancelBtn;
@property (strong, nonatomic) UIButton *firstBtn;
@property (strong, nonatomic) UIButton *secondBtn;
@property (strong, nonatomic) NSString *firstTitle;
@property (strong, nonatomic) NSString *secondTitle;
//@property (readwrite, nonatomic) BOOL  onlyOne;
@end

@implementation SC_ActionSheet
- (instancetype)initWithFirstTitle:(NSString *)firstTitle secondTitle:(NSString *)secondTitle 
{
    if (self = [super init]) {
        self.firstTitle = firstTitle;
        self.secondTitle = secondTitle;
        [self configView];
    }
    return self;
}

- (void)selectedSth:(UIButton *)sender
{
    if (self.selectedSth) {
        self.selectedSth(sender.tag);
    }
    [self dismiss];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.mj_y = UIScreenHeight;
        self.backgroundColor = RGBA(0, 0, 0, 0);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)show
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self];
    
    [UIView animateWithDuration:0.25 animations:^{
    
        
         if (self.firstTitle.length>0) {
             self.contentView.mj_y = UIScreenHeight-RealValue_H(330)-20;
         }else
         {
          
             self.contentView.mj_y = UIScreenHeight-RealValue_H(220)-10;
         }

    } completion:^(BOOL finished) {
        
    }];
}

- (void)configView
{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = RGBA(0, 0, 0, 0.5);
    
    [self addSubview:self.contentView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
    [self addGestureRecognizer:tap];
    
}

- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(0, UIScreenHeight, UIScreenWidth,RealValue_H(330)+20)];
        _contentView.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
       
       
       
        if (self.firstTitle.length>0) {
             [_contentView addSubview:self.cancelBtn];
             [_contentView addSubview:self.secondBtn];
            [_contentView addSubview:self.firstBtn];
        }else
        {
            _contentView.height=RealValue_H(220)+20;
             [_contentView addSubview:self.cancelBtn];
             [_contentView addSubview:self.secondBtn];
          
        }
    }
    return _contentView;
}

- (UIButton *)cancelBtn
{
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, self.contentView.height-10 - RealValue_H(110), self.contentView.width, RealValue_H(110))];
        _cancelBtn.backgroundColor = MAINCOLOR;
        [_cancelBtn setTitle:Localized(@"transaction_cancel") forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
//        _cancelBtn.layer.cornerRadius = 10;
//        _cancelBtn.layer.masksToBounds = YES;
        _cancelBtn.titleLabel.font = AutoFont(15);
    }
    return _cancelBtn;
}

- (UIButton *)secondBtn
{
    if (!_secondBtn) {
        _secondBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.cancelBtn.left, self.cancelBtn.top - 10 - self.cancelBtn.height, self.cancelBtn.width, self.cancelBtn.height)];
        _secondBtn.backgroundColor = MAINCOLOR;
        [_secondBtn setTitle:self.secondTitle forState:UIControlStateNormal];
        [_secondBtn setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
        _secondBtn.tag = 1;
        [_secondBtn addTarget:self action:@selector(selectedSth:) forControlEvents:UIControlEventTouchUpInside];
        _secondBtn.titleLabel.font = AutoFont(15);
        UIRectCorner corner = UIRectCornerBottomLeft|UIRectCornerBottomRight;
        if (self.firstTitle.length<1) {
            corner = UIRectCornerAllCorners;
        }
//        UIBezierPath *maskPath=[UIBezierPath bezierPathWithRoundedRect:_secondBtn.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(10, 10)];
//        CAShapeLayer *maskLayer=[[CAShapeLayer alloc]init];
//        maskLayer.frame = _secondBtn.bounds;
//        maskLayer.path = maskPath.CGPath;
//        _secondBtn.layer.mask = maskLayer;
        
        if (self.firstTitle.length > 0) {
            CALayer *layer = [[CALayer alloc]init];
            layer.frame = CGRectMake(0,  _secondBtn.height - 0.5, _secondBtn.width, 0.5);
            layer.backgroundColor = MainBGCOLOR.CGColor;
            [_secondBtn.layer addSublayer:layer];
        }

    }
    return _secondBtn;
}

- (UIButton *)firstBtn
{
    if (!_firstBtn) {
        _firstBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.cancelBtn.left, 0, self.cancelBtn.width, self.cancelBtn.height)];
        _firstBtn.backgroundColor = MAINCOLOR;
        [_firstBtn setTitle:self.firstTitle forState:UIControlStateNormal];
        [_firstBtn setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
        _firstBtn.tag = 0;
        _firstBtn.titleLabel.font = AutoFont(15);
        [_firstBtn addTarget:self action:@selector(selectedSth:) forControlEvents:UIControlEventTouchUpInside];
        
//        UIBezierPath *maskPath=[UIBezierPath bezierPathWithRoundedRect:_firstBtn.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
//        CAShapeLayer *maskLayer=[[CAShapeLayer alloc]init];
//        maskLayer.frame = _firstBtn.bounds;
//        maskLayer.path = maskPath.CGPath;
//        _firstBtn.layer.mask = maskLayer;
        
        
        if (self.firstTitle.length > 0) {
            CALayer *layer = [[CALayer alloc]init];
            layer.frame = CGRectMake(0, _firstBtn.height-0.5, _firstBtn.width, 0.5);
            layer.backgroundColor = MainBGCOLOR.CGColor;

            [_firstBtn.layer addSublayer:layer];
        }
    }
    return _firstBtn;
}
-(void)cancleColor:(UIColor *)color titleColor:(UIColor *)titleColor
{
    _cancelBtn.backgroundColor = color;
    [_cancelBtn setTitleColor:titleColor forState:UIControlStateNormal];
    

}
-(void)secondBtnColor:(UIColor *)color titleColor:(UIColor *)titleColor
{
    _secondBtn.backgroundColor = color;
    [_secondBtn setTitleColor:titleColor forState:UIControlStateNormal];
}
-(void)firstBtnColor:(UIColor *)color titleColor:(UIColor *)titleColor
{
    _firstBtn.backgroundColor = color;
    [_firstBtn setTitleColor:titleColor forState:UIControlStateNormal];
}
@end
