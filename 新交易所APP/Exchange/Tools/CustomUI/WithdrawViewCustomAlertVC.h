//
//  WithdrawViewCustomAlertVC.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QVCustomAlertDelegate.h"

@interface WithdrawViewCustomAlertVC : UIViewController
/**
 *  自定义的 view;
 */
@property (strong, nonatomic) UIView   *contentView;


/**
 是否已经显现
 */
@property (assign, nonatomic) BOOL isShowing;

/**
 动画持续时间
 */
@property (assign, nonatomic) NSTimeInterval animationDuraction;

/**
 动画方向
 */
@property (assign, nonatomic) AleartShowFromDirection direction;

/**
 初始化
 
 @param contentViewClass 内容view的class名称
 @param delegate view上的点击事件回调的接受者
 @return 实例
 */
- (instancetype)initWithCustomContentViewClass:(NSString *)contentViewClass delegate:(id<QVCustomAlertDelegate>)delegate;

- (void)show;

- (void)dismiss;

@end
