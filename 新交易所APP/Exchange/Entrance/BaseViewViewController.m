//
//  BaseViewViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface BaseViewViewController ()

@end

@implementation BaseViewViewController
- (void)viewWillAppear:(BOOL)animated
{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self statusBarStyleIsDark:[ZBLocalized sharedInstance].StatusBarStyle];
    _activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallSpinFadeLoader tintColor:[ZBLocalized sharedInstance].AnimationColor];
    // 设置基类初始属性
    [self settingsViewControllerAttribute];
}
- (void)settingsViewControllerAttribute{
    // 将打印日志属性设置为默认NO
    //    _axcBasePrintLog = NO;
    //    self.page = 1;
    // 设置每个子类的背景颜色
    self.view.backgroundColor = TABLEVIEWLCOLOR;
   
    // 选择打印当前日志
    [self performSelector:@selector(printNSLog) withObject:nil afterDelay:1.0f];
   
}
- (void)showLoadingAnimation
{
    [_activityIndicatorView startAnimating];
}
- (void)stopLoadingAnimation
{
    
    [_activityIndicatorView stopAnimating];
}
-(void)viewDidAppear:(BOOL)animated{
    
   
    [super viewDidAppear:animated];
   
}
- (void)barLine:(BOOL)isLine
{
    if (isLine) {
        [self.navigationController.navigationBar setShadowImage:[self imageWithColor:CELLCOLOR]];
    }else
    {
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    }
}
- (void)SetTheLoadProperties:(BOOL)enabled{
    if (enabled) {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }else
    {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)SetnavigationBarBackgroundImage:(UIColor *)color Alpha:(NSInteger)Alpha;
{
    [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:[color colorWithAlphaComponent:Alpha]] forBarMetrics:UIBarMetricsDefault];
}
- (UIImage *)imageWithColor:(UIColor *)color
{
    // 描述矩形
    CGRect rect           = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    
    // 开启位图上下文
    UIGraphicsBeginImageContext(rect.size);
    // 获取位图上下文
    CGContextRef context  = UIGraphicsGetCurrentContext();
    // 使用color演示填充上下文
    CGContextSetFillColorWithColor(context, [color CGColor]);
    // 渲染上下文
    CGContextFillRect(context, rect);
    // 从上下文中获取图片
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    // 结束上下文
    UIGraphicsEndImageContext();
    return theImage;
}
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark
{
        if (statusBarStyleIsDark)
        {
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        }else
        {
          [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
         
        }
}
/**
 *  数据请求
 */
- (void)axcBaseRequestData{
}

/**
 *  添加右按钮
 *
 *  @param image 图片
 */
- (void)axcBaseAddRightBtnWithImage:(UIImage *)image{
    UIButton *baseRightImageButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [baseRightImageButton setImage:image forState:UIControlStateNormal];
    [baseRightImageButton addTarget:self action:@selector(axcBaseClickBaseRightImageBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:baseRightImageButton];
}
- (void)axcBaseClickBaseRightImageBtn:(UIButton *)sender{
}
/**
 *  添加右按钮
 *
 *  @param type 系统风格
 */
- (void)axcBaseAddRightBtnWithType:(UIBarButtonSystemItem)type{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:type target:self action:@selector(axcBaseClickBaseRightTypeBtn:)];
}
- (void)axcBaseClickBaseRightTypeBtn:(UIBarButtonItem *)sender{
}

- (void)axcBaseAddTitleLableWithTitle:(NSString *)title{
    
}
/**
 *  弹出警告框
 *
 *  @param message 警告信息
 */
- (void)axcBasePopUpWarningAlertViewWithMessage:(NSString *)message view:(UIView *)view{
    static BOOL isShow = NO;
    if (isShow) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        isShow = YES;
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = AutoFont(14);
        style.verticalPadding =10;
        style.cornerRadius = 0;
        [view makeToast:message
               duration:1.5
               position:CSToastPositionCenter
                  style:style];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShow = NO;
        });
    });
}
/**
 *  弹出提示框
 *
 *  @param message 提示信息
 *  @param target  代理
 */
- (void)axcBasePopUpPromptAlertViewWithMessage:(NSString *)message addTarget:(nullable id)target{
//    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:target cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//    if (_axcBasePrintLog) {
//        NSLog(@"BaseViewController:\n弹出提示框\t方法名：axcBasePopUpPromptAlertViewWithMessage:addTarget\t当前message:%@\n",message);
//    }
//    [alertView show];
}
/**
 *   下拉刷新
 */
- (void)axcBaseLoadNewData{
    _page = 1;
    [self axcBaseRequestData];
    if (_axcBasePrintLog) {
        NSLog(@"BaseViewController:\n当前执行下拉刷新\t方法名：axcBaseLoadNewData\t当前page:%d\n",_page);
    }
}

/**
 *  上拉加载
 */
- (void)axcBaseLoadMoreData{
    _page ++;
    [self axcBaseRequestData];
    if (_axcBasePrintLog) {
        NSLog(@"BaseViewController:\n当前执行上拉加载\t方法名：axcBaseLoadMoreData\t当前page:%d\n",_page);
    }
}
///**
//*  左滑返回手势
//*/
//- (void)axcBaseOpenTheLeftBackSkip{
//    // 获取系统自带滑动手势的target对象.
//    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
//    // 创建全屏滑动手势，调用系统自带滑动手势的target的action方法.
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
//    // 设置手势代理，拦截手势触发.
//    pan.delegate = self;
//    // 给导航控制器的view添加全屏滑动手势.
//    [self.view addGestureRecognizer:pan];
//    // 禁止使用系统自带的滑动手势.
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//}
///**
// *  返回手势代理
// *
// *  @param gestureRecognizer gestureRecognizer
// *
// *  @return BOOL
// */
//- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
//    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
//    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
//    if (self.childViewControllers.count == 1) {
//        // 表示用户在根控制器界面，就不需要触发滑动手势
//        return NO;
//    }
//    return YES;
//}
/**
 *  返回方法
 *
 */
- (void)handleNavigationTransition:(UIPanGestureRecognizer *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)printNSLog{
    if (_axcBasePrintLog) {
        printf("\n----------------=开始设置当前界面属性默认值=----------------\n");
        NSLog(@"BaseViewController:\n设置当前page值为：\t%d\n",_page);
        NSLog(@"BaseViewController:\n设置当前子类的背景颜色为：\t白色\n");
        NSLog(@"BaseViewController:\n设置当前子类的NavBar上的渲染颜色为：\t白色\n");
        NSLog(@"BaseViewController:\n设置当前的Tabbar风格为：\tUIBarStyleBlack\n");
        NSLog(@"BaseViewController:\n设置当前取消ScrollerView自适应属性：\tautomaticallyAdjustsScrollViewInsets = NO\n");
        NSLog(@"BaseViewController:\n设置当前tabbar选中的渲染颜色：\t红色：RGBA：220，9，64，1\n");
        printf("----------------=当前界面属性的默认值设置完毕=----------------\n\n");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
