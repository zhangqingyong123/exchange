//
//  AppDelegate.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseTabBarController.h"
#import "BaseNavigationController.h"
#import "LoginViewController.h"
@interface BaseTabBarController ()<UITabBarDelegate,UITabBarControllerDelegate>

@end

@implementation BaseTabBarController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self.tabBar setBackgroundImage:[UIImage new]];
    [self.tabBar setShadowImage:[self imageWithColor:CELLCOLOR size:CGSizeMake(UIScreenWidth, 0.6f)]];
    self.tabBar.barTintColor = TABBARBGCOLOR;
    self.tabBar.translucent = NO;
    self.delegate =self;
    NSMutableArray *viewCtls = [NSMutableArray arrayWithCapacity:kMTBaseTabbarCount];
    for (int i=0; i<kMTBaseTabbarCount; i++) {
      
        BaseNavigationController *navCtl=[[BaseNavigationController alloc]initWithRootViewController:kMTBaseTabbarStoryboardNames[i]];
        UIViewController *viewCtl = navCtl.topViewController;
        viewCtl.title = kMTBaseTabbarTitles[i];
        UIImage *imagen = [UIImage imageNamed:kMTBaseTabbarNormalImages[i]];
         imagen=[imagen imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]; //不让图片渲染
        viewCtl.tabBarItem.image = imagen;
        viewCtl.tabBarItem.tag = i;
        
         UIImage *image=[UIImage imageNamed:kMTBaseTabbarSelectImages[i]];
        image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]; //不让图片渲染
        viewCtl.tabBarItem.selectedImage=image;
        // 设置 tabbarItem 选中状态下的文字颜色(不被系统默认渲染,显示文字自定义颜色)
        NSDictionary *dictHome = [NSDictionary dictionaryWithObject:MAINTITLECOLOR1 forKey:NSForegroundColorAttributeName];
        NSDictionary *dictHome1 = [NSDictionary dictionaryWithObject:[ZBLocalized sharedInstance].TabBgColor forKey:NSForegroundColorAttributeName];
        [navCtl.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateNormal];
        [navCtl.tabBarItem setTitleTextAttributes:dictHome1 forState:UIControlStateSelected];
        [viewCtls addObject:navCtl];
    }
    
     self.viewControllers = viewCtls;
     
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0)
        return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);UIImage *image = UIGraphicsGetImageFromCurrentImageContext();UIGraphicsEndImageContext();
    return image;
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController NS_AVAILABLE_IOS(3_0){

    
//    if (viewController.tabBarItem.tag == [ZBLocalized sharedInstance].tabNum-1)
//    {
//        if (![EXUserManager isLogin]) {
//
//            LoginViewController *LoginView = [[LoginViewController alloc] init];
//
//            [self presentViewController:LoginView animated:YES completion:^{
//
//            }];
//
//            return NO;
//
//        }else
//        {
//
//            return YES;
//        }
//
//    } else
//    {
//
//        return YES;
//    }
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
