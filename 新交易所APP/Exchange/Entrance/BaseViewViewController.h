//
//  BaseViewViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 *  BaseViewController代理方法
 */
@protocol BaseViewControllerDelegate <NSObject>

/**
 *  预留代理接口
 */
@end
/**
 *  Controller基类：BaseViewController
 */
@interface BaseViewViewController : UIViewController
@property (nonatomic,strong) DGActivityIndicatorView * activityIndicatorView;
/**
 *  打印日志情况默认YES
 */
@property(nonatomic,assign)BOOL axcBasePrintLog;

/**
 *  上拉加载下拉刷新页数page
 */
@property(nonatomic,assign)int page;

/**
 *  自带声明一个tableView对象
 */
//@property(nonatomic,strong)UITableView * tableView;

/**
 *  是否开启全继承左滑返回手势 默认NO
 */
@property(nonatomic,assign)BOOL OpenTheLeftBackOfAll;


/**
 *  预留代理
 */
@property(nonatomic,weak)id <BaseViewControllerDelegate> delegate;

/**
 *  基类请求数据
 */
- (void)axcBaseRequestData;

/**
 *  基类添加右图片按钮
 *
 *  @param image image类型
 */
- (void)axcBaseAddRightBtnWithImage:(UIImage * )image;

/**
 *  基类右图片按钮触发方法
 *
 *  @param sender UIButton
 */
- (void)axcBaseClickBaseRightImageBtn:(UIButton * )sender;

/**
 *  基类添加右系统风格按钮
 *
 *  @param type UIBarButtonSystemItem类型
 */
- (void)axcBaseAddRightBtnWithType:(UIBarButtonSystemItem)type;

/**
 *  基类右图片按钮触发方法
 *
 *  @param sender UIBarButtonItem
 */
- (void)axcBaseClickBaseRightTypeBtn:(UIBarButtonItem * )sender;
/**
 *  AlertView警告框
 *
 *  @param message 警告消息
 *  @param view  视图
 */
- (void)axcBasePopUpWarningAlertViewWithMessage:(NSString *)message view:(UIView *)view;

/**
 *  AlertView提示框
 *
 *  @param message 提示信息
 *  @param target  代理
 */
- (void)axcBasePopUpPromptAlertViewWithMessage:(NSString * )message addTarget:(nullable id)target;

/**
 *  下拉刷新
 */
- (void)axcBaseLoadNewData;

/**
 *  上拉加载
 */
- (void)axcBaseLoadMoreData;

/**
 *  状态栏显示深色
 *  @param statusBarStyleIsDark 显示深色
 */
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark;
/**
 *  禁止左滑
 *  @param enabled 禁止左滑
 */
- (void)SetTheLoadProperties:(BOOL)enabled;
/**
 *  设置导航栏背景色
 *  @param color 颜色
 *  @param Alpha 透明度
 */
- (void)SetnavigationBarBackgroundImage:(UIColor *)color Alpha:(NSInteger)Alpha;
/**
 *  设置导航栏下的线
 *  @param isLine 是否隐藏
 */
- (void)barLine:(BOOL)isLine;
/*
  加载动画
 */
- (void)showLoadingAnimation;
/*
  停止加载动画
 */
- (void)stopLoadingAnimation;
@end
