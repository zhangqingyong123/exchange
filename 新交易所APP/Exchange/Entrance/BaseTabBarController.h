//
//  AppDelegate.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//



#define kMTBaseTabbarCount [ZBLocalized sharedInstance].tabNum

/**
 *  Tabbar标题
 */
#define kMTBaseTabbarTitles [ZBLocalized sharedInstance].tabTitle

/**
 *  Tabbar正常显示图片
 */
#define kMTBaseTabbarNormalImages [ZBLocalized sharedInstance].tabNormalImages


/**
 *  Tabbar选中显示图片
 */
#define kMTBaseTabbarSelectImages [ZBLocalized sharedInstance].tabSelectImages

/**
 *  Tabbar 对应视图
 */
#define kMTBaseTabbarStoryboardNames [ZBLocalized sharedInstance].tabVC
//

@interface BaseTabBarController : UITabBarController

@end
