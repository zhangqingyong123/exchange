//
//  AppDelegate.m
//  Calculated
//
//  Created by 张庆勇 on 2018/3/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseNavigationController.h"


@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialize];
    
    self.interactivePopGestureRecognizer.delegate = (id)self;
//    //自定义返回按钮
//    UIImage *backButtonImage = [UIImage imageNamed:@"icon_back_blue"];
//    backButtonImage = [backButtonImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, RealValue_W(32), 0, 0)];
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    //将返回按钮的文字position设置不在屏幕上显示   “使用该方法，APP间切换时，跳回自己的app会闪动，解决方法：在BaseViewController中去除返回的文字”
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(NSIntegerMin, NSIntegerMin) forBarMetrics:UIBarMetricsDefault];
     
}
/**
 * 定制UINavigationBar
 */
- (void)initialize {
    
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeZero;
    [navigationBar setTitleTextAttributes:@{
                                            NSForegroundColorAttributeName:MAINTITLECOLOR,
                                            NSFontAttributeName:AutoBoldFont(18),
                                            NSShadowAttributeName:shadow
                                            }];
 
    [navigationBar setBackgroundImage:[self createImageWithColor:MAINBLACKCOLOR frame:CGRectMake(0, 0, UIScreenWidth,64)] forBarMetrics:UIBarMetricsDefault];
    [navigationBar setShadowImage:[UIImage new]];
    
}
//根据颜色值生成纯色图片
- (UIImage *)createImageWithColor:(UIColor *)color frame:(CGRect)frame {
    CGRect rect = CGRectMake(0, 0, frame.size.width, frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
/**
 * 可以在这个方法中拦截所有push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    if (self.childViewControllers.count > 0) { // 如果push进来的不是第一个控制器
        
        // 隐藏tabbar
    
        UIButton * backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, -20, 50, 40);
        [backButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].backicon] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //扩大点击区域
        [backButton setEnlargeEdgeWithTop:60 right:60 bottom:60 left:60];
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        viewController.hidesBottomBarWhenPushed = YES; // 隐藏底部的工具条
        
    }
    
    // 这句super的push要放在后面, 让viewController可以覆盖上面设置的leftBarButtonItem
    // 意思是，我们任然可以重新在push控制器的viewDidLoad方法中设置导航栏的leftBarButtonItem，如果设置了就会覆盖在push方法中设置的“返回”按钮，因为 [super push....]会加载push的控制器执行viewDidLoad方法。
    [super pushViewController:viewController animated:animated];
    
}

- (void)back
{
    [self popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIViewController *)childViewControllerForStatusBarStyle{
    return self.topViewController;
}
@end
