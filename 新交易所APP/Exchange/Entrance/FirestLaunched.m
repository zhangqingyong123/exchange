//
//  MZGuidePages.m
//  MZGuidePages
//
//  Created by boco on 15/11/13.
//  Copyright © 2016 南京萌芽之旅智能科技有限公司 All right reserved.
//

#import "FirestLaunched.h"
#import "UIImage+GIF.h"
@interface FirestLaunched () <UIScrollViewDelegate>
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *customLaunchImageView;

@end

@implementation FirestLaunched
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpLaunchScreen];
    }
    return self;
    
}
//添加启动图
- (void)setUpLaunchScreen{
    
    NSURL *fileUrl;
    if (UIScreenHeight==480) {// 屏幕的高度为480
        fileUrl = [[NSBundle mainBundle] URLForResource:@"gifTest" withExtension:@"gif"]; // 加载GIF图片
    }else if(UIScreenHeight==568) {// 屏幕的高度为568
        //5 5s         0.853333 0.851574
        fileUrl = [[NSBundle mainBundle] URLForResource:@"gifTest" withExtension:@"gif"]; // 加载GIF图片
        
    }else if(UIScreenHeight==667){// 屏幕的高度为667
        //6 6s 7       1.000000 1.000000
        fileUrl = [[NSBundle mainBundle] URLForResource:@"gifTest" withExtension:@"gif"]; // 加载GIF图片
    }else if(UIScreenHeight==736){// 屏幕的高度为736
        //6p 6sp 7p
        fileUrl = [[NSBundle mainBundle] URLForResource:@"gifTest" withExtension:@"gif"]; // 加载GIF图片
    }else if(UIScreenHeight==812){// 屏幕的高度为812    375.000000 812.000000
        // x
        fileUrl = [[NSBundle mainBundle] URLForResource:@"gifTest" withExtension:@"gif"]; // 加载GIF图片
    }else{
        
    }
    
    
    
    CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);           //将GIF图片转换成对应的图片源
    size_t frameCout = CGImageSourceGetCount(gifSource);                                         // 获取其中图片源个数，即由多少帧图片组成
    NSMutableArray *frames = [[NSMutableArray alloc] init];                                      // 定义数组存储拆分出来的图片
    for (size_t i = 0; i < frameCout; i++) {
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL); // 从GIF图片中取出源图片
        UIImage *imageName = [UIImage imageWithCGImage:imageRef];                  // 将图片源转换成UIimageView能使用的图片源
        [frames addObject:imageName];                                              // 将图片加入数组中
        CGImageRelease(imageRef);
    }
    _bgView = [[UIView alloc] initWithFrame:kWindow.bounds];
    _bgView.backgroundColor = RGBA(21, 30, 56, 1);
    self.customLaunchImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,-RealValue_H(70), UIScreenWidth, UIScreenHeight)];
    self.customLaunchImageView.backgroundColor= RGBA(21, 30, 56, 1);
    self.customLaunchImageView.userInteractionEnabled = YES;
    
    self.customLaunchImageView.animationImages = frames; // 将图片数组加入UIImageView动画数组中
    self.customLaunchImageView.animationDuration = 2; // 每次动画时长
    [self.customLaunchImageView startAnimating];         // 开启动画，此处没有调用播放次数接口，UIImageView默认播放次数为无限次，故这里不做处理
    [_bgView addSubview:self.customLaunchImageView];
    [[AppDelegate shareAppdelegate].window addSubview:self.bgView];
    [[AppDelegate shareAppdelegate].window bringSubviewToFront:self.bgView];
    
    //5秒后自动关闭
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self yourButtonClick];
    });
}

- (void)yourButtonClick {
    
    // 移动自定义启动图
    if (self.customLaunchImageView) {
        [UIView animateWithDuration:0.3 animations:^{
            self.bgView.alpha = 0;
        } completion:^(BOOL finished) {
            [self.bgView removeFromSuperview];
            self.bgView = nil;
        }];
    }
}
@end
