//
//  AppDelegate.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModle.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) NSInteger index; //所选中的
@property (strong, nonatomic) HomeModle *modle;
@property (strong, nonatomic) NSMutableArray *allCurrency;//所有币种名称
/**
 * 获取所有币价的字段
 */
@property(nonatomic,strong)NSMutableDictionary *dictionary;

/**
 * 获取交易对数组 因为循环次数过大获取表的效率降低直接赋值不会影响卡顿，详情请看数据库源码
 */
@property(nonatomic,strong)NSMutableArray *dataArray;
/**
 * 是否允许转向
 */
@property(nonatomic,assign)BOOL allowRotation;
/*
 是否进入后台默认为yes
 */
@property(nonatomic,assign)BOOL isBecomeActive;

/**
 * 是否有网络
 */
@property(nonatomic,assign)BOOL isNoWork;

/**
 * 获取站点名称
 */
@property(nonatomic,strong)NSString *name;
/**
 * 获取站点邮箱
 */
@property(nonatomic,strong)NSString *emailSite;

/**
 * 所有子站ID
 */
@property(nonatomic,strong)NSString * sizeId;

/**
 * 邀请妈状态1必填 0非必填
 */
@property(nonatomic,strong)NSString * recommend_code_force;


    
/**
 * 注册 1为默认为手机 2默认为邮箱 3只显示邮箱注册 4只显示手机注册
 */
@property(nonatomic,strong)NSString * default_register_method;

/**
 * 1为默认模式需要验证才可以添加地址。 如果是0则不需要验证身份
 */
@property(nonatomic,strong)NSString * is_preaudit;

/**
 * 交易对
 */
@property (nonatomic, strong) NSString *DICKEY;

/**
 * 可配置 4表示c2C 8表示OTC  4,8表示OTC和c2c同时存在
 */
@property (nonatomic, strong) NSString *privilages;

/**
 * 币资产
 */
@property (nonatomic, strong) NSString *otc_asset;
/**
 * 法币
 */
@property (nonatomic, strong) NSString *otc_currency;

+(AppDelegate *)shareAppdelegate;
- (void)initRootVC;
@end



