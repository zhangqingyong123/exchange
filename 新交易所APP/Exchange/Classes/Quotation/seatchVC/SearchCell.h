//
//  SearchCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModle.h"
@interface SearchCell : UITableViewCell
@property (nonatomic,strong)UILabel * symbolLable;

@property (nonatomic,strong)UIImageView * searchimageView;

@property (nonatomic,strong)HomeModle * modle;
@end

