//
//  AJSearchBar.h
//  准到-ipad
//
//  Created by zhundao on 2017/9/8.
//  Copyright © 2017年 zhundao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchDelegate <NSObject>

- (void)searchWithStr :(NSString *)text;
- (void)searchWithcancel;

@end

@interface AJSearchBar : UIView
/*! 输入框 */
@property(nonatomic,strong)UITextField *textField;
/*! 默认文字 默认居中存在 */
@property(nonatomic,strong)UILabel *placeholderLabel;
@property(nonatomic,copy)NSString *AJPlaceholder;

@property(nonatomic,strong)UIColor *AJCursorColor;
@property(nonatomic,assign)BOOL isChina;

@property(nonatomic,weak) id<SearchDelegate> SearchDelegate;
@end
