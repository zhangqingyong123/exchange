//
//  AJSearchBar.m
//  准到-ipad
//
//  Created by zhundao on 2017/9/8.
//  Copyright © 2017年 zhundao. All rights reserved.
//

#import "AJSearchBar.h"
#import "UITextField+TextLeftOffset_ffset.h"
#define ALPHA @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define ALPHANUM @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
@interface AJSearchBar()<UITextFieldDelegate>

/*! 方法镜图片 */
@property(nonatomic,strong)UIImageView *searchImage;
/*! 取消按钮 */
@property(nonatomic,strong)UIButton *cancelButton;
/*! 删除按钮 */
@property(nonatomic,strong)UIButton *deleteButton;

/*! label文字大小 */
@property(nonatomic,assign)CGSize size;
@end;

@implementation AJSearchBar

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.searchImage];
        [self addSubview:self.textField];
       
        [_textField addSubview:self.placeholderLabel];
        [self addSubview:self.deleteButton];
        [self addSubview:self.cancelButton];
        [self baseSetting];
    }
    return self;
}

/*! 基础配置 */

- (void)baseSetting{
    /*! 字体其他 */
    _textField.font = AutoFont(14);
    _textField.tintColor = TABTITLECOLOR;
    _textField.textColor = MAINTITLECOLOR;
    _textField.keyboardType = UIKeyboardTypeASCIICapable;
    _textField.delegate =self;
    /*! 设置键盘return样式为搜索样式 */
    _textField.returnKeyType = UIReturnKeySearch;
    /*! 设置为无文字就灰色不可点 */
    _textField.enablesReturnKeyAutomatically = YES;


    /*! 编辑事件观察 */
    [_textField addTarget:self action:@selector(textFieldDidEditing:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark --- 懒加载
- (UIImageView *)searchImage{
    if (!_searchImage) {
        _searchImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 23/2, 15, 17)];
        _searchImage.image = [UIImage imageNamed:@"searchimage"];
    }
    return _searchImage;
}
- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(_searchImage.right+10, 0, self.frame.size.width-120, self.frame.size.height)];
    }
    return _textField;
}

- (UILabel *)placeholderLabel{
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 180, self.frame.size.height)];
        _placeholderLabel.text = Localized(@"search_symbol");
        _placeholderLabel.textColor = MAINTITLECOLOR1;
        _placeholderLabel.font = AutoFont(14);
        _placeholderLabel.centerY = _textField.centerY;
    }
    return _placeholderLabel;
}
//icon_dele
- (UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(_textField.right, 0, self.frame.size.height, self.frame.size.height)];
        [_deleteButton setImage:[UIImage imageNamed:@"icon_dele"] forState:0];
        [_deleteButton addTarget:self action:@selector(deleteSeatchValue) forControlEvents:UIControlEventTouchUpInside];
        _deleteButton.hidden = YES;
    }
    return _deleteButton;
}

- (UIButton *)cancelButton{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(UIScreenWidth -70 , 0, 50, self.frame.size.height);
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [_cancelButton setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
        [_cancelButton setTitle:Localized(@"transaction_cancel") forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _cancelButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _cancelButton;
}
#pragma mark --- 取消按钮点击事件

- (void)cancelAction{
  
    [self endEditing:YES];
    _placeholderLabel.hidden  = NO;
    _textField.text = @"";
    [_SearchDelegate searchWithcancel];
}

#pragma mark --- UITextFieldDelegate

/*! 当输入框开始编辑的时候 */
- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
}
/*! 输入框结束编辑 */
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length>0) {
        NSLog(@"进行搜索");
       [_SearchDelegate searchWithStr:textField.text];
    }
}
// 清除
- (void)deleteSeatchValue
{
    self.textField.text = nil;
    _deleteButton.hidden = YES;
    _placeholderLabel.hidden  = NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_textField resignFirstResponder];
    
    [_SearchDelegate searchWithStr:[EXUnit removeCharacter:textField.text]];
    NSLog(@"点击了搜索");
    return YES;
}

#pragma mark --- textFieldDidEditing: 
/*! 输入框编辑中 */

- (void)textFieldDidEditing:(UITextField *)textField{
    [self isHiddenLabel:textField];
}

- (void)isHiddenLabel:(UITextField *)textField{
    if (textField.text.length==0) {
        _deleteButton.hidden = YES;
        _placeholderLabel.hidden  = NO;
    }else{
        _deleteButton.hidden = NO;
        _placeholderLabel.hidden = YES;
    }
}
- (void)dealloc{
    NSLog(@"%@ 已经dealloc",NSStringFromClass(self.class));
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ALPHANUM] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if (_isChina) {
        return YES;
    }
    return [string isEqualToString:filtered];
}
@end
