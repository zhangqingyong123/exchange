//
//  SearchCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}
- (void)get_up
{
    [self.contentView addSubview:self.symbolLable];
    [self.contentView addSubview:self.searchimageView];
    
}
- (void)setModle:(HomeModle *)modle
{
    _modle = modle;
    NSArray *array = [modle.name componentsSeparatedByString:@"_"];
    _symbolLable.text = [NSString stringWithFormat:@"%@ / %@",array[0],array[1]];
    
    if ([[EXUnit getCollectionData] containsObject:modle.name]) {
        
        _searchimageView.image = [UIImage imageNamed:@"title-mark01"];;
    }else
    {
       _searchimageView.image = [UIImage imageNamed:@"title-mark02"];;
    }
}
- (UILabel *)symbolLable
{
    if (!_symbolLable) {
        _symbolLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20),RealValue_W(100)/2 -13,RealValue_W(300), 26)];
        _symbolLable.textColor =  MAINTITLECOLOR1;
        _symbolLable.font = AutoBoldFont(15);

    }
    return _symbolLable;
}
- (UIImageView *)searchimageView
{
    if (!_searchimageView) {
        _searchimageView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(20) - RealValue_W(28), RealValue_W(100 -28)/2, RealValue_W(28), RealValue_W(28))];
        _searchimageView.image = [UIImage imageNamed:@"title-mark02"];
        
    }
    return _searchimageView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
