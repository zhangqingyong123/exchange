//
//  SymbolClass.h
//  Exchange
//
//  Created by 张庆勇 on 2018/11/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SymbolClass : NSObject
@property (nonatomic,strong) NSString * ID;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * unique_id;
@property (nonatomic,strong) NSString * shorthand;
@property (nonatomic,strong) NSString * display_order;
@property (nonatomic,strong) NSString * site_id;
@end

