//
//  QuotationViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//
#import "QuotationViewController.h"
#import "MDMultipleSegmentView.h"
#import "MDFlipCollectionView.h"
#import "HomeModle.h"
#import "CollectionViewController.h"
#import "CNTQViewController.h"
#import "SMCustomSegment.h"
#import "SymbolClass.h"
#import "SecarSybolViewController.h"
@interface QuotationViewController ()<SMCustomSegmentDelegate,MDMultipleSegmentViewDeletegate,MDFlipCollectionViewDelegate>
@property(nonatomic,strong) SMCustomSegment *segment;
@property(nonatomic,strong) MDMultipleSegmentView *segView;
@property(nonatomic,strong) MDFlipCollectionView *collectView;
@property(nonatomic,strong)NSMutableArray * titles;
@property(nonatomic,strong)NSMutableArray * ViewControllers;
@property(nonatomic,strong)NSMutableArray * dataArray;
@property(nonatomic,strong)NoNetworkView * workView;
@property(nonatomic,strong)NSMutableArray * symbolArray; //板块数组
@property(nonatomic,strong)NSMutableArray * symbolNameArray; //板块名数组
@end
@implementation QuotationViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self axcBaseAddRightBtnWithImage:[UIImage imageNamed:@"home_search"]];
    [self.titles addObject:Localized(@"quotation_selection")];
    [self.ViewControllers addObject:[[CollectionViewController alloc]init]];
    [self.symbolNameArray addObject:Localized(@"transaction_all")];
    self.navigationItem.title = Localized(@"main_quotation");
    [self showLoadingAnimation];
}
- (void)viewDidAppear:(BOOL)animated
{
        if ( ![AppDelegate shareAppdelegate].isNoWork) {
            
            [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"transaction_Network") view:kWindow];
        }else
        {
            if (_titles.count ==1)
            {
                NSArray * dataArray = [NSArray bg_arrayWithName:FMDBName_Symbol];
                self.dataArray = [NSMutableArray arrayWithArray:dataArray];
                [self getSymbolClass];

            }
        }
}
- (void)getSymbolClass
{
    [self.symbolNameArray removeAllObjects];
    WeakSelf
    [HomeViewModel getSymbolClassWithDic:@{} url:symbolClass callBack:^(BOOL isSuccess, id callBack) {
        if (isSuccess) {
            [weakSelf stopLoadingAnimation];
            weakSelf.symbolArray = callBack;
            for (SymbolClass * symbol in weakSelf.symbolArray) {
                [self.symbolNameArray addObject:symbol.name];
            }
            if (self.symbolNameArray.count>1) {
                [self initSegment];
            }
            [self getData];
        }
    } ];
}
- (void)initSegment
{
    _segment =  [[SMCustomSegment alloc] initWithFrame:CGRectMake(20, 0, UIScreenWidth - 40, 30) titleArray:self.symbolNameArray];
    _segment.selectIndex = 0;
    _segment.delegate = self;
    _segment.normalBackgroundColor = MAINBLACKCOLOR;
    _segment.selectBackgroundColor = [ZBLocalized sharedInstance].TabBgColor;
    _segment.titleNormalColor = MAINTITLECOLOR1;
    _segment.titleSelectColor = [UIColor whiteColor];
    _segment.normalTitleFont = 14;
    _segment.selectTitleFont = 14;
    _segment.borderWidth = 0.5;
    _segment.centerX = self.view.centerX;
    [self.view addSubview:_segment];
}
- (void)customSegmentSelectIndex:(NSInteger)selectIndex
{
    [_titles removeAllObjects];
    [_ViewControllers removeAllObjects];
    [self.titles addObject:Localized(@"quotation_selection")];
    [self.ViewControllers addObject:[[CollectionViewController alloc]init] ];
    if (selectIndex ==0) {

        [self getData];
    }else
    {
      SymbolClass * symbol = _symbolArray[selectIndex-1];
      [self getDataClass:symbol];
    }
    
}
- (void)getData
{
    NSMutableArray * restrs = [[NSMutableArray alloc] init];
    for (HomeModle * modle in  self.dataArray)
    {
        [restrs addObject:modle.quote_asset];
    }
    for (NSString *str in restrs) {
        if (![self.titles containsObject:str]) {
            [self.titles addObject:str];
            [self.ViewControllers addObject:[[CNTQViewController alloc]init]];
        }
    }
    [self initTest:10086];
    
}
- (void)getDataClass:(SymbolClass *)Symbol
{
    NSMutableArray * restrs = [[NSMutableArray alloc] init];
    for (HomeModle * modle in  self.dataArray)
    {
        if (modle.classes.count > 0)
        {
            
            for (NSDictionary * dic in modle.classes) {
                if ([dic[@"symbol_class_id"] integerValue] ==Symbol.ID.integerValue)
                {
                    [restrs addObject:modle.quote_asset];
                }
            }
            
        }
        
    }
    for (NSString *str in restrs) {
        if (![self.titles containsObject:str]) {
            [self.titles addObject:str];
            [self.ViewControllers addObject:[[CNTQViewController alloc]init]];
        }
        
    }
    if (self.titles.count>1) {
         [self initTest:Symbol.ID.integerValue];
    }else
    {
        [self removePageView];
    }
   
    
}
// 移除CurrencyPageView
- (void)removePageView
{
    for (UIView *view in self.view.subviews) {
        if (view.tag == 10086 || view.tag == 10011) {
            [UIView animateWithDuration:0.2 animations:^{
                view.hidden = YES;
                [view removeFromSuperview];
            }];
            
        }
    }
}
- (void )initTest:(NSInteger)ID {
     [self removePageView];
    CGFloat segViewWidth;
    NSInteger pages;
    if (_titles.count>4) {
        segViewWidth = UIScreenWidth;
        pages = 5;
    }else
    {
        segViewWidth = _titles.count * UIScreenWidth/5;
        pages = _titles.count;
    }
    _segView = [[MDMultipleSegmentView alloc] initWithFrame:CGRectMake(0, _segment.bottom, segViewWidth, RealValue_H(88)) ItemsPerPage:pages Type:6];
    _segView.delegate =  self;
    _segView.items = _titles;
    _segView.titleNormalColor = MAINTITLECOLOR1;
    _segView.titleSelectColor = MAINTITLECOLOR;
    _segView.centerX = self.view.centerX;
    if (_titles.count>1)
    {
        [_segView selectIndex:1];
    }else
    {
         [_segView selectIndex:0];
    }

    [self.view addSubview:_segView];
    
    _collectView = [[MDFlipCollectionView alloc] initWithFrame:CGRectMake(0,
                                                                          _segView.bottom,
                                                                          UIScreenWidth,
                                                                          UIScreenHeight- RealValue_H(88) - kStatusBarAndNavigationBarHeight - 49 - IPhoneBottom - _segment.height) withArray:_ViewControllers];
    _collectView.delegate = self;
    
    if (_titles.count>1)
    {
        [_collectView selectIndex:1];
    }else
    {
       [_collectView selectIndex:0];
    }
    _collectView.titles = _titles;
    _collectView.sourceArray = _dataArray;
    
    if (self.symbolNameArray.count>1) {
        _collectView.isSymbolClass = YES;
    }
    _collectView.ID = ID;
    [self.view addSubview:_collectView];
    _segView.tag = 10086;
    _collectView.tag = 10011;
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(0 ,_segView.bottom +1)];
    [bezierPath addLineToPoint:CGPointMake(UIScreenWidth ,_segView.bottom+1)];
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = CELLCOLOR.CGColor;
    shapeLayer.fillColor  = [UIColor clearColor].CGColor;
    shapeLayer.path = bezierPath.CGPath;
    shapeLayer.lineWidth = 0.5f;
    [self.view.layer addSublayer:shapeLayer];
}
- (void)changeSegmentAtIndex:(NSInteger)index
{
    [_collectView selectIndex:index];
}
- (void)flipToIndex:(NSInteger)index
{
    [_segView selectIndex:index];
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
- (NSMutableArray *)titles
{
    if (!_titles) {
        _titles = [[NSMutableArray alloc] init];
    }
    return _titles;
}

- (NSMutableArray *)ViewControllers
{
    if (!_ViewControllers) {
        _ViewControllers = [[NSMutableArray alloc] init];
    }
    return _ViewControllers;
}
- (NSMutableArray *)symbolArray
{
    if (!_symbolArray) {
        _symbolArray = [[NSMutableArray alloc] init];
    }
    return _symbolArray;
}
- (NSMutableArray *)symbolNameArray
{
    if (!_symbolNameArray) {
        _symbolNameArray = [[NSMutableArray alloc] init];
    }
    return _symbolNameArray;
}
//搜索
- (void)axcBaseClickBaseRightImageBtn:(UIButton *)sender{
    SecarSybolViewController * secarVc = [[SecarSybolViewController alloc] init];
    [self.navigationController pushViewController:secarVc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
