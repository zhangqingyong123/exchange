//
//  CurrencyMarketViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface CurrencyMarketViewController : BaseViewViewController
@property (nonatomic,strong)NSArray * dataArray;
@property (nonatomic,assign)NSInteger  ID;
@property (nonatomic,assign)BOOL  isSymbolClass;
@end
