//
//  CollectionViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CollectionViewController.h"
#import "HomeTableViewCell.h"
#import "Y_StockChartViewController.h"
@interface CollectionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,assign)CGFloat segmentHeigh;
/*筛选三种变量 0表示默认 1表示生 2表示降*/
@property (nonatomic,assign) NSInteger variable;
@property (nonatomic,assign) NSInteger variable1;
@property (nonatomic,assign) NSInteger variable2;
@end

@implementation CollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _variable = 0;
    _variable1 = 0;
    _variable2 = 0;
    [self.view addSubview:self.tableView];
    [self reconnect];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(GetCollection) name:CollectionNotificationse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateBPrice:) name:updateSymbolNotificationse object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(valuationMethod) name:ValuationMethodNotificationse object:nil];
    
}
- (void)valuationMethod
{
    [self.tableView reloadData];
}
- (void)UpdateBPrice:(NSNotification *)message
{
    
    // 处理耗时操作的代码块...
    HomeModle * modle = message.object;
    for (int i=0; i<self.dataSource.count; i++)
    {
        HomeModle *homeModle = self.dataSource[i];
        if ([homeModle.name isEqualToString:modle.name])
        {
            homeModle.deal = modle.deal;
            homeModle.volume = modle.volume;
            homeModle.high = modle.high;
            homeModle.open = modle.open;
            homeModle.last = modle.last;
            homeModle.low = modle.low;
            homeModle.increase = [NSString stringWithFormat:@"%.2f",(modle.last.floatValue - modle.open.floatValue)/modle.open.floatValue];
            homeModle.IncreaseDegree =  [NSString stringWithFormat:@"%.2f%%",(modle.last.floatValue - modle.open.floatValue)/modle.open.floatValue * 100];
            homeModle.currency = modle.currency;
            // 回到主队列刷新UI
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            });
            
            break;
        }
    }
   
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)GetCollection
{
    [self.dataSource removeAllObjects];
    [self reconnect];
    [self.tableView reloadData];

}
- (void)reconnect
{
    NSMutableArray * array = [ EXUnit getCollectionData];
    for (HomeModle * modle in self.dataArray)
    {
        if ([array containsObject:modle.name])
        {
            if (self.ID==10086) {
                 [self.dataSource addObject:modle];
            }else
            {
                if (modle.classes.count > 0)
                {
//                    NSDictionary * dic = modle.classes[0];
                    for (NSDictionary * dic in modle.classes) {
                        if ([dic[@"symbol_class_id"] integerValue] ==self.ID)
                        {
                            [self.dataSource addObject:modle];
                        }
                    }
                    
                }
                
            }
           
          
        }
    }
    if (self.dataSource.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:_tableView.frame NoNetwork:NO];
        WeakSelf
        _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
            [weakSelf reconnect];
        };
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        if (!self.isSymbolClass)
        {
            _segmentHeigh  = 0;
        }else
        {
            _segmentHeigh  = 35;
        }
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  UIScreenWidth,
                                                                  UIScreenHeight- RealValue_H(88) - kStatusBarAndNavigationBarHeight - TABBAR_HEIGHT1 - IPhoneBottom - _segmentHeigh)
                                                 style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.rowHeight = RealValue_W(130);
        _tableView.estimatedRowHeight = RealValue_W(130);
        _tableView.estimatedSectionHeaderHeight = RealValue_W(60);
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[HomeTableViewCell class] forCellReuseIdentifier:@"HomeTableViewCell"];
        
        
    }
    return _tableView;
}
- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return _dataSource;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"HomeTableViewCell"];
    
    cell.modle = self.dataSource[indexPath.row];
    return cell;
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *BgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth,RealValue_W(152))];
    BgView.backgroundColor = TABLEVIEWLCOLOR;
    
    UIButton * currencyButton = [[UIButton alloc] initWithFrame:CGRectMake(20,0,  RealValue_W(160), RealValue_W(60))];
    [currencyButton setTitle:Localized(@"home_symbol") forState:UIControlStateNormal];
    switch (_variable) {
        case 0:
            [currencyButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].NormalImage] forState:UIControlStateNormal];
            break;
        case 1:
            [currencyButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].SelectedImage] forState:UIControlStateNormal];
            break;
        case 2:
            [currencyButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].Selected1Image] forState:UIControlStateNormal];
            break;
            
        default:
            
            
            break;
    }
    
    [currencyButton addTarget:self action:@selector(currency:) forControlEvents:(UIControlEventTouchUpInside)];
    [currencyButton setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
    currencyButton.titleLabel.font = AutoFont(11);
    currencyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [currencyButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - currencyButton.imageView.image.size.width -RealValue_W(20), 0, currencyButton.imageView.image.size.width)];
    [currencyButton setImageEdgeInsets:UIEdgeInsetsMake(0, currencyButton.titleLabel.bounds.size.width, 0, -currencyButton.titleLabel.bounds.size.width)];
    [BgView addSubview:currencyButton];
    
    UIButton * rangeButton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - 80 -10 ,0,  70, RealValue_W(60))];
    [rangeButton setTitle:Localized(@"home_high_low")forState:UIControlStateNormal];
    switch (_variable2) {
        case 0:
            [rangeButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].NormalImage] forState:UIControlStateNormal];
            break;
        case 1:
            [rangeButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].SelectedImage] forState:UIControlStateNormal];
            break;
        case 2:
            [rangeButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].Selected1Image] forState:UIControlStateNormal];
            break;
            
        default:
            
            
            break;
    }
    [rangeButton addTarget:self action:@selector(range:) forControlEvents:(UIControlEventTouchUpInside)];
    [rangeButton setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
    rangeButton.titleLabel.font = AutoFont(11);
    rangeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [rangeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - rangeButton.imageView.image.size.width, 0, rangeButton.imageView.image.size.width)];
    [rangeButton setImageEdgeInsets:UIEdgeInsetsMake(0, rangeButton.titleLabel.bounds.size.width, 0, -rangeButton.titleLabel.bounds.size.width-RealValue_W(20))];
    [BgView addSubview:rangeButton];
    
    UIButton * priceButton = [[UIButton alloc] initWithFrame:CGRectMake(rangeButton.left - 43/2 -110 ,0,  100, RealValue_W(60))];
    [priceButton setTitle:Localized(@"home_new_price") forState:UIControlStateNormal];
    switch (_variable1) {
        case 0:
            [priceButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].NormalImage] forState:UIControlStateNormal];
            break;
        case 1:
            [priceButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].SelectedImage] forState:UIControlStateNormal];
            break;
        case 2:
            [priceButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].Selected1Image] forState:UIControlStateNormal];
            break;
            
        default:
            
            
            break;
    }
    [priceButton addTarget:self action:@selector(price:) forControlEvents:(UIControlEventTouchUpInside)];
    [priceButton setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
    priceButton.titleLabel.font = AutoFont(11);
    priceButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [priceButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - priceButton.imageView.image.size.width, 0, priceButton.imageView.image.size.width)];
    [priceButton setImageEdgeInsets:UIEdgeInsetsMake(0, priceButton.titleLabel.bounds.size.width, 0, -priceButton.titleLabel.bounds.size.width-RealValue_W(20))];
    [BgView addSubview:priceButton];
    
   
    return BgView;
}
/*交易对首字母筛选*/
- (void)currency:(UIButton *)button
{
    if (_variable ==1)
    {
        _variable = 2;
        
        NSArray * array =  [EXUnit jiangPxuWith:self.dataSource];
        self.dataSource  = [NSMutableArray arrayWithArray:array];
        
    }else
    {
        _variable =1;
        NSArray * array =  [EXUnit paixuWith:self.dataSource];
        self.dataSource  = [NSMutableArray arrayWithArray:array];
    }
    _variable1 = 0;
    _variable2 = 0;
    [self.tableView reloadData];
    
}
/*交易对最新价格筛选*/
- (void)price:(UIButton *)button
{
    if (_variable1 ==1)
    {
        _variable1 = 2;
       
        NSArray * array = [EXUnit priceshengxuWith:self.dataSource];
        self.dataSource = [NSMutableArray arrayWithArray:array];
        
    }else
    {
        _variable1 =1;
       
        NSArray * array = [EXUnit pricejiangxuWith:self.dataSource];
        self.dataSource = [NSMutableArray arrayWithArray:array];
    }
    _variable = 0;
    _variable2 = 0;
    [self.tableView reloadData];
    
}
/*交易对幅度筛选*/
- (void)range:(UIButton *)button
{
    if (_variable2 ==1)
    {
        _variable2 = 2;
      
        NSArray * array = [EXUnit shengxuWith:self.dataSource];
        self.dataSource  = [NSMutableArray arrayWithArray:array];
        
    }else
    {
        _variable2 =1;
        
       
        NSArray * array = [EXUnit jiangxuWith:self.dataSource];
        self.dataSource  = [NSMutableArray arrayWithArray:array];
    }
    _variable1 = 0;
    _variable = 0;
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeModle *modle = self.dataSource[indexPath.row];
    
    Y_StockChartViewController * Y_StockChartVC = [[Y_StockChartViewController alloc] init];
    Y_StockChartVC.modle = modle;
     Y_StockChartVC.isleftPush = NO;
    [self.navigationController pushViewController:Y_StockChartVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
