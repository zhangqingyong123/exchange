//
//  Y_KineDataView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/29.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "Y_KineDataView.h"

@implementation Y_KineDataView
- (instancetype)init
{
    self = [super init];
    if (self) {
   
        self.backgroundColor = RGBA(16, 22, 36, 1);
        self.layer.masksToBounds     =YES;
        self.layer.borderWidth       =0.5;
        self.layer.cornerRadius      =6;
        self.contentMode             =UIViewContentModeScaleAspectFill;
        self.layer.borderColor       =[RGBA(220, 220, 220, 0.7) CGColor];
        UILabel * lable = [self private_createLabel];
        lable.text = Localized(@"hone_time");
        [lable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(3);
            make.top.equalTo(@5);
            make.height.equalTo(@20);
            make.width.equalTo(@36);
        }];

        
        _dateTimeLable = [self private_createLabel];
        _dateTimeLable.textAlignment = NSTextAlignmentRight;
        [_dateTimeLable mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable.mas_top);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];

        UILabel * lable1 = [self private_createLabel];
        lable1.text = Localized(@"Symbol_open");
        [lable1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@36);
        }];

        _openLabel = [self private_createLabel];
        _openLabel.textAlignment = NSTextAlignmentRight;
        [_openLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable1.mas_top);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];

        UILabel * lable2 = [self private_createLabel];
        lable2.text =Localized(@"Symbol_high");
        [lable2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable1.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@36);
        }];

        _highLabel = [self private_createLabel];
        _highLabel.textAlignment = NSTextAlignmentRight;
        [_highLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable2.mas_top);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];

        UILabel * lable3 = [self private_createLabel];
        lable3.text =Localized(@"Symbol_low");
        [lable3 mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable2.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@36);
        }];

        _lowLabel = [self private_createLabel];
        _lowLabel.textAlignment = NSTextAlignmentRight;
        [_lowLabel mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable2.mas_bottom);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];
        UILabel * lable4 = [self private_createLabel];
        lable4.text =Localized(@"Symbol_close");
        [lable4 mas_makeConstraints:^(MASConstraintMaker *make) {

           make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable3.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@40);
        }];

        _closeLabel = [self private_createLabel];
        _closeLabel.textAlignment = NSTextAlignmentRight;
        [_closeLabel mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable3.mas_bottom);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];

        UILabel * lable5 = [self private_createLabel];
        lable5.text =Localized(@"home_rise");
        [lable5 mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable4.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@40);
        }];

        _rangeLable = [self private_createLabel];
        _rangeLable.textAlignment = NSTextAlignmentRight;
        [_rangeLable mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable4.mas_bottom);
            make.height.equalTo(@18);
            make.width.equalTo(@74);
        }];

        UILabel * lable6 = [self private_createLabel];
        lable6.text = [NSString stringWithFormat:@"%@",Localized(@"hone_bright")];
        [lable6 mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_left);
            make.top.equalTo(lable5.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@40);
        }];
        _volLable = [self private_createLabel];
         _volLable.textAlignment = NSTextAlignmentRight;
        [_volLable mas_makeConstraints:^(MASConstraintMaker *make) {

            make.left.equalTo(lable.mas_right).offset(2);
            make.top.equalTo(lable5.mas_bottom);
            make.height.equalTo(@20);
            make.width.equalTo(@74);
        }];
        
        
    }
    return self;
}
+(instancetype)view
{
    Y_KineDataView *MAView = [[Y_KineDataView alloc]init];
    
    return MAView;
}
- (void)getData:(Y_KLineModel *)model lineKTime:(NSInteger )lineKTime
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:model.Date.doubleValue/1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    if (lineKTime == 0 || lineKTime == 4) {
        formatter.dateFormat = @"HH:mm";
    }
    else if (lineKTime == 3 ||lineKTime == 9 || lineKTime == 8 ){
        formatter.dateFormat = @"YYYY MM-dd";
    }
    else{
        formatter.dateFormat = @"MM-dd HH:mm";
    }
    NSString *dateStr = [formatter stringFromDate:date];
    _dateTimeLable.text = [@" " stringByAppendingString: dateStr];
    
    UIColor *color = (model.Close.doubleValue - model.Open.doubleValue) >= 0 ? [UIColor increaseColor] :[UIColor decreaseColor];
    _rangeLable.textColor = _closeLabel.textColor = _lowLabel.textColor = _highLabel.textColor = _openLabel.textColor = color;
    _openLabel.text = [CommonMethod calculateWithRoundingMode:NSRoundPlain roundingValue:model.Open.floatValue  afterPoint:model.price];
    _highLabel.text = [CommonMethod calculateWithRoundingMode:NSRoundPlain roundingValue:model.High.floatValue  afterPoint:model.price];
    _lowLabel.text = [CommonMethod calculateWithRoundingMode:NSRoundPlain roundingValue:model.Low.floatValue  afterPoint:model.price];
    _closeLabel.text = [CommonMethod calculateWithRoundingMode:NSRoundPlain roundingValue:model.Close.floatValue  afterPoint:model.price];
    _volLable.text = [CommonMethod calculateWithRoundingMode:NSRoundPlain roundingValue:model.Volume  afterPoint:model.coin];
    NSString * IncreaseDegree = [NSString stringWithFormat:@"%.2f%%",(model.Close.floatValue - model.Open.floatValue)/model.Open.floatValue * 100];
    float _x = IncreaseDegree.floatValue;
    if (!isnan(_x)) {
        if (model.Close.floatValue - model.Open.floatValue >=0) {
            _rangeLable.text = [NSString stringWithFormat:@"+ %.2f%%",_x];
        }else
        {
            _rangeLable.text = [NSString stringWithFormat:@"%.2f%%",_x];
        }
        
    }else
    {
        _rangeLable.text = @"0.00%";
    }
}

- (UILabel *)private_createLabel
{
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:10];
    label.textColor = [UIColor whiteColor];
    label.adjustsFontSizeToFitWidth = YES;
    [self addSubview:label];
    return label;
}
@end
