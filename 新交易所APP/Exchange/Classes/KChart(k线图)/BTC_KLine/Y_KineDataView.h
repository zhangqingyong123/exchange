//
//  Y_KineDataView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/29.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Y_KLineModel.h"
@interface Y_KineDataView : UIView
@property(nonatomic,strong)UILabel * dateTimeLable;
@property(nonatomic,strong)UILabel * openLabel;
@property(nonatomic,strong)UILabel * highLabel;
@property(nonatomic,strong)UILabel * closeLabel;
@property(nonatomic,strong)UILabel * lowLabel;
@property(nonatomic,strong)UILabel * volLable;
@property(nonatomic,strong)UILabel * rangeLable;
- (void)getData:(Y_KLineModel *)model lineKTime:(NSInteger )lineKTime;
+(instancetype)view;
@end
