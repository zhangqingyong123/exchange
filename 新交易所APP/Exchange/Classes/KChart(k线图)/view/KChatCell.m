//
//  KChatCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "KChatCell.h"

@implementation KChatCell
    
- (void)awakeFromNib {
    [super awakeFromNib];
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor backgroundColor];
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}
- (void)setModle:(ChatModle *)modle
{
    _modle = modle;
    if ([modle.type isEqualToString:@"buy"])
    {
        _purchasePriclable.text = Localized(@"hone_Purchase");
        _purchasePriclable.textColor = ZHANGCOLOR;
        
    }else
    {
        _purchasePriclable.text =  Localized(@"hone_sell");
        _purchasePriclable.textColor = DIEECOLOR;
    }
    _purchaselable.text =  [EXUnit getTimeFromNOYearTimestamp:modle.time];
    _sellOutlable.text = [EXUnit formatternumber:_homemodle.quote_asset_precision.intValue assess:_modle.price];
    _sellOutnumberlable.text = [EXUnit formatternumber:_homemodle.base_asset_precision.intValue assess:_modle.amount];
    
    
}
    
- (void)get_up
{
    _purchaselable = [[UILabel alloc] init];
    
    _purchaselable.font = SystemBlodFont(12);
    _purchaselable.textColor =WHITECOLOR;
    [self.contentView addSubview:self.purchaselable];
    
    _purchasePriclable = [[UILabel alloc] init];
    
    _purchasePriclable.font = SystemBlodFont(12);
    _purchasePriclable.textColor =WHITECOLOR;
    _purchasePriclable.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.purchasePriclable];
    
    _sellOutlable = [[UILabel alloc] init];
    
    _sellOutlable.font = SystemBlodFont(12);
    _sellOutlable.textColor =WHITECOLOR;
    [self.contentView addSubview:self.sellOutlable];
    _sellOutnumberlable = [[UILabel alloc] init];
    
    _sellOutnumberlable.font = SystemBlodFont(12);
    _sellOutnumberlable.textColor =WHITECOLOR;
    _sellOutnumberlable.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.sellOutnumberlable];
    
    [self.purchaselable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.contentView.mas_left).with.offset(10);
        make.centerY.mas_equalTo(self.contentView.centerY).with.offset(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(18);
    }];
    
    [self.purchasePriclable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.centerY.mas_equalTo(self.contentView.centerY).with.offset(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(self.contentView.centerX).with.offset(-102);
    }];
    
    [self.sellOutlable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.centerY.mas_equalTo(self.contentView.centerY).with.offset(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(self.contentView.centerX).with.offset(32);
    }];
    
    [self.sellOutnumberlable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.right.mas_equalTo(self.contentView.right).with.offset(-10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(18);
        make.centerY.mas_equalTo(self.contentView.centerY).with.offset(0);
    }];
  
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
    
    @end
@implementation AssetInfoCell
    
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor backgroundColor];
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    return self;
    
}
    //数据
- (void)setInfoModle:(AssetInfo *)infoModle
    {
        _infoModle = infoModle;
        if ([infoModle.titleStr isEqualToString:Localized(@"quotation_Official")]||[infoModle.titleStr isEqualToString:Localized(@"quotation_white_paper")]|[infoModle.titleStr isEqualToString:Localized(@"quotation_Block_query")])
        {
            if (![NSString isEmptyString:infoModle.contontStr]) {
                _contentlable.userInteractionEnabled = YES;
            }
            UILongPressGestureRecognizer *touch = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [_contentlable addGestureRecognizer:touch];
            
        }else
        {
            _contentlable.userInteractionEnabled = NO;
            _contentlable.textColor = WHITECOLOR;
        }
        _leftlable.text = infoModle.titleStr;
        _contentlable.text = infoModle.contontStr;
        
    }
-(void)handleTap:(UIGestureRecognizer*) recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"111");
        return;
    }else if (recognizer.state == UIGestureRecognizerStateBegan){
        NSLog(@"222");
        [self becomeFirstResponder];
        UIMenuItem * item = [[UIMenuItem alloc]initWithTitle:Localized(@"copy") action:@selector(newFunc)];
        [[UIMenuController sharedMenuController] setTargetRect:CGRectMake(_contentlable.centerX +_contentlable.width/2, _contentlable.mj_y, 0, 0) inView:self];
        [UIMenuController sharedMenuController].menuItems = @[item];
        [UIMenuController sharedMenuController].menuVisible = YES;
    }
}
-(void)newFunc{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = _infoModle.contontStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
}
-(BOOL)canBecomeFirstResponder {
    
    return YES;
}
    
    // 可以响应的方法
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(newFunc)) {
        return YES;
    }
    return NO;
}
    
- (void)setAssetInfoModle:(AssetInfo *)assetInfoModle
    {
        _titlelable.text = @"简介";
        _assetInfolable.frame = CGRectMake(15, 0 ,UIScreenWidth - 30, 0);
        if (![NSString isEmptyString:assetInfoModle.contontStr]) {
            _assetInfolable.text = assetInfoModle.contontStr;
            _assetInfolable.mj_y = _titlelable.bottom +10;
            [_assetInfolable sizeToFit];
        }
        
    }
-(void)get_up{
    
    [self.contentView addSubview:self.leftlable];
    [self.contentView addSubview:self.contentlable];
    [self.contentView addSubview:self.titlelable];
    [self.contentView addSubview:self.assetInfolable];
}
- (UILabel *)leftlable
    {
        if (!_leftlable) {
            _leftlable = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, 30)];
            _leftlable.textColor = ColorStr(@"#6E6E8A");
            _leftlable.font = [UIFont systemFontOfSize:12];
        }
        return _leftlable;
    }
- (UILabel *)contentlable
    {
        if (!_contentlable) {
            _contentlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 270, 0, 260, 30)];
            _contentlable.textColor = WHITECOLOR;
            _contentlable.font = [UIFont systemFontOfSize:12];
            _contentlable.textAlignment = NSTextAlignmentRight;
        }
        return _contentlable;
    }
- (UILabel *)titlelable
    {
        if (!_titlelable) {
            _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
            _titlelable.textColor = ColorStr(@"#6E6E8A");;
            _titlelable.font = [UIFont systemFontOfSize:12];
        }
        return _titlelable;
    }
- (UILabel *)assetInfolable
    {
        if (!_assetInfolable) {
            _assetInfolable = [[UILabel alloc] initWithFrame:CGRectMake(15, _titlelable.bottom +10, UIScreenWidth - 30, 0)];
            _assetInfolable.textColor = WHITECOLOR;
            _assetInfolable.font = [UIFont systemFontOfSize:12];
            _assetInfolable.numberOfLines = 0;
            [_assetInfolable sizeToFit];
        }
        return _assetInfolable;
    }
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
    
    @end
