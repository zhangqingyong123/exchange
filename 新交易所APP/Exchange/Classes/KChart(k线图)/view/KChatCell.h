//
//  KChatCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatModle.h"
#import "HomeModle.h"
@interface KChatCell : UITableViewCell
@property (nonatomic,strong)UILabel * purchaselable;
@property (nonatomic,strong)UILabel * purchasePriclable;
@property (nonatomic,strong)UILabel * sellOutlable;
@property (nonatomic,strong)UILabel * sellOutnumberlable;
@property (nonatomic,strong)ChatModle * modle;
@property (nonatomic,strong)ChatModle * sellmodle;
@property (nonatomic,strong)HomeModle * homemodle;
@end
@interface AssetInfoCell : UITableViewCell
@property (nonatomic,strong)UILabel * leftlable;
@property (nonatomic,strong)UILabel * contentlable;
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UILabel * assetInfolable;
@property (nonatomic,strong)AssetInfo * infoModle;
@property (nonatomic,strong)AssetInfo * assetInfoModle;
@end
