//
//  Y_ChartHeadView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/30.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Y_ChartTableHeadView : UIView
@property (nonatomic,strong)UILabel * priceLab;
@property (nonatomic,strong)UILabel * cnyPriceLab;
@property (nonatomic,strong)UILabel * rangeLab;
@property (nonatomic,strong)UILabel * highLab;
@property (nonatomic,strong)UILabel * lowLab;
@property (nonatomic,strong)UILabel * volumeLab;

@property (nonatomic,strong)UILabel * highValueLab;
@property (nonatomic,strong)UILabel * lowValueLab;
@property (nonatomic,strong)UILabel * volumeValueLab;
- (void)reloadData:(HomeModle *)modle;
@end
@interface AssetInfoTableHeadView : UIView
@property (nonatomic,strong)UIView * bgView;
@property (nonatomic,strong)UILabel * assetLab;
@property (nonatomic,strong)UILabel * abbreviationLab;
@end
