//
//  Y_ChartHeadView.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/30.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "Y_ChartTableHeadView.h"

@implementation Y_ChartTableHeadView
    
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self get_up];
        self.backgroundColor = [UIColor backgroundColor];
    }
    return self;
    
}
- (void)reloadData:(HomeModle *)modle
    {
        
        WeakSelf
        dispatch_async(dispatch_get_main_queue(), ^{
            
            int baseprecision = modle.base_asset_precision.intValue;
            int precision = modle.quote_asset_precision.intValue;
            
            NSString * price = [NSString stringWithFormat:@"%@ ",[EXUnit formatternumber:precision assess:modle.last]];
            float _x = modle.IncreaseDegree.floatValue;
            if (!isnan(_x)) {
                if (modle.last.floatValue - modle.open.floatValue >=0) {
                    weakSelf.rangeLab.text = [NSString stringWithFormat:@"+ %.2f%%",modle.IncreaseDegree.floatValue];
                }else
                {
                    weakSelf.rangeLab.text = [NSString stringWithFormat:@"%.2f%%",modle.IncreaseDegree.floatValue];
                }
            }else
            {
                weakSelf.rangeLab.text = @"0.00%";
            }
            CGSize size = [weakSelf.rangeLab sizeThatFits:CGSizeMake(100, MAXFLOAT)];
            weakSelf.rangeLab.frame =CGRectMake(weakSelf.priceLab.mj_x, weakSelf.priceLab.bottom +10, size.width +4, 14);
            weakSelf.rangeLab.textAlignment = NSTextAlignmentCenter;
            if (modle.last.floatValue - modle.open.floatValue >=0) {
                weakSelf.priceLab.textColor =ZHANGCOLOR;
                weakSelf.priceLab.attributedText = [EXUnit getcontent:price image:@"k_zhang" index:price.length imageframe:CGRectMake(0, 1, 6, 16)];
                
                weakSelf.rangeLab.backgroundColor = ZHANGCOLOR;
            }else
            {
                weakSelf.priceLab.textColor =DIEECOLOR;
                weakSelf.priceLab.attributedText = [EXUnit getcontent:price image:@"k_dieline" index:price.length imageframe:CGRectMake(0, 1, 6, 16)];
                
                
                weakSelf.rangeLab.backgroundColor = DIEECOLOR;
            }
            
            weakSelf.cnyPriceLab.centerY = weakSelf.rangeLab.centerY;
            weakSelf.cnyPriceLab.mj_x = weakSelf.rangeLab.right +10;
            weakSelf.cnyPriceLab.text = [EXUnit getPriceCurrencyWith:modle];
            
            weakSelf.highValueLab.text = [EXUnit formatternumber:precision assess:modle.high];
            weakSelf.lowValueLab.text = [EXUnit formatternumber:precision assess:modle.low];
            weakSelf.volumeValueLab.text = [EXUnit formatternumber:baseprecision assess:modle.volume];
            weakSelf.highLab.text = Localized(@"hone_high");
            weakSelf.lowLab.text = Localized(@"hone_bottom");
            weakSelf.volumeLab.text = [NSString stringWithFormat:@"24H"];
            CGSize highsize = [weakSelf.highValueLab sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
            CGSize lowsize = [weakSelf.lowValueLab sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
            CGSize volumesize = [weakSelf.volumeValueLab sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
            
            NSArray *array = @[@(highsize.width),@(lowsize.width),@(volumesize.width)];
            NSArray *result = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                
                if ([obj1 floatValue] < [obj2 floatValue])
                {
                    return NSOrderedDescending;
                }
                else
                {
                    return NSOrderedAscending;
                }
                
                
            }];
            CGFloat lableW = [result[0] doubleValue] +4;
            weakSelf.highValueLab.mj_x =  weakSelf.lowValueLab.mj_x = weakSelf.volumeValueLab.mj_x = UIScreenWidth - lableW - 10;
            weakSelf.highValueLab.width = weakSelf.lowValueLab.width = weakSelf.volumeValueLab.width = lableW;
            weakSelf.highValueLab.mj_y = 10;
            weakSelf.lowValueLab.mj_y = weakSelf.highValueLab.bottom +10;
            weakSelf.volumeValueLab.mj_y = weakSelf.lowValueLab.bottom +10;
            weakSelf.highValueLab.height = weakSelf.lowValueLab.height = weakSelf.volumeValueLab.height = 20;
            weakSelf.highLab.mj_x =  weakSelf.lowLab.mj_x =  weakSelf.volumeLab.mj_x = weakSelf.highValueLab.left-60;
            
            
        });
        
       
        
}
- (void)get_up
{
    [self addSubview:self.priceLab];
    [self addSubview:self.rangeLab];
    [self addSubview:self.cnyPriceLab];
    [self addSubview:self.highValueLab];
    [self addSubview:self.lowValueLab];
    [self addSubview:self.volumeValueLab];
    [self addSubview:self.highLab];
    [self addSubview:self.lowLab];
    [self addSubview:self.volumeLab];
    
}
-(UILabel *)priceLab
{
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 200, 30)];
        _priceLab.textColor = MAINTITLECOLOR;
        _priceLab.font = SystemBlodFont(24);
        
        
    }
    return _priceLab;
}
-(UILabel *)rangeLab
{
    if (!_rangeLab) {
        _rangeLab = [[UILabel alloc] init];
        _rangeLab.textColor = ZHANGCOLOR;
        _rangeLab.font = SystemFont(9);
        _rangeLab.numberOfLines = 0;
        [_rangeLab sizeToFit];
        _rangeLab.textColor = [UIColor whiteColor];
    }
    return _rangeLab;
}
-(UILabel *)cnyPriceLab
{
    if (!_cnyPriceLab) {
        _cnyPriceLab = [[UILabel alloc] initWithFrame:CGRectMake(10, _priceLab.bottom +6, 120, 20)];
        _cnyPriceLab.textColor = MAINTITLECOLOR1;
        _cnyPriceLab.font = SystemBlodFont(8);
        
        
    }
    return _cnyPriceLab;
}
-(UILabel *)highValueLab
{
    if (!_highValueLab) {
        _highValueLab = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 180-10, 10, 180, 20)];
        _highValueLab.textColor = [UIColor whiteColor];
        _highValueLab.font = SystemBlodFont(12);
        _highValueLab.textAlignment = NSTextAlignmentRight;
        _highValueLab.numberOfLines = 0;
    }
    return _highValueLab;
}
-(UILabel *)lowValueLab
{
    if (!_lowValueLab) {
        _lowValueLab = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 180-10, 10, 180, 20)];
        _lowValueLab.textColor = [UIColor whiteColor];
        _lowValueLab.font = SystemBlodFont(12);
        _lowValueLab.textAlignment = NSTextAlignmentRight;
        _lowValueLab.numberOfLines = 0;
    }
    return _lowValueLab;
}
-(UILabel *)volumeValueLab
{
    if (!_volumeValueLab) {
        _volumeValueLab = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 180-10, 10, 180, 20)];
        _volumeValueLab.textColor = [UIColor whiteColor];
        _volumeValueLab.font = SystemBlodFont(12);
        _volumeValueLab.textAlignment = NSTextAlignmentRight;
        _volumeValueLab.numberOfLines = 0;
    }
    return _volumeValueLab;
}
-(UILabel *)highLab
{
        if (!_highLab) {
            _highLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 50, 20)];
            _highLab.textColor = MAINTITLECOLOR1;
            _highLab.font = SystemBlodFont(12);
            _highLab.textAlignment = NSTextAlignmentLeft;
        }
        return _highLab;
}
-(UILabel *)lowLab
{
    if (!_lowLab) {
        _lowLab = [[UILabel alloc] initWithFrame:CGRectMake(0,_highLab.bottom + 10, 50, 20)];
        _lowLab.textColor =MAINTITLECOLOR1;
        _lowLab.font = SystemBlodFont(12);
        
        _lowLab.textAlignment = NSTextAlignmentLeft;
    }
    return _lowLab;
}
-(UILabel *)volumeLab
{
    if (!_volumeLab) {
        _volumeLab = [[UILabel alloc] initWithFrame:CGRectMake(0,_lowLab.bottom + 10, 60, 20)];
        _volumeLab.textColor = MAINTITLECOLOR1;
        _volumeLab.font = SystemBlodFont(12);
        _volumeLab.textAlignment = NSTextAlignmentLeft;
        
    }
    return _volumeLab;
}
@end


@implementation AssetInfoTableHeadView
    
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self get_up];
        self.backgroundColor = [UIColor backgroundColor];
    }
    return self;
    
}
- (void)get_up
{
    [self addSubview:self.bgView];
    [_bgView addSubview:self.assetLab];
    [_bgView addSubview:self.abbreviationLab];
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(15, 19, UIScreenWidth -30, 74)];
        _bgView.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_bgView cornerRadius:2];
        _bgView.backgroundColor = RGBA(50, 50, 71, 1);
    }
    return _bgView;
}
- (UILabel *)assetLab
{
    if (!_assetLab) {
        _assetLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 23, _bgView.width -20, 20)];
        _assetLab.textColor = [UIColor whiteColor];
        _assetLab.font = AutoBoldFont(18);
        _assetLab.textAlignment = NSTextAlignmentCenter;
    }
    return _assetLab;
}
- (UILabel *)abbreviationLab
{
    if (!_abbreviationLab) {
        _abbreviationLab = [[UILabel alloc] initWithFrame:CGRectMake(10, _assetLab.bottom, _bgView.width -20, 20)];
        _abbreviationLab.textColor = ColorStr(@"#7A7A99");
        _abbreviationLab.font = [UIFont systemFontOfSize:12];
        _abbreviationLab.textAlignment = NSTextAlignmentCenter;
    }
    return _abbreviationLab;
}
@end
