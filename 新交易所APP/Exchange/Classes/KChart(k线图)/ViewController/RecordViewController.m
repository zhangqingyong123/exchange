//
//  RecordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/11/16.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "RecordViewController.h"
#import "KChatCell.h"
@interface RecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation RecordViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor backgroundColor];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,
                                                              0,
                                                              UIScreenWidth,
                                                              UIScreenHeight - kStatusBarAndNavigationBarHeight - 50 -IPhoneBottom  -44)style:UITableViewStylePlain];
    
    _tableView.tableFooterView = [UIView new];
 
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = RealValue_H(75);
    _tableView.backgroundColor =   [UIColor backgroundColor];
    _tableView.separatorColor = CLEARCOLOR;
    [_tableView registerClass:[KChatCell class] forCellReuseIdentifier:@"KChatCell"];
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return _dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        KChatCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"KChatCell"];
        cell.homemodle = _modle;
     
        cell.modle = _dataArray[indexPath.row];
    
       return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 25)];
    view.backgroundColor = [UIColor backgroundColor];

//    买入量
    UILabel *purchaselable = [UILabel new];
    purchaselable.text = Localized(@"hone_time");
    purchaselable.font = SystemFont(10);
    purchaselable.textColor =WHITECOLOR;
    [view addSubview:purchaselable];
    [purchaselable mas_makeConstraints:^(MASConstraintMaker *make) {

        make.left.mas_equalTo(view.mas_left).with.offset(10);
        make.centerY.mas_equalTo(view.centerY).with.offset(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(18);
    }];
    //    买入价
    UILabel *purchasePriclable = [UILabel new];
    purchasePriclable.text = Localized(@"hone_direction");
    purchasePriclable.font = SystemFont(10);
    purchasePriclable.textColor =WHITECOLOR;
    purchasePriclable.textAlignment = NSTextAlignmentRight;
    [view addSubview:purchasePriclable];

    [purchasePriclable mas_makeConstraints:^(MASConstraintMaker *make) {


        make.centerY.mas_equalTo(view.centerY).with.offset(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(view.centerX).with.offset(-106);
    }];
    NSArray *array = [_modle.name componentsSeparatedByString:@"_"];

    //    卖出价
    UILabel *sellOutlable = [UILabel new];
    sellOutlable.text = [NSString stringWithFormat:@"%@(%@)",Localized(@"hone_price"),array[1]];
    sellOutlable.font = SystemFont(10);
    sellOutlable.textColor =WHITECOLOR;
    [view addSubview:sellOutlable];
    [sellOutlable mas_makeConstraints:^(MASConstraintMaker *make) {


        make.centerY.mas_equalTo(view.centerY).with.offset(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(view.centerX).with.offset(32);
    }];
    //    卖出量
    UILabel *sellOutnumberlable = [UILabel new];
    sellOutnumberlable.text = [NSString stringWithFormat:@"%@(%@)",Localized(@"hone_num"),array[0]];
    sellOutnumberlable.font = SystemFont(10);
    sellOutnumberlable.textColor =WHITECOLOR;
    sellOutnumberlable.textAlignment = NSTextAlignmentRight;
    [view addSubview:sellOutnumberlable];

    [sellOutnumberlable mas_makeConstraints:^(MASConstraintMaker *make) {


        make.right.mas_equalTo(view.right).with.offset(-10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(18);
        make.centerY.mas_equalTo(view.centerY).with.offset(0);
    }];
    return view;
}


@end
