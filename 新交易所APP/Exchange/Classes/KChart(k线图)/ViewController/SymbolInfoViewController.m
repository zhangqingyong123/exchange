//
//  SymbolInfoViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/11/16.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SymbolInfoViewController.h"
#import "KChatCell.h"
#import "ChatModle.h"
#import "Y_ChartTableHeadView.h"
@interface SymbolInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) AssetInfoTableHeadView *tableViewHeadView;
@end

@implementation SymbolInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor backgroundColor];
    CGFloat height;
    if (self.allowRotation ) {
        height = UIScreenHeight - kStatusBarAndNavigationBarHeight;
    }else
    {
        height = UIScreenHeight - kStatusBarAndNavigationBarHeight - 50 -IPhoneBottom  -44;
    }
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,
                                                              0,
                                                              UIScreenWidth,
                                                              height)style:UITableViewStylePlain];
    
    _tableView.tableFooterView = [UIView new];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor =  [UIColor backgroundColor];
    _tableView.separatorColor = CLEARCOLOR;
    [_tableView registerClass:[AssetInfoCell class] forCellReuseIdentifier:@"AssetInfoCell"];
    _tableView.tableFooterView = [UIView new];
    _tableView.tableHeaderView = self.tableViewHeadView;
    [self.view addSubview:_tableView];
    
    NSArray *namearray = [self.modle.name componentsSeparatedByString:@"_"];
    self.tableViewHeadView.assetLab.text = namearray[0];
    self.tableViewHeadView.abbreviationLab.text = _data[@"full_name"];
    [self getData:_data];
}
- (AssetInfoTableHeadView *)tableViewHeadView
{
    if (!_tableViewHeadView) {
        _tableViewHeadView = [[AssetInfoTableHeadView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 100)];
    }
    return _tableViewHeadView;
}
#pragma 获取币的详细信息

- (void)getData:(NSDictionary *)dic
{
    NSString * release_time = [EXUnit getLocalDateFormateUTCDate:dic[@"release_time"]];/*发行时间*/
    NSString * release_total = dic[@"release_total"];/*发行总数*/
    NSString * circulation_total = dic[@"circulation_total"];/*流通总量*/
    NSString * official_website = dic[@"official_website"];/*官网*/
    NSString * white_paper = dic[@"white_paper"];/*白皮书*/
    NSString * block_query = dic[@"block_query"];/*区块查询*/
    NSString * erc = dic[@"erc"];/*ERC20合约*/
    NSString * recommend_organization = dic[@"recommend_organization"];/*推荐机构*/
    NSString * desc = dic[@"desc"];/*简介*/
    NSArray * array = @[@{@"left":Localized(@"quotation_Release_time"),@"content":release_time },
                        @{@"left":Localized(@"quotation_Release_amount"),@"content":release_total },
                        @{@"left":Localized(@"quotation_circulation_amount"),@"content":circulation_total },
                        @{@"left":Localized(@"quotation_Official"),@"content":official_website },
                        @{@"left":Localized(@"quotation_white_paper"),@"content":white_paper },
                        @{@"left":Localized(@"quotation_Block_query"),@"content":block_query },
                        @{@"left":Localized(@"quotation_ERC"),@"content":[NSString isEmptyString:erc]?@"":erc },
                        @{@"left":Localized(@"quotation_Recommendation"),@"content":recommend_organization},
                        @{@"left":Localized(@"quotation_introduction"),@"content":desc},
                        ];
    for (NSDictionary * dic in array)
    {
        AssetInfo * info = [[AssetInfo alloc] init];
        info.titleStr = dic[@"left"];
        info.contontStr = dic[@"content"];
        [self.dataArray addObject:info];
    }
    [self.tableView reloadData];
    
    
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
        
    }
    return _dataArray;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row ==self.dataArray.count-1)
    {
        AssetInfoCell* priceCell = (AssetInfoCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return priceCell.assetInfolable.height + 80;
    }else
    {
        return 30;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetInfoCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"AssetInfoCell"];
  
    
    if (indexPath.row !=_dataArray.count -1)
    {
        cell.infoModle=_dataArray[indexPath.row];
        cell.titlelable.hidden = YES;
        cell.assetInfolable.hidden = YES;
        cell.leftlable.hidden = NO;
        cell.contentlable.hidden = NO;
    }else
    {
        cell.assetInfoModle=_dataArray[indexPath.row];
        cell.titlelable.hidden = NO;
        cell.assetInfolable.hidden = NO;
        cell.leftlable.hidden = YES;
        cell.contentlable.hidden = YES;
    }
    return cell;
    
}
@end
