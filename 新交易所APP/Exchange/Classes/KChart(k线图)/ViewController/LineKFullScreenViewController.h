//
//  CFKLineFullScreenViewController.h
//
//  Created by Zhimi on 2018/9/3.
//  Copyright © 2018年 hexuren. All rights reserved.
//

#import "StoryboardLoader.h"
#import "BaseViewViewController.h"
@interface LineKFullScreenViewController : BaseViewViewController <StoryboardLoader>

@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (copy, nonatomic) void (^ onClickBackButton)(LineKFullScreenViewController *controller);
@property(nonatomic,strong)HomeModle * modle;
@end
