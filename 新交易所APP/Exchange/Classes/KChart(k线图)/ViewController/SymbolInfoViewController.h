//
//  SymbolInfoViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/11/16.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SegmentPageBaseViewController.h"


@interface SymbolInfoViewController : SegmentPageBaseViewController
@property (nonatomic,strong)HomeModle * modle;
@property (nonatomic,assign)BOOL allowRotation;
@property (nonatomic,strong)NSDictionary * data;
@end

