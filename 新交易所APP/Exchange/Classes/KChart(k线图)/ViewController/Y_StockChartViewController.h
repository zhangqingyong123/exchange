//
//  YStockChartViewController.h
//  BTC-Kline
//
//  Created by yate1996 on 16/4/27.
//  Copyright © 2016年 yate1996. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewViewController.h"
#import "HomeModle.h"
@interface Y_StockChartViewController : BaseViewViewController
@property (nonatomic,strong)HomeModle * modle;
@property (nonatomic,assign)BOOL isHomePush;
@property (nonatomic,assign)BOOL isleftPush;
@end
