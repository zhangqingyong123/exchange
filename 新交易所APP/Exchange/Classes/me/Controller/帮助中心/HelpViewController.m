//
//  HelpViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "HelpViewController.h"
#import "HelpCell.h"
#import "HomeModle.h"
#import "NewsInfoViewController.h"

@interface HelpViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * newsArray;

@property (nonatomic, assign)NSInteger         pages;
//1是已提交，2是成功，3是失败，4是审核中
@end


@implementation HelpViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
     self.pages = 0;
    [self.view addSubview:self.tableView];
    
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        [weakSelf getClassid:weakSelf.modle];
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getClassid:weakSelf.modle];
    }];
    self.tableView.mj_footer = fooder;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight - RealValue_W(88) -20)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[HelpCell class] forCellReuseIdentifier:@"HelpCell"];
    }
    return _tableView;
}

- (void)getClassid:(HelpModle *)modle
{
    _modle = modle;
   WeakSelf
    [self GETWithHost:@"" path:articleList param:@{@"type":@"help",@"class_id":modle.id,@"offset":@(_pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.newsArray addObjectsFromArray:[NewsModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.newsArray = [NewsModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
         
            [weakSelf.tableView reloadData];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (NSMutableArray *)newsArray
{
    if (!_newsArray) {
        _newsArray = [[NSMutableArray alloc]init];
    }
    return _newsArray;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(100);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HelpCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"HelpCell"];
    NewsModle * modle = _newsArray[indexPath.row];
    cell.titleLable.text = modle.title;
    if (_newsArray.count ==1)
    {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bgView.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft|UIRectCornerTopRight  cornerRadii:CGSizeMake(2, 2)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame =  cell.bgView.bounds;
        maskLayer.path = maskPath.CGPath;
        cell.bgView.layer.mask = maskLayer;
        
    }else
    {
        if (indexPath.row==0)
        {
            
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bgView.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(4, 4)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame =  cell.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.bgView.layer.mask = maskLayer;
        }else if (indexPath.row ==_newsArray.count-1)
        {
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bgView.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(4, 4)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame =  cell.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.bgView.layer.mask = maskLayer;
        }else
        {
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bgView.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft|UIRectCornerTopRight  cornerRadii:CGSizeMake(0, 0)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame =  cell.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.bgView.layer.mask = maskLayer;
        }
    }
   
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsModle *model = self.newsArray[indexPath.row];
    NewsInfoViewController * NoticeDetailsVC = [[NewsInfoViewController alloc] init];
    NoticeDetailsVC.hidesBottomBarWhenPushed = YES;
    NoticeDetailsVC.title = Localized(@"my_help");
    NoticeDetailsVC.newsID = model.id;
    NoticeDetailsVC.titletext = model.title;
    NoticeDetailsVC.isHelp = YES;
    NoticeDetailsVC.time = [EXUnit getTimeFromTimestamp:model.dateline];
    [self.navigationController pushViewController:NoticeDetailsVC animated:YES];
}
@end
