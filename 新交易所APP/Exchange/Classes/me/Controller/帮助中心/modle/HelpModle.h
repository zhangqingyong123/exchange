//
//  HelpModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/27.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HelpModle : NSObject
@property (nonatomic,strong)NSString * display_order;
@property (nonatomic,strong)NSString * english_name;
@property (nonatomic,strong)NSString * class_id;
@property (nonatomic,strong)NSString * id;
@property (nonatomic,strong)NSString * lang;
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * remark;
@property (nonatomic,strong)NSString * russian_name;
@property (nonatomic,strong)NSString * site_id;
@property (nonatomic,strong)NSString * unique_id;
@end


