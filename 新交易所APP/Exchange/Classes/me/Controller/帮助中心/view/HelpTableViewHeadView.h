//
//  TableViewHeadView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/28.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"



@interface HelpTableViewHeadView : UIView
@property (nonatomic,strong)UIView * topView;

@property (nonatomic,strong)UILabel * titleLable;
@property (nonatomic,strong)UIView * classView;
@property (copy, nonatomic) void(^classBlock)(UIButton * button);
- (void)getClassHelp:(NSArray *)dataArray;
@end


