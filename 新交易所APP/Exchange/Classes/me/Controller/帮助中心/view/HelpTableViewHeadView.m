//
//  TableViewHeadView.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/28.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "HelpTableViewHeadView.h"
#import "HelpModle.h"
@implementation HelpTableViewHeadView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
        [self get_up];
        
    }
    return self;
    
}
- (void)getClassHelp:(NSArray *)dataArray
{
    if (dataArray.count<=3 &&dataArray.count>0) {
        _classView.height = RealValue_H(230);
        [self rankWithTotalColumns:3 andWithAppW:_classView.width/3 andWithAppH:_classView.height dataArray:dataArray row:1];
    }else if (dataArray.count>3 && dataArray.count<=6)
    {
        _classView.height = RealValue_H(230)*2;
        
        [self rankWithTotalColumns:3 andWithAppW:_classView.width/3 andWithAppH:_classView.height/2 dataArray:dataArray row:2];
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(15 , _classView.height/2 -0.5f)];
        [bezierPath addLineToPoint:CGPointMake(_classView.width -15 , _classView.height/2 -0.5f)];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = CELLCOLOR.CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [_classView.layer addSublayer:shapeLayer];
    }else if (dataArray.count>6)
    {
        _classView.height = RealValue_H(230)*3;
        
         [self rankWithTotalColumns:3 andWithAppW:_classView.width/3 andWithAppH:_classView.height/3 dataArray:dataArray row:3];
        for (int i =0 ; i<2; i++) {
            UIBezierPath * bezierPath = [UIBezierPath bezierPath];
            [bezierPath moveToPoint:CGPointMake(15 , _classView.height/3 +i*_classView.height/3 -0.5f)];
            [bezierPath addLineToPoint:CGPointMake(_classView.width -15 , _classView.height/3 +i*_classView.height/3 -0.5f)];
            CAShapeLayer * shapeLayer = [CAShapeLayer layer];
            shapeLayer.strokeColor = CELLCOLOR.CGColor;
            shapeLayer.fillColor  = [UIColor clearColor].CGColor;
            shapeLayer.path = bezierPath.CGPath;
            shapeLayer.lineWidth = 0.5f;
            [_classView.layer addSublayer:shapeLayer];
        }
    }else
    {
        _classView.height = 0;
        _topView.hidden = YES;
    }
    
}

- (void)rankWithTotalColumns:(int)totalColumns andWithAppW:(int)appW andWithAppH:(int)appH  dataArray:(NSArray *)dataArray row:(NSInteger)row{
    //view尺寸
    CGFloat _appW = appW;
    CGFloat _appH = appH;
    
    //横向间隙 (控制器view的宽度 － 列数＊应用宽度)/(列数 ＋ 1)
    CGFloat margin = 0;
    
    //    for (int index = 0; index < self.appsArray.count; index++) {
    for (int index = 0; index < dataArray.count; index++) {
        HelpModle * modle = dataArray[index];
        //创建一个小框框//
        UIButton * button = [[UIButton alloc] init];
        [button setTitle:modle.name forState:0];
        [button setTitleColor:[ZBLocalized sharedInstance].helpClassColor forState:0];
//        [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
        button.tag =index;
        button.titleLabel.font = AutoFont(14);
        [button addTarget:self action:@selector(classclick:) forControlEvents:UIControlEventTouchUpInside];
        [_classView addSubview:button];
        int row = index / totalColumns; //行号为框框的序号对列数取商
        //列号
        int col = index % totalColumns; //列号为框框的序号对列数取余
        CGFloat appX = margin + col * (appW + margin);
        CGFloat appY =  row * (appH + margin);
        button.frame = CGRectMake(appX, appY, _appW, _appH);
        
      
        [_classView addSubview:button];
        

    }
}
- (void)classclick:(UIButton *)button
{
    if (self.classBlock) {
        self.classBlock(button);
    }
}
- (void)get_up
{
    [self addSubview:self.topView];

    [self addSubview:self.classView];
}

- (UIView *)topView
{
    if (!_topView) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(122) )];
        _topView.backgroundColor = [ZBLocalized sharedInstance].helpNavColor;
    }
    return _topView;
}

- (UIView *)classView
{
    if (!_classView) {
        _classView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, UIScreenWidth -30, RealValue_W(460))];
        _classView.backgroundColor = TABLEVIEWLCOLOR;
        
        
    }
    return _classView;
}
@end
