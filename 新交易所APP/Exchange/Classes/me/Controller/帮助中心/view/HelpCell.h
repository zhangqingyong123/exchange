//
//  HelpCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/28.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HelpCell : UITableViewCell
@property (nonatomic,strong)UIView * bgView;
@property (nonatomic,strong)UILabel * titleLable;
@property (nonatomic,strong)UIImageView * returnImg;
@end


