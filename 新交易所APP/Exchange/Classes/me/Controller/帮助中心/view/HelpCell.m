//
//  HelpCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/28.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "HelpCell.h"

@implementation HelpCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.bgView];
    [_bgView addSubview:self.titleLable];
     [_bgView addSubview:self.returnImg];
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, UIScreenWidth -30, RealValue_W(100))];
        _bgView.backgroundColor = TABLEVIEWLCOLOR;
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(15 , _bgView.height -0.5f)];
        [bezierPath addLineToPoint:CGPointMake(_bgView.width -15 , _bgView.height -0.5f)];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = CELLCOLOR.CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [_bgView.layer addSublayer:shapeLayer];
    }
    return _bgView;
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, RealValue_W(30) , _bgView.width - 50, RealValue_W(40))];
        _titleLable.textColor = MAINTITLECOLOR1;
        _titleLable.font = AutoFont(14);
 ;

    }
    return _titleLable;
}
- (UIImageView *)returnImg
{
    if (!_returnImg) {
        _returnImg = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLable.right +8,
                                                                  _bgView.height/2 - RealValue_W(16)/2,
                                                                   RealValue_W(10),
                                                                   RealValue_W(16))];
        _returnImg.image = [UIImage imageNamed:@"icon_list_go"];
    }
    return _returnImg;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
