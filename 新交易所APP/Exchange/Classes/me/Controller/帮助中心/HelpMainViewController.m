//
//  HelpMainViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/3.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "HelpMainViewController.h"
#import "HelpPageView.h"
#import "HelpModle.h"

@interface HelpMainViewController ()
@property(nonatomic,strong) HelpPageView *helpView;
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)NSMutableArray * titles;
@property (nonatomic,strong)NSMutableArray * contontss;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation HelpMainViewController
- (void)viewDidLoad {
    [super viewDidLoad];
     [self getData];
   
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (NSMutableArray *)titles
{
    if (!_titles) {
        _titles = [[NSMutableArray alloc]init];
    }
    return _titles;
}
- (NSMutableArray *)contontss
{
    if (!_contontss) {
        _contontss = [[NSMutableArray alloc]init];
    }
    return _contontss;
}
- (void)getData
{
    WeakSelf
    [self GETWithHost:@"" path:articleClass param:@{@"type":@"help"} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSArray * list = responseObject[@"result"];
            weakSelf.dataAry = [HelpModle mj_objectArrayWithKeyValuesArray:list];
            
            for (HelpModle * modle in weakSelf.dataAry) {
                [self.titles addObject:modle.name];
                [self.contontss addObject:@"HelpViewController"];
            }
            
            if (self.titles.count>0)
            {
                [self.view addSubview:[self test1]];
            }
            [self placeholderViewWithFrame:self.view.frame  NoNetwork:NO];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        [self placeholderViewWithFrame:self.view.frame  NoNetwork:YES];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    WeakSelf
    if (_dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:self.view.frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        
        [self.view addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
- (HelpPageView *)test1 {
    _helpView = [[HelpPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight)
                                          withTitles:self.titles
                                 withViewControllers:self.contontss
                                      withParameters:nil];
    
    _helpView.selectedColor = WHITECOLOR;
    _helpView.unselectedColor = maintitleLCOLOR ;
    _helpView.backgroundColor = MAINBLACKCOLOR;
    _helpView.dataArray = self.dataAry;
    
    return _helpView;
}
@end
