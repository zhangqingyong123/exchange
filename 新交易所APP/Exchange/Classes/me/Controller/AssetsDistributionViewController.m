//
//  AssetsDistributionViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "AssetsDistributionViewController.h"
#import "AssetsCell.h"
#import "AssetsTableViewHeadView.h"
@interface AssetsDistributionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) AssetsTableViewHeadView * tableViewHeadView;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation AssetsDistributionViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [ZBLocalized sharedInstance].assetsCellColor;
    [self.view addSubview:self.tableView];
}
- (void)getZFChart:(NSMutableArray *)dataAry;
{
    _dataAry = dataAry;
    if (_dataAry.count>0) {
         _tableViewHeadView.hidden = NO;
        [_tableViewHeadView getHeadData:_dataAry];
        [self.tableView reloadData];
    }else
    {
        [self placeholderViewWithFrame:self.tableView.bounds NoNetwork:NO];
        _tableViewHeadView.hidden = YES;
    }
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        if ([EXUnit isHideCurrencyPrice]) {
            _workView.titlelable.text = Localized(@"Asset_Chat_Nodata_message");
        }
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
- (UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[MainTouchTableTableView alloc] initWithFrame:CGRectMake(RealValue_W(30), 10, UIScreenWidth - RealValue_W(60), UIScreenHeight - kStatusBarAndNavigationBarHeight - 44  - 50 -10 - IPhoneTop)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 10)];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.tableHeaderView = self.tableViewHeadView;
        [_tableView registerClass:[DistributionCell class] forCellReuseIdentifier:@"DistributionCell"];
        _tableView.layer.masksToBounds     =YES;
        _tableView.layer.cornerRadius      =6;
        _tableView.contentMode             =UIViewContentModeScaleAspectFill;
    }
    return _tableView;
}
- (AssetsTableViewHeadView *)tableViewHeadView
{
    if (!_tableViewHeadView) {
        _tableViewHeadView = [[AssetsTableViewHeadView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth - RealValue_W(60), RealValue_W(360) +40)];
    }
    return _tableViewHeadView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(70);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DistributionCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"DistributionCell"];
    cell.isopen = self.isOpen;
    cell.modle=_dataAry[indexPath.row];
    if (indexPath.row ==0) {
        cell.progress.progressValue = @"100";
        cell.ratioLable.mj_x = cell.progress.right - 40;
    }
    return cell;
}
@end
