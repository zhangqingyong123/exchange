//
//  InvitingModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/17.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvitingModle : NSObject
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * level;
@property (nonatomic,strong)NSString * recommend_user_id;
@property (nonatomic,strong)NSString * recommend_user_name;
@property (nonatomic,strong)NSString * status;
@property (nonatomic,strong)NSString * user_id;
@end
