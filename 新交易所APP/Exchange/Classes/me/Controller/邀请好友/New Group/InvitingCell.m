//
//  InvitingCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/17.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "InvitingCell.h"
#define titleWith (UIScreenWidth-RealValue_W(44))/4
@implementation InvitingCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        
    }
    return self;
    
}
- (void)setModle:(InvitingModle *)modle
{
    NSString * levelStr;
    switch (modle.level.integerValue) {
        case 1:
            levelStr = Localized(@"my_Inviting_one");
            break;
        case 2:
            levelStr = Localized(@"my_Inviting_two");
            break;
        case 3:
            levelStr = Localized(@"my_Inviting_three");
            break;
        default:
            break;
    }
    _leaveLable.text = levelStr;
    _invitingId.text = [NSString stringWithFormat:@"%@",modle.user_id];
    _bInvitingID.text = [NSString stringWithFormat:@"%@",modle.recommend_user_id];
    if (modle.status.integerValue ==0) {
        _stausLable.text = Localized(@"my_Uncertified");
        _stausLable.textColor = TABTITLECOLOR;
    }else if (modle.status.integerValue ==1)
    {
        _stausLable.text = Localized(@"my_Inviting_Certified");
        _stausLable.textColor = MAINTITLECOLOR1;
    }
    
    
    
}
- (void)get_up
{
    [self.contentView addSubview:self.leaveLable];
    [self.contentView addSubview:self.invitingId];
    [self.contentView addSubview:self.bInvitingID];
    [self.contentView addSubview:self.stausLable];
}
- (UILabel *)leaveLable
{
    if (!_leaveLable) {
        _leaveLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_W(90)/2 -10, titleWith, 20)];
        
        _leaveLable.font = AutoFont(13);
        _leaveLable.textColor = MAINTITLECOLOR1;
    }
    return _leaveLable;
}
- (UILabel *)invitingId
{
    if (!_invitingId) {
        _invitingId = [[UILabel alloc] initWithFrame:CGRectMake(_leaveLable.right, RealValue_W(90)/2 -10, titleWith, 20)];
        
        _invitingId.font = AutoFont(13);
        _invitingId.textAlignment = NSTextAlignmentCenter;
        _invitingId.textColor = MAINTITLECOLOR1;
    }
    return _invitingId;
}
- (UILabel *)bInvitingID
{
    if (!_bInvitingID) {
        _bInvitingID = [[UILabel alloc] initWithFrame:CGRectMake(_invitingId.right, RealValue_W(90)/2 -10, titleWith, 20)];
        
        _bInvitingID.font = AutoFont(13);
        _bInvitingID.textAlignment = NSTextAlignmentCenter;
        _bInvitingID.textColor = MAINTITLECOLOR1;
    }
    return _bInvitingID;
}
- (UILabel *)stausLable
{
    if (!_stausLable) {
        _stausLable = [[UILabel alloc] initWithFrame:CGRectMake(_bInvitingID.right, RealValue_W(90)/2 -10, titleWith, 20)];
        
        _stausLable.textAlignment = NSTextAlignmentRight;
        _stausLable.font = AutoFont(13);
        
    }
    return _stausLable;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
