//
//  InvitingCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/17.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InvitingModle.h"
@interface InvitingCell : UITableViewCell
@property (strong, nonatomic)  UILabel *leaveLable;
@property (strong, nonatomic)  UILabel *invitingId;
@property (strong, nonatomic)  UILabel *bInvitingID;
@property (strong, nonatomic)  UILabel *stausLable;
@property (nonatomic,strong)InvitingModle * modle;
@end
