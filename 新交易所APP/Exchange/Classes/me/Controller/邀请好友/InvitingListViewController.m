//
//  InvitingListViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/17.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "InvitingListViewController.h"
#import "InvitingCell.h"
#import "InvitingModle.h"
@interface InvitingListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (assign,nonatomic)NSUInteger    pages;
@property (nonatomic,strong)NoNetworkView * workView;
@end
@implementation InvitingListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        
        [weakSelf getAddreasslist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getAddreasslist];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)getAddreasslist
{
    WeakSelf
    [weakSelf GETWithHost:@"" path:recommendlog param:@{@"offset":@(weakSelf.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataAry addObjectsFromArray:[InvitingModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataAry = [InvitingModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
            [weakSelf.tableView reloadData];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:YES];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    WeakSelf
    if (_dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:_tableView.frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getAddreasslist];
            };
        }
        
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[InvitingCell class] forCellReuseIdentifier:@"InvitingCell"];
    }
    return _tableView;
}

- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_H(52);
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(90);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InvitingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvitingCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.modle=_dataAry[indexPath.row];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat titleWith =(UIScreenWidth-RealValue_W(44))/4;
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(56))];
    view.backgroundColor = MainBGCOLOR;
    NSArray * lables = @[Localized(@"Invitation_relationship"),Localized(@"Invitation_ID"),Localized(@"Invited_ID"),Localized(@"Invitation_state")];
    for (int i =0; i<lables.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20) + titleWith*i, RealValue_W(56)/2 -10, titleWith, 20)];
        lable.text = lables[i];
        lable.font = AutoFont(13);
        lable.textColor = MAINTITLECOLOR;
        [view addSubview:lable];
        if (i == lables.count -1)
        {
            lable.textAlignment = NSTextAlignmentRight;
        }else if (i!=0)
        {
            lable.textAlignment = NSTextAlignmentCenter;
        }
    }
    //    UILabel * line = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), view.height - 0.5f, UIScreenWidth - RealValue_W(40), 0.5f)];
    //    line.backgroundColor =RGBA(240, 240, 240, 0.7);
    //    [view addSubview:line];
    return view;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
