//
//  InvitingFriendsViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/17.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface InvitingFriendsViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *invitingCode;
@property (weak, nonatomic) IBOutlet UILabel *invitingCodeLable;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageVIew;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic)  UIImageView *firestQrcodeImage;
@property (weak, nonatomic) IBOutlet UIImageView *QrcodeImage;
@property (weak, nonatomic) IBOutlet UILabel *copylable;

- (IBAction)copyCode:(UIButton *)sender;

@end
