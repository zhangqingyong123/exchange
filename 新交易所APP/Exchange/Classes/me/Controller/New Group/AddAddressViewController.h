//
//  AddAddressViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "AssetsModle.h"
@interface AddAddressViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UITextField *addreassTextField;
@property (weak, nonatomic) IBOutlet UITextField *addreassTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField *remakTextField;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleL;
@property (weak, nonatomic) IBOutlet UILabel *remakL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
- (IBAction)okButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *okbutton;
- (IBAction)QRbutton:(UIButton *)sender;
@property (nonatomic,strong)AssetsModle * modle;
@property (copy, nonatomic) void(^addRelastblock)(BOOL isAddSuccess);
@end
