//
//  AddressCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModle.h"
@interface AddressCell : UITableViewCell
@property (nonatomic,strong)UILabel * remarkName;
@property (nonatomic,strong)UILabel * address;
@property (nonatomic,strong)UILabel * addressTitle;
@property (nonatomic,strong)UIButton * copyAddressBtn;
@property (nonatomic,strong)AddressModle * modle;
@end
