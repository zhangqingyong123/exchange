//
//  AddressCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(0), 0, RealValue_W(0))];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.remarkName];
    [self.contentView addSubview:self.address];
    [self.contentView addSubview:self.addressTitle];
    [self.contentView addSubview:self.copyAddressBtn];
}
- (void)setModle:(AddressModle *)modle
{
    _modle = modle;
    if (![NSString isEmptyString:modle.remark]) {
         _remarkName.text = modle.remark;
    }else
    {
        _remarkName.text = modle.name;
    }
    NSArray * array = [modle.address componentsSeparatedByString:@":"];
    if (array.count>1)
    {
         _address.text = array[0];
        if (![NSString isEmptyString:array[1]])
        {
            _addressTitle.hidden = NO;
            _addressTitle.text = [NSString stringWithFormat:@"标签：%@",array[1]];
        }
        
    }else
    {
        _address.text = array[0];
    }
}
- (UILabel *)remarkName
{
    if (!_remarkName) {
        _remarkName = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(22), RealValue_W(40), 200, RealValue_W(40))];
        _remarkName.textColor = MAINTITLECOLOR;
        _remarkName.font = AutoFont(15);
        
    }
    return _remarkName;
}
- (UILabel *)address
{
    if (!_address) {
        _address = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(22),_remarkName.bottom, UIScreenWidth - RealValue_W(44 +200) , 40)];
        _address.textColor = MAINTITLECOLOR1;
        _address.font = AutoFont(15);
        _address.numberOfLines = 0;
    }
    return _address;
}
- (UILabel *)addressTitle
    {
        if (!_addressTitle) {
            _addressTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(22),_address.bottom, UIScreenWidth - RealValue_W(44), RealValue_W(40))];
            _addressTitle.textColor = MAINTITLECOLOR1;
            _addressTitle.font = AutoFont(14);
            _addressTitle.hidden = YES;
        }
        return _addressTitle;
    }
- (UIButton *)copyAddressBtn
{
    if (!_copyAddressBtn) {
        _copyAddressBtn = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(UIScreenWidth - RealValue_W(20) -RealValue_W(120), 0,  RealValue_W(100), RealValue_W(40)) title:Localized(@"Asset_Click_Copy_address") backgroundColor:DIEECOLOR titleColor:WHITECOLOR tapAction:^(UIButton *button) {
            
        }];
        _copyAddressBtn.titleLabel.font = AutoFont(11);
        KViewRadius(_copyAddressBtn, 2);
    }
    _copyAddressBtn.centerY = _address.centerY;
    return _copyAddressBtn;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
