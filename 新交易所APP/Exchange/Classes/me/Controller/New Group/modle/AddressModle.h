//
//  addressModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModle : NSObject
@property (nonatomic,strong)NSString *CreatedAt;
@property (nonatomic,strong)NSString *ID;
@property (nonatomic,strong)NSString *UpdatedAt;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,strong)NSString *asset;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *remark;
@property (nonatomic,strong)NSString *user_id;
@end
