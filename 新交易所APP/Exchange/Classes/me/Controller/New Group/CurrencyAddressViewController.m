//
//  CurrencyAddressViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CurrencyAddressViewController.h"
#import "AddressListViewController.h"
#import "WithdrawCashViewController.h"
#import "AssetsModle.h"
#import "CurrencyCell.h"
#import "CheckVersionAlearView.h"
#import "IdentityAuthenticationViewController.h"
@interface CurrencyAddressViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSDictionary * dataDic;
@property (nonatomic,strong)NSMutableArray * dataArray;
@end

@implementation CurrencyAddressViewController
- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self.view addSubview:self.tableView];
    
    [self getassets];
}
- (void)getassets
{
    WeakSelf
    [self GETWithHost:@"" path:assets param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            weakSelf.dataDic= responseObject[@"result"];
            NSArray * array = [NSMutableArray arrayWithArray:weakSelf.dataDic.allKeys];
            for (NSString * str in array) {
                NSString *recharge_status  =self.dataDic[str][@"withdraw_status"];
                NSDictionary *dic = weakSelf.dataDic[str];
                if (recharge_status.integerValue ==1) {
                    AssetsModle * modle = [[AssetsModle alloc] init];
                    modle.name =str;
                    modle.has_memo = dic[@"has_memo"];
                    [weakSelf.dataArray addObject:modle];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight- kStatusBarAndNavigationBarHeight)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[CurrencyCell class] forCellReuseIdentifier:@"CurrencyCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrencyCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"CurrencyCell"];
    AssetsModle * asset = self.dataArray[indexPath.row];
    cell.titlelable.text = asset.name;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetsModle * asset = self.dataArray[indexPath.row];
    NSString *recharge_status  =self.dataDic[asset.name][@"withdraw_status"];
    
    if (recharge_status.integerValue ==1)
    {
        if (_isWithdraw) {
            AssetsModle * modle;
            for (AssetsModle * assetsModle in _dataArray) {
                if ([assetsModle.name isEqualToString:asset.name]) {
                    modle = assetsModle;
                }
            }
            WithdrawCashViewController *  WithdrawVC = [[WithdrawCashViewController alloc] init];
            WithdrawVC.modle = modle;
            WithdrawVC.dataArray = self.dataArray;
            [self.navigationController pushViewController:WithdrawVC animated:YES];
        }else
        {
            AddressListViewController * addressListVC = [[AddressListViewController alloc] init];
            addressListVC.modle = asset;
            [self.navigationController pushViewController:addressListVC animated:YES];
        }
        
    }else
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Asset_off_shelves") view:kWindow];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
