//
//  QBViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import <AVFoundation/AVFoundation.h>
/**
 *  屏幕 高 宽 边界
 */
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_BOUNDS  [UIScreen mainScreen].bounds

#define TOP (SCREEN_HEIGHT-220)/2
#define LEFT (SCREEN_WIDTH-220)/2

#define kScanRect CGRectMake(LEFT, TOP, 220, 220)
@interface QBViewController : BaseViewViewController
@property (copy, nonatomic) void(^QBrelastblock)(NSString *str);
@end
