//
//  AddAddressViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AddAddressViewController.h"
#import "QBViewController.h"
@interface AddAddressViewController ()
@property (nonatomic,strong)NSString * iban;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * token;
@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBGCOLOR;
    [self customUI];
}
- (void)customUI
{
    [_okbutton setTitle:Localized(@"transaction_OK") forState:0];
    _addressL.textColor = MAINTITLECOLOR;
    _addressL.text = Localized(@"address");
    _addressTitleL.textColor = MAINTITLECOLOR;
    _addressTitleL.text = Localized(@"Address_title");
    _remakL.textColor = MAINTITLECOLOR;
    _remakL.text = Localized(@"address_Remarks");
    _bgView.backgroundColor = textFieldColor;
    _bgView1.backgroundColor = textFieldColor;
    _bgView2.backgroundColor = textFieldColor;
    _okbutton.backgroundColor = TABTITLECOLOR;
    [self initTextField:_addreassTextField placeholder:Localized(@"address_add_message")];
    [self initTextField:_remakTextField placeholder:Localized(@"address_fill_remarks")];
    [self initTextField:_addreassTitleTextField placeholder:Localized(@"Address_title_msg1")];

    if ([[EXUnit isLocalizable] isEqualToString:@"ja"])
    {
        [_addreassTextField setValue:AutoFont(11) forKeyPath:@"_placeholderLabel.font"];
        [_addreassTitleTextField setValue:AutoFont(11) forKeyPath:@"_placeholderLabel.font"];
        [_remakTextField setValue:AutoFont(11) forKeyPath:@"_placeholderLabel.font"];
    }else
    {
        [_addreassTextField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
        [_addreassTitleTextField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
        [_remakTextField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
    }
    KViewRadius(self.okbutton, 2);
    if (_modle.has_memo.integerValue ==0)
    {
        _bgView2.hidden = YES;
        _addressTitleL.hidden = YES;
        [self performSelector:@selector(modifyConstant) withObject:nil afterDelay:0.1];//延迟加载,执行
    }
}
- (void)modifyConstant
{
    self.top.constant -=82;
}
- (void)initTextField:(UITextField *)textField placeholder:(NSString *)placeholder
{
    textField.placeholder = placeholder;
    [textField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    textField.tintColor = TABTITLECOLOR;
    textField.textColor = MAINTITLECOLOR;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)okButton:(UIButton *)sender {
    
    [_addreassTextField resignFirstResponder];
    [_remakTextField resignFirstResponder];
    
    if (![EXUnit judgeAddressLegal:_addreassTextField.text])
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Address_format") view:kWindow];
        return;
    }else if ([NSString isEmptyString:_remakTextField.text])
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"address_fill_remarks") view:kWindow];
        return;
    }
        CustomAlertVC * custom = [[CustomAlertVC alloc] initWithCustomContentViewClass:@"BindCustonAlert" delegate:nil];
        custom.direction = FromBottom;
        [custom show];
        BindCustonAlert *aleatView = (BindCustonAlert *)custom.contentView;
        aleatView.type = @"14";
        aleatView.isBindGoole = NO;
        aleatView.cancelBlock = ^{
            [custom dismiss];
        };
        WeakSelf
        WeakObject(aleatView)
        [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
            
            
            
            if (![NSString isEmptyString:weakaleatView.emailField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.emailField.text]) {
                    [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
            if (![NSString isEmptyString:weakaleatView.phoneField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.phoneField.text]) {
        
                    [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
        NSDictionary * param = @{@"asset":weakSelf.modle.name,
                                     @"name":weakSelf.remakTextField.text,
                                     @"address":weakSelf.addreassTextField.text,
                                     @"remark":@"",
                                     @"memo":[NSString isEmptyString:weakSelf.addreassTitleTextField.text]?@"":[EXUnit removeCharacter:weakSelf.addreassTitleTextField.text],
                                     @"sms_code":[NSString isEmptyString:weakaleatView.phoneField.text]?@"":weakaleatView.phoneField.text,
                                     @"email_code":[NSString isEmptyString:weakaleatView.emailField.text]?@"":weakaleatView.emailField.text,
                                     @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text
                                    };


            [weakSelf POSTWithHost:@"" path:BuildwithdrawAddress param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 [weakaleatView.okButton stopAnimation];
                if ([responseObject[@"code"] integerValue]==0)//成功
                {
                    
                    [custom dismiss];
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                    if (self.addRelastblock) {
                        self.addRelastblock(YES);
                    }
                    [self.navigationController popViewControllerAnimated:YES];

                }else
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                }

            } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                 [weakaleatView.okButton stopAnimation];

            }];

        }];
}
- (IBAction)QRbutton:(UIButton *)sender {
    QBViewController * QBVC = [[QBViewController alloc] init];
    [self presentViewController:QBVC animated:YES completion:nil];
    WeakSelf
    QBVC.QBrelastblock = ^(NSString *str) {
        //        iban:XE38ENF2SS9BZHHT4IAVWWHAS8E00A29M36?amount=0&token=ETH
        //        NSMutableDictionary *dic = [weakSelf getURLParameters:str];
        weakSelf.addreassTextField.text = str;
     
    };
}
/**
 *  截取URL中的参数
 *
 *  @return NSMutableDictionary parameters
 */
- (NSMutableDictionary *)getURLParameters:(NSString *)urlStr {
    
    // 查找参数
    NSRange range = [urlStr rangeOfString:@"?"];
    if (range.location == NSNotFound) {
        return nil;
    }
    
    // 以字典形式将参数返回
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    // 截取参数
    NSString *parametersString = [urlStr substringFromIndex:range.location + 1];
    
    // 判断参数是单个参数还是多个参数
    if ([parametersString containsString:@"&"]) {
        
        // 多个参数，分割参数
        NSArray *urlComponents = [parametersString componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents) {
            // 生成Key/Value
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key = [pairComponents.firstObject stringByRemovingPercentEncoding];
            NSString *value = [pairComponents.lastObject stringByRemovingPercentEncoding];
            
            // Key不能为nil
            if (key == nil || value == nil) {
                continue;
            }
            
            id existValue = [params valueForKey:key];
            
            if (existValue != nil) {
                
                // 已存在的值，生成数组
                if ([existValue isKindOfClass:[NSArray class]]) {
                    // 已存在的值生成数组
                    NSMutableArray *items = [NSMutableArray arrayWithArray:existValue];
                    [items addObject:value];
                    
                    [params setValue:items forKey:key];
                } else {
                    
                    // 非数组
                    [params setValue:@[existValue, value] forKey:key];
                }
                
            } else {
                
                // 设置值
                [params setValue:value forKey:key];
            }
        }
    } else {
        // 单个参数
        
        // 生成Key/Value
        NSArray *pairComponents = [parametersString componentsSeparatedByString:@"="];
        
        // 只有一个参数，没有值
        if (pairComponents.count == 1) {
            return nil;
        }
        
        // 分隔值
        NSString *key = [pairComponents.firstObject stringByRemovingPercentEncoding];
        NSString *value = [pairComponents.lastObject stringByRemovingPercentEncoding];
        
        // Key不能为nil
        if (key == nil || value == nil) {
            return nil;
        }
        
        // 设置值
        [params setValue:value forKey:key];
    }
    
    return params;
}

@end
