//
//  CurrencyCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyCell : UITableViewCell
@property (nonatomic,strong)UIImageView * rightImg;
@property (nonatomic,strong)UILabel * titlelable;
@end
