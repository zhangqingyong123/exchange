//
//  RechargeViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface RechargeViewController : BaseViewViewController
@property (strong,nonatomic) NSString *assetsName;
@property (weak, nonatomic) IBOutlet UIImageView *QRCode;
@property (weak, nonatomic) IBOutlet UILabel *QRCodeLable;
@property (weak, nonatomic) IBOutlet UILabel *QRCodeTitleLable;
@property (weak, nonatomic) IBOutlet UIButton *QRCodebutton;
@property (weak, nonatomic) IBOutlet UIButton *QRCodebutton1;
@property (weak, nonatomic) IBOutlet UIView *BgView;
@property (weak, nonatomic) IBOutlet UILabel *currencyName;
@property (weak, nonatomic) IBOutlet UILabel *explainLable;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *titleMessage;
@property (nonatomic,strong)NSMutableArray * dataArray;
- (IBAction)QRCodebutton:(UIButton *)sender;
- (IBAction)copyAddress:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UILabel *addressTitle;

@end
