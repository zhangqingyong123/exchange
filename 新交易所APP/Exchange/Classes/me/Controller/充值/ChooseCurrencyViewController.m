//
//  ChooseCurrencyViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "ChooseCurrencyViewController.h"
#import "RechargeViewController.h"
#import "CurrencyCell.h"
@interface ChooseCurrencyViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSDictionary * dataDic;
@end

@implementation ChooseCurrencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"Asset_currencies");
    [self.view addSubview:self.tableView];
    [self getassets];
}
- (void)getassets
{
    WeakSelf
    [self GETWithHost:@"" path:assets param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            weakSelf.dataDic= responseObject[@"result"];
             NSArray * array = [NSMutableArray arrayWithArray:weakSelf.dataDic.allKeys];
            for (NSString * str in array) {
                NSString *recharge_status  =self.dataDic[str][@"recharge_status"];
                if (recharge_status.integerValue ==1) {
                    [weakSelf.dataAry addObject:str];
                }
            }
            [weakSelf.tableView reloadData];
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight- kStatusBarAndNavigationBarHeight)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
          [_tableView registerClass:[CurrencyCell class] forCellReuseIdentifier:@"CurrencyCell"];
   
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrencyCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"CurrencyCell"];
    cell.titlelable.text = self.dataAry[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * str = self.dataAry[indexPath.row];
    NSString *recharge_status  =self.dataDic[str][@"recharge_status"];
    if (recharge_status.integerValue ==1)
    {
        RechargeViewController * RechargeVC = [[RechargeViewController alloc] init];
        RechargeVC.assetsName = self.dataAry[indexPath.row];
        RechargeVC.dataArray = self.dataAry;
        [self.navigationController pushViewController:RechargeVC animated:YES];
    }else
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Asset_off_shelves") view:kWindow];
    }
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
