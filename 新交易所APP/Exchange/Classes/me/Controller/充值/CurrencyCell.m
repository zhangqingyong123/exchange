//
//  CurrencyCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CurrencyCell.h"

@implementation CurrencyCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
        [self get_up];
    }
    return self;
}
- (void)get_up
{
    [self.contentView addSubview:self.returnImg];
    [self.contentView addSubview:self.titlelable];

}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(22),  RealValue_H(120)/2 - 15, 240, 30)];
        _titlelable.font = AutoFont(17);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.adjustsFontSizeToFitWidth = YES;
    }
    return _titlelable;
}
- (UIImageView *)returnImg
{
    if (!_rightImg) {
        _rightImg = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(30) - RealValue_W(10),
                                                                   RealValue_H(120)/2 - RealValue_W(16)/2,
                                                                   RealValue_W(10),
                                                                   RealValue_W(16))];
        _rightImg.image = [UIImage imageNamed:@"icon_list_go"];
    }
    return _rightImg;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
