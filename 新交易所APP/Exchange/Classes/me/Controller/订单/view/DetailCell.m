//
//  DetailCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "DetailCell.h"

@implementation DetailCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.leftlable];
    [self.contentView addSubview:self.rightlable];
}
- (UILabel *)leftlable
{
    if (!_leftlable) {
        _leftlable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(40), 120, RealValue_W(40))];
        _leftlable.textColor = MAINTITLECOLOR1;
        _leftlable.font = AutoFont(15);
        
    }
    return _leftlable;
}
- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(30) -200, RealValue_W(40), 200, RealValue_W(40))];
        _rightlable.textColor = MAINTITLECOLOR;
        _rightlable.font = AutoFont(15);
        _rightlable.textAlignment = NSTextAlignmentRight;
        
    }
    return _rightlable;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
