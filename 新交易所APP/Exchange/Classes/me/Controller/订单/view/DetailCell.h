//
//  DetailCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdersModle.h"
@interface DetailCell : UITableViewCell
@property (nonatomic,strong)UILabel * leftlable;
@property (nonatomic,strong)UILabel * rightlable;

@end
