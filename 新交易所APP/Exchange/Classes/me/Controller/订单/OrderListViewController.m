//
//  OrderListViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "OrderListViewController.h"
#import "OrderPageView.h"
@interface OrderListViewController ()
@property(nonatomic,strong) OrderPageView *pageView;
@end

@implementation OrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:[self test1]];
}
- (OrderPageView *)test1 {
    _pageView = [[OrderPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight)
                                                withTitles:@[Localized(@"Current_entrustment"),Localized(@"Historical_records")]
                                       withViewControllers:@[@"OrderEntrustmentViewController",@"OrderHistoricalViewController"]
                                            withParameters:nil];
    
    _pageView.selectedColor = WHITECOLOR;
    _pageView.unselectedColor = maintitleLCOLOR ;
    _pageView.backgroundColor = MAINBLACKCOLOR;
  
    return _pageView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
