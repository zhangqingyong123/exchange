//
//  OrdeDealsViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "OrdeDealsViewController.h"
#import "OrdersModle.h"
#import "DetailCell.h"
@interface OrdeDealsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray; //全部数组
@property (assign, nonatomic) NSUInteger         pages;
@property (strong, nonatomic) NSArray *  leftTitles;
@end

@implementation OrdeDealsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self.view addSubview:self.tableView];
    _leftTitles = @[Localized(@"Transaction_price"),Localized(@"hone_num"),Localized(@"Turnover"),Localized(@"Asset_Service_Charge")];
    // 上拉加载
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        
        [weakSelf datalist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf datalist];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)datalist
{
    WeakSelf
    [weakSelf POSTWithHost:@"" path:orderDeals param:@{@"order_id":_orderId,@"offset":@(weakSelf.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
               
                [weakSelf.dataArray addObjectsFromArray:[OrdersModle mj_objectArrayWithKeyValuesArray:list]];
                
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
               
                
            }else
            {
                weakSelf.dataArray = [OrdersModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
              
            }
            
              [weakSelf.tableView reloadData];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    }];
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight  - IPhoneBottom) style:(UITableViewStyleGrouped)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[DetailCell class] forCellReuseIdentifier:@"DetailCell"];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.000001)];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(120);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
    
    cell.leftlable.text = _leftTitles[indexPath.row];
    OrdersModle * modle = _dataArray[indexPath.section];
    switch (indexPath.row) {
        case 0:
            cell.rightlable.text = [NSString stringWithFormat:@"%@ %@",modle.price, _makes[1]];
            break;
        case 1:
            cell.rightlable.text =[NSString stringWithFormat:@"%@ %@",modle.amount,_makes[0]];
            break;
        case 2:
            cell.rightlable.text = [NSString stringWithFormat:@"%@ %@",modle.deal, _makes[1]];
            break;
        case 3:
            
            cell.rightlable.text =[NSString stringWithFormat:@"%@ %@",modle.fee, _isSell?_makes[1]:_makes[0]];
            break;
        default:
            break;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_W(60);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    OrdersModle * modle = _dataArray[section];
    UIView * bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(60))];
    bottomView.backgroundColor = TABLEVIEWLCOLOR;
   
    UILabel * leftlable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(10), 200, RealValue_W(40))];
    leftlable.textColor = RGBA(63, 92, 107, 1);
    leftlable.font = AutoFont(12);
    leftlable.text = [EXUnit getTimeFromTimestamp:modle.time];
    [bottomView addSubview:leftlable];
    
//    UILabel * line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 1)];
//    line.backgroundColor = CELLCOLOR;
//    [bottomView addSubview:line];
    return bottomView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
