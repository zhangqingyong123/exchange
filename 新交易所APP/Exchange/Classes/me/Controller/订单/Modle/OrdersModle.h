//
//  ordersModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrdersModle : NSObject
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * deal;
@property (nonatomic,strong)NSString * fee;
@property (nonatomic,strong)NSString * id;
@property (nonatomic,strong)NSString * price;
@property (nonatomic,strong)NSString * role;
@property (nonatomic,strong)NSString * time;
@property (nonatomic,strong)NSString * user;
@end

