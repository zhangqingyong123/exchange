//
//  OrdeDealsViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface OrdeDealsViewController : BaseViewViewController
@property (nonatomic,strong)NSString * orderId;
@property (nonatomic,strong)NSArray * makes;
@property (nonatomic,assign)BOOL  isSell;
@end
