//
//  OrderEntrustmentViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/31.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "OrderEntrustmentViewController.h"
#import "JGPopView.h"
#import "TransactionCell.h"
#import "TranOrder.h"
@interface OrderEntrustmentViewController ()<selectIndexPathDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray; //全部数组
@property (nonatomic, strong) NSMutableArray * sellArray; //卖出数组
@property (nonatomic, strong) NSMutableArray * buyArray; //买入数组
@property (nonatomic, strong) NSMutableArray * titlesArray; //币种数组
@property (nonatomic, strong) NSString * market;
@property (nonatomic, strong) NSString * titlelable;
@property (nonatomic, strong) NSString * aMaket;
@property (nonatomic, assign) BOOL  isChooseMarket;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation OrderEntrustmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SRWebSocketDidOpen) name:kWebSocketDidOpenNote object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SRWebSocketDidReceiveMsg:) name:kWebSocketdidReceiveMessageNote object:nil];
    for (NSString * str in [AppDelegate shareAppdelegate].allCurrency) {
        NSArray *array = [str componentsSeparatedByString:@"_"];
        [self.titlesArray addObject:[NSString stringWithFormat:@"%@ / %@",array[0],array[1]]];
    }
    _market = [AppDelegate shareAppdelegate].allCurrency.firstObject;
    _aMaket = self.titlesArray.firstObject;
    _titlelable = Localized(@"transaction_all");
    [self getOrderQueryWithMaket:_market];//获取当前委托
    _Type = ALLType;
    [self.view addSubview:self.tableView];
    
 
}
- (void)SRWebSocketDidOpen
{
    
}
- (void)getOrderQueryWithMaket:(NSString *)maket
{
    NSArray * dataArray1  = @[maket,@0,@100];
    NSData *data1 = [EXUnit NSJSONSerializationWithmethod:@"order.query" parameter:dataArray1 id:1892];
    [[SocketRocketUtility instance] sendData:data1];    // 发送数据
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight - RealValue_W(88) - IPhoneBottom)style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[TransactionCell class] forCellReuseIdentifier:@"TransactionCell"];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.000001)];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (NSMutableArray *)sellArray
{
    if (!_sellArray) {
        _sellArray = [[NSMutableArray alloc]init];
    }
    return _sellArray;
}
- (NSMutableArray *)buyArray
{
    if (!_buyArray) {
        _buyArray = [[NSMutableArray alloc]init];
    }
    return _buyArray;
}
- (NSMutableArray *)titlesArray
{
    if (!_titlesArray) {
        _titlesArray = [[NSMutableArray alloc]init];
    }
    return _titlesArray;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(188);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (_Type) {
        case PurchaseType:
            return self.buyArray.count;
            break;
        case SellOutType:
            return self.sellArray.count;
            break;
        case ALLType:
            return self.dataArray.count;
            break;
        default:
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_W(108);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TransactionCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"TransactionCell"];
            if (_Type == PurchaseType) {
                 cell.orderModle = self.buyArray[indexPath.row];
                WeakSelf
                [cell.revokebutton add_BtnClickHandler:^(NSInteger tag) {
                    
                    [weakSelf revokeWith:indexPath array: self.buyArray];
                }];
            }else if (_Type == SellOutType)
            {
                cell.orderModle = self.sellArray[indexPath.row];
                WeakSelf
                [cell.revokebutton add_BtnClickHandler:^(NSInteger tag) {
                    
                    [weakSelf revokeWith:indexPath array:self.sellArray];
                }];
            }else if (_Type == ALLType)
            {
                 cell.orderModle = self.dataArray[indexPath.row];
                WeakSelf
                [cell.revokebutton add_BtnClickHandler:^(NSInteger tag) {
                    
                    [weakSelf revokeWith:indexPath array:self.dataArray];
                }];
            }
   
    return cell;
}
- (void)revokeWith:(NSIndexPath *)indexPath array:(NSMutableArray *)array
{
    TranOrder * modle = array[indexPath.row];
    if (![EXUserManager isLogin])
    {
         [EXUnit showMessage:Localized(@"login_first")];
        return;
    }
    WeakSelf
    if (![NSString isEmptyString:[EXUserManager userInfo].token]) {
        [self POSTWithHost:@"" path:cancelOrder param:@{@"market":_market,@"order_id":modle.id} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue]==0)
            {
                [array removeObjectAtIndex:indexPath.row];
                [weakSelf.tableView reloadData];
            }else
            {
                 [EXUnit showMessage:responseObject[@"message"]];
              
            }
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        }];
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(108))];
    bottomView.backgroundColor = TABLEVIEWLCOLOR;
   
    UIButton * leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(25), RealValue_W(108)/2 - 15, 60, 30)];
    [leftbutton setTitle:_titlelable forState:(UIControlStateNormal)];
    [leftbutton setImage:[UIImage imageNamed:@"Drop-down"] forState:UIControlStateNormal];
    [leftbutton addTarget:self action:@selector(transactionButton:) forControlEvents:UIControlEventTouchUpInside];
    leftbutton.titleLabel.font = AutoFont(13);
    [leftbutton setTitleColor:RGBA(114, 145, 161, 1) forState:UIControlStateNormal];
    [leftbutton setTitleEdgeInsets:UIEdgeInsetsMake(0, -RealValue_W(30), 0, RealValue_W(30))];
    [leftbutton setImageEdgeInsets:UIEdgeInsetsMake(0, RealValue_W(80), 0, -RealValue_W(80))];
    leftbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;// 水平左对齐
    [bottomView addSubview:leftbutton];

    UIButton * rightbutton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(25) -100, RealValue_W(108)/2 - 15, 100, 30)];
    [rightbutton setTitle:_aMaket forState:(UIControlStateNormal)];
    [rightbutton setImage:[UIImage imageNamed:@"Drop-down"] forState:UIControlStateNormal];
    rightbutton.titleLabel.font = AutoFont(13);
    [rightbutton addTarget:self action:@selector(currencyButton:) forControlEvents:UIControlEventTouchUpInside];
    [rightbutton setTitleColor:RGBA(114, 145, 161, 1) forState:UIControlStateNormal];
    rightbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;// 水平左对齐
    [rightbutton centerHorizontallyImageAndTextWithPadding:-30];
    [bottomView addSubview:rightbutton];
    return bottomView;
}
- (void)popDataWithButton:(UIButton *)button  array:(NSArray *)dataArray PointX:(CGFloat)PointX Width:(CGFloat)Width
{
    
    CGPoint point = CGPointMake(PointX ,RealValue_W(108)/2 +10);
    CGFloat height;
    if (dataArray.count>8)
    {
        height = 8 * RealValue_W(80);
    }else
    {
        height =  dataArray.count * RealValue_W(80);
    }
    JGPopView *view2 = [[JGPopView alloc] initWithOrigin:point Width:Width Height:height Type:JGTypeOfUpRight Color:MAINBLACKCOLOR];
    view2.dataArray = dataArray;
    
    view2.row_height = 40;
    view2.delegate = self;
    [view2 popView];
    [self.view addSubview:view2];
}
//买入卖出
- (void)transactionButton:(UIButton *)button
{
    _isChooseMarket = NO;
    [self popDataWithButton:button array:@[Localized(@"transaction_all"),Localized(@"C2C_buy"),Localized(@"C2C_sell")]PointX:button.right - RealValue_W(30) Width:RealValue_W(140)];
}
- (void)currencyButton:(UIButton *)button
{
     _isChooseMarket = YES;
    CGFloat with;
    with =  RealValue_W(200);
    [self popDataWithButton:button array:_titlesArray PointX:button.right - RealValue_W(10) Width:with];
}
#pragma JGPopViewDelegate
- (void)selectIndexPathRow:(NSInteger)index{
    if (_isChooseMarket) {
        _Type = ALLType;
        [self.dataArray removeAllObjects];
        [self.buyArray removeAllObjects];
        [self.sellArray removeAllObjects];
        _market = [AppDelegate shareAppdelegate].allCurrency[index];
        _aMaket = _titlesArray[index];
        [self getOrderQueryWithMaket:_market];
    }else
    {
        switch (index) {
            case 0: //买入
            {
                _Type = ALLType;
                _titlelable = Localized(@"transaction_all");
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
            case 1: //买入
            {
                _Type = PurchaseType;
                _titlelable = Localized(@"C2C_buy");
                [self.buyArray removeAllObjects];
                for (TranOrder * modle in self.dataArray)
                {
                    if (modle.side.integerValue ==2) {
                        [self.buyArray addObject:modle];
                    }
                }
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                
                break;
            case 2: //卖出
            {
                _Type = SellOutType;
                _titlelable = Localized(@"C2C_sell");
                [self.buyArray removeAllObjects];
                for (TranOrder * modle in self.dataArray)
                {
                    if (modle.side.integerValue ==1) {
                        [self.sellArray addObject:modle];
                    }
                }
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
                
            default:
                break;
        }
    }
   
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
       
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
    
            };
        }
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
- (void)wbsoketSucessRelasWithMessage:(NSString *)message {
    
   
}
- (void)SRWebSocketDidReceiveMsg:(NSNotification *)note {
    //收到服务端发送过来的消息
    NSString * message = note.object;
    if ([NSString isEmptyString:message]) {
        return;
    }
    NSData *jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *message1 = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves  error:nil];
    if ([message1[@"id"] isEqual:[NSNull null]]) {
        return;
    }
    if ([message1[@"id"] integerValue] ==1892)
    {
        if ([message1[@"result"] isEqual:[NSNull null]]) {
            return;
        }
        NSArray * data = message1[@"result"][@"records"];
        self.dataArray = [TranOrder mj_objectArrayWithKeyValuesArray:data];
        NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
        [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        [self placeholderViewWithFrame:CGRectMake(0, RealValue_W(108), _tableView.width, _tableView.height-RealValue_W(108)) NoNetwork:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
