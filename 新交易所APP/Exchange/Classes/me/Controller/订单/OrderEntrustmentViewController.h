//
//  OrderEntrustmentViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/31.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
typedef enum : NSUInteger {
    PurchaseType = 0,//买入
    SellOutType = 1,  //卖出
    ALLType = 2,  //全部
}Status;
@interface OrderEntrustmentViewController : BaseViewViewController
@property (nonatomic,readwrite)Status Type;
@end
