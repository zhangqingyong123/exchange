//
//  AssetsDistributionViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "SegmentPageBaseViewController.h"
#import "MainTouchTableTableView.h"
@interface AssetsDistributionViewController : SegmentPageBaseViewController
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,assign)BOOL isOpen;
- (void)getZFChart:(NSMutableArray *)dataAry;
@end
