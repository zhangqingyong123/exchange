//
//  WithdrawCashViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "AssetsModle.h"
@interface WithdrawCashViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *titleWithdraw;
@property (weak, nonatomic) IBOutlet UILabel *availTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *frozenTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *totalTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *addreasTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *tibiTitle;
@property (weak, nonatomic) IBOutlet UILabel *remakeTitleLable;

@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UILabel *feeTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *accountTitleLable;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UICountingLabel *available;
@property (weak, nonatomic) IBOutlet UICountingLabel *Frozen;
@property (weak, nonatomic) IBOutlet UICountingLabel *Total;
@property (weak, nonatomic) IBOutlet UITextField *addreasField;
@property (weak, nonatomic) IBOutlet UITextField *remakeField;
@property (weak, nonatomic) IBOutlet UITextField *feeField;
@property (weak, nonatomic) IBOutlet UITextField *tibiTitleField;
@property (weak, nonatomic) IBOutlet UILabel *accountsNum;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *bgView3;
@property (weak, nonatomic) IBOutlet UIView *bgView4;
- (IBAction)withdrawButton:(UIButton *)sender;
- (IBAction)allButton:(UIButton *)sender;
- (IBAction)chooseAddress:(UIButton *)sender;
- (IBAction)chooseAddressTitle:(UIButton *)sender;
@property (nonatomic,strong)AssetsModle * modle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@property (nonatomic,strong)NSMutableArray * dataArray;
@end
