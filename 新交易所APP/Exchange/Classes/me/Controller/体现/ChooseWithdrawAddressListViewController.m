//
//  ChooseWithdrawAddressListViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "ChooseWithdrawAddressListViewController.h"
#import "AddressModle.h"
#import "ChooseAddressTableViewCell.h"
#import "AddAddressViewController.h"
@interface ChooseWithdrawAddressListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (assign, nonatomic)NSUInteger    pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation ChooseWithdrawAddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self axcBaseAddRightBtnWithImage:[UIImage imageNamed:@"icon-add"]];
    
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        
        [weakSelf getAddreasslist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getAddreasslist];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)axcBaseClickBaseRightImageBtn:(UIButton *)sender{
    AddAddressViewController * AddAddressVC = [[AddAddressViewController alloc] init];
    AddAddressVC.title = Localized(@"Asset_Add_the_address");
    AddAddressVC.modle = _modle;
    [self.navigationController pushViewController:AddAddressVC animated:YES];
    AddAddressVC.addRelastblock = ^(BOOL isAddSuccess) {
        if (isAddSuccess) {
            self.pages = 0;
            [self getAddreasslist];
        }
    };
}
- (void)getAddreasslist
{
    WeakSelf
    [weakSelf GETWithHost:@"" path:withdrawAddress param:@{@"asset":_modle.name,@"offset":@(weakSelf.pages *10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataAry addObjectsFromArray:[AddressModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataAry = [AddressModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
            [weakSelf.tableView reloadData];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:YES];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        WeakSelf
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                weakSelf.pages = 0;
                [weakSelf getAddreasslist];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[ChooseAddressTableViewCell class] forCellReuseIdentifier:@"ChooseAddressTableViewCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseAddressTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ChooseAddressTableViewCell"];
    cell.modle=_dataAry[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddressModle * modle = _dataAry[indexPath.row];
    if (self.addRelastblock) {
        self.addRelastblock(modle.address, modle.ID);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
