//
//  ChooseAddressTableViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModle.h"
@interface ChooseAddressTableViewCell : UITableViewCell
/*备注名称*/
@property (nonatomic,strong)UILabel *remakeLable;
/*地址*/
@property (nonatomic,strong)UILabel *addressLable;
@property (nonatomic,strong)AddressModle * modle;
@end
