//
//  ChooseAddressTableViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "ChooseAddressTableViewCell.h"

@implementation ChooseAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
    }
    return self;
    
}
- (void)setModle:(AddressModle *)modle
{
    if (![NSString isEmptyString:modle.remark]) {
        _remakeLable.text = modle.remark;
    }else
    {
        _remakeLable.text = modle.name;
    }
    _addressLable.text = modle.address;
}
- (void)get_up
{
    [self.contentView addSubview:self.remakeLable];
    [self.contentView addSubview:self.addressLable];
    
}
- (UILabel *)remakeLable
{
    if (!_remakeLable) {
        _remakeLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_H(120)/2 - RealValue_W(36)/2,RealValue_W(180), RealValue_W(36))];
        _remakeLable.textColor =  MAINTITLECOLOR;
        _remakeLable.font = AutoFont(15);
        
    }
    return _remakeLable;
    
    
}
- (UILabel *)addressLable
{
    if (!_addressLable) {
        _addressLable = [[UILabel alloc] initWithFrame:CGRectMake(_remakeLable.right, 0,UIScreenWidth - _remakeLable.right -5, RealValue_W(36))];
        _addressLable.textColor =  MAINTITLECOLOR1;
        _addressLable.font = AutoFont(15);
        _addressLable.centerY = _remakeLable.centerY;
        
    }
    return _addressLable;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
