//
//  ChooseWithdrawAddressListViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "AssetsModle.h"
@interface ChooseWithdrawAddressListViewController : BaseViewViewController
@property (copy, nonatomic) void(^addRelastblock)(NSString* addreass, NSString * addressId);
@property (nonatomic,strong)AssetsModle * modle;
@end
