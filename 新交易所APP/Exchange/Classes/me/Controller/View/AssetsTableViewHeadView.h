//
//  TableViewHeadView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface AssetsTableViewHeadView : UIView
- (void)getHeadData:(NSMutableArray *)dataAry;
@end
