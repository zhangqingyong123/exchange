//
//  PersonalCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetsModle.h"
#import "RecordModle.h"
#import "ZYProGressView.h"
@interface AssetsCell : UITableViewCell
@property (nonatomic,strong)UIView *bgView;
/*币种*/
@property (nonatomic,strong)UILabel *currencyLable;
/*可用*/
@property (nonatomic,strong)UILabel *availableLable;
/*冻结*/
@property (nonatomic,strong)UILabel *frozenLable;
/*折合*/
@property (nonatomic,strong)UILabel *compromiseLable;
@property (nonatomic,strong)AssetsModle * modle;
/*资金是否可以看*/
@property (nonatomic,assign)BOOL isopen;
@end
@interface AssetsRecordsCell : UITableViewCell
/*币种*/
@property (nonatomic,strong)UILabel *currencyLable;
/*时间*/
@property (nonatomic,strong)UILabel *timeLable;
/*购买数量*/
@property (nonatomic,strong)UILabel *numLable;
@property (nonatomic,strong)CurrencyModle * Cmodle;
@property (nonatomic,strong)RecordModle * modle;
@property (nonatomic,strong)UIButton *stateButton;
@end
@interface AssetsDetailedCell : UITableViewCell
@property (nonatomic,strong)UILabel *leftLable;
@property (nonatomic,strong)UILabel *rightLable;
@property (nonatomic,strong)AssetsDetaileModle * modle;

@end

@interface DistributionCell : UITableViewCell
/*币种*/
@property (nonatomic,strong)UILabel *currencyLable;

/*cny*/
@property (nonatomic,strong)UILabel *cnyNumberLable;
/*白分比*/
@property (nonatomic,strong)UILabel *ratioLable;
/*进度条*/
@property (nonatomic,strong)ZYProGressView * progress;
/*资金是否可以看*/
@property (nonatomic,assign)BOOL isopen;
@property (nonatomic,strong)ZTChatModle * modle;
@end
