//
//  PersonalCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AssetsCell.h"
#define titleWith (UIScreenWidth-RealValue_W(60) -RealValue_W(40) )/3
@implementation AssetsCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].assetsCellColor;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
    }
    return self;
    
}

-(NSString*)getStringFrom:(double)doubleVal {
    NSString* stringValue = @"0.00";
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    formatter.usesSignificantDigits = true;
    formatter.maximumSignificantDigits = 100;
    formatter.groupingSeparator = @"";
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    stringValue = [formatter stringFromNumber:@(doubleVal)];
    
    return stringValue;
}
- (void)setModle:(AssetsModle *)modle
{
    _currencyLable.text = modle.name;

  
    _availableLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@\n%@",Localized(@"transaction_available"),_isopen? modle.available:@"*******"]
                                                        attributedString:_isopen? modle.available:@"*******"
                                                                   color:MAINTITLECOLOR
                                                                    font:AutoFont(15)];
    
    NSDecimalNumber*jianfa1 = [NSDecimalNumber decimalNumberWithString:modle.freeze];
    NSDecimalNumber*jianfa2 = [NSDecimalNumber decimalNumberWithString:modle.other_freeze];
    //减法运算函数  decimalNumberByAdding
    NSDecimalNumber*jianfa = [jianfa1 decimalNumberByAdding:jianfa2];
    
   
    NSString * toalfreeze=[NSString stringWithFormat:@"%@",jianfa];//总冻结
   
    _frozenLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@\n%@",Localized(@"Asset_Frozen"),_isopen? toalfreeze:@"*******"]
                                                     attributedString:_isopen? toalfreeze:@"*******"
                                                                color:MAINTITLECOLOR
                                                                 font:AutoFont(15)];
    _frozenLable.textAlignment = NSTextAlignmentCenter;
    NSString * str = [NSString stringWithFormat:Localized(@"Asset_Compromise"),[EXUserManager getValuationMethod]];
    if (_isopen) {
        
        _compromiseLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@\n%.2f",str,modle.totalprofit]
                                                             attributedString:[NSString stringWithFormat:@"%.2f",modle.totalprofit]
                                                                        color:MAINTITLECOLOR
                                                                         font:AutoFont(15)];
    }else
    {
        _compromiseLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@\n*******",str]
                                                             attributedString:@"*******"
                                                                        color:MAINTITLECOLOR
                                                                         font:AutoFont(15)];
    }
   
    if ([EXUnit isHideCurrencyPrice]) {
         _compromiseLable.hidden = YES;
    }else
    {
         _compromiseLable.hidden = NO;
    }
    _compromiseLable.textAlignment = NSTextAlignmentRight;
}
- (NSString *)formartScientificNotationWithString:(NSString *)str{
  
        long double num = [[NSString stringWithFormat:@"%@",str] floatValue];
   
       NSNumberFormatter * formatter = [[NSNumberFormatter alloc]init];
    
        formatter.numberStyle = kCFNumberFormatterNoStyle;
  
        NSString * string = [formatter stringFromNumber:[NSNumber numberWithDouble:num]];
 
       return string;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.bgView];
    [_bgView addSubview:self.currencyLable];
    [_bgView addSubview:self.availableLable];
    [_bgView addSubview:self.frozenLable];
    [_bgView addSubview:self.compromiseLable];
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(30), 0, UIScreenWidth -RealValue_W(60) , RealValue_W(200))];
        _bgView.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
        _bgView.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_bgView cornerRadius:4];
    }
    return _bgView;
}
- (UILabel *)currencyLable
{
    if (!_currencyLable) {
        _currencyLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_W(28),RealValue_W(320), RealValue_W(28))];
        _currencyLable.textColor =  MAINTITLECOLOR;
        _currencyLable.font = AutoBoldFont(18);
         _currencyLable.adjustsFontSizeToFitWidth = YES;
    }
    return _currencyLable;
    
}
- (UILabel *)availableLable
{
    if (!_availableLable) {
        _availableLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20),_currencyLable.bottom + RealValue_W(25),titleWith, RealValue_W(90))];
        _availableLable.textColor =  MAINTITLECOLOR1;
        _availableLable.font = AutoFont(12);
        _availableLable.numberOfLines = 0;
        _availableLable.adjustsFontSizeToFitWidth = YES;
        
    }
    return _availableLable;
    
}
- (UILabel *)frozenLable
{
    if (!_frozenLable) {
        _frozenLable = [[UILabel alloc] initWithFrame:CGRectMake(_availableLable.right,_availableLable.mj_y,titleWith, RealValue_W(90))];
        _frozenLable.textColor =  MAINTITLECOLOR1;
        _frozenLable.font = AutoFont(12);
         _frozenLable.adjustsFontSizeToFitWidth = YES;
        _frozenLable.numberOfLines = 0;
        
    }
    return _frozenLable;
    
}
- (UILabel *)compromiseLable
{
    if (!_compromiseLable) {
        _compromiseLable = [[UILabel alloc] initWithFrame:CGRectMake(_frozenLable.right,_availableLable.mj_y,titleWith, RealValue_W(90))];
        _compromiseLable.textColor =  MAINTITLECOLOR1;
        _compromiseLable.font = AutoFont(12);
        
        _compromiseLable.numberOfLines = 0;
        
    }
    return _compromiseLable;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@implementation AssetsRecordsCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(0), 0, RealValue_W(0))];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.currencyLable];
    [self.contentView addSubview:self.timeLable];
    [self.contentView addSubview:self.numLable];
    [self.contentView addSubview:self.stateButton];
}
- (void)setModle:(RecordModle *)modle
{
    _currencyLable.text = modle.asset;
    _timeLable.text = [EXUnit getLocalDateFormateUTCDate:modle.CreatedAt];
    
    _numLable.text = [NSString stringWithFormat:@"+ %@ %@",modle.amount,modle.asset];
    NSString * status =@"";
    if (modle.status.integerValue ==1) {
        status = Localized(@"Asset_Success");
    }else if (modle.status.integerValue ==2)
    {
        status = Localized(@"C2C_Audit");
    }else if (modle.status.integerValue ==3)
    {
        status = Localized(@"my_Audit_failure");
        _stateButton.backgroundColor = [UIColor redColor];
    }
    
    [_stateButton setTitle:status forState:(UIControlStateNormal)];
}
- (void)setCmodle:(CurrencyModle *)Cmodle
{
    _currencyLable.text = Cmodle.asset;
    _timeLable.text = [EXUnit getLocalDateFormateUTCDate:Cmodle.CreatedAt];
    _numLable.text = [NSString stringWithFormat:@"+ %@ %@",Cmodle.amount,Cmodle.asset];
    NSString * status =@"";
    if (Cmodle.status.integerValue ==0) {
        status = Localized(@"Asset_Submission");
    }else if (Cmodle.status.integerValue ==1)
    {
        status = Localized(@"C2C_Audit");
    }else if (Cmodle.status.integerValue ==2)
    {
        status = Localized(@"Asset_in_hand");
    }else if (Cmodle.status.integerValue ==3)
    {
        status = Localized(@"Asset_Success");
        _stateButton.backgroundColor = RGBA(250, 74, 97, 1);
    }else if (Cmodle.status.integerValue ==4)
    {
        status = Localized(@"Asset_failure");
    }else if (Cmodle.status.integerValue ==5)
    {
        status = Localized(@"Asset_Cancellation");
        
    }
    
    [_stateButton setTitle:status forState:(UIControlStateNormal)];
}
- (UILabel *)currencyLable
{
    if (!_currencyLable) {
        _currencyLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_W(44),RealValue_W(320), RealValue_W(28))];
        _currencyLable.textColor =  MAINTITLECOLOR;
        _currencyLable.font = AutoFont(18);
        
    }
    return _currencyLable;
}
- (UILabel *)timeLable
{
    if (!_timeLable) {
        _timeLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20),_currencyLable.bottom + RealValue_W(40),RealValue_W(320), RealValue_W(28))];
        _timeLable.textColor =  MAINTITLECOLOR1;
        _timeLable.font = AutoFont(13);
        
    }
    return _timeLable;
}
- (UILabel *)numLable
{
    if (!_numLable) {
        _numLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(320) - RealValue_W(20),_currencyLable.mj_y,RealValue_W(320) ,RealValue_W(28))];
        _numLable.textColor =  MAINTITLECOLOR1;
        _numLable.font = AutoFont(13);
        _numLable.textAlignment = NSTextAlignmentRight;
        
    }
    return _numLable;
}
- (UIButton *)stateButton
{
    if (!_stateButton) {
        _stateButton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(UIScreenWidth - RealValue_W(120), 0, RealValue_W(100), RealValue_W(40)) title:Localized(@"Asset_Confirmation") backgroundColor:DIEECOLOR titleColor:WHITECOLOR tapAction:^(UIButton *button) {
            
        }];
        _stateButton.titleLabel.font = AutoFont(11);
        _stateButton.centerY = _timeLable.centerY;
        KViewRadius(_stateButton, 2);
    }
    return _stateButton;
}
@end
@implementation AssetsDetailedCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(30), 0, RealValue_W(30))];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.leftLable];
    [self.contentView addSubview:self.rightLable];
}
- (void)setModle:(AssetsDetaileModle *)modle
{
    _leftLable.text = modle.leftstr;
    _rightLable.text = modle.rightstr;
}
- (UILabel *)leftLable
{
    if (!_leftLable) {
        _leftLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30),RealValue_W(120)/2 - RealValue_W(15),RealValue_W(130), RealValue_W(30))];
        _leftLable.textColor =  MAINTITLECOLOR1;
        _leftLable.font = AutoFont(15);
        
    }
    return _leftLable;
}
- (UILabel *)rightLable
{
    if (!_rightLable) {
        _rightLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 280 - RealValue_W(30),RealValue_W(120)/2 - RealValue_W(15),280, RealValue_W(30))];
        _rightLable.textColor =  MAINTITLECOLOR;
        _rightLable.font = AutoFont(15);
        
        _rightLable.textAlignment = NSTextAlignmentRight;
        _rightLable.adjustsFontSizeToFitWidth = YES;
    }
    return _rightLable;
}
@end

@implementation DistributionCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
    }
    return self;
    
}
- (void)setModle:(ZTChatModle *)modle
{
    _modle = modle;
    _currencyLable.text = modle.name;
    _cnyNumberLable.text =!_isopen? [NSString stringWithFormat:@"***** %@",[EXUserManager getValuationMethod]]: [NSString stringWithFormat:@"%@ %@",modle.cnyNumber,[EXUserManager getValuationMethod]];
    CGSize highsize = [_cnyNumberLable sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
    _cnyNumberLable.width = highsize.width+2;
    if (modle.Proportion.floatValue<0.0001)
    {
        _progress.progressValue = @"0.01";
        _ratioLable.text = !_isopen?@"****%":@"<0.01%";
    }else
    {
        if (modle.Proportion.floatValue<0.01) {
             _progress.progressValue = @"0.01";
        }else
        {
            _progress.progressValue = modle.Proportion;
        }
        
        _ratioLable.text =!_isopen?@"****%" : modle.ratio;
    }
    _progress.progressColor = modle.color;
    if (modle.Proportion.floatValue<0.45) {
        _ratioLable.mj_x = _cnyNumberLable.right;
        
    }else
    {

      _ratioLable.mj_x = _progress.mj_x +  _progress.width*modle.Proportion.floatValue - 40;
        
    }
    
}
- (void)get_up
{
    [self.contentView addSubview:self.currencyLable];
    [self.contentView addSubview:self.cnyNumberLable];
    [self.contentView addSubview:self.ratioLable];
    [self.contentView addSubview:self.progress];
}
- (UILabel *)currencyLable
{
    if (!_currencyLable) {
        _currencyLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(40),RealValue_W(10),RealValue_W(120), RealValue_W(50))];
        _currencyLable.textColor =  MAINTITLECOLOR;
        _currencyLable.font = AutoBoldFont(12);
    }
    return _currencyLable;
}
- (UILabel *)cnyNumberLable
{
    if (!_cnyNumberLable) {
        _cnyNumberLable = [[UILabel alloc] initWithFrame:CGRectMake(_currencyLable.right,RealValue_W(15),0, RealValue_W(16))];
        _cnyNumberLable.textColor =  MAINTITLECOLOR1;
        _cnyNumberLable.font = AutoFont(10);
        _cnyNumberLable.numberOfLines = 0;
    }
    return _cnyNumberLable;
}
- (UILabel *)ratioLable
{
    if (!_ratioLable) {
        _ratioLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(100)-120, RealValue_W(15) ,40, RealValue_W(16))];
        _ratioLable.textColor =  MAINTITLECOLOR;
        _ratioLable.font = AutoBoldFont(10);
        _ratioLable.textAlignment = NSTextAlignmentRight;
    }
    return _ratioLable;
}
- (ZYProGressView *)progress
{
    if (!_progress) {
        _progress = [[ZYProGressView alloc]initWithFrame:CGRectMake(_cnyNumberLable.mj_x, _cnyNumberLable.bottom + RealValue_W(10), UIScreenWidth - RealValue_W(100) - _currencyLable.right, RealValue_W(8))];
       
        _progress.bottomColor =[ZBLocalized sharedInstance].assetsCellviewbgColor;
       
    }
    return _progress;
}
@end
