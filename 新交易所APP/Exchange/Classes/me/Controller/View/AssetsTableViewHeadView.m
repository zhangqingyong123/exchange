//
//  TableViewHeadView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "AssetsTableViewHeadView.h"
#import "ZFChart.h"
#import "AssetsModle.h"
@interface AssetsTableViewHeadView ()<ZFPieChartDataSource, ZFPieChartDelegate>
@property (nonatomic, strong) ZFPieChart * pieChart;
@property (nonatomic, strong) NSMutableArray * colorArray;
@property (nonatomic, strong) NSMutableArray * valueArray;
@end
@implementation AssetsTableViewHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
        [self addSubview:self.pieChart];
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(0, _pieChart.bottom , self.width, 20)];
        lable.text = Localized(@"Asset_Chat_message");
        lable.font = AutoFont(12);
        lable.textColor = MAINTITLECOLOR1;
        lable.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lable];
    }
    return self;
    
}
- (NSMutableArray *)colorArray
{
    if (!_colorArray) {
        _colorArray = [[NSMutableArray alloc] init];
    }
    return _colorArray;
}
- (NSMutableArray *)valueArray
{
    if (!_valueArray) {
        _valueArray = [[NSMutableArray alloc] init];
    }
    return _valueArray;
}
- (void)getHeadData:(NSMutableArray *)dataAry;
{
    [self.colorArray removeAllObjects];
    [self.valueArray removeAllObjects];
    for (ZTChatModle * model in dataAry)
    {
        if (model.Proportion.floatValue<0.01)
        {
            
           [self.valueArray addObject:[NSString stringWithFormat:@"%.2f",model.cnyNumber.floatValue]];
        }else
        {
            [self.valueArray addObject:model.cnyNumber];
        }
        [self.colorArray addObject:model.color];
       
    }
    [_pieChart strokePath];
}
- (ZFPieChart *)pieChart
{
    if (!_pieChart) {
        _pieChart = [[ZFPieChart alloc] initWithFrame:CGRectMake(0, 10, RealValue_W(360), RealValue_W(360))];
        _pieChart.dataSource = self;
        _pieChart.centerX = self.centerX;
        _pieChart.delegate = self;
        _pieChart.piePatternType = kPieChartPatternTypeCirque;
        _pieChart.percentType = kPercentTypeInteger;
        _pieChart.isShadow = NO;
        _pieChart.isAnimated = YES;
        _pieChart.isShowPercent = NO;
        _pieChart.opacity = 1;
        _pieChart.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
       
    }
    return _pieChart;
}
#pragma mark - ZFPieChartDataSource

- (NSArray *)valueArrayInPieChart:(ZFPieChart *)chart{
    
    return _valueArray;
}

- (NSArray *)colorArrayInPieChart:(ZFPieChart *)chart{
    return _colorArray;
}

#pragma mark - ZFPieChartDelegate

- (void)pieChart:(ZFPieChart *)pieChart didSelectPathAtIndex:(NSInteger)index{
    NSLog(@"第%ld个",(long)index);
}

- (CGFloat)allowToShowMinLimitPercent:(ZFPieChart *)pieChart{
    return 0.f;
}

- (CGFloat)radiusForPieChart:(ZFPieChart *)pieChart{
    return 76;
}

/** 此方法只对圆环类型(kPieChartPatternTypeForCirque)有效 */
- (CGFloat)radiusAverageNumberOfSegments:(ZFPieChart *)pieChart{
    return 0.6;
}
@end
