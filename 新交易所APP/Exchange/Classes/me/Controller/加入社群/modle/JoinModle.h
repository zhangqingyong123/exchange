//
//  JoinModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/27.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JoinModle : NSObject
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * api_url; //优先复制URL
@property (nonatomic,strong)NSString * class_id;//:关于我们 3:关注我们 4:友情链接
@property (nonatomic,strong)NSString * icon;
@property (nonatomic,strong)NSString * label_name;
@property (nonatomic,strong)NSString * lang;
@property (nonatomic,strong)NSString * order;
@property (nonatomic,strong)NSString * qrcode;
@property (nonatomic,strong)NSString * site_id;
@property (nonatomic,strong)NSString * type;
@end

NS_ASSUME_NONNULL_END
