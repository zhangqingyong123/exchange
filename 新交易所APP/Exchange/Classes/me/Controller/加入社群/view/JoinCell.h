//
//  JoinCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/27.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JoinModle.h"
NS_ASSUME_NONNULL_BEGIN

@interface JoinCell : UITableViewCell
@property (nonatomic,strong)UIImageView * icon;
@property (nonatomic,strong)UILabel * titleLable;
@property (nonatomic,strong)UILabel * rightLable;
@property (nonatomic,strong)UIImageView * returnImg;
@property (nonatomic,strong)JoinModle * modle;
@end

NS_ASSUME_NONNULL_END
