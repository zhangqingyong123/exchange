//
//  JoinCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/27.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "JoinCell.h"

@implementation JoinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(32), 0, RealValue_W(32))];
    }
    return self;
    
}
- (void)setModle:(JoinModle *)modle
{
    _modle = modle;
    [self.icon sd_setImageWithURL:[NSURL URLWithString:modle.icon]];
    _titleLable.text = modle.label_name;
    _rightLable.text = modle.api_url;
}
- (void)get_up
{
    [self.contentView addSubview:self.icon];
    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.returnImg];
    [self.contentView addSubview:self.rightLable];
}
- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(32),
                                                             RealValue_W(100 -60)/2,
                                                              RealValue_W(60),
                                                              RealValue_W(60))];
    }
    return _icon;
    
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(_icon.right +9,0 , 120, RealValue_W(40))];
        _titleLable.textColor = [ZBLocalized sharedInstance].segmentBaColor;
        _titleLable.font = AutoFont(14);
        _titleLable.centerY = _icon.centerY;
    }
    return _titleLable;
}
- (UIImageView *)returnImg
{
    if (!_returnImg) {
        _returnImg = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth -RealValue_W(42) ,
                                                                  0,
                                                                   RealValue_W(10),
                                                                   RealValue_W(16))];
        _returnImg.image = [UIImage imageNamed:@"icon_list_go"];
        _returnImg.centerY = _icon.centerY;
    }
    return _returnImg;
    
}
- (UILabel *)rightLable
{
    if (!_rightLable) {
        _rightLable = [[UILabel alloc] initWithFrame:CGRectMake(_returnImg.mj_x - 230,0 , 220, RealValue_W(40))];
        _rightLable.textColor = ColorStr(@"#317EF3");
        _rightLable.font = AutoFont(14);
        _rightLable.centerY = _icon.centerY;
        _rightLable.textAlignment = NSTextAlignmentRight;
    }
    return _rightLable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
