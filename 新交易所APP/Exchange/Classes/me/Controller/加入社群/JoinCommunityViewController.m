//
//  JoinCommunityViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "JoinCommunityViewController.h"
#import "JoinModle.h"
#import "JoinCell.h"
#import "CheckVersionAlearView.h"
@interface JoinCommunityViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)NSMutableArray * screenDataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong) NoNetworkView * workView;
//1是已提交，2是成功，3是失败，4是审核中
@end
@implementation JoinCommunityViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self getData];
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - TABBAR_HEIGHT1 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[JoinCell class] forCellReuseIdentifier:@"JoinCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (NSMutableArray *)screenDataAry
{
    if (!_screenDataAry) {
        _screenDataAry = [[NSMutableArray alloc]init];
    }
    return _screenDataAry;
}
#pragma mark----数据
- (void)getData
{
    WeakSelf
    [self GETWithHost:@"" path:labelconfig param:@{@"type":@"2"} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSArray * list = responseObject[@"result"];
            weakSelf.dataAry = [JoinModle mj_objectArrayWithKeyValuesArray:list];
            for (JoinModle * modle in weakSelf.dataAry)
            {
                if (modle.class_id.integerValue ==3)
                {
                    [self.screenDataAry addObject:modle];
                }
            }
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
            [self.tableView reloadData];
            
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    WeakSelf
    if (_screenDataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:_tableView.frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
    
        [_workView dissmiss];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(100);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _screenDataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JoinCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"JoinCell"];
    cell.modle = _screenDataAry[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JoinModle * modle = _screenDataAry[indexPath.row];
    
    if ([NSString isEmptyString:modle.api_url] && ![NSString isEmptyString:modle.qrcode])//复制URL
    {
       
        CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"Asset_Long_to_album_message")  titleStr:@"" openUrl:@"" confirm:@"" cancel:@"" state:4 RechargeBlock:^{
            
            
        }];
        aleartVC.animationStyle =  LXASAnimationTopShake;
        [aleartVC showLXAlertView];
        aleartVC.qrcodeUrl = modle.qrcode;
        
    }else if ([NSString isEmptyString:modle.qrcode] && ![NSString isEmptyString:modle.api_url])//保持二维码
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = modle.api_url;
        [EXUnit showSVProgressHUD:Localized(@"Copy_to_paste")];
        
    }else if (![NSString isEmptyString:modle.qrcode] && ![NSString isEmptyString:modle.api_url]) //复制URL
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = modle.api_url;
        [EXUnit showSVProgressHUD:Localized(@"Copy_to_paste")];

    }
    
}
@end
