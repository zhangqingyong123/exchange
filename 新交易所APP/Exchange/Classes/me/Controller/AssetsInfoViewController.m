//
//  AssetsInfoViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "AssetsInfoViewController.h"
#import "AssetsCell.h"
@interface AssetsInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation AssetsInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [ZBLocalized sharedInstance].assetsCellColor;
    [self.view addSubview:self.tableView];
}
- (UITableView *)tableView
{
    if (!_tableView) {
        CGFloat height;
        if ([EXUnit isHideCurrencyPrice]) {
            height = 0;
        }else
        {
            height = 173;
        }
        _tableView = [[MainTouchTableTableView alloc] initWithFrame:CGRectMake(0, 10, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight - 44  - 50 -10 - IPhoneTop)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 10)];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.backgroundColor = [ZBLocalized sharedInstance].assetsCellColor;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[AssetsCell class] forCellReuseIdentifier:@"AssetsCell"];
    }
    return _tableView;
   
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(220);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetsCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"AssetsCell"];
    cell.isopen = _isOpen;
    if (_dataAry.count> 0) {
        cell.modle=_dataAry[indexPath.row];
    }
    
    return cell;
}
@end
