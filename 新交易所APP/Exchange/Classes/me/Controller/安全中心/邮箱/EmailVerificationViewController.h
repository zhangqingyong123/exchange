//
//  EmailVerificationViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "QCCountdownButton.h"
@interface EmailVerificationViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailVeriTextfield;
@property (weak, nonatomic) IBOutlet QCCountdownButton *verButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *emailTitle;
@property (weak, nonatomic) IBOutlet UILabel *emailVeriTitle;
- (IBAction)Verbutton:(QCCountdownButton *)sender;
- (IBAction)nextButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;

@end
