//
//  EmailVerificationViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "EmailVerificationViewController.h"

@interface EmailVerificationViewController ()

@end

@implementation EmailVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customUI];
}
- (void)dealloc
{
    [self.verButton stopTime];
    self.verButton = nil;
}
- (void)customUI
{
    
    self.view.backgroundColor = MainBGCOLOR;
    _bgView1.backgroundColor = textFieldColor;
    _bgView.backgroundColor = textFieldColor;
    _nextButton.backgroundColor = TABTITLECOLOR;
    
    self.emailTitle.text = Localized(@"Security_mailbox");
    self.emailVeriTitle.text = Localized(@"verification_email");
    _emailTextField.placeholder = Localized(@"register_Mailbox_message");
    _emailVeriTextfield.placeholder = Localized(@"register_Mailbox_verification_code");

    _verButton.title = Localized(@"register_get_Mailbox_verification_code");
    _verButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_nextButton setTitle:Localized(@"Security_Next_step") forState:0];
    [_emailTextField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    [_emailTextField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
     _emailTextField.tintColor = TABTITLECOLOR;
    _emailTextField.textColor = MAINTITLECOLOR;
    [_emailVeriTextfield setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    [_emailVeriTextfield setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
    _emailVeriTextfield.tintColor = TABTITLECOLOR;
    _emailVeriTextfield.textColor = MAINTITLECOLOR;
    KViewRadius(self.nextButton, 2);
    RACSignal*emailSignal=[_emailTextField.rac_textSignal map:^id(NSString* value) {
        return @([NSString isEmailString:value]);
    }];
    RACSignal*emailVeriSignal=[_emailVeriTextfield.rac_textSignal map:^id(NSString* value) {
        return @([EXUnit CodeissSixplace:value]);
    }];
    RACSignal*Signal=[RACSignal combineLatest:@[emailSignal,emailVeriSignal] reduce:^id(NSNumber*emailValid,NSNumber*emailVerValid){
        return @([emailValid boolValue]&&[emailVerValid boolValue]);
    }];
    WeakSelf
    [Signal subscribeNext:^(NSNumber* x) {
        
        if ([x boolValue]) {
            weakSelf.nextButton.enabled=YES;
            weakSelf.nextButton.alpha = 1;
            
        }else
        {
            weakSelf.nextButton.enabled=NO;
            weakSelf.nextButton.alpha = 0.5;
        }
    }];
    // 倒计时的时长
    _verButton.totalSecond = 60;
    [_verButton setTitleColor:TABTITLECOLOR forState:0];
    [_verButton processBlock:^(NSUInteger second)
     {
         weakSelf.verButton.title = [NSString stringWithFormat:@"%lis", (unsigned long)second] ;
     } onFinishedBlock:^{  // 倒计时完毕
         weakSelf.verButton.title = Localized(@"Regain_validation_code");
     }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Verbutton:(QCCountdownButton *)sender {
    if (![NSString isEmailString:_emailTextField.text])
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"register_Mailbox_format_message") view:kWindow];
    }else if (_emailTextField.text.length ==0)
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"register_Mailbox_message") view:kWindow];
    }else
    {
       
        [self POSTWithHost:@"" path:userSMS param:@{@"type":@(1),@"use_type":@(5),@"email":_emailTextField.text,@"country_id":@""} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [self.verButton startTime];
            }else
            {
                [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }
    
}

- (IBAction)nextButton:(UIButton *)sender {
    [_emailTextField resignFirstResponder];
    [_emailVeriTextfield resignFirstResponder];
    CustomAlertVC * custom = [[CustomAlertVC alloc] initWithCustomContentViewClass:@"BindCustonAlert" delegate:nil];
    custom.direction = FromBottom;
    [custom show];
    BindCustonAlert *aleatView = (BindCustonAlert *)custom.contentView;
    aleatView.type = @"5";
    aleatView.isBindGoole = NO;
    aleatView.cancelBlock = ^{
        [custom dismiss];
    };
    WeakSelf
    WeakObject(aleatView)
    [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
        
        
        
        if (![NSString isEmptyString:weakaleatView.emailField.text])
        {
            if (![EXUnit CodeissSixplace:weakaleatView.emailField.text]) {
                [EXUnit showMessage:Localized(@"VerificationMessage")];
                return ;
            }
        }
        if (![NSString isEmptyString:weakaleatView.phoneField.text])
        {
            if (![EXUnit CodeissSixplace:weakaleatView.phoneField.text]) {
                [EXUnit showMessage:Localized(@"VerificationMessage")];
                return ;
            }
        }
        NSDictionary * param = @{@"email":weakSelf.emailTextField.text,
                                 @"email_code":weakSelf.emailVeriTextfield.text,
                                 @"sms_code":[NSString isEmptyString:weakaleatView.phoneField.text]?@"":weakaleatView.phoneField.text,
                                 @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text};
        
        [weakSelf POSTWithHost:@"" path:bindEmail param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
              [weakaleatView.okButton stopAnimation];
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [EXUserManager saveemail:@"1"];
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                [custom dismiss];
               
                 [[NSNotificationCenter defaultCenter] postNotificationName:SetSecurityNotificationse object:nil];
                [self.navigationController popViewControllerAnimated:YES];
                

            }else
            {
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
              [weakaleatView.okButton stopAnimation];
        }];
        
    }];
}
@end
