//
//  PhoneVerificationViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "QCCountdownButton.h"
@interface PhoneVerificationViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet UITextField *phoneVerification;
@property (weak, nonatomic) IBOutlet QCCountdownButton *Verificationbutton;
@property (weak, nonatomic) IBOutlet UILabel *areaLable;
- (IBAction)verificationbutton:(QCCountdownButton *)sender;
- (IBAction)nextbutton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextbutton;
- (IBAction)choosebutton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *bgview;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UILabel *phoneTitle;
@property (weak, nonatomic) IBOutlet UILabel *verificationTitle;

@end
