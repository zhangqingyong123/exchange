//
//  SetPasswordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SetPasswordViewController.h"

@interface SetPasswordViewController ()

@end

@implementation SetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBGCOLOR;
    [self customUI];
}
- (void)customUI
{
    _oldTitleLab.text = Localized(@"Security_Old_Password");
    _newsTitleLab.text = Localized(@"Security_new_Password");
    _angeinTitleLab.text = Localized(@"Security_Confirm_password");
    _oldPWField.placeholder = Localized(@"Security_Old_Password_message");
    _newsPWField.placeholder = Localized(@"Security_new_Password_message");
    _angeinPWField.placeholder = Localized(@"Security_Confirm_password");
    [_submitButton setTitle:Localized(@"submit") forState:0];
    _bgView.backgroundColor = textFieldColor;
    _bgView1.backgroundColor = textFieldColor;
    _bgView2.backgroundColor = textFieldColor;
    _submitButton.backgroundColor = TABTITLECOLOR;
    [_oldPWField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    [_oldPWField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
     _oldPWField.tintColor = TABTITLECOLOR;
    _oldPWField.textColor = MAINTITLECOLOR;
 
    [_newsPWField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    [_newsPWField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
    _newsPWField.tintColor = TABTITLECOLOR;
    _newsPWField.textColor = MAINTITLECOLOR;
    [_angeinPWField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
    [_angeinPWField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
     _angeinPWField.tintColor = TABTITLECOLOR;
     _angeinPWField.textColor = MAINTITLECOLOR;
    KViewRadius(self.submitButton, 2);
    
    RACSignal*oldPWField=[_oldPWField.rac_textSignal map:^id(NSString* value) {
        return @(value.length>PassworldLastlength && value.length<Passworldhighlength);
    }];
    RACSignal*newsPWSignal=[_newsPWField.rac_textSignal map:^id(NSString* value) {
        return @(value.length>PassworldLastlength && value.length<Passworldhighlength);
    }];
    RACSignal*angeinPWSignal=[_angeinPWField.rac_textSignal map:^id(NSString* value) {
        return @(value.length>PassworldLastlength && value.length<Passworldhighlength);
    }];
    RACSignal*loginSignal=[RACSignal combineLatest:@[oldPWField,newsPWSignal,angeinPWSignal] reduce:^id(NSNumber*oldPWField,NSNumber*newsPWSignal,NSNumber*angeinPWSignal){
        return @([oldPWField boolValue]&&[newsPWSignal boolValue] &&[angeinPWSignal boolValue]);
    }];
    WeakSelf
    [loginSignal subscribeNext:^(NSNumber* x) {
        if ([x boolValue]) {
            weakSelf.submitButton.enabled=YES;
            weakSelf.submitButton.alpha = 1;
        }else{
            weakSelf.submitButton.enabled=NO;
            weakSelf.submitButton.alpha = 0.5;
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitButton:(UIButton *)sender {
    
    if (![_newsPWField.text isEqualToString:_angeinPWField.text]) {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"register_two_password_inconsistent_message") view:kWindow];
    }else
    {
        [self POSTWithHost:@"" path:resetPassword param:@{@"origin_password":_oldPWField.text,@"password":_newsPWField.text,@"password_confirmation":_angeinPWField.text} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter]postNotificationName:SetThePasswordNotificationse object:nil];
            }else
            {
                [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }
}
@end
