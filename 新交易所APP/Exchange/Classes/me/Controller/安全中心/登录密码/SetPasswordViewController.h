//
//  SetPasswordViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface SetPasswordViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *oldTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *angeinTitleLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UITextField *oldPWField;

@property (weak, nonatomic) IBOutlet UITextField *newsPWField;

@property (weak, nonatomic) IBOutlet UITextField *angeinPWField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
- (IBAction)submitButton:(UIButton *)sender;

@end
