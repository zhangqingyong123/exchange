//
//  VerificationViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/3.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
typedef enum : NSUInteger {
    PhoneVerificationType = 0,//手机
    emailVerificationType = 1,  //邮箱
     gooleVerificationType = 2,  //谷歌
}VerificationStatus;

@interface VerificationViewController : BaseViewViewController
@property (nonatomic,readwrite)VerificationStatus Type;
@end
