//
//  SecurityCenterViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SecurityCenterViewController.h"
#import "PresonaModle.h"
#import "SecurityTableCell.h"
#import "SetPasswordViewController.h"
#import "CapitalPasswordViewController.h"
#import "PhoneVerificationViewController.h"
#import "EmailVerificationViewController.h"
#import "GoogleVerificationViewController.h"
#import "VerificationViewController.h"
#import "YWFingerprintVerification.h"
#import <LocalAuthentication/LocalAuthentication.h>
@interface SecurityCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,assign)BOOL isOpenTouchid;
@property (nonatomic,assign)BOOL isOpenGesture;
@end

@implementation SecurityCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(SetSecurity) name:SetSecurityNotificationse object:nil];
    [self getData];
    [self SetSecurity];
    _isOpenTouchid = [EXUnit isOpenTouchid];
    _isOpenGesture = [EXUnit isOpenGesture];
    
}
- (void)SetSecurity
{
    [self getUserInfo];
    [EXUserManager getbindInfo:^(BOOL isSuccess) {
        
    }];
}
- (void)getUserInfo
{
    WeakSelf
    [HomeViewModel getUserWithDic:@{} url:getUser callBack:^(BOOL isSuccess, id callBack) {
        
        if (!isSuccess) {
            
            [self axcBasePopUpWarningAlertViewWithMessage:callBack[@"message"] view:kWindow];
            
        }else
        {
            [weakSelf.tableView reloadData];
        }
    }];
}

#pragma mark----数据
- (void)getData
{
    NSArray * array  =@[@{@"title":Localized(@"Security_Login_password"),@"stats":Localized(@"Security_Reset")},
                        @{@"title":Localized(@"Security_Capital_cipher"),@"stats":Localized(@"Security_Not_set")},
                        @{@"title":Localized(@"Security_phone"),@"stats":Localized(@"Security_Not_set")},
                        @{@"title":Localized(@"Security_mailbox"),@"stats":Localized(@"Security_Unbound")},
                        @{@"title":Localized(@"Security_Google_Authenticator"),@"stats":Localized(@"Security_Unopened")},
                        @{@"title":Localized(@"Security_Gesture_cipher"),@"stats":@""},
                        @{@"title":Localized(@"Security_Fingerprint_cipher"),@"stats":@""},
                        ];
    
    for (NSDictionary *dic in array)
    {
        
        PresonaModle *modle=[[PresonaModle alloc]init];
        modle.title=dic[@"title"];
        modle.stats=dic[@"stats"];
        [self.dataAry addObject:modle];
    }
    [_tableView reloadData];
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight  - 64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[SecurityTableCell class] forCellReuseIdentifier:@"SecurityTableCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SecurityTableCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"SecurityTableCell"];
    cell.modle = _dataAry[indexPath.row];
    if (indexPath.row==5||indexPath.row ==6)
    {
        cell.rightSwitch.hidden = NO;
        cell.rightLable.hidden = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else
    {
        cell.rightSwitch.hidden = YES;
        cell.rightLable.hidden = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        switch (indexPath.row) {
            case 1:
                
                cell.rightLable.text = [EXUserManager personalData].withdraw_password_set?Localized(@"Security_Set_up"):Localized(@"Security_Not_set");
                break;
            case 2:
                if ([EXUserManager personalData].phone_bound) {
                    if ([EXUnit isPhoneBind]) {
                        cell.rightLable.text = Localized(@"Security_Already_opened");
                    }else
                    {
                        cell.rightLable.text = Localized(@"Security_Unopened");
                    }
                }else
                {
                    cell.rightLable.text = Localized(@"Security_Unbound");
                }
                
                break;
            case 3:
                if ([EXUserManager personalData].email_bound) {
                    if ([EXUnit isEmailBind]) {
                        cell.rightLable.text =Localized(@"Security_Already_opened");
                    }else
                    {
                        cell.rightLable.text = Localized(@"Security_Unopened");
                    }
                }else
                {
                    cell.rightLable.text = Localized(@"Security_Unbound");
                }
                
                break;
            case 4:
                if ([EXUserManager personalData].two_step_set) {
                    if ([EXUnit isGooleBind]) {
                        cell.rightLable.text = Localized(@"Security_Already_opened");
                    }else
                    {
                        cell.rightLable.text = Localized(@"Security_Unopened");
                    }
                }else
                {
                    cell.rightLable.text = Localized(@"Security_Unbound");
                }
                break;
            default:
                break;
        }
    }
    if (indexPath.row ==5) {
        [cell.rightSwitch setOn:_isOpenGesture];
    }else if (indexPath.row ==6)
    {
        
        [cell.rightSwitch setOn:_isOpenTouchid];
    }
    WeakSelf
    cell.Switchblock = ^(UISwitch *sender) {
        [weakSelf touchIDWithswitch:sender indexPath:indexPath];
    };
    
    return cell;
}
- (void)touchIDWithswitch:(UISwitch *)aswitch indexPath:(NSIndexPath *)indexPath
{
    WeakSelf;
    if (indexPath.row ==5) {//手势密码
        if (aswitch.isOn) {
            
            //创建手势密码
            [YWUnlockView showUnlockViewWithType:YWUnlockViewCreate callBack:^(BOOL result) {
                if (result) {
                    [EXUserManager saveIsTouchid:@"0"];
                    [EXUserManager saveIsGesture:@"1"];
                    weakSelf.isOpenTouchid = [EXUnit isOpenTouchid];
                    weakSelf.isOpenGesture = [EXUnit isOpenGesture];
                    [weakSelf.tableView reloadData];
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"Security_Set_success") view:kWindow];
                }else
                {
                    [EXUserManager saveIsGesture:@"0"];
                    weakSelf.isOpenGesture = [EXUnit isOpenGesture];
                    [weakSelf.tableView reloadData];
                }
                
            }];
        }else
        {
            
            [EXUserManager saveIsGesture:@"0"];
            weakSelf.isOpenGesture = [EXUnit isOpenGesture];
            [weakSelf.tableView reloadData];
            
            
        }
        
    }else if (indexPath.row ==6) //指纹密码
    {
        if (aswitch.isOn) {
            [YWFingerprintVerification fingerprintVerificationCallBack:^(NSError *error) {
                if(!error){
                    //验证成功，主线程处理UI
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        // 处理耗时操作的代码块...
                        
                        //通知主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //回调或者说是通知主线程刷新，
                            [EXUserManager saveIsTouchid:@"1"];
                            [EXUserManager saveIsGesture:@"0"];
                            weakSelf.isOpenTouchid = [EXUnit isOpenTouchid];
                            weakSelf.isOpenGesture = [EXUnit isOpenGesture];
                            [weakSelf.tableView reloadData];
                            [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"Security_Set_success") view:kWindow];
                        });
                        
                    });
                }else{
                    //验证成功，主线程处理UI
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        // 处理耗时操作的代码块...
                        
                        //通知主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //回调或者说是通知主线程刷新，
                            [EXUserManager saveIsTouchid:@"0"];
                            weakSelf.isOpenTouchid = [EXUnit isOpenTouchid];
                            [weakSelf.tableView reloadData];
                        });
                        
                    });
                    
                }
            }];
        }else
        {
            [EXUserManager saveIsTouchid:@"0"];
            weakSelf.isOpenTouchid = [EXUnit isOpenTouchid];
            [weakSelf.tableView reloadData];
        }
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0://重制密码
        {
            SetPasswordViewController * setPVC = [[SetPasswordViewController alloc] init];
            setPVC.title = Localized(@"Security_Login_password_reset");
            [self.navigationController pushViewController:setPVC animated:YES];
        }
            break;
        case 1://重制密码
        {
            CapitalPasswordViewController * capitalPasswordVC = [[CapitalPasswordViewController alloc] init];
            capitalPasswordVC.title = [EXUserManager personalData].withdraw_password_set?Localized(@"Security_fund_password"):Localized(@"Security_Set_funds_password");
            capitalPasswordVC.isEdit = [EXUserManager personalData].withdraw_password_set;
            [self.navigationController pushViewController:capitalPasswordVC animated:YES];
        }
            break;
        case 2://手机号绑定
        {
            if ([EXUserManager personalData].phone_bound) {
                VerificationViewController * VerificationVC = [[VerificationViewController alloc] init];
                VerificationVC.Type = PhoneVerificationType;
                VerificationVC.title = Localized(@"Security_phone_verification");
                [self.navigationController pushViewController:VerificationVC animated:YES];
                
                
            }else
            {
                PhoneVerificationViewController * VerificationVC = [[PhoneVerificationViewController alloc] init];
                VerificationVC.title = Localized(@"Security_Binding_cell_phone");
                [self.navigationController pushViewController:VerificationVC animated:YES];
            }
            
        }
            break;
        case 3://邮箱验证
        {
            if ([EXUserManager personalData].email_bound) {
                VerificationViewController * VerificationVC = [[VerificationViewController alloc] init];
                VerificationVC.Type = emailVerificationType;
                VerificationVC.title = Localized(@"Security_Mailbox_verification");
                [self.navigationController pushViewController:VerificationVC animated:YES];
                
                
            }else
            {
                EmailVerificationViewController * VerificationVC = [[EmailVerificationViewController alloc] init];
                VerificationVC.title = Localized(@"Security_Binding_mailbox");
                [self.navigationController pushViewController:VerificationVC animated:YES];
            }
            
        }
            break;
        case 4://谷歌身份验证
        {
            if ([EXUserManager personalData].two_step_set) {
                VerificationViewController * VerificationVC = [[VerificationViewController alloc] init];
                VerificationVC.Type = gooleVerificationType;
                VerificationVC.title = Localized(@"Security_Google_Authenticator");
                [self.navigationController pushViewController:VerificationVC animated:YES];
                
                
            }else
            {
                GoogleVerificationViewController * VerificationVC = [[GoogleVerificationViewController alloc] init];
                VerificationVC.title = Localized(@"Security_Google_Authenticator");
                [self.navigationController pushViewController:VerificationVC animated:YES];
            }
            
        }
            break;
            
            
        default:
            break;
    }
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
