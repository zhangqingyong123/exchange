//
//  VerificationViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/3.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "VerificationViewController.h"
#import "PresonaModle.h"
#import "SecurityTableCell.h"
@interface VerificationViewController ()<UITableViewDelegate,UITableViewDataSource,QVCustomAlertDelegate>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,assign)BOOL isOpen;
@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self getData];
}
#pragma mark----数据
- (void)getData
{
    NSString * classString;
    NSString * valeStrIng;
    NSString * verificationStr;
    if (_Type ==PhoneVerificationType) {
        classString = Localized(@"Security_phone");
        valeStrIng = [EXUserManager personalData].phone;
        verificationStr = Localized(@"Security_SMS_verification");
        _isOpen = [EXUnit isPhoneBind];
    }else if (_Type ==emailVerificationType)
    {
        classString = Localized(@"Security_mailbox");
        valeStrIng = [EXUserManager personalData].email;
        verificationStr = Localized(@"Security_Mailbox_verification");
        _isOpen = [EXUnit isEmailBind];
    }else if (_Type ==gooleVerificationType)
    {
        classString = Localized(@"Security_Google_Authenticator");
        valeStrIng =Localized(@"Security_Bindings");
        verificationStr = Localized(@"Security_Google_Authenticator");
        _isOpen = [EXUnit isGooleBind];
    }
    NSArray * array  =@[@{@"title":classString,@"stats":valeStrIng},
                        @{@"title":verificationStr,@"stats":@""}];
                        
    
    for (NSDictionary *dic in array)
    {
        
        PresonaModle *modle=[[PresonaModle alloc]init];
        modle.title=dic[@"title"];
        modle.stats=dic[@"stats"];
        [self.dataAry addObject:modle];
    }
     [_tableView reloadData];
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight  - 64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[SecurityTableCell class] forCellReuseIdentifier:@"SecurityTableCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SecurityTableCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"SecurityTableCell"];
    cell.rightLable.mj_x = UIScreenWidth - RealValue_W(40)- 180;
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (indexPath.row==0)
    {
        cell.rightSwitch.hidden = YES;
        cell.rightLable.hidden = NO;
    }else
    {
        cell.rightSwitch.hidden = NO;
        cell.rightLable.hidden = YES;
    }
    [cell.rightSwitch setOn:_isOpen];
    [cell.rightSwitch addTarget:self action:@selector(switchBurron:) forControlEvents:(UIControlEventTouchUpInside)];
    cell.modle = _dataAry[indexPath.row];
    return cell;
}
- (void)switchBurron:(UISwitch *)sender
{
    WeakSelf
    if ([sender isOn]) {
        NSDictionary * dic;
        if (_Type == PhoneVerificationType) {
            
            dic = @{@"type":@"1"};
        }else if (_Type == emailVerificationType)
        {
            dic = @{@"type":@"0"};
        }else if (_Type ==gooleVerificationType)
        {
            dic = @{@"type":@"2"};
        }
        [self POSTWithHost:@"" path:userReopen param:dic cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                if (weakSelf.Type == PhoneVerificationType) {
                    [EXUserManager savephone:@"1"];
                    
                }else if (weakSelf.Type == emailVerificationType)
                {
                   [EXUserManager saveemail:@"1"];
                    
                }else if (weakSelf.Type ==gooleVerificationType)
                {
                   [EXUserManager saveGoole:@"1"];
                }
                weakSelf.isOpen = YES;
                [weakSelf.tableView reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:SetSecurityNotificationse object:nil];
             
            }else
            {
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }else
    {
        weakSelf.isOpen = YES;
        CustomAlertVC * custom = [[CustomAlertVC alloc] initWithCustomContentViewClass:@"BindCustonAlert" delegate:self];
        custom.direction = FromBottom;
        [custom show];
        BindCustonAlert *aleatView = (BindCustonAlert *)custom.contentView;
        aleatView.isBindGoole = NO;
        if (weakSelf.Type == PhoneVerificationType) {
            
            aleatView.type = @"8";
        }else if (weakSelf.Type  == emailVerificationType)
        {
            aleatView.type = @"9";
        }else if (weakSelf.Type  ==gooleVerificationType)
        {
            aleatView.type = @"10";
        }
        aleatView.cancelBlock = ^{
            [custom dismiss];
            [weakSelf.tableView reloadData];
           
        };
        WeakObject(aleatView)
        [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
            
            if (![NSString isEmptyString:weakaleatView.emailField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.emailField.text]) {
     
                    [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
            if (![NSString isEmptyString:weakaleatView.phoneField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.phoneField.text]) {
                   [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
            
            NSDictionary *param = @{@"sms_code":[NSString isEmptyString:weakaleatView.phoneField.text]?@"":weakaleatView.phoneField.text,
                                    @"email_code":[NSString isEmptyString:weakaleatView.emailField.text]?@"":weakaleatView.emailField.text,
                                    @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text};
            NSString * postApi;
            if (weakSelf.Type == PhoneVerificationType) {
                
                postApi = closePhone;
            }else if (weakSelf.Type  == emailVerificationType)
            {
                postApi = closeEmail;
            }else if (weakSelf.Type  ==gooleVerificationType)
            {
                postApi = closeTwoStep;
            }
            
            [weakSelf POSTWithHost:@"" path:postApi param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                 [weakaleatView.okButton stopAnimation];
                if ([responseObject[@"code"] integerValue]==0)//成功
                {
                    if (weakSelf.Type == PhoneVerificationType) {
                        [EXUserManager savephone:@"0"];
                        
                    }else if (weakSelf.Type == emailVerificationType)
                    {
                        [EXUserManager saveemail:@"0"];
                        
                    }else if (weakSelf.Type ==gooleVerificationType)
                    {
                        [EXUserManager saveGoole:@"0"];
                    }
                     [[NSNotificationCenter defaultCenter] postNotificationName:SetSecurityNotificationse object:nil];
                    weakSelf.isOpen = NO;
                    [weakSelf.tableView reloadData];
                    [custom dismiss];
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                    
                }else
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                }
            } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                 [weakaleatView.okButton stopAnimation];
            }];
            
        }];
       
        
    }
   
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
