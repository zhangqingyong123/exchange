//
//  CapitalPasswordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CapitalPasswordViewController.h"
#import "PasswordView.h"
#import "ZJPayPasswordView.h"
@interface CapitalPasswordViewController ()<QVCustomAlertDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITextField  *passWordField;
@property (nonatomic,strong)UITextField  *aginField;
@property (nonatomic,strong)UIButton  *NextButton;
@property (nonatomic,strong)UIButton  *openButton;
@end
@implementation CapitalPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
    [self initCapital];
    [self.view addSubview:self.NextButton];
    RACSignal*passWord=[_passWordField.rac_textSignal map:^id(NSString* value) {
       return @(value.length>PassworldLastlength && value.length<Passworldhighlength);
    }];
    RACSignal*aginFieldSignal=[_aginField.rac_textSignal map:^id(NSString* value) {
        return @(value.length>PassworldLastlength && value.length<Passworldhighlength);
    }];
    RACSignal*loginSignal=[RACSignal combineLatest:@[passWord,aginFieldSignal] reduce:^id(NSNumber*phoneValid,NSNumber*pswValid){
        return @([phoneValid boolValue]&&[pswValid boolValue]);
    }];
    WeakSelf
    [loginSignal subscribeNext:^(NSNumber* x) {
        if ([x boolValue]) {
            weakSelf.NextButton.enabled=YES;
            weakSelf.NextButton.alpha = 1;
        }else{
            weakSelf.NextButton.enabled=NO;
            weakSelf.NextButton.alpha = 0.5;
            
        }
    }];
}
- (void)initCapital
{
    NSArray * titles = @[Localized(@"Security_Password_funds"),Localized(@"Security_Duplicate_password")];
    NSArray * placeholders = @[Localized(@"Security_Password_funds_message"),Localized(@"Security_Duplicate_password_message")];
    
    for (int i =0; i<titles.count; i++)
    {
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(36) + i*90, 240, RealValue_W(30))];
        lable.textColor =RGBA(114, 145, 161, 1);
        lable.text = titles[i];
        lable.font = AutoFont(13);
        [self.view addSubview:lable];
        UITextField *passWordField = [[UITextField alloc]initWithFrame:CGRectMake(RealValue_W(30),
                                                                      lable.bottom+RealValue_W(26),
                                                                      UIScreenWidth - RealValue_W(88),
                                                                      RealValue_W(88))];
        
        passWordField.placeholder = placeholders[i];
        [passWordField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        [passWordField setValue:AutoFont(13) forKeyPath:@"_placeholderLabel.font"];
        passWordField.tintColor = TABTITLECOLOR;
        passWordField.font = AutoFont(13);
        passWordField.delegate = self;
        passWordField.textColor = MAINTITLECOLOR;
        passWordField.keyboardType = UIKeyboardTypeEmailAddress;
        passWordField.secureTextEntry = YES;
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 5)];
//        passWordField.leftView = view;
        passWordField.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview:passWordField];
        if (i ==0) {
            _passWordField = passWordField;
        }else
        {
             _aginField = passWordField;
        }
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - 60, 0, 40, 40)];
        [button setImage:[UIImage imageNamed:@"icon-hide"] forState:(UIControlStateNormal)];
        [button setImage:[UIImage imageNamed:@"icon_show"] forState:(UIControlStateSelected)];
        [button addTarget:self action:@selector(openPassword:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        button.centerY = passWordField.centerY;
        [self.view addSubview:button];
        
        
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(RealValue_W(30) , passWordField.bottom )];
        [bezierPath addLineToPoint:CGPointMake(UIScreenWidth -RealValue_W(30) , passWordField.bottom )];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = CELLCOLOR.CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [self.view.layer addSublayer:shapeLayer];
        
    }
}
- (UIButton *)NextButton
{
    if (!_NextButton) {
        _NextButton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(24), _aginField.bottom +RealValue_W(100), UIScreenWidth - RealValue_W(48), RealValue_W(80))];
        KViewRadius(_NextButton, 2);
        [_NextButton setTitle:Localized(@"Security_Next_step") forState:(UIControlStateNormal)];
        _NextButton.backgroundColor = TABTITLECOLOR;
        [_NextButton setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        [_NextButton addTarget:self action:@selector(next) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _NextButton;
}
- (void)openPassword:(UIButton *)sender {
     sender.selected = !sender.selected;
    if (sender.tag ==0)
    {
        self.passWordField.secureTextEntry = !self.passWordField.secureTextEntry;
    }else
    {
        self.aginField.secureTextEntry = !self.aginField.secureTextEntry;
    }
   
    
}
- (void)next
{
    if (![EXUnit judgePassWordLegal:_passWordField.text])
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"login_Illegal_password") view:kWindow];
        return;
    }else if (![_passWordField.text isEqualToString:_aginField.text])
    {
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"register_two_password_inconsistent_message") view:kWindow];
        return;
    }
    CustomAlertVC * custom = [[CustomAlertVC alloc] initWithCustomContentViewClass:@"BindCustonAlert" delegate:self];
    custom.direction = FromBottom;
    [custom show];
    BindCustonAlert *aleatView = (BindCustonAlert *)custom.contentView;
    if (self.isEdit)
    {
         aleatView.type = @"11";
    }else
    {
         aleatView.type = @"6";
    }
    aleatView.isBindGoole = NO;
    aleatView.cancelBlock = ^{
        [custom dismiss];
    };
    WeakSelf
    WeakObject(aleatView)
    [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
        
        if (weakSelf.isEdit)
        {
            
            if (![NSString isEmptyString:weakaleatView.emailField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.emailField.text]) {
               
                    [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
            if (![NSString isEmptyString:weakaleatView.phoneField.text])
            {
                if (![EXUnit CodeissSixplace:weakaleatView.phoneField.text]) {
                    [EXUnit showMessage:Localized(@"VerificationMessage")];
                    return ;
                }
            }
            
            
            NSDictionary * param = @{@"password":weakSelf.passWordField.text,
                                     @"password_confirmation":weakSelf.aginField.text,
                                     @"sms_code":[NSString isEmptyString:weakaleatView.phoneField.text]?@"":weakaleatView.phoneField.text,
                                     @"email_code":[NSString isEmptyString:weakaleatView.emailField.text]?@"":weakaleatView.emailField.text,
                                     @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text};
            [weakSelf POSTWithHost:@"" path:resetWithdrawPassword param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                 [weakaleatView.okButton stopAnimation];
                if ([responseObject[@"code"] integerValue]==0)//成功
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                    [custom dismiss];
                   
                     [[NSNotificationCenter defaultCenter] postNotificationName:SetSecurityNotificationse object:nil];
                    
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                }
                                                                              
            } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [weakaleatView.okButton stopAnimation];
            }];
        }else
        {
            NSDictionary * param = @{@"password":weakSelf.passWordField.text,
                                     @"password_confirmation":weakSelf.aginField.text,
                                     @"sms_code":[NSString isEmptyString:weakaleatView.phoneField.text]?@"":weakaleatView.phoneField.text,
                                     @"email_code":[NSString isEmptyString:weakaleatView.emailField.text]?@"":weakaleatView.emailField.text,
                                     @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text};
            [weakSelf POSTWithHost:@"" path:createWithdrawPassword param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                 [weakaleatView.okButton stopAnimation];
                if ([responseObject[@"code"] integerValue]==0)//成功
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                    [custom dismiss];
                    [self  getUserInfo];
                   
                }else
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                }
            } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [weakaleatView.okButton stopAnimation];
            }];
            
        }
    }];
}
- (void)getUserInfo
{
    WeakSelf
    [HomeViewModel getUserWithDic:@{} url:getUser callBack:^(BOOL isSuccess, id callBack) {
        
        if (!isSuccess) {
            
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:callBack[@"message"] view:kWindow];
            
        }else
        {
             [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}
-(void)dealloc{
    NSLog(@"%s",__FUNCTION__);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
