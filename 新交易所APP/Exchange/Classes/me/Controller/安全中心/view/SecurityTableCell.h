//
//  SecurityTableCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PresonaModle.h"
@interface SecurityTableCell : UITableViewCell
@property (nonatomic,strong)UILabel * leftLable;
@property (nonatomic,strong)UILabel * rightLable;
@property (nonatomic,strong)UISwitch * rightSwitch;
@property (nonatomic,strong)PresonaModle * modle;
@property (copy, nonatomic) void(^Switchblock)(UISwitch * sender);
@end
