//
//  SecurityTableCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SecurityTableCell.h"

@implementation SecurityTableCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.leftLable];
    [self.contentView addSubview:self.rightLable];
    [self.contentView addSubview:self.rightSwitch];
}
- (void)switchClick:(UISwitch *)sender
{
    if (self.Switchblock) {
        self.Switchblock(sender);
    }
}
- (void)setModle:(PresonaModle *)modle
{
    _leftLable.text = modle.title;
    _rightLable.text = modle.stats;
}
- (UILabel *)leftLable
{
    if (!_leftLable) {
        _leftLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(22),  RealValue_H(120)/2 - 15, 180, 30)];
        if ([[EXUnit isLocalizable] isEqualToString:@"en"]) {
            _leftLable.font = AutoFont(13);
        }else
        {
            _leftLable.font = AutoFont(15);
        }
        _leftLable.textColor = MAINTITLECOLOR;
    }
    return _leftLable;
}
- (UILabel *)rightLable
{
    if (!_rightLable) {
        _rightLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(74)-180,  RealValue_H(120)/2 - 15, 180, 30)];
        if ([[EXUnit isLocalizable] isEqualToString:@"en"]) {
            _rightLable.font = AutoFont(13);
        }else
        {
            _rightLable.font = AutoFont(15);
        }
        _rightLable.textColor = MAINTITLECOLOR;
        _rightLable.textAlignment = NSTextAlignmentRight;
    }
    return _rightLable;
}
- (UISwitch *)rightSwitch
{
    if (!_rightSwitch) {
        _rightSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(40)- 51, RealValue_H(120)/2 - 31/2, 51, 31)];
        //        _rightSwitch.tintColor = MAINTITLECOLOR1;//关状态下的背景颜色
        //        _rightSwitch.onTintColor = TABTITLECOLOR;//开状态下的背景颜色
        //        _rightSwitch.thumbTintColor = WHITECOLOR;
        //        _rightSwitch.backgroundColor = TABLEVIEWLCOLOR;
        
        
        [_rightSwitch setBackgroundColor:RGBA(240, 240, 240, 1)];
        [_rightSwitch setOnTintColor:TABTITLECOLOR];
        [_rightSwitch setThumbTintColor:[UIColor whiteColor]];
        _rightSwitch.layer.cornerRadius = 15.5f;
        _rightSwitch.layer.masksToBounds = YES;
        [_rightSwitch addTarget:self action:@selector(switchClick:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _rightSwitch;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
