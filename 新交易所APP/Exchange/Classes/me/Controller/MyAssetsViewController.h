//
//  MyAssetsViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "MainTouchTableTableView.h"
@interface MyAssetsViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UICountingLabel *RMBlable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;


@property (weak, nonatomic) IBOutlet MainTouchTableTableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *BgView;

@property (strong, nonatomic) UIButton *Rechargebutton;
@property (strong, nonatomic) UIView   *bottomView;
@property (strong, nonatomic) UIButton *withdrawbutton;
@property (weak, nonatomic) IBOutlet UIButton *hideButton;
- (IBAction)hideButtonClick:(UIButton *)sender;
@end
