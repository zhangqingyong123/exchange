//
//  RechargeDetailedViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "RechargeDetailedViewController.h"
#import "AssetsCell.h"
#import "AssetsModle.h"
@interface RechargeDetailedViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UILabel * priceLable;
@end

@implementation RechargeDetailedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    
    if (_Status ==RechargeType) {
        NSString * time = [EXUnit getLocalDateFormateUTCDate:_recordModle.CreatedAt];
        NSArray * array  = @[
                             @{@"title":Localized(@"Asset_Amount_money"),@"contro":_recordModle.amount},
                             @{@"title":Localized(@"Asset_money_time"),@"contro":time},
                             ];
        for (NSDictionary *dic in array)
        {
            
            AssetsDetaileModle *modle=[[AssetsDetaileModle alloc]init];
            modle.leftstr=dic[@"title"];
            modle.rightstr=dic[@"contro"];
            [self.dataAry addObject:modle];
        }
        _priceLable.text = [NSString stringWithFormat:@"+ %@ %@",_recordModle.amount,_recordModle.asset];
        _priceLable.textColor = TABTITLECOLOR;
        [_tableView reloadData];
        
    }else
    {
        NSString * status = @"";
        if (_cModle.status.integerValue ==0) {
            status = Localized(@"Asset_Submission");
        }else if (_cModle.status.integerValue ==1)
        {
            status = Localized(@"C2C_Audit");
        }else if (_cModle.status.integerValue ==2)
        {
            status = Localized(@"Asset_in_hand");
        }else if (_cModle.status.integerValue ==3)
        {
            status = Localized(@"Asset_Success");
        }else if (_cModle.status.integerValue ==4)
        {
            status = Localized(@"Asset_failure");
        }else if (_cModle.status.integerValue ==5)
        {
            status = Localized(@"Asset_Cancellation");
        }
        NSArray * array  = @[
                             @{@"title":Localized(@"my_Currency_address"),@"contro":_cModle.address},
                             @{@"title":Localized(@"Asset_Amount_of_money"),@"contro":_cModle.amount},
                             @{@"title":Localized(@"Asset_Service_Charge"),@"contro":_cModle.fee},
                             @{@"title":Localized(@"Asset_Withdraw_time"),@"contro":[EXUnit getLocalDateFormateUTCDate:_cModle.CreatedAt]},
                             @{@"title":Localized(@"Asset_current_state"),@"contro":status},
                             ];
        for (NSDictionary *dic in array)
        {
            
            AssetsDetaileModle *modle=[[AssetsDetaileModle alloc]init];
            modle.leftstr=dic[@"title"];
            modle.rightstr=dic[@"contro"];
            [self.dataAry addObject:modle];
        }
        _priceLable.text = [NSString stringWithFormat:@"+ %@ %@",_cModle.amount,_cModle.asset];
        _priceLable.textColor = MAINTITLECOLOR1;
        [_tableView reloadData];
    }
}

#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -49 -64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[AssetsDetailedCell class] forCellReuseIdentifier:@"AssetsDetailedCell"];
        _tableView.tableHeaderView = [self headView];
        
    }
    return _tableView;
}
- (UIView *)headView
{
    UIView * headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(120))];
    headView.backgroundColor = TABLEVIEWLCOLOR;
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(RealValue_H(20) , headView.height -1)];
    [bezierPath addLineToPoint:CGPointMake(UIScreenWidth -RealValue_H(20) , headView.height -1)];
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = CELLCOLOR.CGColor;
    shapeLayer.fillColor  = [UIColor clearColor].CGColor;
    shapeLayer.path = bezierPath.CGPath;
    shapeLayer.lineWidth = 0.5f;
    [headView.layer addSublayer:shapeLayer];
    _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(0, RealValue_H(120)/2 -20, UIScreenWidth, 40)];
    
    _priceLable.font = AutoFont(22);
    _priceLable.textAlignment = NSTextAlignmentCenter;
    
    [headView addSubview:_priceLable];
    return headView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetsDetailedCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"AssetsDetailedCell"];
    cell.modle=_dataAry[indexPath.row];
    if (_Status ==WithdrawType) {
        if (indexPath.row == _dataAry.count -1) {
            cell.rightLable.textColor = MAINTITLECOLOR1;
        }
        
    }else
    {
        cell.rightLable.textColor = MAINTITLECOLOR;
    }
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
