//
//  RechargeRecordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "RechargeRecordViewController.h"
#import "JGPopView.h"
#import "AssetsCell.h"
#import "RechargeDetailedViewController.h"
#import "RecordModle.h"
@interface RechargeRecordViewController ()<selectIndexPathDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray; //全部数组
@property (nonatomic, strong) NSString * market;
@property (assign, nonatomic) NSUInteger     pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation RechargeRecordViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _market = _array.firstObject;
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        
        [weakSelf datalist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf datalist];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)datalist
{
    WeakSelf
    
    [weakSelf GETWithHost:@"" path:rechargeMoneyLog param:@{@"asset":_market,@"offset":@(weakSelf.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataArray addObjectsFromArray:[RecordModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataArray = [RecordModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                [weakSelf.tableView reloadData];
            }
            [self placeholderViewWithFrame:CGRectMake(0, RealValue_W(108), weakSelf.tableView.width, weakSelf.tableView.height-RealValue_W(108)) NoNetwork:NO];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [weakSelf placeholderViewWithFrame:CGRectMake(0, RealValue_W(108), weakSelf.tableView.width, weakSelf.tableView.height-RealValue_W(108)) NoNetwork:YES];
    }];
    
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        WeakSelf
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                weakSelf.pages = 0;
                [weakSelf datalist];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight - RealValue_W(88) - IPhoneBottom)style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[AssetsRecordsCell class] forCellReuseIdentifier:@"AssetsRecordsCell"];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.000001)];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(188);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_W(108);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetsRecordsCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"AssetsRecordsCell"];
    cell.modle = _dataArray[indexPath.row];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(108))];
    bottomView.backgroundColor = TABLEVIEWLCOLOR;
    
    UIButton * rightbutton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(25) -100, RealValue_W(108)/2 - 15, 100, 30)];
    [rightbutton setTitle:_market forState:(UIControlStateNormal)];
    [rightbutton setImage:[UIImage imageNamed:@"Drop-down"] forState:UIControlStateNormal];
    rightbutton.titleLabel.font = AutoBoldFont(13);
    [rightbutton addTarget:self action:@selector(currencyButton:) forControlEvents:UIControlEventTouchUpInside];
    [rightbutton setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
    rightbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;// 水平左对齐
    [rightbutton centerHorizontallyImageAndTextWithPadding:-30];
    [bottomView addSubview:rightbutton];
    return bottomView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecordModle * model = _dataArray[indexPath.row];
    RechargeDetailedViewController * rechargeRecordVC = [[RechargeDetailedViewController alloc] init];
    rechargeRecordVC.title = Localized(@"Asset_Recharge");
    rechargeRecordVC.Status = RechargeType;
    rechargeRecordVC.recordModle = model;
    [self.navigationController pushViewController:rechargeRecordVC animated:YES];
}
- (void)popDataWithButton:(UIButton *)button  array:(NSArray *)dataArray PointX:(CGFloat)PointX
{
    CGPoint point = CGPointMake(PointX ,RealValue_W(108)/2 +15);
     CGFloat height;
    if (dataArray.count>8)
    {
        height = 8 * RealValue_W(80);
    }else
    {
        height =  dataArray.count * RealValue_W(80);
    }
    JGPopView *view2 = [[JGPopView alloc] initWithOrigin:point Width:RealValue_W(160) Height:height Type:JGTypeOfUpRight Color:TRABSACTIONColor];
    view2.dataArray = dataArray;
    
    view2.row_height = 40;
    view2.delegate = self;
    [view2 popView];
    [_tableView addSubview:view2];
}
- (void)currencyButton:(UIButton *)button
{
    [self popDataWithButton:button array:_array PointX:button.right - RealValue_W(16)];
}
#pragma JGPopViewDelegate
- (void)selectIndexPathRow:(NSInteger)index{
    
    _market = _array[index];
    self.pages = 0;
    [self datalist];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
