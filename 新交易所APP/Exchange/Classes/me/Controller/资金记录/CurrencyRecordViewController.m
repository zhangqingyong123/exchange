//
//  CurrencyRecordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CurrencyRecordViewController.h"
#import "JGPopView.h"
#import "AssetsCell.h"
#import "RechargeDetailedViewController.h"
@interface CurrencyRecordViewController ()<selectIndexPathDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray; //全部数组
@property (nonatomic, strong) NSMutableArray * StatusArray;
@property (nonatomic, strong) NSArray * statuss;
@property (nonatomic, strong) NSString * market;
@property (nonatomic, strong) NSString * statussName;
@property (nonatomic, assign) BOOL  isChooseMarket;
@property (assign, nonatomic) NSUInteger     pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation CurrencyRecordViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _market = _array.firstObject;
    _Type = ALLType;
    _statuss = @[Localized(@"Asset_Submission"),Localized(@"C2C_Audit"),Localized(@"Asset_in_hand"),Localized(@"Asset_Success"),Localized(@"Asset_failure"),Localized(@"Asset_Cancellation")];
    _statussName = Localized(@"Asset_whole");
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    EXRefrechHeader *header = [EXRefrechHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        
        [weakSelf datalist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf datalist];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)datalist
{
    WeakSelf
    [weakSelf GETWithHost:@"" path:withdrawMoneyLog param:@{@"asset":_market,@"offset":@(weakSelf.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataArray addObjectsFromArray:[CurrencyModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataArray = [CurrencyModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
            [self placeholderViewWithFrame:CGRectMake(0, RealValue_W(108), weakSelf.tableView.width, weakSelf.tableView.height-RealValue_W(108)) NoNetwork:NO];
            [weakSelf.tableView reloadData];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [weakSelf placeholderViewWithFrame:CGRectMake(0, RealValue_W(108), weakSelf.tableView.width, weakSelf.tableView.height-RealValue_W(108)) NoNetwork:YES];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        WeakSelf
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                weakSelf.pages = 0;
                [weakSelf datalist];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight - RealValue_W(88) - IPhoneBottom)style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[AssetsRecordsCell class] forCellReuseIdentifier:@"AssetsRecordsCell"];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.000001)];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (NSMutableArray *)StatusArray
{
    if (!_StatusArray) {
        _StatusArray = [[NSMutableArray alloc]init];
    }
    return _StatusArray;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(188);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (_Type) {
        case StatusType:
            return self.StatusArray.count;
            break;
        case ALLType:
            return _dataArray.count;
            break;
        default:
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_W(108);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetsRecordsCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"AssetsRecordsCell"];
    if (_Type == StatusType) {
        cell.Cmodle = _StatusArray[indexPath.row];
    }else if (_Type == ALLType)
    {
        cell.Cmodle = _dataArray[indexPath.row];
    }
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(108))];
    bottomView.backgroundColor = TABLEVIEWLCOLOR;
    
    UIButton * leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(25), RealValue_W(108)/2 - 15, 120, 30)];
    [leftbutton setTitle:_statussName forState:(UIControlStateNormal)];
    [leftbutton setImage:[UIImage imageNamed:@"Drop-down"] forState:UIControlStateNormal];
    [leftbutton addTarget:self action:@selector(transactionButton:) forControlEvents:UIControlEventTouchUpInside];
    leftbutton.titleLabel.font = AutoBoldFont(13);
    [leftbutton setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
    [leftbutton setTitleEdgeInsets:UIEdgeInsetsMake(0, -RealValue_W(30), 0, RealValue_W(30))];
    [leftbutton setImageEdgeInsets:UIEdgeInsetsMake(0, RealValue_W(100), 0, -RealValue_W(100))];
    leftbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;// 水平左对齐
    [bottomView addSubview:leftbutton];
    
    UIButton * rightbutton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(25) -100, RealValue_W(108)/2 - 15, 100, 30)];
    [rightbutton setTitle:_market forState:(UIControlStateNormal)];
    [rightbutton setImage:[UIImage imageNamed:@"Drop-down"] forState:UIControlStateNormal];
    rightbutton.titleLabel.font = AutoBoldFont(13);
    [rightbutton addTarget:self action:@selector(currencyButton:) forControlEvents:UIControlEventTouchUpInside];
    [rightbutton setTitleColor:MAINTITLECOLOR forState:UIControlStateNormal];
    rightbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;// 水平左对齐
    [rightbutton centerHorizontallyImageAndTextWithPadding:-30];
    [bottomView addSubview:rightbutton];
    return bottomView;
}
- (void)popDataWithButton:(UIButton *)button  array:(NSArray *)dataArray PointX:(CGFloat)PointX Width:(CGFloat)Width
{

    
    CGPoint point = CGPointMake(PointX ,RealValue_W(108)/2 +15);
    CGFloat height;
    if (dataArray.count>8)
    {
        height = 8 * RealValue_W(80);
    }else
    {
        height =  dataArray.count * RealValue_W(80);
    }
    JGPopView *view2 = [[JGPopView alloc] initWithOrigin:point Width:Width Height:height Type:JGTypeOfUpRight Color:TRABSACTIONColor];
    view2.dataArray = dataArray;
    view2.row_height = 40;
    view2.delegate = self;
    [view2 popView];
    [_tableView addSubview:view2];
}
//买入卖出
- (void)transactionButton:(UIButton *)button
{
    _isChooseMarket = NO;
    if ([[EXUnit isLocalizable] isEqualToString:@"en"])
    {
        [self popDataWithButton:button array:_statuss PointX:button.right+20 Width:160];
    }else
    {
        [self popDataWithButton:button array:_statuss PointX:button.right - RealValue_W(122) Width:RealValue_W(150)];
    }
    
}
- (void)currencyButton:(UIButton *)button
{
    _isChooseMarket = YES;
    [self popDataWithButton:button array:_array PointX:button.right - RealValue_W(16)Width:RealValue_W(150)];
}
#pragma JGPopViewDelegate
- (void)selectIndexPathRow:(NSInteger)index{
    if (_isChooseMarket) {
        
        _Type = ALLType;
        _pages = 0;
        _market = _array[index];
        _statussName = Localized(@"Asset_whole");
        [self.StatusArray removeAllObjects];
        [self datalist];
    }else
    {
        _statussName = _statuss[index];
        _Type = StatusType;
        [self.StatusArray removeAllObjects];
        switch (index) {
            case 0: //已提交
            {
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==0) {
                        [self.StatusArray addObject:modle];
                    }
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                
                break;
            case 1: //审核中
            {
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==1) {
                        [self.StatusArray addObject:modle];
                    }
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
            case 2: //处理中
            {
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==2) {
                        [self.StatusArray addObject:modle];
                    }
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
            case 3: //成功
            {
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==3) {
                        [self.StatusArray addObject:modle];
                    }
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
            case 4: //提现失败
            {
                
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==4) {
                        [self.StatusArray addObject:modle];
                    }
                }
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
            case 5: //取消提现
            {
                for ( CurrencyModle *modle in _dataArray) {
                    if (modle.status.integerValue ==5) {
                        [self.StatusArray addObject:modle];
                    }
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
                break;
                
            default:
                break;
        }
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrencyModle *modle;
    if (_Type == StatusType) {
        modle = _StatusArray[indexPath.row];
        
    }else if (_Type == ALLType)
    {
        modle = _dataArray[indexPath.row];
    }
    RechargeDetailedViewController * RechargeDetailedVC = [[RechargeDetailedViewController alloc] init];
    RechargeDetailedVC.title = Localized(@"Asset_Withdraw_money");
    RechargeDetailedVC.Status = WithdrawType;
    RechargeDetailedVC.cModle = modle;
    [self.navigationController pushViewController:RechargeDetailedVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
