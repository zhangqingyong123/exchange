//
//  AssetRecordsViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AssetRecordsViewController.h"
#import "AssetPageView.h"
@interface AssetRecordsViewController ()
@property(nonatomic,strong) AssetPageView *pageView;
@end

@implementation AssetRecordsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:[self test1]];
}
- (AssetPageView *)test1 {
    _pageView = [[AssetPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight)
                                          withTitles:@[Localized(@"Asset_Recharge_record"),Localized(@"Asset_Currency_record")]
                                 withViewControllers:@[@"RechargeRecordViewController",@"CurrencyRecordViewController"]
                                      withParameters:nil];
    
    _pageView.selectedColor = WHITECOLOR;
    _pageView.unselectedColor = maintitleLCOLOR ;
    _pageView.backgroundColor = MAINBLACKCOLOR;
    _pageView.index = _index;
    _pageView.dataArray = _dataArray;
    return _pageView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
