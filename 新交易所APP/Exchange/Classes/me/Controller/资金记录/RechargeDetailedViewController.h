//
//  RechargeDetailedViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "RecordModle.h"
typedef enum : NSUInteger {
    RechargeType = 0,//充值明细
    WithdrawType = 1,  //提币明细
}DetailedStatus;

@interface RechargeDetailedViewController : BaseViewViewController
@property (nonatomic,readwrite)DetailedStatus Status;
@property (nonatomic,strong)RecordModle * recordModle;
@property (nonatomic,strong)CurrencyModle * cModle;
@end
