//
//  CurrencyRecordViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
typedef enum : NSUInteger {
    StatusType = 0,//
  
    ALLType = 2,  //全部
}Status;
@interface CurrencyRecordViewController : BaseViewViewController
@property (nonatomic,strong)NSMutableArray * array;
@property (nonatomic,readwrite)Status Type;
@end
