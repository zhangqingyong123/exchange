//
//  AssetRecordsViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface AssetRecordsViewController : BaseViewViewController
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,strong)NSMutableArray * dataArray;
@end
