//
//  LanguageViewController.m
//  ZBLocalized
//
//  Created by NQ UEC on 2017/5/12.
//  Copyright © 2017年 Suzhibin. All rights reserved.
//

#import "LanguageViewController.h"
#import "BaseTabBarController.h"
#import "SetCell.h"
@interface LanguageViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,assign)NSInteger  index;

@end

@implementation LanguageViewController
- (void)viewWillAppear:(BOOL)animated
{
    [self barLine:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self barLine:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *languageCode = [[NSUserDefaults standardUserDefaults] objectForKey:appLanguage];
    NSLog(@"系统语言:%@",languageCode);
    if([languageCode hasPrefix:@"zh-Hans"]){
        _index = 1;  //繁体中文
    }else if([languageCode hasPrefix:@"zh-Hant"]){
        _index = 2;//简体中文
    } if([languageCode hasPrefix:@"ja"]){
        _index = 3;//日语
    }else if([languageCode hasPrefix:@"en"]){
        _index = 4;//英语
    }else{
        //        languageCode = @"zh-Hans";//英语
    }
    self.view.backgroundColor= TABLEVIEWLCOLOR;
    [self.view addSubview:self.tableView];
    [self.dataArray addObject:Localized(@"set_default_language")];
    [self.dataArray addObject:@"中文(简)"];
    [self.dataArray addObject:@"中文(繁)"];
    [self.dataArray addObject:@"日本語"];
    [self.dataArray addObject:@"English"];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ValuationCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ValuationCell"];
    cell.titlelable.text = _dataArray[indexPath.row];
    if (indexPath.row == _index) {
        cell.imgView.image = [UIImage imageNamed:@"ValuationChoose"];
    }else
    {
        cell.imgView.image = [UIImage imageNamed:@""];
    }
    return cell;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [EXUserManager saveDefaultLanguage:@"0"];
    
    if (indexPath.row==0) {
        NSArray *languages = [NSLocale preferredLanguages];
        NSString *currentLanguage = [languages objectAtIndex:0];
        [[ZBLocalized sharedInstance]setLanguage:currentLanguage];
        [EXUserManager saveDefaultLanguage:@"1"];
        
        [[ZBLocalized sharedInstance] initLanguage]; //设置app语言
    }
    if (indexPath.row==1) {
        
        [[ZBLocalized sharedInstance]setLanguage:@"zh-Hans"];
        
        
    }
    if (indexPath.row==2) {
        
        [[ZBLocalized sharedInstance]setLanguage:@"zh-Hant"];
        
    }
    if (indexPath.row==3) {
        
        [[ZBLocalized sharedInstance]setLanguage:@"ja"];
    }
    if (indexPath.row==4) {
        
        [[ZBLocalized sharedInstance]setLanguage:@"en"];
    }
    [self initRootVC];
}

- (void)initRootVC{
    [AppDelegate shareAppdelegate].window.backgroundColor=MAINBLACKCOLOR;
    [AppDelegate shareAppdelegate].window.rootViewController = [[BaseTabBarController alloc] init];
    [[AppDelegate shareAppdelegate].window makeKeyAndVisible];
    
}
//懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -TABBAR_HEIGHT1 -64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[ValuationCell class] forCellReuseIdentifier:@"ValuationCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
