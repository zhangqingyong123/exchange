//
//  SetCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetModle.h"
@interface SetCell : UITableViewCell
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UILabel * rightlable;
@property (nonatomic,strong)UIImageView *returnImg;
@property (nonatomic,strong)SetModle * modle;
@end
@interface ValuationCell : UITableViewCell
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UIImageView * imgView;
@end
