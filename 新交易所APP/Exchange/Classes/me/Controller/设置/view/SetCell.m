//
//  SetCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SetCell.h"

@implementation SetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
    }
    return self;
    
}
- (void)setModle:(SetModle *)modle
{
    _titlelable.text = modle.leftStr;
}
- (void)get_up
{
    [self.contentView addSubview:self.titlelable];
    [self.contentView addSubview:self.rightlable];
    [self.contentView addSubview:self.returnImg];
}
- (UIImageView *)returnImg
{
    if (!_returnImg) {
        _returnImg = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(30) - RealValue_W(10),
                                                                   RealValue_H(120)/2 - RealValue_W(16)/2,
                                                                   RealValue_W(10),
                                                                   RealValue_W(16))];
        _returnImg.image = [UIImage imageNamed:@"icon_list_go"];
    }
    return _returnImg;
    
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(+RealValue_W(30),  RealValue_H(120)/2 - 15, 240, 30)];
        _titlelable.font = AutoFont(15);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.adjustsFontSizeToFitWidth = YES;
    }
    return _titlelable;
}
- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(74) -120,  RealValue_H(120)/2 - 15, 120, 30)];
        _rightlable.font = AutoFont(15);
        _rightlable.textColor = MAINTITLECOLOR1;
        _rightlable.adjustsFontSizeToFitWidth = YES;
        _rightlable.textAlignment = NSTextAlignmentRight;
    }
    return _rightlable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

@implementation ValuationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.titlelable];
    [self.contentView addSubview:self.imgView];
}

- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(+RealValue_W(30),  RealValue_H(106)/2 - 15, 240, 30)];
        _titlelable.font = AutoFont(15);
        _titlelable.textColor = MAINTITLECOLOR;
    
    }
    return _titlelable;
}
- (UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(50) - RealValue_W(34),
                                                                 RealValue_H(106)/2 - RealValue_W(24)/2,
                                                                 RealValue_W(34),
                                                                 RealValue_W(24))];
        _imgView.image = [UIImage imageNamed:@"ValuationChoose"];
    }
    return _imgView;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
