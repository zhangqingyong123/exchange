//
//  ValuationMethodViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/22.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "ValuationMethodViewController.h"
#import "SetCell.h"
@interface ValuationMethodViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,assign)NSInteger  index;
@end

@implementation ValuationMethodViewController
- (void)viewWillAppear:(BOOL)animated
{
    [self barLine:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self barLine:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[EXUserManager getValuationMethod] isEqualToString:@"CNY"]) {
        _index = 0;
    }else if ([[EXUserManager getValuationMethod] isEqualToString:@"USD"])
    {
        _index = 1;
    }
   
    if (![[EXUnit getSite] isEqualToString:@"BitAladdin"]) {
         [self.dataArray addObject:@"CNY"];
        
    }else
    {
       _index = 0;
    }
    [self.dataArray addObject:@"USD"];
    [self.view addSubview:self.tableView];
  
  
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -TABBAR_HEIGHT1 -64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[ValuationCell class] forCellReuseIdentifier:@"ValuationCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(106);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ValuationCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ValuationCell"];
    cell.titlelable.text = _dataArray[indexPath.row];
    if (indexPath.row == _index) {
        cell.imgView.image = [UIImage imageNamed:@"ValuationChoose"];
    }else
    {
        cell.imgView.image = [UIImage imageNamed:@""];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _index = indexPath.row;
    [EXUserManager saveValuationMethod:_dataArray[indexPath.row]];
     [[NSNotificationCenter defaultCenter]postNotificationName:ValuationMethodNotificationse object:nil];
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
