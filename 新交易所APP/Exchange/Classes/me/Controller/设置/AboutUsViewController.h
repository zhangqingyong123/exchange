//
//  AboutUsViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface AboutUsViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end
