//
//  AboutUsViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AboutUsViewController.h"
#import "TGWebProgressLayer.h"
@interface AboutUsViewController ()
@property (nonatomic, strong)TGWebProgressLayer *webProgressLayer;
@end
@implementation AboutUsViewController
- (void)viewWillAppear:(BOOL)animated
{
    [self barLine:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self barLine:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
    _textView.editable = NO;
    _textView.showsVerticalScrollIndicator = NO;
     [self getMessageInfo];
    self.webProgressLayer = [[TGWebProgressLayer alloc] init];
    self.webProgressLayer.frame = CGRectMake(0, 48, UIScreenWidth, 2);
    self.webProgressLayer.strokeColor =  TABTITLECOLOR.CGColor;
    [self.navigationController.navigationBar.layer addSublayer:self.webProgressLayer];
    [self.webProgressLayer tg_startLoad];
}
#pragma mark----数据
- (void)getMessageInfo
{
    WeakSelf
    NSString * str = [NSString stringWithFormat:@"%@/about-us",about];
    [self GETWithHost:@"" path:str param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSString * result = responseObject[@"result"][@"content"];
            //验证成功，主线程处理UI
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                // 处理耗时操作的代码块...
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[result dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                //通知主线程刷新
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.webProgressLayer tg_finishedLoadWithError:nil];
                    weakSelf.textView.attributedText = attrStr;
                    weakSelf.textView.textColor = RGBA(114, 145, 161, 1);
                });
                
            });
        }else
        {
            [self.webProgressLayer tg_finishedLoadWithError:nil];
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
