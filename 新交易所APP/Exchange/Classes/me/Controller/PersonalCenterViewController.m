//
//  PersonalCenterViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "PersonalCenterViewController.h"
#import "PresonaModle.h"
#import "PresonalHeadView.h"
#import "PresonaCell.h"
#import "AboutUsViewController.h"
#import "CurrencyAddressViewController.h"
#import "SecurityCenterViewController.h"
#import "IdentityAuthenticationViewController.h"
#import "MyAssetsViewController.h"
#import "OrderListViewController.h"
#import "LoginViewController.h"
#import "FailViewController.h"
#import "SuccessViewController.h"
#define headViewHeight RealValue_W(200)
@interface PersonalCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)PresonalHeadView *tableviewHeadView;
@property (nonatomic,strong)NSString *identity_status; //0,未认证 1，审核中 2.认证成功 3.失败
@end

@implementation PersonalCenterViewController
- (void)LoginSusess
{
    [self getUserInfo];
}
- (void)exitLogon
{
    self.navigationController.tabBarController.hidesBottomBarWhenPushed=NO;
    self.navigationController.tabBarController.selectedIndex=0; 
    LoginViewController * loginView = [[LoginViewController alloc] init];
    [self presentViewController:loginView animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   [self.view addSubview:self.tableView];
   [self getData];
   [self getUserInfo];
   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LoginSusess) name:LoginSusessNotificationse object:nil];
   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(exitLogon) name:ExitLogonNotificationse object:nil];
   
}
- (void)getUserInfo
{
    WeakSelf
    [self GETWithHost:@"" path:getUser param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            [weakSelf.tableviewHeadView getUserInfo:responseObject[@"result"][@"phone"] lastLoginTime:responseObject[@"result"][@"last_login"]];
            weakSelf.identity_status = responseObject[@"result"][@"identity_status"];
          
            [self.tableView reloadData];
            
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma mark----数据
- (void)getData
{
    NSArray * array  =@[@{@"image":@"icon_me_order",@"title":@"订单管理",@"controllerClass":[OrderListViewController class]},
                        @{@"image":@"icon_me_wallet",@"title":@"我的资产",@"controllerClass":[MyAssetsViewController class]},
                        @{@"image":@"icon_me_agenda",@"title":@"身份认证",@"controllerClass":[IdentityAuthenticationViewController class]},
                        @{@"image":@"icon_me_shield",@"title":@"安全中心",@"controllerClass":[SecurityCenterViewController class]},
                        @{@"image":@"icon_me_shield",@"title":@"提币地址",@"controllerClass":[CurrencyAddressViewController class]},
                        @{@"image":@"icon_me_info",@"title":@"关于我们",@"controllerClass":[AboutUsViewController class]} ];
                  
    for (NSDictionary *dic in array)
    {
        
        PresonaModle *modle=[[PresonaModle alloc]init];
        modle.title=dic[@"title"];
        modle.image=dic[@"image"];
        modle.controllerClass=dic[@"controllerClass"];
        [self.dataAry addObject:modle];
    }
    [_tableView reloadData];
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -49 -64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[PresonaCell class] forCellReuseIdentifier:@"PresonaCell"];
        _tableView.tableHeaderView = self.tableviewHeadView;
        
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (PresonalHeadView*)tableviewHeadView{
    WeakSelf
    if (!_tableviewHeadView) {
        
        _tableviewHeadView =[[ PresonalHeadView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, headViewHeight)];
        _tableviewHeadView.backgroundColor = MAINBLACKCOLOR;
    };
    return _tableviewHeadView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(120);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PresonaCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PresonaCell"];
    cell.model=_dataAry[indexPath.row];
    if (indexPath.row ==2)
    {
        if (_identity_status.integerValue ==1)
        {
            cell.rightImg.hidden = YES;
            cell.rightlable.hidden = NO;
            cell.rightlable.text = @"审核中";
        }else if (_identity_status.integerValue ==2)
        {
            cell.rightImg.hidden = NO;
            cell.rightlable.hidden = YES;
        }else if (_identity_status.integerValue ==3)
        {
            cell.rightlable.hidden = NO;
            cell.rightImg.hidden = YES;
            cell.rightlable.text = @"审核失败";
            cell.rightlable.textColor = RGBA(250, 74, 97, 1);
        }else if (_identity_status.integerValue ==0)
        {
            cell.rightImg.hidden = YES;
            cell.rightlable.hidden = NO;
            cell.rightlable.text = @"未认证";
        }
    }else
    {
         cell.rightlable.hidden = YES;
         cell.rightImg.hidden = YES;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==2)
    {
        switch (_identity_status.integerValue) {
            case 0: //未认证
            {
                IdentityAuthenticationViewController * idACationVC = [[IdentityAuthenticationViewController alloc] init];
                idACationVC.title = @"身份认证";
                [self.navigationController pushViewController:idACationVC animated:YES];
            }
                break;
            case 1: //审核中
                
                break;
            case 2: //已认证
            {
                IdentityAuthenticationViewController * idACationVC = [[IdentityAuthenticationViewController alloc] init];
                idACationVC.title = @"身份认证";
                [self.navigationController pushViewController:idACationVC animated:YES];
            }
                
                break;
            case 3: //认证失败
                
                break;
                
            default:
                break;
        }
    }else
    {
        PresonaModle *si = _dataAry[indexPath.row];
        BaseViewViewController *vc = [[si.controllerClass alloc] init];
        vc.title = si.title;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
    
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
