//
//  IdentityModel.h
//  Exchange
//
//  Created by 张庆勇 on 2019/1/25.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IdentityModel : NSObject
@property (nonatomic,strong)NSString * photoTitle;
@property (nonatomic,strong)NSString * title;
@property (nonatomic,strong)NSString * contont;
@property (nonatomic,strong)NSString * iamgeStr;
@end

NS_ASSUME_NONNULL_END
