//
//  FootView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/25.
//  Copyright © 2019年 张庆勇. All rights reserved.
//



#import "FootView.h"
@implementation FootView
#define Start_X 15.0f           // 第一个按钮的X坐标
#define Start_Y 5.0f           // 第一个按钮的Y坐标
#define Width_Space 10.0f        // 2个按钮之间的横间距
#define Height_Space 40.0f      // 竖间距
#define Button_Height RealValue_H(212)     // 高
#define Button_Width RealValue_H(334)    // 宽
- (instancetype)initWithisAU:(BOOL)isAu
{
    if (isAu) {
       self = [super initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(360 *2))];
    }else
    {
      self = [super initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(186 *2) +10)];
    }
   
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        [self addSubview:self.titleLable];
        
        NSArray * titles;
        NSArray * images;
        if (isAu) {
            titles = @[Localized(@"Authentication_Document_photo_message"),Localized(@"C2CBuyMessage_Address_photo"),Localized(@"Authentication_qianming")];
            images = @[@"shili1",@"shili2_1",@"shili2_2"];
        }else
        {
            titles = @[Localized(@"Authentication_Document_photo_message"),Localized(@"Authentication_Statement_photo_message")];
            images = @[@"shili1",@"shili2"];
        }
        for (int i=0; i<images.count; i++) {
            NSInteger index = i % 2;
            NSInteger page = i / 2;
            UIImageView * imageview = [[UIImageView alloc] init];
            imageview.frame = CGRectMake(index * (Button_Width + Width_Space) + Start_X, page  * (Button_Height + Height_Space)+ _titleLable.bottom + 10 + Start_Y, Button_Width, Button_Height);
            imageview.image = [UIImage imageNamed:images[i]];
            [self addSubview:imageview];
            UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(0, imageview.bottom +10, 120, 20)];
            lable.textColor =MAINTITLECOLOR1;
            lable.font = AutoFont(13);
            lable.textAlignment = NSTextAlignmentCenter;
            lable.centerX = imageview.centerX;
            lable.text = titles[i];
            [self addSubview:lable];
            if (!isAu) {
                if (i ==1)
                {
                    UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(imageview.right -20 -53, imageview.bottom -45, 53, 45)];
                    lable.textColor = BLACKCOLOR;
                    lable.numberOfLines = 0;
                    lable.text = [NSString stringWithFormat:@"%@\nID:%@\n%@",[AppDelegate shareAppdelegate].name,[EXUserManager personalData].id,[EXUnit getCurrentTimes]];
                    lable.textAlignment = NSTextAlignmentCenter;
                    lable.font = [UIFont fontWithName:@"Chalkduster" size:7];//又
                    [self addSubview:lable];
                }
            }
            
        }
       
    }
    return self;
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(14, 10, 200, 20)];
        _titleLable.text = Localized(@"Authentication_Sample_picture_message");
        _titleLable.font = AutoBoldFont(16);
        _titleLable.textColor = MAINTITLECOLOR;
     
    }
    return _titleLable;
}
@end
