//
//  IdentityCell.m
//  Exchange
//
//  Created by 张庆勇 on 2019/1/25.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "IdentityCell.h"

@implementation IdentityCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    return self;
    
}
- (void)certificatestapAction
{
    if (self.upPhotoBlock) {
        self.upPhotoBlock();
    }
}
- (void)setModel:(IdentityModel *)model
{
    _model = model;
    self.title.text = _model.title;
    NSString * colorstr = [ZBLocalized sharedInstance].AppbBGColor;
    UIColor * lableColor;
    if ([NSString isEmptyString:colorstr] ||[colorstr isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        lableColor = RGBA(98, 98, 192, 1);
    }else
    {
       lableColor = [UIColor blackColor];
    }
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:model.contont];
    NSRange range1 = [[str string] rangeOfString:Localized(@"Authentication_document_information_message")];
    [str addAttribute:NSForegroundColorAttributeName value:lableColor range:range1];
    NSRange range2 = [[str string] rangeOfString:Localized(@"Authentication_Statement_photo_message")];
    [str addAttribute:NSForegroundColorAttributeName value:lableColor range:range2];
    NSRange range0 = [[str string] rangeOfString:Localized(@"Authentication_date_message")];
    [str addAttribute:NSForegroundColorAttributeName value:lableColor range:range0];
    NSRange range3 = [[str string] rangeOfString:Localized(@"Authentication_user_id_message")];
    [str addAttribute:NSForegroundColorAttributeName value:lableColor range:range3];
    NSRange range4 = [[str string] rangeOfString:[AppDelegate shareAppdelegate].name];
    [str addAttribute:NSForegroundColorAttributeName value:lableColor range:range4];
    self.conton.attributedText = str;
    CGSize highsize = [self.conton sizeThatFits:CGSizeMake(UIScreenWidth -30, MAXFLOAT)];
  
    self.conton.height = highsize.height;
    self.photoView.mj_y = _conton.bottom +10;
    self.photoTitle.text = _model.photoTitle;
    [self.bgphoto sd_setImageWithURL:[NSURL URLWithString:model.iamgeStr]];
}
- (void)get_up
{
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.conton];
    [self.contentView addSubview:self.photoView];
    [_photoView addSubview:self.photo];
    [_photoView addSubview:self.photoTitle];
    [_photoView addSubview:self.bgphoto];
}
- (UILabel *)title
{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 200, 20)];
        _title.font = AutoBoldFont(16);
        _title.textColor = MAINTITLECOLOR;
    }
    return _title;
}
- (UILabel *)conton
{
    if (!_conton) {
        _conton = [[UILabel alloc] initWithFrame:CGRectMake(15, _title.bottom + 15, UIScreenWidth -30, 0)];
        _conton.font = AutoFont(12);
        _conton.textColor = MAINTITLECOLOR1;
        _conton.numberOfLines = 0;
        [_conton sizeToFit];
        _conton.width = UIScreenWidth -30;
    }
    return _conton;
}
- (UIView *)photoView
{
    if (!_photoView) {
        _photoView = [[UIView alloc] initWithFrame:CGRectMake(15, _conton.bottom +10, UIScreenWidth -30, 200)];
        _photoView.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
         KViewRadius(_photoView, 4);
    }
    return _photoView;
}
- (UIImageView *)photo
{
    if (!_photo) {
        _photo = [[UIImageView alloc] initWithFrame:CGRectMake(_photoView.width/2 - 45/2, _photoView.height/2 -29, 45, 36)];
        _photo.image = [UIImage imageNamed:@"xiangji"];
    }
    return _photo;
}
- (UILabel *)photoTitle
{
    if (!_photoTitle) {
        _photoTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, _photo.bottom +8, _photoView.width -30, 18)];
        _photoTitle.font = AutoFont(14);
        _photoTitle.textColor = MAINTITLECOLOR1;
        _photoTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _photoTitle;
}
- (UIImageView *)bgphoto
{
    if (!_bgphoto) {
        _bgphoto = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, _photoView.width, _photoView.height)];
        UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(certificatestapAction)];
        _bgphoto.userInteractionEnabled = YES;
        [_bgphoto addGestureRecognizer:tapGesturRecognizer];
    }
    return _bgphoto;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
