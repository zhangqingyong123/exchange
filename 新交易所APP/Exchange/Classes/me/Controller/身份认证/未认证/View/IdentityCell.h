//
//  IdentityCell.h
//  Exchange
//
//  Created by 张庆勇 on 2019/1/25.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdentityModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface IdentityCell : UITableViewCell
@property (nonatomic,strong)UILabel * title;
@property (nonatomic,strong)UILabel * conton;
@property (nonatomic,strong)UIView * photoView;
@property (nonatomic,strong)UIImageView * photo;
@property (nonatomic,strong)UILabel * photoTitle;
@property (nonatomic,strong)UIImageView * bgphoto;
@property (nonatomic,strong)IdentityModel * model;
@property (copy, nonatomic) void(^upPhotoBlock)(void);
@end

NS_ASSUME_NONNULL_END
