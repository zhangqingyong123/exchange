//
//  IdentityAuthenticationViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface IdentityAuthenticationViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *countryTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *countryLable;
@property (weak, nonatomic) IBOutlet UILabel *nameTitleLable;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UILabel *surnameTitleLable;
@property (weak, nonatomic) IBOutlet UITextField *surnameField;
@property (weak, nonatomic) IBOutlet UILabel *idTitlelable;
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView3;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *bgView1;



@property (weak, nonatomic) IBOutlet UILabel *messageLable;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;


- (IBAction)submitButton:(UIButton *)sender;
- (IBAction)chooseCountry:(UIButton *)sender;

@end
