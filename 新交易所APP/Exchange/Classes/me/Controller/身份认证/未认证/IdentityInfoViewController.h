//
//  IdentityInfoViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/11/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"


@interface IdentityInfoViewController : BaseViewViewController
@property (strong, nonatomic) NSString *country_code;//国家ID 澳大利亚身份认证详情有差异 AU
@property (strong, nonatomic) NSString *country_id;//国家ID 澳大利亚身份认证详情有差异 AU
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *contentL;
@property (weak, nonatomic) IBOutlet UILabel *full_photo;
@property (weak, nonatomic) IBOutlet UILabel *titleL1;
@property (weak, nonatomic) IBOutlet UILabel *full_photo1;
@property (weak, nonatomic) IBOutlet UILabel *titleL2;
@property (weak, nonatomic) IBOutlet UILabel *contentL2;
@property (weak, nonatomic) IBOutlet UILabel *full_photo2;
@property (weak, nonatomic) IBOutlet UIImageView *addressImageView;

@property (weak, nonatomic) IBOutlet UIView *certificatesView;
@property (weak, nonatomic) IBOutlet UIImageView *certificatesImageView;
@property (weak, nonatomic) IBOutlet UIView *avowView;
@property (weak, nonatomic) IBOutlet UIImageView *avowImageView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UILabel *instriationsLable;
@property (weak, nonatomic) IBOutlet UILabel *infoLable;





- (IBAction)submitbutton:(UIButton *)sender;

@property (nonatomic,strong)NSString * identity_id;
@property (nonatomic,strong)NSArray * imgArray;

@end


