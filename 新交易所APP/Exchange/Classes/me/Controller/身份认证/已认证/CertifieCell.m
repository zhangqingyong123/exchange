//
//  CertifieCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CertifieCell.h"

@implementation CertifieCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];

    }
    return self;
    
}
- (void)setModle:(CertifiedModle *)modle
{
    _imagetitle.hidden = YES;
    _image.hidden = YES;
    _titlelable.hidden = NO;
    _rightlable.hidden = NO;
    _titlelable.text = modle.leftStr;
    _rightlable.text = modle.contontStr;
}
- (void)setImgmodle:(CertifiedModle *)imgmodle
{
    _titlelable.hidden = YES;
    _rightlable.hidden = YES;
    _imagetitle.hidden = NO;
    _image.hidden = NO;
    _imagetitle.text = imgmodle.leftStr;
    
        
//    [[SDWebImageDownloader sharedDownloader] setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
   


    [_image sd_setImageWithURL:[NSURL URLWithString:imgmodle.contontStr] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        NSLog(@"错误信息:%@",error);
        
    }];
   
}
- (void)get_up
{
    [self.contentView addSubview:self.titlelable];
    [self.contentView addSubview:self.rightlable];
    [self.contentView addSubview:self.imagetitle];
    [self.contentView addSubview:self.image];
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30),  RealValue_H(120)/2 - 15, 240, 30)];
        _titlelable.font = AutoFont(15);
        _titlelable.textColor = MAINTITLECOLOR1;
        _titlelable.adjustsFontSizeToFitWidth = YES;
    }
    return _titlelable;
}
- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(26) -200,  RealValue_H(120)/2 - 15, 200, 30)];
        _rightlable.font = AutoFont(15);
        _rightlable.textColor = RGBA(50, 78, 92, 1);
        _rightlable.adjustsFontSizeToFitWidth = YES;
        _rightlable.textAlignment = NSTextAlignmentRight;
    }
    return _rightlable;
}
- (UILabel *)imagetitle
{
    if (!_imagetitle) {
        _imagetitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30),  10, 240, 20)];
        _imagetitle.font = AutoFont(15);
        _imagetitle.textColor = MAINTITLECOLOR1;
        _imagetitle.adjustsFontSizeToFitWidth = YES;
    }
    return _imagetitle;
}
- (UIImageView *)image
{
    if (!_image) {
        _image = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(30), 40, UIScreenWidth - RealValue_W(60), RealValue_W(600) - 50)];
//        _image.layer.masksToBounds     =YES;
//        _image.contentMode             =UIViewContentModeScaleAspectFill;
    }
    return _image;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
