//
//  CertifiedViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CertifiedViewController.h"
#import "CertifiedModle.h"
#import "CertifieCell.h"
#import "CountryModle.h"
@interface CertifiedViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UIView * footView;
@property (nonatomic,strong)NSString  * name;
@property (nonatomic,strong)NSString  * number;
@property (nonatomic,strong)NSString  * country;
@property (nonatomic,strong)NSArray * imgs;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,strong)NSString *country_id;

@end

@implementation CertifiedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   [self.view addSubview:self.tableView];
    [self getidentityInfo];
}
- (void)getcountry:(NSString *)country_id
{
    WeakSelf
    [self GETWithHost:@"" path:countryHttp param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self stopLoadingAnimation];
        if ([responseObject[@"code"] integerValue] ==0)//成功
        {
            NSArray * list = responseObject[@"result"];
            for (NSDictionary * dic in list)
            {
                CountryModle * modle = [[CountryModle alloc] init];
                modle.CreatedAt = dic[@"CreatedAt"];
                modle.ID = dic[@"ID"];
                modle.UpdatedAt = dic[@"UpdatedAt"];
                modle.country_code = dic[@"code"];
                modle.is_show = dic[@"is_show"];
                modle.name_cn = dic[@"name_cn"];
                modle.name_en = dic[@"name_en"];
                modle.area_code = dic[@"area_code"];
                [weakSelf.dataArray addObject:modle];
                if (modle.ID.integerValue ==country_id.integerValue)
                {
                    weakSelf.country = [NSString stringWithFormat:@"%@ (%@)",modle.name_cn,modle.area_code];
                }
            }
             [self getData];
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self stopLoadingAnimation];
    }];
}

- (void)getidentityInfo
{
    [self showLoadingAnimation];
    [self GETWithHost:@"" path:identity param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {

            self.name = responseObject[@"result"][@"info"][@"name"];
            self.number =responseObject[@"result"][@"info"][@"number"];
            self.imgs = responseObject[@"result"][@"images"];
            self.country_id = responseObject[@"result"][@"info"][@"country_id"];
            [self getcountry:self.country_id];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma mark----数据
- (void)getData
{
    NSMutableArray * array = [[NSMutableArray alloc] init];
    [array addObject: @{@"title":Localized(@"Authentication_international"),@"content":[NSString isEmptyString:self.country]?Localized(@"China"):self.country}];
    [array addObject:@{@"title":Localized(@"Authentication_account"),@"content":[NSString isEmptyString:[EXUserManager personalData].phone] ? [EXUserManager personalData].email:[EXUserManager personalData].phone}];
    [array addObject:@{@"title":Localized(@"Identity_name"),@"content":self.name}];
    [array addObject: @{@"title":Localized(@"set_ID_Account"),@"content":self.number}];
    
    NSArray * titles;
    if (_imgs.count >2)
    {
        titles = @[Localized(@"set_ID_certification"),Localized(@"Authentication_uAddress_proof"),Localized(@"Authentication_autograph_title")];
    }else
    {
         titles = @[Localized(@"set_ID_certification"),Localized(@"Authentication_Statement_photo_message")];
    }
    for (int i =0 ; i<_imgs.count; i++) {

        [array addObject: @{@"title":titles[i],@"content":self.imgs[i][@"value"]}];
    }
    for (NSDictionary *dic in array)
    {
        
        CertifiedModle *modle=[[CertifiedModle alloc]init];
        modle.leftStr=dic[@"title"];
        modle.contontStr=dic[@"content"];
        [self.dataAry addObject:modle];
    }
    [_tableView reloadData];
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - 64 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[CertifieCell class] forCellReuseIdentifier:@"CertifieCell"];
//        _tableView.tableFooterView = self.footView;
   
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
//- (UIView *)footView
//{
//    if (!_footView) {
//        _footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_W(100))];
//        _footView.backgroundColor = TABLEVIEWLCOLOR;
//        UILabel * line = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), 0.5f, UIScreenWidth - RealValue_W(40), 1)];
//        line.backgroundColor = CELLCOLOR;
//        [_footView addSubview:line];
//        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30),
//                                                                    RealValue_W(60) -RealValue_W(24),
//                                                                    UIScreenWidth - RealValue_W(60),
//                                                                    RealValue_W(24))];
//        lable.textColor = RGBA(250, 74, 97, 1);
//        lable.text = Localized(@"set_certification_message");
//        lable.font =AutoFont(12);
//        [_footView addSubview:lable];
//    }
//    return _footView;
//}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row<4) {
        
         return RealValue_H(120);
    }else
    {
       return RealValue_H(600);
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CertifieCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"CertifieCell"];
     if (indexPath.row<4) {
          cell.modle=_dataAry[indexPath.row];
     }else
     {
         cell.imgmodle = _dataAry[indexPath.row];
     }
   
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
