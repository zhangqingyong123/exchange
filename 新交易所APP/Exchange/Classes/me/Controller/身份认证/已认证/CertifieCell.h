//
//  CertifieCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CertifiedModle.h"
@interface CertifieCell : UITableViewCell
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UILabel * rightlable;
@property (nonatomic,strong)CertifiedModle * modle;

@property (nonatomic,strong)UILabel *imagetitle;
@property (nonatomic,strong)UIImageView * image;
@property (nonatomic,strong)CertifiedModle * imgmodle;
@end
