//
//  FailViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "FailViewController.h"
#import "IdentityAuthenticationViewController.h"
@interface FailViewController ()
@property(nonatomic,assign)NSInteger count;
@property(nonatomic,assign)BOOL isfale;
@end

@implementation FailViewController
-(void)viewDidAppear:(BOOL)animated
{
    _isfale = NO;
    [self performSelectorInBackground:@selector(thread) withObject:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
     KViewRadius(self.button, 2);
    _button.backgroundColor = TABTITLECOLOR;
    [_button setTitle:Localized(@"Identity_Re_certification") forState:0];
    _remarkLable.textColor = MAINTITLECOLOR;
    [self getidentityInfo];
    
}
//在异步线程中无法操作UI，如果想要操作UI必须回调主线程
- (void)thread
{
    for(int i=5;i>=0;i--)
    {
        _count = i;
        // 回调主线程
        [self performSelectorOnMainThread:@selector(mainThread) withObject:nil waitUntilDone:YES];
        //一秒一次
        sleep(1);
    }
}
//此函数主线程执行
- (void)mainThread
{
     _timeLable.text=[NSString stringWithFormat:Localized(@"Identity_automatically_jump_message"),(long)_count];
    if (_isfale) {
        return;
    }
    if (_count==0) {
        //这里操作倒计时结束后的事情
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
// 第一次获取认证信息
- (void)getidentityInfo
{
    WeakSelf
    [self GETWithHost:@"" path:identity param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSString * remark = responseObject[@"result"][@"info"][@"remark"];
            weakSelf.remarkLable.text = [NSString stringWithFormat:@"%@-%@",Localized(@"Identity_FailView_message"),[NSString isEmptyString:remark]?Localized(@"Identity_FailView_message1"):remark];  
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)button:(UIButton *)sender {
    _isfale = YES;
    IdentityAuthenticationViewController * idACationVC = [[IdentityAuthenticationViewController alloc] init];
    idACationVC.title = @"身份认证";
    [self.navigationController pushViewController:idACationVC animated:YES];
    
}
@end
