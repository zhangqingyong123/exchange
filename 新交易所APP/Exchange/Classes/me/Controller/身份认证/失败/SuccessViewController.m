//
//  SuccessViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "SuccessViewController.h"

@interface SuccessViewController ()
@property(nonatomic,assign)NSInteger count;
@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABBARBGCOLOR;
    KViewRadius(self.button, 2);
    _remarkLable.textColor = MAINTITLECOLOR;
    _remarkLable.text = Localized(@"Identity_success_message");
    [_button setTitle:Localized(@"Identity_to_back") forState:0];
    [self performSelectorInBackground:@selector(thread) withObject:nil];
}
//在异步线程中无法操作UI，如果想要操作UI必须回调主线程
- (void)thread
{
    for(int i=5;i>=0;i--)
    {
        _count = i;
        // 回调主线程
        [self performSelectorOnMainThread:@selector(mainThread) withObject:nil waitUntilDone:YES];
        //一秒一次
        sleep(1);
    }
}
//此函数主线程执行
- (void)mainThread
{
    _timeLable.text=[NSString stringWithFormat:Localized(@"Identity_automatically_jump_message"),(long)_count];
    if (_count==0) {
        //这里操作倒计时结束后的事情
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onreturnbutton:(UIButton *)sender;{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
