//
//  MyAssetsViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
#import "MyAssetsViewController.h"
#import "AssetsModle.h"
#import "AssetsCell.h"
#import "CurrencyAddressViewController.h"
#import "ChooseCurrencyViewController.h"
#import "CheckVersionAlearView.h"
#import "IdentityAuthenticationViewController.h"
#import "AssetsSegmentView.h"
#import "AssetsInfoViewController.h"
#import "AssetsDistributionViewController.h"
@interface MyAssetsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UIButton * openButton;
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)NSMutableArray * showdataAry;
@property (nonatomic,strong)NSMutableArray * allChartAry; //全部
@property (nonatomic,strong)NSMutableArray * chartAry;//处理后的
@property (nonatomic,strong)NSDictionary * dataDic;
@property (nonatomic,assign)double  allTotalprofit;
@property (nonatomic,assign)double  otherTotalprofit;
@property (nonatomic, assign) BOOL canScroll;
@property (nonatomic, assign) BOOL isTopIsCanNotMoveTabView;
@property (nonatomic, assign) BOOL isTopIsCanNotMoveTabViewPre;
@property (nonatomic, strong) AssetsSegmentView *segmentView;
@property (nonatomic, strong) AssetsInfoViewController *assetsInfoVC;
@property (nonatomic, strong) AssetsDistributionViewController *assetsDistributionVC;
@end
@implementation MyAssetsViewController
- (void)viewWillAppear:(BOOL)animated
{
    [self barLine:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self barLine:NO];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self getassets];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self showLoadingAnimationWithIsShow:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leaveTop:) name:@"leaveTop" object:nil];

    [self custonUI];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightItem];
    if ([EXUnit isHideCurrencyPrice]) {
        _RMBlable.hidden = YES;
        _titleLable.hidden = YES;
        _headImage.hidden = YES;
    }
   
}
- (void)showLoadingAnimationWithIsShow:(BOOL)isShow
{
    if (isShow) {
        _tableView.hidden = YES;
        [self showLoadingAnimation];
        
    }else
    {
         [self stopLoadingAnimation];
        _tableView.hidden = NO;
    }
}
- (void)getassets
{
    [_dataAry removeAllObjects];
    [_showdataAry removeAllObjects];
    self.allTotalprofit = 0.0;
    WeakSelf
    [self GETWithHost:@"" path:Userassets param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            weakSelf.dataDic= responseObject[@"result"];
            double totalprofit;
            NSArray * keyArray = weakSelf.dataDic.allKeys;
            for (NSString *str in keyArray) {
                NSDictionary *dic = weakSelf.dataDic[str];
                AssetsModle * modle = [[AssetsModle alloc] init];
                modle.name =str;
                modle.available =dic[@"available"];
                modle.c2c_freeze =dic[@"c2c_freeze"];
                modle.freeze =dic[@"freeze"];
                modle.has_memo = dic[@"has_memo"];
                modle.recharge_status =dic[@"recharge_status"];
                modle.trade_status =dic[@"trade_status"];
                modle.withdraw_fee =dic[@"withdraw_fee"];
                modle.other_freeze =dic[@"other_freeze"];
                totalprofit = modle.available.floatValue + modle.c2c_freeze.floatValue + modle.freeze.floatValue + modle.withdraw_freeze.floatValue +modle.other_freeze.floatValue ;
                NSString * toalstr = [EXUnit getCapitalPriceNoUnitWithName:modle.name Price:[NSString stringWithFormat:@"%f",totalprofit]];
                modle.totalprofit = toalstr.doubleValue ;
                weakSelf.allTotalprofit += modle.totalprofit;    
                [weakSelf.dataAry addObject:modle];
                if ([str isEqualToString:@"GLB"]) {
                    NSLog(@"--->%f", modle.totalprofit);
                }
                if (modle.totalprofit >0)
                {
                    [weakSelf.showdataAry addObject:modle];
                   
                }
                
            }
            if (![[EXUserManager getState] isEqualToString:@"1"]) {
                
                weakSelf.RMBlable.text = @"*******";
            }else
            {
                
                [weakSelf.RMBlable countFrom:0 to:self.allTotalprofit withDuration:1];
            }
            
             [self loginButtonHighLightStatus];
             weakSelf.assetsInfoVC.dataAry = weakSelf.dataAry;
             [weakSelf.assetsInfoVC.tableView reloadData];
            [self getKChartData];
            [self showLoadingAnimationWithIsShow:NO];
           
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
// 获取分布数据
- (void)getKChartData
{
    /*数组数据降序排序*/
    [self.allChartAry removeAllObjects];
    [self.chartAry removeAllObjects];
    WeakSelf
    dispatch_async(dispatch_get_global_queue(0, 0), ^{ // 处理耗时操作在此次添加
       
        NSArray *result = [weakSelf.showdataAry sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            AssetsModle * modle1 = obj1;
            AssetsModle * modle2 = obj2;
            if (modle1.totalprofit< modle2.totalprofit)
            {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedAscending;
            }
            
            
        }];
        self.allChartAry  = [NSMutableArray arrayWithArray:result];
        
        NSArray * colors = @[ColorStr(@"#E6943C"),ColorStr(@"#E6722B"),ColorStr(@"#EBB446"),ColorStr(@"#E6CB45"),ColorStr(@"#E5E65F"),ColorStr(@"#3CC48E"),ColorStr(@"#24B3AB"),ColorStr(@"#10D4E6"),ColorStr(@"#4E9AE6"),ColorStr(@"#01B4E4")];
        for (int i =0; i<self.allChartAry.count; i++)
        {
            ZTChatModle * chatModel = [[ZTChatModle alloc] init];
            AssetsModle * assetsModle = self.allChartAry[i];
            if (self.allChartAry.count<11)
            {
                chatModel.name = assetsModle.name;
                chatModel.color = colors[i];
                chatModel.cnyNumber = [NSString stringWithFormat:@"%.2f",assetsModle.totalprofit];
                chatModel.ratio = [NSString stringWithFormat:@"%.2f%%",(assetsModle.totalprofit / self.allTotalprofit) *100.00];
                chatModel.Proportion = [NSString stringWithFormat:@"%@",@(assetsModle.totalprofit / self.allTotalprofit)];
                [self.chartAry addObject:chatModel];
            }else
            {
                if (i<9)
                {
                    chatModel.name = assetsModle.name;
                    chatModel.color = colors[i];
                    chatModel.cnyNumber = [NSString stringWithFormat:@"%.2f",assetsModle.totalprofit];
                    chatModel.ratio = [NSString stringWithFormat:@"%.2f%%",(assetsModle.totalprofit / self.allTotalprofit) *100.00];
                    chatModel.Proportion = [NSString stringWithFormat:@"%@",@(assetsModle.totalprofit / self.allTotalprofit)];
                    [self.chartAry addObject:chatModel];
                    
                }else
                {
                    weakSelf.otherTotalprofit += assetsModle.totalprofit; //其他总额数
                    if (i ==self.allChartAry.count-1) //循环完毕
                    {
                        chatModel.name = Localized(@"Asset_other");
                        chatModel.color = colors[9];
                        chatModel.cnyNumber = [NSString stringWithFormat:@"%.2f",weakSelf.otherTotalprofit];
                        chatModel.ratio =  [NSString stringWithFormat:@"%.2f%%",weakSelf.otherTotalprofit / self.allTotalprofit *100.00];
                        chatModel.Proportion = [NSString stringWithFormat:@"%@",@(weakSelf.otherTotalprofit / self.allTotalprofit)];
                        [self.chartAry addObject:chatModel];
                        
                        
                    }
                }
            }
            
            
        }
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.assetsDistributionVC getZFChart:self.chartAry];
            
        });
 
    });
    
    
    
}
-(NSString *)notRounding:(float)price afterPoint:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *ouncesDecimal;
    NSDecimalNumber *roundedOunces;
    
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    return [NSString stringWithFormat:@"%@",roundedOunces];
}
- (void)open:(UIButton *)button
{
    button.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        button.userInteractionEnabled = YES;
    });
    
    _openButton.selected = !_openButton.selected;
    _assetsInfoVC.isOpen =  !_openButton.selected;
    _assetsDistributionVC.isOpen = !_openButton.selected;
    if (_openButton.selected) {
        
        _RMBlable.text = @"*******";
        [EXUserManager saveOpenAssetsState:@"0"];
    }else
    {
       [EXUserManager saveOpenAssetsState:@"1"];
       [_RMBlable countFrom:0 to:self.allTotalprofit withDuration:1];
    }
    [self.assetsDistributionVC.tableView reloadData];
    [self.assetsInfoVC.tableView reloadData];
}
- (void)loginButtonHighLightStatus
{
    _withdrawbutton.userInteractionEnabled = YES;//恢复点击
    _withdrawbutton.alpha = 1;
    _Rechargebutton.userInteractionEnabled = YES;//恢复点击
    _Rechargebutton.alpha = 1;
}
- (void)loginButtonNormalStatus
{
    _withdrawbutton.userInteractionEnabled = NO;//不可点击
    _withdrawbutton.alpha = 0.5;
    _Rechargebutton.userInteractionEnabled = NO;//不可点击
    _Rechargebutton.alpha = 0.5;
}
- (UIView *)rightItem
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    view.backgroundColor = [UIColor clearColor];
    _openButton = [[UIButton alloc] initWithFrame:CGRectMake(38, 0, 60, 50)];
    [_openButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].openNormalImage] forState:UIControlStateNormal];
    [_openButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].closeNormalImage] forState:UIControlStateSelected];
    [_openButton addTarget:self action:@selector(open:) forControlEvents:UIControlEventTouchUpInside];
    [_openButton sizeToFit];
    _openButton.centerY = view.centerY;
    if ([[EXUserManager getState] isEqualToString:@"1"]) { //打开状态
        _openButton.selected =  NO;
    }else
    {
       _openButton.selected =  YES;
    }
    [view addSubview:_openButton];
    return view;
}
- (void)custonUI
{
    _titleLable.text = [NSString stringWithFormat:@"%@(%@)", Localized(@"Asset_Total_assets"),[EXUserManager getValuationMethod]];
    _titleLable.textColor = [ZBLocalized sharedInstance].assetsTitleColor;
    _titleLable.font = AutoFont(15);
    _RMBlable.textColor =  [ZBLocalized sharedInstance].ALLTotalTitleColor;
    _RMBlable.font = AutoBoldFont(30);
    _RMBlable.format= @"%.2f";
    _RMBlable.method = UILabelCountingMethodEaseOutBounce;

    [self loginButtonNormalStatus];


    _tableView.separatorColor = CLEARCOLOR;
    _tableView.delegate = self;
    _tableView.dataSource=self;
    _tableView.backgroundColor =  TABLEVIEWLCOLOR;
    _tableView.rowHeight = UIScreenHeight;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.bounces = YES;
    _headImage.image = [UIImage imageNamed:[ZBLocalized sharedInstance].assetsHeadBgImage];


    [_hideButton setTitle:[NSString stringWithFormat:Localized(@"Asset_Currencies_cny"),[EXUserManager getValuationMethod]] forState:(UIControlStateNormal)];
    _hideButton.titleLabel.font = AutoFont(13);
    [_hideButton setTitleColor:MAINTITLECOLOR1 forState:UIControlStateNormal];
    [_hideButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].choosecloseNormalImage] forState:UIControlStateNormal];
    [_hideButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].chooseOpenNormalImage] forState:UIControlStateSelected];

    if ([EXUnit isHideCurrencyPrice]) {
        _hideButton.hidden = YES;
    }
    [self.view addSubview:self.bottomView];
   
    [_bottomView addSubview:self.withdrawbutton];
    [_bottomView addSubview:self.Rechargebutton];
}
- (void)viewDidLayoutSubviews
{
    self.BgView.backgroundColor = [ZBLocalized sharedInstance].assetsHeadBGColor;
    if ([EXUnit isHideCurrencyPrice]) {
        self.BgView.height = 0;
    }else
    {
       self.BgView.height = 173;
    }
    
}
- (UIView *)bottomView
{
    if (!_bottomView ) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, UIScreenHeight -50 - kStatusBarAndNavigationBarHeight - IPhoneBottom, UIScreenWidth, 50)];
        _bottomView.backgroundColor = [ZBLocalized sharedInstance].assetsCellviewbgColor;
    }
    return _bottomView;
}

- (UIButton *)withdrawbutton
{
    if (!_withdrawbutton) {
        _withdrawbutton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(10), RealValue_W(168*2), RealValue_W(80))];
        
       
        _withdrawbutton.layer.masksToBounds     =YES;
        _withdrawbutton.layer.borderWidth       =1;
        _withdrawbutton.layer.cornerRadius      =2;
        _withdrawbutton.contentMode             =UIViewContentModeScaleAspectFill;
        _withdrawbutton.layer.borderColor       =[TABTITLECOLOR CGColor];
        [_withdrawbutton setImage:[UIImage imageNamed:@"Rechargeimage"] forState:0];
        [_withdrawbutton setTitle:Localized(@"Asset_Withdraw") forState:0];
        [_withdrawbutton centerHorizontallyImageAndTextWithPadding:8];
        _withdrawbutton.backgroundColor =  [ZBLocalized sharedInstance].assetsCellviewbgColor;
        [_withdrawbutton setTitleColor:TABTITLECOLOR forState:0];
        _withdrawbutton.titleLabel.font = AutoFont(15);
         [_withdrawbutton addTarget:self action:@selector(withdraw:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _withdrawbutton;
}
- (UIButton *)Rechargebutton
{
    if (!_Rechargebutton) {
        _Rechargebutton = [[UIButton alloc] initWithFrame:CGRectMake(_withdrawbutton.right + RealValue_W(20), RealValue_W(10), RealValue_W(168*2), RealValue_W(80))];
        [_Rechargebutton setImage:[UIImage imageNamed:@"withdrawbutton"] forState:0];
        [_Rechargebutton setTitle:Localized(@"Asset_Recharge") forState:0];
        [_Rechargebutton centerHorizontallyImageAndTextWithPadding:8];
        _Rechargebutton.titleLabel.font = AutoFont(15);
        [_Rechargebutton addTarget:self action:@selector(Recharge:) forControlEvents:UIControlEventTouchUpInside];
        _Rechargebutton.backgroundColor = TABTITLECOLOR;
        KViewRadius(_Rechargebutton, 2);
    }
    return _Rechargebutton;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (NSMutableArray *)chartAry
{
    if (!_chartAry) {
        _chartAry = [[NSMutableArray alloc]init];
    }
    return _chartAry;
}
- (NSMutableArray *)allChartAry
{
    if (!_allChartAry) {
        _allChartAry = [[NSMutableArray alloc]init];
    }
    return _allChartAry;
}
- (NSMutableArray *)showdataAry
{
    if (!_showdataAry) {
        _showdataAry = [[NSMutableArray alloc]init];
    }
    return _showdataAry;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UIScreenHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:self.segmentView];
    
    return cell;
}
- (UIView *)segmentView
{
    
    if (!_segmentView) {
        NSArray *controllerArray;
        NSArray *titleArray;
        _assetsInfoVC = [[AssetsInfoViewController alloc] init];
        if ([[EXUserManager getState] isEqualToString:@"1"]) { //打开状态
             _assetsInfoVC.isOpen =  YES;
        }else
        {
             _assetsInfoVC.isOpen =  NO;
        }
        _assetsDistributionVC = [[AssetsDistributionViewController alloc] init];
       
        if ([[EXUserManager getState] isEqualToString:@"1"]) { //打开状态
             _assetsDistributionVC.isOpen =  YES;
        }else
        {
            _assetsDistributionVC.isOpen =  NO;
        }
        titleArray = @[Localized(@"Asset_detail"),Localized(@"Asset_Distribution")];
        controllerArray = @[_assetsInfoVC,_assetsDistributionVC];
        CGFloat height;
       
        if ([EXUnit isHideCurrencyPrice]) {
            height = 0;
        }else
        {
            height = 173;
        }
        _segmentView = [[AssetsSegmentView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight ) controllers:controllerArray titleArray:titleArray ParentController:self lineWidth:RealValue_H(250) lineHeight:3];
        _segmentView.backgroundColor = [ZBLocalized sharedInstance].assetsSegmentbgColor;
        return _segmentView;

    }
    return _segmentView;
}

- (void)leaveTop:(NSNotification *)notification{
    
    NSDictionary *userInfo = notification.userInfo;
    NSString *canScroll = userInfo[@"canScroll"];
    if ([canScroll isEqualToString:@"1"]) {
        _canScroll = YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)Recharge:(UIButton *)sender {
    if ([AppDelegate shareAppdelegate].is_preaudit.integerValue ==0)
    {
        ChooseCurrencyViewController * ChooseCurrencyVc = [[ChooseCurrencyViewController alloc] init];
        [self.navigationController pushViewController:ChooseCurrencyVc animated:YES];
    }else
    {
        if ([EXUserManager RealName].integerValue ==1 ||[EXUserManager RealName].integerValue ==4)
        {
            [EXUnit showMessage:Localized(@"C2C_under_review")];
            
        }else if ([EXUserManager RealName].integerValue ==3)
        {
            [self popRealNameWithtitle:Localized(@"transaction_authenticated_title") message:Localized(@"my_Authentication_failure_message")];
            
        }else if ([EXUserManager RealName].integerValue ==0)
        {
            [self popRealNameWithtitle:Localized(@"transaction_authenticated_title") message:Localized(@"my_real_name_message")];
            
        }else if ([EXUserManager RealName].integerValue ==2)
        {
            ChooseCurrencyViewController * ChooseCurrencyVc = [[ChooseCurrencyViewController alloc] init];
            [self.navigationController pushViewController:ChooseCurrencyVc animated:YES];
        }
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat yOffset  = scrollView.contentOffset.y;
    CGFloat tabOffsetY = [_tableView rectForSection:0].origin.y;
    _isTopIsCanNotMoveTabViewPre = _isTopIsCanNotMoveTabView;
    
    if (yOffset >= tabOffsetY) {
        
        //不能滑动
        scrollView.contentOffset = CGPointMake(0, tabOffsetY);
        _isTopIsCanNotMoveTabView = YES;
        
    }else{
        
        //可以滑动
        _isTopIsCanNotMoveTabView = NO;
        
         _segmentView.backgroundColor = [ZBLocalized sharedInstance].assetsSegmentbgColor;
        
    }
    if (_isTopIsCanNotMoveTabView != _isTopIsCanNotMoveTabViewPre) {
        
        if (!_isTopIsCanNotMoveTabViewPre && _isTopIsCanNotMoveTabView) {
            //NSLog(@"子视图控制器滑动到顶端");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goTop" object:nil userInfo:@{@"canScroll":@"1"}];
            _canScroll = NO;
            _segmentView.backgroundColor = [ZBLocalized sharedInstance].assetsSegmentSelectbgColor;
        }
        if(_isTopIsCanNotMoveTabViewPre && !_isTopIsCanNotMoveTabView){
            //NSLog(@"父视图控制器滑动到顶端");
            if (!_canScroll) {
                
                scrollView.contentOffset = CGPointMake(0, tabOffsetY);
               
            }
        }
    }
    
    //    self.tableOffsetY = scrollView.contentOffset.y;
}
- (void)withdraw:(UIButton *)sender {
    if ([EXUserManager RealName].integerValue ==1 ||[EXUserManager RealName].integerValue ==4)
    {
        [EXUnit showMessage:Localized(@"C2C_under_review")];
        
    }else if ([EXUserManager RealName].integerValue ==3)
    {
        [self popRealNameWithtitle:Localized(@"transaction_authenticated_title") message:Localized(@"my_Authentication_failure_message")];
        
    }else if ([EXUserManager RealName].integerValue ==0)
    {
        [self popRealNameWithtitle:Localized(@"transaction_authenticated_title") message:Localized(@"my_real_name_message")];
        
    }else if ([EXUserManager RealName].integerValue ==2)
    {
        CurrencyAddressViewController * currency = [[CurrencyAddressViewController alloc] init];
        currency.isWithdraw = YES;
        currency.title = Localized(@"Asset_currencies");
        [self.navigationController pushViewController:currency animated:YES];
    }
   
}
- (void)popRealNameWithtitle:(NSString *)title message:(NSString *)message
{
    CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:message
                                                                             titleStr:title
                                                                              openUrl:@""
                                                                              confirm:Localized(@"transaction_OK")
                                                                               cancel:Localized(@"transaction_cancel")
                                                                                state:2
                                                                        RechargeBlock:^
                                        {
                                            
                                            IdentityAuthenticationViewController * idACationVC = [[IdentityAuthenticationViewController alloc] init];
                                            idACationVC.title = Localized(@"my_identity_authentication");
                                            [[EXUnit currentViewController].navigationController pushViewController:idACationVC animated:YES];
                                            
                                            
                                        }];
    aleartVC.animationStyle = LXASAnimationTopShake;
    [aleartVC showLXAlertView];
    
}
- (IBAction)hideButtonClick:(UIButton *)sender {
    
    sender.selected = ! sender.selected;
    if (sender.selected) {
       
        _assetsInfoVC.dataAry = _showdataAry;
    }else
    {
        _assetsInfoVC.dataAry = _dataAry;
    }
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
    [_assetsInfoVC.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}
@end
