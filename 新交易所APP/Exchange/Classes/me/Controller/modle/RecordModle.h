//
//  RecordModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/2.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordModle : NSObject
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * asset;
@property (nonatomic,strong)NSString * confirmation;
@property (nonatomic,strong)NSString * tx_hash;
@property (nonatomic,strong)NSString * user_id;
@property (nonatomic,strong)NSString * status; //1成功，2审核中，3失败
@end
@interface CurrencyModle : NSObject
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * address;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * asset;
@property (nonatomic,strong)NSString * fee;
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * real_amount;
@property (nonatomic,strong)NSString * site_id;
@property (nonatomic,strong)NSString * status; //0,已提交，1审核中，2处理中 3，成功 4，提现失败 5，取消提现
@property (nonatomic,strong)NSString * user_id;
@property (nonatomic,strong)NSString * user_withdraw_address_id;
@end
