//
//  PresonalModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/1.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetsModle : NSObject
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * available;
@property (nonatomic,strong)NSString * has_memo; //0 表示没有提币标题 1表示有提币标题
@property (nonatomic,strong)NSString * c2c_freeze;
@property (nonatomic,strong)NSString * freeze;
@property (nonatomic,strong)NSString * precision;
@property (nonatomic,strong)NSString * other_freeze;
@property (nonatomic,strong)NSString * recharge_status;
@property (nonatomic,strong)NSString * trade_status;
@property (nonatomic,strong)NSString * withdraw_fee;
@property (nonatomic,strong)NSString * withdraw_freeze;
@property (nonatomic,strong)NSString * withdraw_status;
@property (nonatomic,strong)NSString * withdraw_max;
@property (nonatomic,strong)NSString * withdraw_min;
@property (nonatomic,assign)float  totalprofit;//单个资产
@property (nonatomic,assign)double  allTotalprofit;//总资金

@end
@interface AssetsDetaileModle : NSObject
@property (nonatomic,strong)NSString * rightstr;
@property (nonatomic,strong)NSString * leftstr;
@end
@interface ZTChatModle : NSObject
@property (nonatomic,strong)NSString * name; //名称
@property (nonatomic,strong)UIColor * color; //颜色
@property (nonatomic,strong)NSString * cnyNumber; //cny个数
@property (nonatomic,strong)NSString * ratio; //百分比
@property (nonatomic,strong)NSString * Proportion; //比利
@end
