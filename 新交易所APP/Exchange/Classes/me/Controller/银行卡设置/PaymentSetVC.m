//
//  PaymentSetVCViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "PaymentSetVC.h"
#import "PaypalPageView.h"

@interface PaymentSetVC ()
@property(nonatomic,strong) PaypalPageView *paypalView;
@property (nonatomic,strong)NSMutableArray * titles;
@property (nonatomic,strong)NSMutableArray * contontss;
@end

@implementation PaymentSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[AppDelegate shareAppdelegate].privilages containsString:@"8"]) {
        
     
        [self.contontss addObject:@"BankWithSetViewController"];
        [self.contontss addObject:@"AlipayViewController"];
        [self.contontss addObject:@"WeChatViewController"];
        [self.titles addObject:Localized(@"C2C_Bank_card")];
        [self.titles addObject:Localized(@"OTC_alipay")];
        [self.titles addObject:Localized(@"OTC_weChat")];
        
    } else {
        
        [self.contontss addObject:@"BankWithSetViewController"];
        [self.titles addObject:Localized(@"C2C_Bank_card")];
        
    }
    [self.view addSubview:[self test1]];
}
- (NSMutableArray *)titles
{
    if (!_titles) {
        _titles = [[NSMutableArray alloc]init];
    }
    return _titles;
}
- (NSMutableArray *)contontss
{
    if (!_contontss) {
        _contontss = [[NSMutableArray alloc]init];
    }
    return _contontss;
}
- (PaypalPageView *)test1 {
    _paypalView = [[PaypalPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight)
                                         withTitles:self.titles
                                withViewControllers:self.contontss
                                     withParameters:nil];
    
    _paypalView.selectedColor = WHITECOLOR;
    _paypalView.unselectedColor = maintitleLCOLOR ;
    _paypalView.backgroundColor = MAINBLACKCOLOR;
    return _paypalView;
}

@end
