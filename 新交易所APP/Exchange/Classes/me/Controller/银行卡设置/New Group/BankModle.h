//
//  BankModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankModle : NSObject
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * bank;
@property (nonatomic,strong)NSString * card_number;
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * pre_phone;
@property (nonatomic,strong)NSString * sub_bank;
@property (nonatomic,strong)NSString * user_id;
@end
