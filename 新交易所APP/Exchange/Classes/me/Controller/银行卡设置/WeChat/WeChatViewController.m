//
//  WeChatViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "WeChatViewController.h"
#import "BindBankViewController.h"
#import "CheckVersionAlearView.h"
@interface WeChatViewController ()
@property(nonatomic,strong)UIView * bgView;
@property (nonatomic,strong) UILabel * alipayNmuberTitle;
@property (nonatomic,strong) UILabel * alipayNmuber;
@property (nonatomic,strong) UILabel * nameTitle;
@property (nonatomic,strong) UILabel * name;
//@property (nonatomic,strong) UIButton * activationbutton;
@property (nonatomic,strong) UIImageView * QRimage;
@property (nonatomic,strong) UILabel * QRTitle;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,assign) BOOL  isData;//是否有数据
@property (nonatomic,strong) UIButton * addBankButton;
@property (nonatomic,strong) UILabel * paypalMessage;
@property (nonatomic,strong) NSArray * dataAraay;
@property (nonatomic,strong) NSString * id; //账号在表中的id
@end

@implementation WeChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    [self getData];
    [self.view addSubview:self.bgView];
    [_bgView addSubview:self.alipayNmuberTitle];
    [_bgView addSubview:self.alipayNmuber];
    [_bgView addSubview:self.nameTitle];
    [_bgView addSubview:self.name];
    //    [_bgView addSubview:self.activationbutton];
    [_bgView addSubview:self.QRimage];
    [_bgView addSubview:self.QRTitle];
    [self.view addSubview:self.addBankButton];
    [self.view addSubview:self.paypalMessage];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getData) name:bindbnakNotificationse object:nil];
}
#pragma make--获取数据
- (void)getData:(NSNotification *)message
{
    NSDictionary * dic = message.object;
    if ([dic[@"type"] isEqualToString:@"2"]) {
        [self getData];
    }
}
- (void)getData
{
    [_dataAraay reverseObjectEnumerator];
    WeakSelf
    [HomeViewModel getMobilePayDataListWithDic:@{} url:mobilePay callBack:^(BOOL isSuccess, id callBack) {
        if (isSuccess) {
            NSDictionary * dic = callBack[@"result"];
            NSArray * logs = dic[@"logs"];
            [weakSelf getLogsWithLogs:logs];
            [self placeholderViewWithFrame:self.view.frame NoNetwork:NO];
        }
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (!_isData)
    {
        [_workView removeFromSuperview];
        _bgView.hidden = YES;
        _workView = [[NoNetworkView alloc] initWithFrame:CGRectMake(0, 10, UIScreenWidth, UIScreenHeight  -74 -IPhoneTop -100) NoNetwork:NoNetwork];
      
        
        [self.view addSubview:_workView];
    }else
    {
         _bgView.hidden = NO;
        [_workView dissmiss];
    }
    
}
- (void)getLogsWithLogs:(NSArray *)Logs
{
    _dataAraay = Logs;
    for (NSDictionary * dic in Logs)
    {
        if ([dic[@"status"] integerValue] ==2) //微信
        {
            [self getAlipaydata:dic];
        }
    }
}
- (void)getAlipaydata:(NSDictionary *)dic
{
    _isData = YES;
    [_addBankButton setTitle:Localized(@"address_delete") forState:0];
    _alipayNmuber.text = dic[@"account"];
    _name.text = dic[@"name"];
    _id = dic[@"ID"];
    [_QRimage sd_setImageWithURL:[NSURL URLWithString:dic[@"image"]]];
    
}
- (void)addBack:(UIButton *)sender{
    if (_isData) { //删除支付宝
        [self deletePay];
        
    }else
    {
        BindBankViewController * bindbank = [[BindBankViewController alloc] init];
        bindbank.title = Localized(@"OTC_Income_Payment");
        bindbank.index = 2;
        bindbank.dataAry = _dataAraay;
        [self.navigationController pushViewController:bindbank animated:YES];
    }
}
- (void)deletePay
{
    WeakSelf
    CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"bank_unbind") titleStr:Localized(@"Remove_bank_message") openUrl:@"" confirm:Localized(@"transaction_OK") cancel:Localized(@"transaction_cancel")   state:2 RechargeBlock:^{
        
        
        NSDictionary * param = @{@"id":weakSelf.id};
        
        [weakSelf POSTWithHost:@"" path:deletePay param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
                weakSelf.isData = NO;
                [weakSelf getData];
                [weakSelf.addBankButton setTitle:Localized(@"OTC_Income_Payment") forState:0];
                
            }else
            {
                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        
        
        
        
    }];
    
    aleartVC.animationStyle = LXASAnimationTopShake;
    [aleartVC showLXAlertView];
}
#pragma make--懒加载
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, UIScreenWidth, UIScreenHeight  -74 -IPhoneTop)];
        _bgView.backgroundColor = TABLEVIEWLCOLOR;
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(RealValue_W(36) , RealValue_W(200))];
        [bezierPath addLineToPoint:CGPointMake(self.view.width - RealValue_W(36) , RealValue_W(200))];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = CELLCOLOR.CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [_bgView.layer addSublayer:shapeLayer];
        _bgView.hidden = YES;
    }
    return _bgView;
}
- (UIButton *)addBankButton
{
    if (!_addBankButton) {
        _addBankButton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(30), UIScreenHeight -45 - RealValue_W(100) - kStatusBarAndNavigationBarHeight - IPhoneBottom, UIScreenWidth - RealValue_W(60), 40)];
        _addBankButton.backgroundColor = TABTITLECOLOR;
        [_addBankButton addTarget:self action:@selector(addBack:) forControlEvents:UIControlEventTouchUpInside];
        [_addBankButton setTitle:Localized(@"OTC_Income_Payment") forState:0];
        _addBankButton.titleLabel.font = AutoBoldFont(15);
        [_addBankButton setTitleColor:WHITECOLOR forState:0];
    }
    return _addBankButton;
}
- (UILabel *)paypalMessage
{
    if (!_paypalMessage) {
        _paypalMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
        _paypalMessage.textColor = MAINTITLECOLOR1;
        _paypalMessage.font = AutoFont(13);
        _paypalMessage.text = Localized(@"OTC_Income_Payment_message");
        _paypalMessage.numberOfLines = 0;
        _paypalMessage.bottom = _addBankButton.mj_y -10;
        _paypalMessage.centerX = self.view.centerX;
    }
    return _paypalMessage;
}
- (UILabel *)alipayNmuberTitle
{
    if (!_alipayNmuberTitle) {
        _alipayNmuberTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(38), RealValue_W(50), 100, RealValue_W(32))];
        _alipayNmuberTitle.textColor = MAINTITLECOLOR1;
        _alipayNmuberTitle.font = AutoFont(13);
        _alipayNmuberTitle.text = [NSString stringWithFormat:@"%@：",Localized(@"OTC_weChat_num")];
        [_alipayNmuberTitle sizeToFit];
        _alipayNmuberTitle.width = _alipayNmuberTitle.width;
    }
    return _alipayNmuberTitle;
}
- (UILabel *)alipayNmuber
{
    if (!_alipayNmuber) {
        _alipayNmuber = [[UILabel alloc] initWithFrame:CGRectMake(_alipayNmuberTitle.right +10 , 0, 200, RealValue_W(36))];
        _alipayNmuber.textColor = MAINTITLECOLOR;
        _alipayNmuber.font = AutoBoldFont(18);
        _alipayNmuber.centerY = _alipayNmuberTitle.centerY;
    }
    return _alipayNmuber;
}
- (UILabel *)nameTitle
{
    if (!_nameTitle) {
        _nameTitle = [[UILabel alloc] initWithFrame:CGRectMake(_alipayNmuberTitle.mj_x,_alipayNmuberTitle.bottom + RealValue_W(30), _alipayNmuberTitle.width, RealValue_W(36))];
        _nameTitle.textColor = MAINTITLECOLOR1;
        _nameTitle.font = AutoFont(13);
        _nameTitle.text = [NSString stringWithFormat:@"%@：",Localized(@"Identity_name")];
        
    }
    return _nameTitle;
}
- (UILabel *)name
{
    if (!_name) {
        _name = [[UILabel alloc] initWithFrame:CGRectMake(_alipayNmuber.mj_x , 0, 200, RealValue_W(32))];
        _name.textColor = MAINTITLECOLOR;
        _name.font = AutoBoldFont(18);
        _name.centerY = _nameTitle.centerY;
    }
    return _name;
}
- (UIImageView *)QRimage
{
    if (!_QRimage) {
        _QRimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, RealValue_H(360), RealValue_H(290), RealValue_H(290))];
        _QRimage.centerX = self.view.centerX;
        //1.创建长按手势
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick:)];
        
        //2.开启人机交互
        _QRimage.userInteractionEnabled = YES;
        
        //3.添加手势
        [_QRimage addGestureRecognizer:longTap];
    }
    return _QRimage;
}

- (UILabel *)QRTitle
{
    if (!_QRTitle) {
        _QRTitle = [[UILabel alloc] initWithFrame:CGRectMake(0 , _QRimage.bottom +10, 320, RealValue_W(42))];
        _QRTitle.textColor = MAINTITLECOLOR;
        _QRTitle.font = AutoFont(15);
        _QRTitle.text = Localized(@"OTC_Wechat_receipt_message");
        _QRTitle.numberOfLines = 0;
        _QRTitle.textAlignment = NSTextAlignmentCenter;
        _QRTitle.centerX = self.view.centerX;
    }
    return _QRTitle;
}
#pragma mark 长按手势弹出警告视图确认
-(void)imglongTapClick:(UILongPressGestureRecognizer*)gesture

{
    
    if(gesture.state==UIGestureRecognizerStateBegan)
        
    {
        
        CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"Asset_Sure_album") titleStr:Localized(@"Asset_Save_picture") openUrl:@"" confirm:Localized(@"transaction_OK") cancel:Localized(@"transaction_cancel")   state:2 RechargeBlock:^{
            
            // 保存图片到相册
            UIImageWriteToSavedPhotosAlbum(self.QRimage.image,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),nil);
            
            
            
        }];
        
        aleartVC.animationStyle = LXASAnimationDefault;
        [aleartVC showLXAlertView];
        
        
        
    }
    
}
#pragma mark 保存图片后的回调
- (void)imageSavedToPhotosAlbum:(UIImage*)image didFinishSavingWithError:  (NSError*)error contextInfo:(id)contextInfo
{
    NSString*message =@"";
    
    if(!error) {
        
        message =Localized(@"Asset_Saved_album");
        
        [EXUnit showSVProgressHUD:message];
        
    }else
        
    {
        
        message = [error description];
        
        [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"album_message") view:kWindow];
        
    }
    
}

@end
