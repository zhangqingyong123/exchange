//
//  BankWithSetViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "BankModle.h"
@interface BankWithSetViewController : BaseViewViewController
@property (nonatomic,assign)BOOL ischooseBank;
@property (copy, nonatomic) void(^addBankblock)(BankModle* modle);
@end
