//
//  BindBankViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
@interface BindBankViewController : BaseViewViewController
@property (assign, nonatomic) NSInteger index;
@property (nonatomic,strong)NSArray *dataAry;
@end
