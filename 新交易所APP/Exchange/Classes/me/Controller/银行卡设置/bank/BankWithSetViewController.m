//
//  BankWithSetViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/6.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BankWithSetViewController.h"
#import "BankCell.h"
#import "BankModle.h"
#import "CheckVersionAlearView.h"
#import "BindBankViewController.h"
@interface BankWithSetViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,strong) UIButton * addBankButton;
@property (nonatomic,strong) UILabel * paypalMessage;
@property (nonatomic,strong) NSArray * dataArrry;
@end

@implementation BankWithSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    [self.view addSubview:self.tableView];
    [self getData];
    if (!_ischooseBank)
    {
       [self.view addSubview:self.addBankButton];
        if ([[AppDelegate shareAppdelegate].privilages containsString:@"8"]) {

            [self.view addSubview:self.paypalMessage];
            
        }
      
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getData:) name:bindbnakNotificationse object:nil];
    
}
- (void)addBack:(UIButton *)sender{
    BindBankViewController * bindbank = [[BindBankViewController alloc] init];
    bindbank.title = Localized(@"OTC_Income_Payment");
    bindbank.index = 0;
    bindbank.dataAry = _dataArrry;
    [self.navigationController pushViewController:bindbank animated:YES];
}
- (void)getData:(NSNotification *)message
{
    [self getData];
}
- (void)getData
{
     [_dataAry removeAllObjects];
     WeakSelf
    [HomeViewModel getMobilePayDataListWithDic:@{} url:mobilePay callBack:^(BOOL isSuccess, id callBack) {
        if (isSuccess) {
            NSArray * list = callBack[@"result"][@"banks"];
            NSArray * logs = callBack[@"result"][@"logs"];
            weakSelf.dataArrry = logs;
            [weakSelf.dataAry addObjectsFromArray:[BankModle mj_objectArrayWithKeyValuesArray:list]];
            [weakSelf.tableView reloadData];
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
        }
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    WeakSelf
    if (_dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:_tableView.frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10, UIScreenWidth, UIScreenHeight  -74 -IPhoneTop)];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[BankCell class] forCellReuseIdentifier:@"BankCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (UIButton *)addBankButton
{
    if (!_addBankButton) {
        _addBankButton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(30), UIScreenHeight -45 - RealValue_W(100) - kStatusBarAndNavigationBarHeight - IPhoneBottom, UIScreenWidth - RealValue_W(60), 40)];
        _addBankButton.backgroundColor = TABTITLECOLOR;
        [_addBankButton addTarget:self action:@selector(addBack:) forControlEvents:UIControlEventTouchUpInside];
        [_addBankButton setTitle:Localized(@"OTC_Income_Payment") forState:0];
        _addBankButton.titleLabel.font = AutoBoldFont(15);
        [_addBankButton setTitleColor:WHITECOLOR forState:0];
    }
    return _addBankButton;
}
- (UILabel *)paypalMessage
{
    if (!_paypalMessage) {
        _paypalMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
        _paypalMessage.textColor = MAINTITLECOLOR1;
        _paypalMessage.font = AutoFont(13);
        _paypalMessage.text = Localized(@"OTC_Income_Payment_message");
        _paypalMessage.numberOfLines = 0;
        _paypalMessage.bottom = _addBankButton.mj_y -10;
        _paypalMessage.centerX = self.view.centerX;
    }
    return _paypalMessage;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(300);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BankCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"BankCell"];
    cell.modle = _dataAry[indexPath.row];
    WeakSelf
    cell.deleteBank = ^(BankModle *modle) {
        [weakSelf deletebankWith:modle];
    };
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_ischooseBank) {
        BankModle *modle = _dataAry[indexPath.row];
        if (self.addBankblock) {
            self.addBankblock(modle);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//    删除
- (void)deletebankWith:(BankModle *)modle
{
    if (_ischooseBank) {
        return;
    }
    CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"bank_unbind") titleStr:Localized(@"Remove_bank_message") openUrl:@"" confirm:Localized(@"transaction_OK") cancel:Localized(@"transaction_cancel")   state:2 RechargeBlock:^{
        
        [self POSTWithHost:@"" path:deleteBank param:@{@"id":modle.ID} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [self.dataAry removeObject:modle];
                
                [self.tableView reloadData];
            }else
            {
                [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        
        
        
        
    }];
    
    aleartVC.animationStyle = LXASAnimationTopShake;
    [aleartVC showLXAlertView];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
