//
//  BankCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankModle.h"
@interface BankCell : UITableViewCell
@property (nonatomic,strong)UIImageView * bankImage;
@property (nonatomic,strong)UILabel * titlename;
@property (nonatomic,strong)UILabel * bankNumber;
@property (nonatomic,strong)UILabel * bankbranch;
@property (nonatomic,strong)BankModle * modle;
@property (copy, nonatomic) void(^deleteBank)(BankModle * modle);
@end
