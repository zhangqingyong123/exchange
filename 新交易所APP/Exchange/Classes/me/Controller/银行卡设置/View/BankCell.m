//
//  BankCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BankCell.h"

@implementation BankCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        
    }
    return self;
    
}
- (void)setModle:(BankModle *)modle
{
    _modle = modle;
    _titlename.text = modle.bank;
    _bankNumber.text = modle.card_number;
    _bankbranch.text = modle.sub_bank;
}
- (void)get_up
{
    [self.contentView addSubview:self.bankImage];
    [self.bankImage addSubview:self.titlename];
    [self.bankImage addSubview:self.bankNumber];
    [self.bankImage addSubview:self.bankbranch];
}
- (void)imglongTapClick:(UILongPressGestureRecognizer*)gesture
{
    if(gesture.state==UIGestureRecognizerStateBegan)
        
    {
        if (self.deleteBank) {
            self.deleteBank(_modle);
        };
    }
    
}
- (UIImageView *)bankImage
{
    if (!_bankImage) {
        _bankImage = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(44), RealValue_W(22), UIScreenWidth - RealValue_W(40 +44), RealValue_W(278))];
        _bankImage.image = [UIImage imageNamed:@"img_cardbg"];
        //1.创建长按手势
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick:)];
        _bankImage.userInteractionEnabled = YES;
        [_bankImage addGestureRecognizer:longTap];
    }
    return _bankImage;
}
- (UILabel *)titlename
{
    if (!_titlename) {
        _titlename = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(34), RealValue_W(50), 200, RealValue_W(40))];
        
        _titlename.textColor = WHITECOLOR;
        _titlename.font = AutoBoldFont(20);
    }
    return _titlename;
}
- (UILabel *)bankNumber
{
    if (!_bankNumber) {
        _bankNumber = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(34),_titlename.bottom + RealValue_W(34), _bankImage.width -RealValue_W(68), RealValue_W(34))];
        
        _bankNumber.textColor = WHITECOLOR;
        _bankNumber.font = AutoBoldFont(22);
    }
    return _bankNumber;
}
- (UILabel *)bankbranch
{
    if (!_bankbranch) {
        _bankbranch = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(34),_bankNumber.bottom + RealValue_W(34), _bankImage.width -RealValue_W(68), RealValue_W(34))];
        
        _bankbranch.textColor = WHITECOLOR;
        _bankbranch.font = AutoFont(13);
    }
    return _bankbranch;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
