//
//  PresonaModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PresonaModle : NSObject
@property (nonatomic,strong)NSString * title;
@property (nonatomic,strong)NSString * image;
@property (nonatomic, assign) Class controllerClass;
@property (nonatomic,strong)NSString * stats;
@end
