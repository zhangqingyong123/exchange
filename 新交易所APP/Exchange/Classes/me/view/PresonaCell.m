//
//  PresonaCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "PresonaCell.h"

@implementation PresonaCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, 30, 0, 0)];
//        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    }
    return self;
    
}
- (void)setModel:(PresonaModle *)model
{
    _model = model;
    _titlelable.text = _model.title;
    _img.image = [UIImage imageNamed:model.image];
}
- (void)get_up
{
    [self.contentView addSubview:self.img];
    [self.contentView addSubview:self.titlelable];
    [self.contentView addSubview:self.rightImg];
    [self.contentView addSubview:self.rightlable];
    [self.contentView addSubview:self.returnImg];
    
}
- (UIImageView *)img
{
    if (!_img) {
        _img = [[UIImageView alloc] initWithFrame:CGRectMake(30, 54/2 - RealValue_W(34)/2, RealValue_W(34), RealValue_W(34))];
    }
    return _img;
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(_img.right+RealValue_W(34),  54/2 - 15, 240, 30)];
        _titlelable.font = AutoFont(15);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.adjustsFontSizeToFitWidth = YES;
    }
    return _titlelable;
}
- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(80) -80,  54/2 - 15, 80, 30)];
        _rightlable.font = AutoFont(15);
        _rightlable.textColor = MAINTITLECOLOR1;
        _rightlable.adjustsFontSizeToFitWidth = YES;
        _rightlable.textAlignment = NSTextAlignmentRight;
    }
    return _rightlable;
}
- (UIImageView *)returnImg
{
    if (!_returnImg) {
        _returnImg = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - 30 - RealValue_W(10),
                                                                   54/2 - RealValue_W(16)/2,
                                                                   RealValue_W(10),
                                                                   RealValue_W(16))];
        _returnImg.image = [UIImage imageNamed:@"icon_list_go"];
    }
    return _returnImg;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
