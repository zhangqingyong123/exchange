//
//  PresonaCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PresonaModle.h"
@interface PresonaCell : UITableViewCell
@property (nonatomic,strong)UIImageView * img;
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UIImageView * rightImg;
@property (nonatomic,strong)UIImageView * returnImg;
@property (nonatomic,strong)UILabel * rightlable;
@property (nonatomic,strong)PresonaModle * model;
@end
