//
//  PresonalHeadView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PresonalHeadView : UIView
@property(nonatomic,strong)UIImageView    * bgImageview;//背景VIEW
@property(nonatomic,strong)UILabel        * titleLable;//标题
@property(nonatomic,strong)UIImageView    * userImageview;//背景VIEW
@property(nonatomic,strong)UILabel        * timeLable;//账号资金
@property(nonatomic,strong)UILabel        * authenticationLable;//认证标签
@property(nonatomic,strong)UIButton       * skinButton;//认证标签
@property(nonatomic,strong)UIButton       * setButton;//认证标签
@property (nonatomic,strong)NSMutableArray * contonts;
@property (copy, nonatomic) void(^setSkinBlock)(UIButton * button);
@property (copy, nonatomic) void(^setBlock)(UIButton * button);
@property (copy, nonatomic) void(^authenticationBlock)(UIButton * button);
- (void)getUserInfo:(NSString *)phone
      lastLoginTime:(NSString *)lastLoginTime;

- (void)getRealName:(NSInteger)RealName;

@end
