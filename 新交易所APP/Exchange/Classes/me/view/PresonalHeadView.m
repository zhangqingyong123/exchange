//
//  PresonalHeadView.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "PresonalHeadView.h"
#import "IdentityAuthenticationViewController.h"
#import "SecurityCenterViewController.h"
#import "PaymentSetVC.h"
#import "LoginViewController.h"
@implementation PresonalHeadView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self get_up];
    }
    return self;
    
}
- (void)getUserInfo:(NSString *)phone
      lastLoginTime:(NSString *)lastLoginTime
{
    _titleLable.text =phone;
    _timeLable.text = [NSString stringWithFormat:@"ID: %@",lastLoginTime];
    CGSize size =  [_timeLable sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
    _timeLable.width = size.width +4;
    _timeLable.height = 12;

}
////1是已提交，2是成功，3是失败，4是审核中
- (void)getRealName:(NSInteger)RealName;
{
    self.authenticationLable.hidden = NO;
    if ([EXUserManager authenticatedName].integerValue ==1)
    {
        _authenticationLable.hidden = NO;
        _authenticationLable.text = Localized(@"my_Inviting_Certified");
        _authenticationLable.backgroundColor = TABTITLECOLOR;
    }else
    {
        if (RealName ==1)
        {
            _authenticationLable.hidden = YES;
            
        }else if (RealName ==2)
        {
            _authenticationLable.hidden = NO;
            _authenticationLable.text = Localized(@"my_Inviting_Certified");
            _authenticationLable.backgroundColor = TABTITLECOLOR;
            
        }else if (RealName ==3)
        {
            _authenticationLable.hidden = NO;
            _authenticationLable.text = Localized(@"my_Audit_failure");
            _authenticationLable.backgroundColor = ColorStr(@"#575775");
            
        }else if (RealName ==4)
        {
            _authenticationLable.hidden = NO;
            _authenticationLable.text = Localized(@"C2C_Audit");
            _authenticationLable.backgroundColor = ColorStr(@"#575775");
            
            
        }else if (RealName ==0)
        {
           _authenticationLable.hidden = NO;
           _authenticationLable.text = Localized(@"my_Uncertified");
           _authenticationLable.backgroundColor = ColorStr(@"#575775");
        }
    }
         CGSize size = [_authenticationLable sizeThatFits:CGSizeMake(100, MAXFLOAT)];
        _authenticationLable.frame = CGRectMake(_timeLable.right +4,0, size.width+7, 15);
        _authenticationLable.centerY = _timeLable.centerY;
        _authenticationLable.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_authenticationLable cornerRadius:2];
   
}
// 设置
- (void)setClick:(UIButton *)button
{
    if (self.setBlock) {
        self.setBlock(button);
    }
}
//设置皮肤
- (void)setskin:(UIButton *)button
{
    if (self.setSkinBlock) {
        self.setSkinBlock(button);
    }
}
- (void)get_up
{
    [self addSubview:self.bgImageview];
    [_bgImageview addSubview:self.userImageview];
    [_bgImageview addSubview:self.titleLable];
    [_bgImageview addSubview:self.timeLable];
    [_bgImageview addSubview:self.authenticationLable];
    [self addSubview:self.setButton];
    [self addSubview:self.skinButton];
  
    [self.contonts addObject:@{@"image":@"icon_me_agenda",@"title":Localized(@"my_identity_authentication"),@"controllerClass":[IdentityAuthenticationViewController class]}];
    [self.contonts  addObject:@{@"image":@"icon_me_shield",@"title":Localized(@"my_Security_Center"),@"controllerClass":[SecurityCenterViewController class]}];
    if ([[EXUnit getSite] isEqualToString:@"BitAladdin"] || [[EXUnit getSite] isEqualToString:@"Coinbetter"] || [[EXUnit getSite] isEqualToString:@"Ex.pizza"])
    {
       
    }else
    {
         [self.contonts  addObject:@{@"image":@"icon_me_card",@"title":Localized(@"my_Bank_card_settings"),@"controllerClass":[PaymentSetVC class]}];
    }
    
    for (int i =0; i< self.contonts.count; i++)
    {
        NSString * title = _contonts[i][@"title"];
        NSString * img = _contonts[i][@"image"];
        UIButton * button = [[UIButton alloc ] initWithFrame:CGRectMake(i * UIScreenWidth/self.contonts.count, _bgImageview.bottom, UIScreenWidth/self.contonts.count, RealValue_H(190))];
        [button setTitle:title forState:0];
        [button setImage:[UIImage imageNamed:img] forState:0];
         button.tag = i;
        
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = TABLEVIEWLCOLOR;
        button.titleLabel.font = AutoFont(15);
        [button setTitleColor:MAINTITLECOLOR forState:0];
        [button centerVerticallyImageAndTextWithPadding:10];
        [self addSubview:button];
    }
}
- (void)buttonClick:(UIButton *)button
{
    if (self.authenticationBlock) {
        self.authenticationBlock(button);
    }
}
- (NSMutableArray *)contonts
{
    if (!_contonts) {
        _contonts = [[NSMutableArray alloc] init];
    }
    return _contonts;
}
- (UIImageView *)bgImageview
{
    if (!_bgImageview) {
        _bgImageview = [UIImageView dd_imageViewWithFrame:CGRectMake(0,  0, UIScreenWidth, RealValue_W(320) +IPhoneTop) islayer:NO imageStr:[UIImage imageNamed:@"icon_headImage"] tapAction:^(UIImageView *image) {
            if (![EXUserManager isLogin]) {
                LoginViewController * loginView = [[LoginViewController alloc] init];
                [ [EXUnit currentViewController] presentViewController:loginView animated:YES completion:nil];
            }
           
        }];
    }
    return _bgImageview;
}
- (UIImageView *)userImageview
{
    if (!_userImageview) {
        _userImageview = [UIImageView dd_imageViewWithFrame:CGRectMake(RealValue_W(60),  _bgImageview.height/2 - RealValue_W(70)/2+5, RealValue_W(70), RealValue_W(70)) islayer:NO imageStr:[UIImage imageNamed:@"me_avatar"] tapAction:^(UIImageView *image) {
         
        }];
   
       
    }
    return _userImageview;
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(_userImageview.right +RealValue_W(29), _userImageview.mj_y -5  , 220, 26)];
        _titleLable.font = AutoBoldFont(22);
        _titleLable.textColor = [UIColor whiteColor];
  
        
     
    }
    return _titleLable;
}
- (UILabel *)timeLable
{
    if (!_timeLable) {
        _timeLable = [[UILabel alloc] initWithFrame:CGRectMake(_userImageview.right +RealValue_W(29), _titleLable.bottom +7, 0, 12)];
        _timeLable.font = AutoFont(12);
        _timeLable.numberOfLines = 0;
        _timeLable.textColor = MAINTITLECOLOR1;
    }
    return _timeLable;
}
- (UILabel *)authenticationLable
{
    if (!_authenticationLable) {
        _authenticationLable = [[UILabel alloc] initWithFrame:CGRectMake(_timeLable.right +5, 0, 0, 12)];
        _authenticationLable.backgroundColor = RGBA(47, 47, 63, 1);
        _authenticationLable.textColor = [UIColor whiteColor];
        _authenticationLable.centerY = _authenticationLable.centerY;
        _authenticationLable.font = AutoFont(10);
        _authenticationLable.textAlignment = NSTextAlignmentCenter;
      
    }
    return _authenticationLable;
}
- (UIButton *)setButton
{
    if (!_setButton) {
        _setButton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - 50, 20 +IPhoneTop, 40, 40)];
        [_setButton setImage:[UIImage imageNamed:@"setimage"] forState:0];
        [_setButton addTarget:self action:@selector(setClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _setButton;
}
- (UIButton *)skinButton
{
    if (!_skinButton) {
        _skinButton = [[UIButton alloc] initWithFrame:CGRectMake(_setButton.mj_x - 50, 20 +IPhoneTop, 40, 40)];
        [_skinButton setImage:[UIImage imageNamed:@"nightColor"] forState:UIControlStateNormal];
        [_skinButton setImage:[UIImage imageNamed:@"Daycolor"] forState:UIControlStateSelected];
        if ([[ZBLocalized sharedInstance].AppbBGColor isEqualToString:BLACK_COLOR])
        {
            _skinButton.selected = YES;
        }else
        {
            _skinButton.selected = NO;
        }
        [_skinButton addTarget:self action:@selector(setskin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _skinButton;
}
@end
