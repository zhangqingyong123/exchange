//
//  HomeViewModle.h
//  Exchange
//
//  Created by 张庆勇 on 2019/1/9.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeModle.h"
#import "TableViewHeadview.h"
#import "BankModle.h"
#define HOSTGET @"GET"
#define HOSTPOST @"POST"
@interface HomeViewModel : NSObject
#pragma mark - 通用的接口
+ (void)getUserWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


#pragma mark - 首页
/*获取汇率*/
+ (void)getCurrencyWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;
/*配置size*/
+ (void)getSiteWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;

/*获取bannerList*/
+ (void)getbannerListWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;

/*获取公告*/
+ (void)getArticleListWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


/*获取最新交易对*/
+ (void)getSymbolDataListWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;

/*获取支付方式*/
+ (void)getMobilePayDataListWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;

/*获取最新k交易对并切排序返回新的数组
 @param articlelArray 交易对返回到数据数组
 @param deal
 @param volume
 @param high
 @param open
 @param last
 @param low
 @param name
 @param dataArray 排序返回新的数组
 @param recommendArray 推荐的数组
 @param DICKEY 交易对 （cnt CNY）
 @param mutableDic 最新交易对的价格
 */

+ (void)getFirestSymbolWithArticlelArray:(NSMutableArray *)articlelArray
                             ataWithDeal:(NSString *)deal
                                  volume:(NSString *)volume
                                    high:(NSString *)high
                                    open:(NSString *)open
                                    last:(NSString *)last
                                     low:(NSString *)low
                                    name:(NSString *)name
                               dataArray:(NSMutableArray *)dataArray
                          recommendArray:(NSMutableArray *)recommendArray
                                  DICKEY:(NSString *)DICKEY
                              mutableDic:(NSMutableDictionary *)mutableDic
                                callBack:(void(^)(NSMutableArray * dataArray,NSMutableArray * recommendArray,NSMutableDictionary * mutableDic))callback;


/*更新交易对并切排序返回新的数组
 @param articlelArray 交易对返回到数据数组
 @param deal
 @param volume
 @param high
 @param open
 @param last
 @param low
 @param name
 @param dataArray 排序返回新的数组
 @param recommendArray 推荐的数组
 @param DICKEY 交易对 （cnt CNY）
 @param isGetnews 判断是否获取咯广告接口
 @param isrecommen 已经计算过headview高不需要在计算
 @param tableviewHeadView tableviewHeadView
 @param buttonIndex 1 涨幅榜 2为跌幅榜
 @param mutableDic 最新交易对的价格
 */
+ (void)getUpdataSymbolWithArticlelArray:(NSMutableArray *)articlelArray
                             ataWithDeal:(NSString *)deal
                                  volume:(NSString *)volume
                                    high:(NSString *)high
                                    open:(NSString *)open
                                    last:(NSString *)last
                                     low:(NSString *)low
                                    name:(NSString *)name
                               dataArray:(NSMutableArray *)dataArray
                          recommendArray:(NSMutableArray *)recommendArray
                                  DICKEY:(NSString *)DICKEY
                               isGetnews:(BOOL )isGetnews
                              isrecommen:(BOOL)isrecommen
                       tableviewHeadView:(TableViewHeadview *)tableviewHeadView
                             buttonIndex:(NSInteger )buttonIndex
                              mutableDic:(NSMutableDictionary *)mutableDic
                                callBack:(void(^)(NSMutableArray * dataArray,NSMutableArray * recommendArray,NSMutableDictionary * mutableDic))callback;


/*更新FMDB数据
 @param deal
 @param volume
 @param high
 @param open
 @param last
 @param low
 @param name
 */
+ (void)UpDataFMDBSymbolWithDeal:(NSString *)deal
                          volume:(NSString *)volume
                            high:(NSString *)high
                            open:(NSString *)open
                            last:(NSString *)last
                             low:(NSString *)low
                            name:(NSString *)name;




#pragma mark - 交易对详情

/*获取交易详情
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getAssetInfoWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


#pragma mark - 行情

/*获取交易对分类
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getSymbolClassWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


#pragma mark - 交易

/*获取个人资产
 @param dic 传的参数
 @param urr 接口名称
 @param homeModle
 @param isSell 是否为卖出
 @return sellAvailable 卖出的交易对余额
 @return purchaseAvailable买入交易对余额
 @return balance个人资产
 */
+ (void)getUserAssetsWithDic:(NSDictionary *)dic url:(NSString *)url homeModle:(HomeModle *)modle isSell:(BOOL)isSell callBack:(void(^)(BOOL isSuccess, NSString * sellAvailable, NSString * purchaseAvailable , NSString * balance))callback;

/*取消订单
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getCancelOrderWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


/*购买&卖出
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getTradeWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


#pragma mark - C2C 交易

/*银行卡信息
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getUserbankWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


/*C2C配置信息
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getC2CPriceInfoWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;


/*未完成订单list
 @param dic 传的参数
 @param urr 接口名称
 @return isSuccess 成功 数组
 */
+ (void)getTradeListWithDic:(NSDictionary *)dic url:(NSString *)url callBack:(void(^)(BOOL isSuccess,id callBack))callback;
@end

