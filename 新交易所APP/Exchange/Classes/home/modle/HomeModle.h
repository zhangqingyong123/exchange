//
//  HomeModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXCoding.h"
#import "BGFMDB.h" //添加该头文件,本类就具有了存储功能.
@interface SymbolModle : NSObject
@property (nonatomic,strong) NSString * CreatedAt;
@property (nonatomic,strong) NSString * ID;
@property (nonatomic,strong) NSString * UpdatedAt;
@property (nonatomic,strong) NSArray  * classes;
@property (nonatomic,strong) NSString * base_asset;
@property (nonatomic,strong) NSString * base_asset_name;
@property (nonatomic,strong) NSString * limit_maker_fee;
@property (nonatomic,strong) NSString * limit_taker_fee;
@property (nonatomic,strong) NSString * market_taker_fee;
@property (nonatomic,strong) NSString * min_quantity;
@property (nonatomic,strong) NSString * quote_asset;
@property (nonatomic,strong) NSString * quote_asset_name;
@property (nonatomic,strong) NSString * recommend;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * symbol;
@property (nonatomic,strong) NSString * tick_size;
@property (nonatomic,strong) NSString * base_asset_precision; //币种前面的精度
@property (nonatomic,strong) NSString * quote_asset_precision;//币种后面的精度

@end
@interface HomeModle : NSObject
@property (nonatomic,strong) NSString * quote_asset;
@property (nonatomic,strong) NSString * base_asset;
@property (nonatomic,strong) NSArray * classes;
@property (nonatomic,strong) NSString * recommend; // 0不推荐 1为推荐
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * deal;
@property (nonatomic,strong) NSString * volume;

@property (nonatomic,strong) NSString * high;
@property (nonatomic,strong) NSString * open;
@property (nonatomic,strong) NSString * last;
@property (nonatomic,strong) NSString * low; //市场最低价

@property (nonatomic,strong) NSString * IncreaseNum;
@property (nonatomic,strong) NSString *  currency; //汇率
@property (nonatomic,strong) NSString * min_quantity;//数量幅度
@property (nonatomic,strong) NSString * tick_size;//价格幅度
@property (nonatomic,strong) NSString * IncreaseDegree;//增幅度
@property (nonatomic,strong) NSString * increase;//增幅度
@property (nonatomic,strong) NSString * base_asset_precision; //币种前面的精度 数量
@property (nonatomic,strong) NSString * quote_asset_precision;//币种后面的精度 价格
@end
@interface NewsModle : NSObject
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString * id;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * view_count;
@property (nonatomic,strong) NSString * resume;
@property (nonatomic,strong) NSString * key_words;
@property (nonatomic,strong) NSString * dateline;
@property (nonatomic,strong) NSString * site_id;
@property (nonatomic,strong) NSString * russian_title;
@end
@interface BanneModle : NSObject
@property (nonatomic,strong) NSString * image;
@property (nonatomic,strong) NSString * url;
@property (nonatomic,strong) NSString * url_type;
@property (nonatomic,strong) NSString * type;
@property (nonatomic,strong) NSString * id;
@property (nonatomic,strong) NSString * title;
@end
