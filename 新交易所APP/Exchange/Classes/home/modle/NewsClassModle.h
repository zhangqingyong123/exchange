//
//  NewsModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/23.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsClassModle : NSObject
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * dateline;
@property (nonatomic,strong) NSString * id;
@property (nonatomic,strong) NSString * view_count;
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString * resume; 
@end

