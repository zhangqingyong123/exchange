//
//  NewsInfoViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "NewsInfoViewController.h"
#import "NewsTableViewCell.h"
#import "TGWebProgressLayer.h"
@interface NewsInfoViewController ()
@property (nonatomic, strong) UILabel * titlelable;
@property (nonatomic, strong) UILabel * dataTimelable;
@property (nonatomic, strong) UITextView * textview;
@property (nonatomic, strong)TGWebProgressLayer *webProgressLayer;
@property (nonatomic,strong) NSString * content;
@end
@implementation NewsInfoViewController
- (void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO]; //设置隐藏

}
- (void)viewDidDisappear:(BOOL)animated
{
    [self.webProgressLayer removeFromSuperlayer];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.titlelable];
    [self.view addSubview:self.dataTimelable];
    [self.view addSubview:self.textview];
    self.webProgressLayer = [[TGWebProgressLayer alloc] init];
    self.webProgressLayer.frame = CGRectMake(0, 46, UIScreenWidth, 2);
    self.webProgressLayer.strokeColor =  TABTITLECOLOR.CGColor;
    [self.navigationController.navigationBar.layer addSublayer:self.webProgressLayer];
    [self.webProgressLayer tg_startLoad];
    [self getNewsInfo];
}
- (void)getNewsInfo
{
     NSString * str = [NSString stringWithFormat:@"%@/%@",article,_newsID];
    WeakSelf
    [self GETWithHost:@"" path:str param:@{@"type":_isHelp?@"help":@"news"} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)
        {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                //通知主线程刷新
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    weakSelf.titlelable.text =  responseObject[@"result"][@"title"];
                    weakSelf.titlelable.numberOfLines = 0;
                    [weakSelf.titlelable sizeToFit];
                    weakSelf.titlelable.width = UIScreenWidth-40;
                    weakSelf.dataTimelable.text = [EXUnit getTimeFromTimestamp:responseObject[@"result"][@"dateline"]];
                    weakSelf.dataTimelable.mj_y =   weakSelf.titlelable.bottom + RealValue_H(38/2);
                    weakSelf.textview.mj_y = weakSelf.dataTimelable.bottom;
                    
                    weakSelf.content =  responseObject[@"result"][@"content"];
                    NSString*str = [NSString stringWithFormat:@"<head><style>img{width:%f !important;height:auto}</style></head>%@",self.view.width - 30,weakSelf.content];
                    // 处理耗时操作的代码块...
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    weakSelf.textview.attributedText = attrStr;
                    weakSelf.textview.font = AutoFont(12);
                    weakSelf.textview.textColor = [ZBLocalized sharedInstance].newsTitleColor;
                    [weakSelf.webProgressLayer tg_finishedLoadWithError:nil];
                    
                    
                });
                
            });
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(20, RealValue_H(22), UIScreenWidth-40, 0)];
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.font = AutoBoldFont(15);
       
        _titlelable.numberOfLines = 0;
        [_titlelable sizeToFit];
        _titlelable.width = UIScreenWidth-40;
    }
    return _titlelable;
}
- (UILabel *)dataTimelable
{
    if (!_dataTimelable) {
        _dataTimelable = [[UILabel alloc] initWithFrame:CGRectMake(20,_titlelable.bottom + RealValue_H(38/2), UIScreenWidth-40, 15)];
        _dataTimelable.textColor = MAINTITLECOLOR;
        _dataTimelable.font = AutoFont(12);
       
    }
    return _dataTimelable;
}
- (UITextView *)textview
{
    if (!_textview) {
        _textview = [[UITextView alloc] initWithFrame:CGRectMake(_titlelable.mj_x, _dataTimelable.bottom , _dataTimelable.width, UIScreenHeight - kStatusBarAndNavigationBarHeight -_dataTimelable.mj_y -15)];
        _textview.editable = NO;
        _textview.showsVerticalScrollIndicator = NO;
        _textview.backgroundColor = self.view.backgroundColor;
       
        
      
    }
    return _textview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
