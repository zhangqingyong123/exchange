//
//  NewsInfoViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface NewsInfoViewController : BaseViewViewController
@property (nonatomic,strong) NSString * newsID;
@property (nonatomic,strong) NSString * titletext;
@property (nonatomic,strong) NSString * time;
@property (nonatomic,assign)BOOL isHelp;

@end
