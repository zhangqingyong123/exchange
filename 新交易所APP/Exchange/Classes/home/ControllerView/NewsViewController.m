//
//  NewsViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/20.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsInfoViewController.h"
#import "NewsClassModle.h"
#import "NewsTableViewCell.h"
@interface NewsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;//数据
@property (nonatomic, assign) NSInteger         pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation NewsViewController
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES]; //设置隐藏
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = TABLEVIEWLCOLOR;
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    WLRefreshGifHeader *header = [WLRefreshGifHeader headerWithRefreshingBlock:^{
        
        weakSelf.pages = 0;
        
        [weakSelf getNewslist];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getNewslist];
    }];
    self.tableView.mj_footer = fooder;
}
#pragma mark----数据
- (void)getNewslist
{
    WeakSelf
    [self GETWithHost:@"" path:articleList param:@{@"type":@"news",@"class_id":@"",@"offset":@(_pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)
        {
            
            NSArray *list = responseObject[@"result"][@"records"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataArray addObjectsFromArray:[NewsClassModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else
            {
                self.dataArray = [NewsClassModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
             [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
        }
        
        [self.tableView reloadData];
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [weakSelf placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:YES];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self axcBasePopUpWarningAlertViewWithMessage:@"请求错误" view:kWindow];

    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
        WeakSelf
        if (self.dataArray.count == 0)
        {
            [_workView removeFromSuperview];
            _workView = [[NoNetworkView alloc] initWithFrame:_tableView.frame NoNetwork:NoNetwork];
            if (NoNetwork) {
                _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                    [weakSelf getNewslist];
                };
            }
            
            [_tableView addSubview:_workView];
        }else
        {
            [_workView dissmiss];
        }  
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight - 64 - IPhoneBottom)style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        [_tableView registerNib:[UINib nibWithNibName:@"NewsTableViewCell" bundle:nil]  forCellReuseIdentifier:@"NewsTableViewCell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
        
    }
    return _dataArray;
}
#pragma  mark -----tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    CATransform3D rotation;
//    rotation = CATransform3DMakeRotation((90.0*M_PI/180), 0.0, 0.7, 0.4);
//    rotation.m44 = 1.0/-600;
//    //阴影
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    //阴影偏移
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    //透明度
//    cell.alpha = 0;
//
//    cell.layer.transform = rotation;
//
//    //锚点
//    cell.layer.anchorPoint = CGPointMake(0.5, 0.5);
//
//    [UIView beginAnimations:@"rotaion" context:NULL];
//
//    [UIView setAnimationDuration:0.8];
//
//    cell.layer.transform = CATransform3DIdentity;
//
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//
//    [UIView commitAnimations];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NewsTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"NewsTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellAccessoryNone;
    cell.backgroundColor = TABLEVIEWLCOLOR;
    cell.modle = self.dataArray[indexPath.row];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsClassModle *model = _dataArray[indexPath.row];
    NewsInfoViewController * NoticeDetailsVC = [[NewsInfoViewController alloc] init];
    NoticeDetailsVC.newsID = model.id;
    NoticeDetailsVC.titletext = model.title;
    NoticeDetailsVC.title = Localized(@"Public_details");
    NoticeDetailsVC.time = [EXUnit getTimeFromTimestamp:model.dateline];
    [self.navigationController pushViewController:NoticeDetailsVC animated:YES];
    
}

@end
