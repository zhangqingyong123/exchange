//
//  BTCCollectionViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTCCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIView *layerView;
@property (nonatomic,strong)UILabel *BTCLable;
@property (nonatomic,strong)UILabel *priceLable;
@property (nonatomic,strong)UILabel *rangeLable;
@property (nonatomic,strong)UILabel *lineLable;
@property (nonatomic,strong)UIImageView *imageview;
@property (nonatomic,strong)HomeModle *modle;
@end
