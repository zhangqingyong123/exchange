//
//  TableViewHeadview.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HW3DBannerView.h"
#import "SGAdvertScrollView.h"
@interface TableViewHeadview : UIView
@property (nonatomic,strong) UIView     *bgView;
@property (nonatomic,strong) HW3DBannerView *scrollView;
@property (nonatomic,strong) UIView     *noticeview;
@property (nonatomic,strong) UIImageView *noticeimageview;
@property (strong, nonatomic) UICollectionView *mianCollView;
@property (strong, nonatomic) UIPageControl *page;
@property(nonatomic, strong) NSArray *items;
@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, assign) NSUInteger pageCount;
@property(nonatomic, strong) UIButton * newsButton;
@property (copy, nonatomic) void(^Advertblock)(NSInteger index);
@property (copy, nonatomic) void(^BancelsUrlblock)(NSInteger index);
- (void)refushNewsSymbolDataWithArray:(NSArray *)dataArray;
- (void)uploadSymbol:(HomeModle *)modle;
- (void)getNoticeList:(NSArray *)noticeList;
@end
