//
//  BTCCollectionViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BTCCollectionViewCell.h"

@implementation BTCCollectionViewCell
- (id)initWithFrame:(CGRect)frame
{
    self =  [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [ZBLocalized sharedInstance].HomeBgColor;
        [self get_up];
    }
    return self;
}
- (void)get_up
{
    [self.contentView addSubview:self.bgView];
    [_bgView addSubview:self.imageview];
    [_bgView addSubview:self.BTCLable];
    [_bgView addSubview:self.priceLable];
    [_bgView addSubview:self.rangeLable];
    
}
- (void)setModle:(HomeModle *)modle
{

    NSArray *array = [modle.name componentsSeparatedByString:@"_"];
    int precision = modle.quote_asset_precision.intValue;
    _BTCLable.text = [NSString stringWithFormat:@"%@/%@",array[0],array[1]];
    _priceLable.text = [NSString stringWithFormat:@"%@",[EXUnit formatternumber:precision assess:modle.last]];
    
    float _x = modle.IncreaseDegree.floatValue;
    if (!isnan(_x)) {
        if (modle.last.floatValue - modle.open.floatValue >=0) {
            _rangeLable.text = [NSString stringWithFormat:@"+ %.2f%%",modle.IncreaseDegree.floatValue];
        }else
        {
            _rangeLable.text = [NSString stringWithFormat:@"%.2f%%",modle.IncreaseDegree.floatValue];
        }
        
    }else
    {
        _rangeLable.text = @"0.00%";
    }
    if (modle.last.floatValue - modle.open.floatValue >=0) {
        
        
        _rangeLable.textColor = ZHANGCOLOR;
        _imageview.image = [UIImage imageNamed:[ZBLocalized sharedInstance].symbolhighicon];
    }else
    {
        
        _rangeLable.textColor = DIEECOLOR;
         _imageview.image = [UIImage imageNamed:[ZBLocalized sharedInstance].symboldieicon];
    }
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        _bgView.backgroundColor = [ZBLocalized sharedInstance].HomeBgColor;
        _bgView.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_bgView cornerRadius:8];
    }
    return _bgView;
}
- (UIImageView *)imageview
{
    if (!_imageview) {
        _imageview = [UIImageView dd_imageViewWithFrame:CGRectMake(0, 0 , _bgView.width , _bgView.height) islayer:NO imageStr:[ZBLocalized sharedInstance].symbolhighicon];
        _imageview.center = _bgView.center;
    }
    return _imageview;
}
- (UILabel *)BTCLable
{
    if (!_BTCLable) {
        _BTCLable = [[UILabel alloc] initWithFrame:CGRectMake(0,_imageview.mj_y + 16,_imageview.width, 25/2)];
        _BTCLable.textColor =  MAINTITLECOLOR1;
        _BTCLable.font = SystemFont(10);
        _BTCLable.textAlignment = NSTextAlignmentCenter;
    }
    return _BTCLable;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(0,_BTCLable.bottom + 7,_imageview.width, 20)];
        _priceLable.textColor =  MAINTITLECOLOR;
        _priceLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        
        
        _priceLable.textAlignment = NSTextAlignmentCenter;
    }
    return _priceLable;
}

- (UILabel *)rangeLable
{
    if (!_rangeLable) {
        _rangeLable = [[UILabel alloc] initWithFrame:CGRectMake(0,_priceLable.bottom + 7,_imageview.width, 25/2)];
        _rangeLable.textColor =  DIEECOLOR;
        _rangeLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
        
        _rangeLable.textAlignment = NSTextAlignmentCenter;
    }
    return _rangeLable;
}
@end
