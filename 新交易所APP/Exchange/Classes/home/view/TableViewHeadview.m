//
//  TableViewHeadview.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "TableViewHeadview.h"
#import "BCCollectionViewHorizontalLayout.h"
#import "BTCCollectionViewCell.h"
#import "CycleImageViewPageControl.h"
#import "Y_StockChartViewController.h"
#define SCREEN_SCALE ([ UIScreen mainScreen ].bounds.size.width/320)
@interface TableViewHeadview ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong,nonatomic) BCCollectionViewHorizontalLayout *layout2;
@property (nonatomic,strong) UIView  *lightView;
@end
@implementation TableViewHeadview
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self get_up];
        self.backgroundColor = [ZBLocalized sharedInstance].HomeBgColor;
       
    }
    return self;
    
}

- (void)get_up
{
    [self addSubview:self.bgView];
    [_bgView addSubview:self.scrollView];
    [_bgView addSubview:self.noticeimageview];
    [_bgView addSubview:self.newsButton];
    [_bgView addSubview:self.noticeview];
    [_bgView addSubview:self.mianCollView];
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
- (void)refushNewsSymbolDataWithArray:(NSArray *)dataArray;
{
    _mianCollView.mj_y = _noticeview.bottom;
    _lightView.mj_y = self.height  -10;
    WeakSelf
    weakSelf.items = dataArray;
    weakSelf.pageCount = weakSelf.items.count;
  
    if (weakSelf.pageCount < 3) {
        weakSelf.mianCollView.hidden = YES;
    }else
    {
        weakSelf.mianCollView.hidden = NO;
    }
   
    [weakSelf.mianCollView reloadData];
   
}
- (void)uploadSymbol:(HomeModle *)modle;
{
    
    WeakSelf
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i=0; i<self.items.count; i++)
        {
            HomeModle *homeModle = self.items[i];
            if ([homeModle.name isEqualToString:modle.name])
            {
                homeModle.deal = modle.deal;
                homeModle.volume = modle.volume;
                homeModle.high = modle.high;
                homeModle.open = modle.open;
                homeModle.last = modle.last;
                homeModle.low = modle.low;
                homeModle.increase = [NSString stringWithFormat:@"%.2f",(modle.last.floatValue - modle.open.floatValue)/modle.open.floatValue];
                homeModle.IncreaseDegree =  [NSString stringWithFormat:@"%.2f%%",(modle.last.floatValue - modle.open.floatValue)/modle.open.floatValue * 100];

                // 回到主队列刷新UI
                dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.mianCollView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:i inSection:0]]];
                });
                
                break;

            }
        }
        
    });
    
}

- (void)getNoticeList:(NSArray *)noticeList;
{
    for (UIView *view in self.noticeview.subviews) {
        if (view.tag == 10089 ||view.tag == 10090) {
            [view removeFromSuperview];
        }
    }
    for (int i =0; i<noticeList.count; i++)
    {
        _noticeview.height = 12 + 58/2*i + 58/2;
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(23, 10 + 27*i, _noticeview.width - 46, 17)];
        lable.textColor = [ZBLocalized sharedInstance].newsTitleColor;
        lable.font = AutoFont(12);
        lable.tag = i +10089;
        lable.text = noticeList[i] ;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouchUpInside:)];
        lable.userInteractionEnabled = YES;
        [lable addGestureRecognizer:labelTapGestureRecognizer];
        
        [_noticeview addSubview:lable];
        
        UIImageView * image = [UIImageView dd_imageViewWithFrame:CGRectMake(_noticeview.width - 14, 0, 4, 7) islayer:NO imageStr:@"home_return"];
        image.centerY = lable.centerY;
        [_noticeview addSubview:image];
        
    }

    
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(0 , _noticeview.height -1)];
    [bezierPath addLineToPoint:CGPointMake(UIScreenWidth , _noticeview.height -1)];
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = CELLCOLOR.CGColor;
    shapeLayer.fillColor  = [UIColor clearColor].CGColor;
    shapeLayer.path = bezierPath.CGPath;
    shapeLayer.lineWidth = 0.5f;
    [_noticeview.layer addSublayer:shapeLayer];
 
    if (noticeList.count ==0) {
        _noticeview.height = 0;
    }
}

-(void) labelTouchUpInside:(UITapGestureRecognizer *)recognizer{
    
    UILabel *label=(UILabel*)recognizer.view;
    
    WeakSelf
    if (self.Advertblock) {
        weakSelf.Advertblock(label.tag - 10089);
    }
    
}
#pragma  mark -----懒加载
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 390)];
        _bgView.backgroundColor = [ZBLocalized sharedInstance].HomeBgColor;
    }
    return _bgView;
}
- (HW3DBannerView*)scrollView{
    
    if (!_scrollView) {
        _scrollView =  [HW3DBannerView initWithFrame:CGRectMake(14, 19, UIScreenWidth - 28,  (UIScreenWidth - 28)*260/540) imageSpacing:0 imageWidth:UIScreenWidth - 28];
        _scrollView.placeHolderImage = [UIImage imageNamed:@"placeHolder"]; // 设置占位图片
        _scrollView.backgroundColor = [ZBLocalized sharedInstance].HomeBgColor;
        _scrollView.curPageControlColor = [ZBLocalized sharedInstance].TabBgColor;
        _scrollView.initAlpha = 1; // 设置两边卡片的透明度
        _scrollView.imageRadius = 4; // 设置卡片圆角
        _scrollView.imageHeightPoor = 6; // 设置中间卡片与两边卡片的高度差
      
        WeakSelf
        _scrollView.clickImageBlock = ^(NSInteger currentIndex) { // 点击中间图片的回调
            
            if (weakSelf.BancelsUrlblock) {
                weakSelf.BancelsUrlblock(currentIndex);
            }
            
        };
    }
    return _scrollView;
}
- (UIImageView *)noticeimageview
{
    if (!_noticeimageview) {
        _noticeimageview = [[UIImageView alloc] initWithFrame:CGRectMake(7,_scrollView.bottom +10, 14, 25/2)];
        _noticeimageview.image = [UIImage imageNamed:@"home-notice"];
        
        UILabel * lable = [[UILabel alloc] initWithFrame:CGRectMake(_noticeimageview.right +4, 0, 200, 20)];
        lable.textColor = MAINTITLECOLOR;
        lable.font = AutoBoldFont(14);
        lable.centerY = _noticeimageview.centerY;
        lable.text = Localized(@"home_new_announcement");
        [self addSubview:lable];
        
        
    }
    return _noticeimageview;
}
- (UIButton *)newsButton
{
    if (!_newsButton) {
        _newsButton = [[UIButton alloc] initWithFrame:CGRectMake(self.width - 210, 0, 200, 30)];
         _newsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_newsButton setTitle:Localized(@"home_more") forState:0];
        [_newsButton setTitleColor:MAINTITLECOLOR1 forState:0];
        _newsButton.titleLabel.font = AutoFont(12);
        _newsButton.centerY = _noticeimageview.centerY;
    }
    return _newsButton;
}
- (UIView *)noticeview
{
    if (!_noticeview) {
        _noticeview = [[UIView alloc] initWithFrame:CGRectMake(0, _noticeimageview.bottom + 10, UIScreenWidth , RealValue_W(0))];
        _noticeview.backgroundColor = TABLEVIEWLCOLOR;
  
      
    }
    return _noticeview;
}

- (UICollectionView *)mianCollView
{
    if (!_mianCollView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(UIScreenWidth/2.8, 90);
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
//        layout.minimumLineSpacing = 10;
//        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        _mianCollView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,
                                                                          _noticeview.bottom,
                                                                          UIScreenWidth ,
                                                                          110) collectionViewLayout:layout];
        _mianCollView.backgroundColor = MAINCOLOR;
        _mianCollView.delegate =self;
        _mianCollView.dataSource= self;     
        [_mianCollView registerClass:[BTCCollectionViewCell class] forCellWithReuseIdentifier:@"BTCCollectionViewCell"];
        _mianCollView.showsVerticalScrollIndicator = NO;
        _mianCollView.showsHorizontalScrollIndicator = NO;
        
    }
    return _mianCollView;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  _items.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.item >= _items.count) {
        NSLog(@"点击了====空白");
    } else {
       
        HomeModle *modle = _items[indexPath.row];
        Y_StockChartViewController * Y_StockChartVC = [[Y_StockChartViewController alloc] init];
        Y_StockChartVC.modle = modle;
         Y_StockChartVC.isleftPush = NO;
        [[EXUnit currentViewController].navigationController pushViewController:Y_StockChartVC animated:YES];
    }
    
    
    
}
//定义每个Section的四边间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 10, 5, 10);//分别为上、左、下、右
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTCCollectionViewCell *item = [self.mianCollView dequeueReusableCellWithReuseIdentifier:@"BTCCollectionViewCell" forIndexPath:indexPath];
    item.modle = _items[indexPath.row];
    return item;
    
}
//- (void)scrollViewDidScroll:(UIScrollView*)scrollView
//{
//    if (_mianCollView ==scrollView)
//    {
//        _contentOffX = scrollView.contentOffset.x;
//        //获得页码
//        CGFloat doublePage = scrollView.contentOffset.x/UIScreenWidth;
//        int intPage = (int)(doublePage+0.8);
//        _currentPage = intPage;
//        //设置页码
//        self.pageControl.currentPage= intPage;
//    }
//
//}
@end
