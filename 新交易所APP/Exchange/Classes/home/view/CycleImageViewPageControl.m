//
//  CycleImageViewPageControl.m
//  CycleIMG
//
//  Created by Rainy on 2017/12/28.
//  Copyright © 2017年 Rainy. All rights reserved.
//

#define dotW     16
#define dotH     3
#define magrin   4

#import "CycleImageViewPageControl.h"

@implementation CycleImageViewPageControl

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat marginX = dotW + magrin;
    CGFloat newW = (self.subviews.count - 1) * marginX;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, newW, self.frame.size.height);
    
    CGPoint center = self.center;
    center.x = self.superview.center.x;
    self.center = center;
    
    for (int i=0; i<[self.subviews count]; i++) {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        
        [dot setFrame:CGRectMake(i * marginX, dot.frame.origin.y, dotW, dotH)];
        
        dot.layer.masksToBounds = YES;
        dot.layer.cornerRadius = dotH/2;
    }
}

@end

@implementation BannerImageViewPageControl

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat marginX = dotW+2 ;
    CGFloat newW = (self.subviews.count - 1) * marginX ;
    self.frame = CGRectMake(UIScreenWidth -RealValue_W(500), self.frame.origin.y, newW, self.frame.size.height);
    
    CGPoint center = self.center;
    center.x = self.superview.center.x;
    self.center = center;
    
    for (int i=0; i<[self.subviews count]; i++) {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        
        [dot setFrame:CGRectMake(UIScreenWidth -RealValue_W(500) + i * marginX, dot.frame.origin.y, 14, 2)];
        
        dot.layer.masksToBounds = YES;
        dot.layer.cornerRadius = dotH/2;
    }
}

@end
