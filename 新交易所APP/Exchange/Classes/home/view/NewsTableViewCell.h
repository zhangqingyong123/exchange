//
//  NewsTableViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/23.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsClassModle.h"
@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *timelable;
@property (nonatomic,strong) NewsClassModle * modle;
@end
@interface NewsInfoCell : UITableViewCell
@property (nonatomic,strong)UILabel * Newstcontent;//名称
@end
