//
//  NewsTableViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/23.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "NewsTableViewCell.h"

@implementation NewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _titleLable.textColor = MAINTITLECOLOR;
    _titleLable.font = AutoBoldFont(13);
    _timelable.textColor = [ZBLocalized sharedInstance].newsTitleColor;
}
- (void)setModle:(NewsClassModle *)modle
{
    _modle = modle;
    _titleLable.text = modle.title;
    _timelable.text = [EXUnit getTimeFromTimestamp:modle.dateline];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
@implementation NewsInfoCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
    }
    return self;
}
-(void)get_up{
    [self.contentView addSubview:self.Newstcontent];
}
- (UILabel *)Newstcontent
{
    if (!_Newstcontent) {
        _Newstcontent = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_H(48), UIScreenWidth - RealValue_W(60), 0)];
       
        _Newstcontent.font = AutoFont(13);
        _Newstcontent.numberOfLines = 0;
        [_Newstcontent sizeToFit];
    }
    return _Newstcontent;
}
@end
