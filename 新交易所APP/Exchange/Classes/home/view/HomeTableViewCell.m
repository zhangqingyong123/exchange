//
//  HomeTableViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}

- (void)get_up
{
    [self.contentView addSubview:self.BTCLable];
    [self.contentView addSubview:self.consumptionLable];
    _rangeLable = [self private_createLabel];
    _rangeLable.textColor =  RGBA(255, 255, 255, 1);
    _rangeLable.font = SystemBlodFont(14);
    _rangeLable.textAlignment = NSTextAlignmentCenter;
    _rangeLable.layer.masksToBounds     =YES;
    _rangeLable.layer.cornerRadius      =2;
    [_rangeLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(@30);
        make.width.equalTo(@80);
    }];
   
    
    
    _incrementLable = [self private_createLabel];
    _incrementLable.font = SystemBlodFont(14);
    [_incrementLable mas_makeConstraints:^(MASConstraintMaker *make) {

        make.left.equalTo(self.rangeLable.mas_left).offset(-43/2 - 150);
        make.centerY.equalTo(self.BTCLable.mas_centerY);
        make.height.equalTo(@13);
        make.width.equalTo(@150);
    }];

    _priceLable = [self private_createLabel];
    _priceLable.font = [UIFont systemFontOfSize:12];
    [_priceLable mas_makeConstraints:^(MASConstraintMaker *make) {

        make.left.equalTo(self.incrementLable.mas_left);
        make.centerY.equalTo(self.consumptionLable.mas_centerY);
        make.height.equalTo(@11);
        make.width.equalTo(@150);
    }];
   
}
- (void)setModle:(HomeModle *)modle
{
    WeakSelf
    
    NSArray *array = [modle.name componentsSeparatedByString:@"_"];
    int baseprecision = modle.base_asset_precision.intValue;
    int precision = modle.quote_asset_precision.intValue;
    weakSelf.BTCLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ / %@",array[0],array[1]]
                                                     attributedString:[NSString stringWithFormat:@"/ %@",array[1]]
                                                                color:MAINTITLECOLOR1
                                                                 font:SystemBlodFont(14)];
    
    weakSelf.consumptionLable.text = [NSString stringWithFormat:@"24h %@  %@",Localized(@"hone_bright"), [EXUnit formatternumber:baseprecision assess:modle.volume]];
    float _x = modle.IncreaseDegree.floatValue;
    if (!isnan(_x)) {
        if (modle.last.floatValue - modle.open.floatValue >=0) {
            weakSelf.rangeLable.text = [NSString stringWithFormat:@"+ %.2f%%",modle.IncreaseDegree.floatValue];
        }else
        {
            weakSelf.rangeLable.text = [NSString stringWithFormat:@"%.2f%%",modle.IncreaseDegree.floatValue];
        }
        
    }else
    {
        weakSelf.rangeLable.text = @"0.00%";
    }
    
    if (modle.last.floatValue - modle.open.floatValue >=0) {
        
        weakSelf.incrementLable.textColor =ZHANGCOLOR;
        weakSelf.rangeLable.backgroundColor = ZHANGCOLOR;
    }else
    {
        weakSelf.incrementLable.textColor =DIEECOLOR;
        weakSelf.rangeLable.backgroundColor = DIEECOLOR;
    }
    weakSelf.incrementLable.text = [NSString stringWithFormat:@"%@",[EXUnit formatternumber:precision assess:modle.last]];
    
 
    weakSelf.priceLable.text = [EXUnit getPriceCurrencyWith:modle];
  
    

   
}
- (UILabel *)BTCLable
{
    if (!_BTCLable) {
        _BTCLable = [[UILabel alloc] initWithFrame:CGRectMake(10, 18,150, 13)];
        _BTCLable.textColor =  MAINTITLECOLOR;
        _BTCLable.font = SystemBlodFont(15);
        
       
    }
    return _BTCLable;
}
- (UILabel *)consumptionLable
{
    if (!_consumptionLable) {
        _consumptionLable = [[UILabel alloc] initWithFrame:CGRectMake(10,_BTCLable.bottom+ 10,150, 13)];
        _consumptionLable.textColor =  MAINTITLECOLOR1;
        _consumptionLable.font = [UIFont systemFontOfSize:12];
       
       
    }
    return _consumptionLable;
}
- (UILabel *)rangeLable
{
    if (!_rangeLable) {
        _rangeLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 10 -80,
                                                              75/2- 15,
                                                              80,
                                                              30)];
        _rangeLable.textColor =  RGBA(255, 255, 255, 1);
        _rangeLable.font = SystemBlodFont(14);
       
        _rangeLable.textAlignment = NSTextAlignmentCenter;
        _rangeLable.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_rangeLable cornerRadius:3];
    }
    return _rangeLable;
}

- (UILabel *)private_createLabel
{
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:10];
    label.textColor = MAINTITLECOLOR1;
    label.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:label];
    return label;
}
- (UILabel *)incrementLable
{
    if (!_incrementLable) {
        _incrementLable = [[UILabel alloc] initWithFrame:CGRectMake(_rangeLable.left - 43/2 -150
                                                                    ,0,
                                                                    150,
                                                                    13)];
        _incrementLable.textColor =  MAINTITLECOLOR1;
        _incrementLable.font = SystemBlodFont(14);
      
        _incrementLable.centerY = _BTCLable.centerY;
        _incrementLable.textAlignment = NSTextAlignmentRight;
        
    }
    return _incrementLable;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(_rangeLable.left - 43/2 -150
                                                                    ,0,
                                                                    150,
                                                                    11)];
        _priceLable.textColor =  MAINTITLECOLOR1;
        _priceLable.font = [UIFont systemFontOfSize:12];
       
        _priceLable.centerY = _consumptionLable.centerY;
        _priceLable.textAlignment = NSTextAlignmentRight;
        
    }
    return _priceLable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
