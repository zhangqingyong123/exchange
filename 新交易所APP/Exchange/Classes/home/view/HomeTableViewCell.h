//
//  HomeTableViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModle.h"
//所有增幅度cell
@interface HomeTableViewCell : UITableViewCell
/*类型*/
@property (nonatomic,strong)UILabel *BTCLable;
/*用量*/
@property (nonatomic,strong)UILabel *consumptionLable;
/*增量*/
@property (nonatomic,strong)UILabel *incrementLable;
/*价格*/
@property (nonatomic,strong)UILabel *priceLable;
/*幅度*/
@property (nonatomic,strong)UILabel *rangeLable;
@property (nonatomic,strong)HomeModle * modle;
@end
