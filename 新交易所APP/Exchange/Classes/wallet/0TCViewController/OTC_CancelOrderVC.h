//
//  OTC_CancelOrderVC.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTC_CancelOrderVC : BaseViewViewController
@property(nonatomic,strong)NSString * order_no;
@end

NS_ASSUME_NONNULL_END
