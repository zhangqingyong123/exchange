//
//  OTCModel.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OTCModel : NSObject
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * expire_time;
@property (nonatomic,strong)NSString * bank_name;
@property (nonatomic,strong)NSString * finished_count;
@property (nonatomic,strong)NSString * limit_money_higher;
@property (nonatomic,strong)NSString * limit_money_lower;
@property (nonatomic,strong)NSString * merchant_account_id;
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * pay_method;
@property (nonatomic,strong)NSString * price;
@property (nonatomic,strong)NSString * total_amount;
@end

@interface OTCOrderModel : NSObject
@property (nonatomic,strong)NSDictionary * merchant;
@property (nonatomic,strong)NSDictionary * order;
@property (nonatomic,strong)NSArray * otc_user_account;
@property (nonatomic,strong)NSArray * user_bank;
@end

@interface OTCOrderListModel : NSObject
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * create_time;
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * order_no;
@property (nonatomic,strong)NSString * pay_method;
@property (nonatomic,strong)NSString * price;
@property (nonatomic,strong)NSString * status; //1：未成交，2：已支付，3：商家已确认，4：已取消，5：处理中，6：已过期，7：已完成
@property (nonatomic,strong)NSString * type;//1买入 2卖出
@end
NS_ASSUME_NONNULL_END
