//
//  OTC_WaitingpaymentVC.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTC_WaitingpaymentVC.h"
#import "OTCTimeLable.h"
#import "PaypalView.h"
#import "CountryListViewController.h"
#import "PaypalCustomAlert.h"
#import "PaypalModel.h"
@interface OTC_WaitingpaymentVC ()
@property (nonatomic,strong)NSMutableArray * payArray;
@property (nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,strong)UIButton * returnbutton;
@property (nonatomic,strong)UILabel * stateLab;
@property (nonatomic,strong)OTCTimeLable * orderTimeLab;
@property (nonatomic,strong)UIImageView * orderImg1;
@property (nonatomic,strong)UILabel * priceTitle;
@property (nonatomic,strong)UILabel * totalPriceLab;
@property (nonatomic,strong)UILabel * priceLable;
@property (nonatomic,strong)UILabel * numberLable;
@property (nonatomic,strong)UIButton * choosebutton;
@property (nonatomic,strong)UIImageView * orderImg2;
@property (nonatomic,strong)PaypalView * paypalView;


@property (nonatomic,strong)UIImageView * payPalIcon;
@property (nonatomic,strong)UILabel * payPalNameLab;
@property (nonatomic,strong)UILabel * payPalMessageLab;
@property (nonatomic,strong)UIImageView * choosePayPalIcon;

@property (nonatomic,strong)UILabel * paypalMessageTitle;
@property (nonatomic,strong)UIButton * cancelButton;
@property (nonatomic,strong)LcButton * OKButton;
@property (nonatomic,strong)LcButton * cell_OKButton;
@property (nonatomic,strong)NSString  * name;//收款人
@property (nonatomic,strong)NSString  * user_id;//备注信息
@property (nonatomic,strong)NSString  * expire_time;//倒计时
@property (nonatomic,strong)PModel * pModel;
@property (nonatomic,strong)UIButton * usbutton;
@property (nonatomic,strong)NSString * phone;
@property (nonatomic,assign)NSInteger  index;
@end

@implementation OTC_WaitingpaymentVC
- (void)viewWillAppear:(BOOL)animated
{
    [self statusBarStyleIsDark:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO]; //设置隐藏
    [super viewWillDisappear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
     [self statusBarStyleIsDark:[ZBLocalized sharedInstance].StatusBarStyle];
    [self.navigationController setNavigationBarHidden:NO animated:NO]; //设置隐藏
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBA(45, 45, 62, 1);
    [self initOtherUI];
    
    [self getData];
    [self showAnimation];
    if (_type.integerValue ==2)
    {
        _OKButton.hidden = YES;
        _cancelButton.hidden = YES;
        _choosebutton.hidden = YES;
        _choosePayPalIcon.hidden = YES;
        _payPalMessageLab.hidden = YES;
        _orderImg2.height = RealValue_H(720);
        _paypalMessageTitle.hidden = YES;
        _cell_OKButton.mj_y =  _orderImg2.height- RealValue_W(76 +40);
        _orderTimeLab.text = Localized(@"OTC_cell_pay_order_message");
    }else
    {
        _cell_OKButton.hidden = YES;
    }
}
- (void)showAnimation
{
    _orderImg1.hidden = YES;
    _orderImg2.hidden = YES;
    [self showLoadingAnimation];
}
- (void)stopAnimation
{
    _orderImg1.hidden = NO;
    _orderImg2.hidden = NO;
    [self stopLoadingAnimation];
}
- (void)getData
{
    WeakSelf
    if (![NSString isEmptyString:_order_no])
    {
        
        [self GETWithHost:@"" path:otc_detail param:@{@"orderNo":_order_no} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                NSDictionary * dic = responseObject[@"result"];
                [self initButtonWithStatus:[dic[@"status"] integerValue]];
                
                NSString * expire_time = [EXUnit getLocalDateFormateUTCDate:dic[@"expire_time"]];
                weakSelf.expire_time = expire_time;
                
                if (weakSelf.type.integerValue ==1)
                {
                    [self dateTimeDifferenceWithStartTime:[EXUnit getLocalDateFormateUTCDate:dic[@"expire_time"]] endTime:[EXUnit getNowTimeymd]];
                }
                
                NSString * price = [NSString stringWithFormat:@"%@",dic[@"price"]];
                weakSelf.priceLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@%@ %@",[EXUnit getZTPriceUnit],price,Localized(@"OTC_price")] attributedString:Localized(@"OTC_price") color:MAINTITLECOLOR1 font:AutoFont(12)];
                NSString * number = [NSString stringWithFormat:@"%@ %@",dic[@"amount"],dic[@"otc_asset"]];
                weakSelf.numberLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@",number,Localized(@"hone_num")] attributedString:Localized(@"hone_num") color:MAINTITLECOLOR1 font:AutoFont(12)];
                CGFloat total = [dic[@"price"] floatValue]* [dic[@"amount"] floatValue];
                weakSelf.totalPriceLab.text = [NSString stringWithFormat:@"%@ %.2f",[EXUnit getZTPriceUnit],total];
                weakSelf.user_id = dic[@"remark"];
                weakSelf.phone = [NSString stringWithFormat:@"%@",dic[@"phone"]];
                [weakSelf.usbutton setTitle:[NSString stringWithFormat:@"%@：%@",Localized(@"Authentication_us_message"), weakSelf.phone] forState:0];
                [weakSelf getPayinfoWith:dic[@"merchant_account_id"] pay_method:dic[@"pay_method"]];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }
}
- (void)getPayinfoWith:(NSString *)merchantID pay_method:(NSString *)pay_method
{
    WeakSelf
    if ([_type isEqualToString:@"1"]) {//买入 //获取商家支付方式
        
        [self POSTWithHost:@"" path:merchantAccount param:@{@"order_no":_order_no} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                [weakSelf.dataArray addObjectsFromArray:[PModel mj_objectArrayWithKeyValuesArray:responseObject[@"result"][@"account"]]];
                weakSelf.name = responseObject[@"result"][@"name"];
                if (weakSelf.dataArray.count>0) {
                    
                    [self getpay_method:weakSelf.dataArray[0]];
                }
                for (PModel * model in weakSelf.dataArray)
                {
                    PaypalModel * M;
                     M = [[PaypalModel alloc] init];
                    if ([model.pay_method integerValue] ==0)
                    {
                        M.title = Localized(@"C2C_Bank_card");
                        M.icon = @"otc_icon_card";
                    }else if ([model.pay_method integerValue] ==1)
                    {
                        M.title = Localized(@"OTC_alipay");
                        M.icon = @"otc_icon_zhi";
                    }else if ([model.pay_method integerValue] ==2)
                    {
                        M.title = Localized(@"OTC_weChat");
                        M.icon = @"otc_icon_vx";
                    }
                    
                    [self.payArray addObject:M];
                }
                
            }
            [weakSelf stopAnimation];
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        
    }else //卖出
    {
        [HomeViewModel getMobilePayDataListWithDic:@{} url:mobilePay callBack:^(BOOL isSuccess, id callBack) {
            if (isSuccess) {
                if([pay_method hasPrefix:@"0"])
                {
                     NSArray * list = callBack[@"result"][@"banks"];
                     NSDictionary * dic = list[0];
                     weakSelf.name = dic[@"name"];
                     PModel * model = [[PModel alloc] init];
                     model.pay_method = @"0";
                     model.pay_method_name = dic[@"bank"];
                     model.pay_method_sub_name = dic[@"sub_bank"];
                     model.pay_method_no = dic[@"card_number"];
                    [self getpay_method:model];
                }else
                {
                    NSArray * logs = callBack[@"result"][@"logs"];
                    
                    for (NSDictionary *dic in logs)
                    {
                        NSString *status = [NSString stringWithFormat:@"%@",dic[@"status"]];
                        if ([pay_method isEqualToString:status])
                        {
                            weakSelf.name = dic[@"name"];
                            PModel * model = [[PModel alloc] init];
                            model.pay_method = status;
                            model.pay_method_name = dic[@"account"];
                            model.pay_method_sub_name = dic[@"name"];
                            model.pay_method_no = dic[@"image"];
                            [self getpay_method:model];
                        }
                    }
                }
               
                
               
                
            }
            [weakSelf stopAnimation];
        }];
        
    }
}
- (void)initButtonWithStatus:(NSInteger)status
{
    switch (status) {
        case 1:
            _stateLab.text = Localized(@"C2C_To_paid");
            
            break;
        case 2:
            _stateLab.text = Localized(@"C2C_Already_paid");
            break;
        case 3:
            _stateLab.text = Localized(@"OTC_Confirmed");
            break;
        case 4:
        
            _stateLab.text = Localized(@"C2C_cancelled");
            
            break;
        case 5:
      
            _stateLab.text = Localized(@"Asset_in_hand");
            break;
        case 6:
             _stateLab.text = Localized(@"OTC_Expired");
            _OKButton.hidden = YES;
            _cancelButton.hidden = YES;
            _choosebutton.hidden = YES;
            _choosePayPalIcon.hidden = YES;
            _payPalMessageLab.hidden = YES;
            _orderImg2.height = RealValue_H(620);
            _paypalMessageTitle.hidden = YES;
            
            break;
        case 7:
            _stateLab.text = Localized(@"C2C_Completed");
            break;
            
        default:
            break;
    }
  
    
}
- (void )dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = start - end;
    int second = (int)value %60;//秒
    int minute = (int)value /60%60;
    int house = (int)value / (24 *3600)%3600;
    int day = (int)value / (24 *3600);
    NSString *str;
    if (day > 0) {
        str = [NSString stringWithFormat:@"%d:%d:%d:%d",day,house,minute,second];
    }else if (day==0 && house >0) {
        str = [NSString stringWithFormat:@"%d:%d:%d",house,minute,second];
    }else if (day==0 && house==0 && minute>0) {
        
        str = [NSString stringWithFormat:@"00:%d:%d",minute,second];
        
    }else if (day==0 && house==0 && minute ==0 &&second >0){
        
        str = [NSString stringWithFormat:@"00:00:%d",second];
        
    }else if (day==0 && house==0 && minute ==0 &&second ==0)
    {

        return;
    }
    if (day>0)
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _orderTimeLab.day = [array[0] integerValue];
        _orderTimeLab.hour = [array[1] integerValue];
        _orderTimeLab.minute = [array[2] integerValue];
        _orderTimeLab.second = [array[3] integerValue];
         _orderTimeLab.expire_time = _expire_time;
    }else
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _orderTimeLab.hour = [array[0] integerValue];
        _orderTimeLab.minute = [array[1] integerValue];
        _orderTimeLab.second = [array[2] integerValue];
        _orderTimeLab.expire_time = _expire_time;
    }
    
}
- (void)getpay_method:(PModel *)model
{
    _pModel = model;
    if ([model.pay_method integerValue] ==0) //银行卡
    {
        self.payPalIcon.image = [UIImage imageNamed:@"otc_icon_card"];
        self.payPalNameLab.text = Localized(@"C2C_Bank_card");
        self.paypalView = [[PaypalView alloc] initWithPaypalWithIndex:0];
        [self.orderImg2 addSubview:self.paypalView];
        [self.paypalView getPaypalWithDic:@{@"pay_method_name":model.pay_method_name,@"pay_method_sub_name":model.pay_method_sub_name,@"name":self.name,@"user_id":_user_id,@"pay_method_no":model.pay_method_no}];
        
    }else if ([model.pay_method integerValue] ==1)//支付宝
    {
        self.payPalIcon.image = [UIImage imageNamed:@"otc_icon_zhi"];
        self.payPalNameLab.text = Localized(@"OTC_alipay");
        self.paypalView = [[PaypalView alloc] initWithPaypalWithIndex:1];
         [self.orderImg2 addSubview:self.paypalView];
        [self.paypalView getPaypalWithmodle:model name:_name];
       
        
    }else if ([model.pay_method integerValue] ==2)//微信
    {
        self.payPalIcon.image = [UIImage imageNamed:@"otc_icon_vx"];
        self.payPalNameLab.text = Localized(@"OTC_weChat");
        self.paypalView = [[PaypalView alloc] initWithPaypalWithIndex:2];
        [self.orderImg2 addSubview:self.paypalView];
         [self.paypalView getPaypalWithmodle:model name:_name];
        

    }
    _paypalMessageTitle.mj_y = _paypalView.bottom ;
    
   
}
#pragma make  选择支付方式
- (void)choosePaypalWithButton:(UIButton *)button
{
    WeakSelf
    CountryListViewController * custom = [[CountryListViewController alloc] initWithCustomContentViewClass:@"PaypalCustomAlert" delegate:nil];
    custom.direction = FromBottom;
    [custom showWith:self.payArray wihtISSell:NO];
    PaypalCustomAlert *aleatView = (PaypalCustomAlert *)custom.contentView;
    aleatView.index = _index;
    aleatView.cancelBlock = ^{
        [custom dismiss];
    };
    aleatView.selectIndexPathRow = ^(NSInteger index,NSString *capital) {
        weakSelf.index = index;
        PModel * model = weakSelf.dataArray[index];
        [weakSelf.paypalView removeFromSuperview];
        [self getpay_method:model];
       
        [custom dismiss];
    };
    
}
#pragma mark----买入操作
// 确认
- (void)buttonClick:(LcButton *)button
{
    if (_type.integerValue ==1) //买入
    {
        [self OrderProcessingWithAction:@"confirm"];
        [button stopAnimation];
    }
}
//取消
- (void)cancelorder:(UIButton *)button
{
    if (_type.integerValue ==1) //买入
    {
        [self OrderProcessingWithAction:@"cancel"];
    }
}
#pragma mark----卖出操作
//卖出取消订单
- (void)cancelClick:(LcButton *)button
{
    NSDictionary * dic = @{@"order_no":_order_no,@"action":@"cancel",@"pay_method":@"",@"pay_method_id":@""};
    [self POSTWithHost:@"" path:otc_update param:dic cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:OTCupdateNotificationse object:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
         [button stopAnimation];
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [button stopAnimation];
    }];
}
/*执行动作，cancel表示取消订单，如果是买单confirm表示确认支付，如果是卖单表示确认选择了*/
- (void)OrderProcessingWithAction:(NSString *)action
{
    NSDictionary * dic = @{@"order_no":_order_no,@"action":action,@"pay_method":_pModel.pay_method,@"pay_method_id":_pModel.id};
    [self POSTWithHost:@"" path:otc_update param:dic cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
             [[NSNotificationCenter defaultCenter] postNotificationName:OTCupdateNotificationse object:nil];
             [self.navigationController popViewControllerAnimated:YES];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
/*设置其他的UI*/
- (void)initOtherUI
{
    [self.view addSubview:self.returnbutton];
    [self.view addSubview:self.stateLab];
    [self.view addSubview:self.orderTimeLab];
    [self.view addSubview:self.orderImg1];
    [_orderImg1 addSubview:self.priceTitle];
    [_orderImg1 addSubview:self.totalPriceLab];
    [_orderImg1 addSubview:self.priceLable];
    [_orderImg1 addSubview:self.numberLable];
    [self.view addSubview:self.orderImg2];
    [_orderImg2 addSubview:self.payPalIcon];
    [_orderImg2 addSubview:self.payPalNameLab];
    [_orderImg2 addSubview:self.choosePayPalIcon];
    [_orderImg2 addSubview:self.payPalMessageLab];
    [_orderImg2 addSubview:self.choosebutton];
    [_orderImg2 addSubview:self.paypalMessageTitle];
    [_orderImg2 addSubview:self.cancelButton];
    [_orderImg2 addSubview:self.OKButton];
    [_orderImg2 addSubview:self.cell_OKButton];
    [self.view addSubview:self.usbutton];
}
#pragma mark - 懒加载
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}
- (NSMutableArray *)payArray
{
    if (!_payArray) {
        _payArray = [NSMutableArray new];
    }
    return _payArray;
}
- (UIButton *)returnbutton
{
    WeakSelf
    if (!_returnbutton) {
        _returnbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 50, 50)];
        [_returnbutton setImage:[UIImage imageNamed:@"title-back"] forState:0];
        [_returnbutton add_BtnClickHandler:^(NSInteger tag) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
        
        
    }
    return _returnbutton;
}
- (UILabel *)stateLab
{
    if (!_stateLab) {
        _stateLab = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), _returnbutton.bottom +5, UIScreenWidth - RealValue_W(120), 25)];
        _stateLab.textColor = WHITECOLOR;
        _stateLab.font = AutoBoldFont(25);
    }
    return _stateLab;
}
- (OTCTimeLable *)orderTimeLab
{
    if (!_orderTimeLab) {
        _orderTimeLab = [[OTCTimeLable alloc] initWithFrame:CGRectMake(RealValue_W(60), _stateLab.bottom +5, UIScreenWidth - RealValue_W(120), 25)];
        _orderTimeLab.textColor = RGBA(255, 255, 255, 0.4);
        _orderTimeLab.font = AutoFont(12);
        
    }
    return _orderTimeLab;
}
- (UIImageView *)orderImg1
{
    if (!_orderImg1) {
        _orderImg1 = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(30), _orderTimeLab.bottom + 10, UIScreenWidth - RealValue_W(60), RealValue_W(176))];
        _orderImg1.image = [UIImage imageNamed:@"4otc_dd_bg01"];
    }
    return _orderImg1;
}
- (UIImageView *)orderImg2
{
    if (!_orderImg2) {
        _orderImg2 = [[UIImageView alloc] initWithFrame:CGRectMake(_orderImg1.mj_x, _orderImg1.bottom , UIScreenWidth - RealValue_W(60), RealValue_W(780))];
        _orderImg2.image = [UIImage imageNamed:@"4otc_dd_bg02"];
        _orderImg2.userInteractionEnabled = YES;
       
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(RealValue_W(28) , RealValue_W(98))];
        [bezierPath addLineToPoint:CGPointMake(_orderImg2.width - RealValue_W(28) , RealValue_W(98))];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = ColorStr(@"#F0F0F0").CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.7f;
        [_orderImg2.layer addSublayer:shapeLayer];

    }
    return _orderImg2;
}
- (UILabel *)priceTitle
{
    if (!_priceTitle) {
        _priceTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(25), RealValue_W(38),  160, 18)];
        if (_type.integerValue ==1)
        {
            _priceTitle.text = Localized(@"OTC_pay_order_message");
        }else
        {
            _priceTitle.text = Localized(@"C2C_Total");
        }
        
        _priceTitle.textColor = RGBA(170, 170, 170, 1);
        _priceTitle.font = AutoFont(12);
    }
    return _priceTitle;
}
- (UILabel *)totalPriceLab
{
    if (!_totalPriceLab) {
        _totalPriceLab = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(25),_priceTitle.bottom + RealValue_W(20),  200, 20)];
     
        _totalPriceLab.textColor = BLACKCOLOR;
        _totalPriceLab.font = AutoBoldFont(25);
    }
    return _totalPriceLab;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(_orderImg1.width - RealValue_W(30) - 200, 0, 200, RealValue_W(32))];
        _priceLable.textColor = BLACKCOLOR;
        _priceLable.textAlignment = NSTextAlignmentRight;
        _priceLable.font = AutoBoldFont(12);
       
        _priceLable.centerY = _priceTitle.centerY;
    }
    return _priceLable;
}
- (UILabel *)numberLable
{
    if (!_numberLable) {
        _numberLable = [[UILabel alloc] initWithFrame:CGRectMake(_priceLable.mj_x, 0, 200, RealValue_W(32))];
        _numberLable.textColor = BLACKCOLOR;
        _numberLable.textAlignment = NSTextAlignmentRight;
        _numberLable.font = AutoBoldFont(12);
        _numberLable.centerY = _totalPriceLab.centerY;
      
        
    }
    return _numberLable;
}
- (UIImageView *)payPalIcon
{
    if (!_payPalIcon) {
        _payPalIcon = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(28), RealValue_W(98 -28)/2  ,  RealValue_W(28), RealValue_W(28))];
       
    }
    return _payPalIcon;
}
- (UILabel *)payPalNameLab
{
    if (!_payPalNameLab) {
        _payPalNameLab = [[UILabel alloc] initWithFrame:CGRectMake(_payPalIcon.right +4, 0, 100, RealValue_W(32))];
        _payPalNameLab.textColor = BLACKCOLOR;
        _payPalNameLab.font = AutoBoldFont(15);
       
        _payPalNameLab.centerY = _payPalIcon.centerY;
        
    }
    return _payPalNameLab;
}
- (UIImageView *)choosePayPalIcon
{
    if (!_choosePayPalIcon) {
        _choosePayPalIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_orderImg2.width -  RealValue_W(28) - RealValue_W(10), 0  ,  RealValue_W(10), RealValue_W(16))];
        _choosePayPalIcon.image = [UIImage imageNamed:@"icon_list_go"];
        _choosePayPalIcon.centerY = _payPalIcon.centerY;
    }
    return _choosePayPalIcon;
}
- (UILabel *)payPalMessageLab
{
    if (!_payPalMessageLab) {
        _payPalMessageLab = [[UILabel alloc] initWithFrame:CGRectMake(_choosePayPalIcon.left - 130, 0, 120, RealValue_W(32))];
        _payPalMessageLab.textColor = ColorStr(@"#AAAAAA");
        _payPalMessageLab.font = AutoFont(12);
        _payPalMessageLab.textAlignment = NSTextAlignmentRight;
        _payPalMessageLab.centerY = _payPalIcon.centerY;
        _payPalMessageLab.text = Localized(@"OTC_Payment_Method");
        
    }
    return _payPalMessageLab;
}
- (UIButton *)choosebutton
{
    WeakSelf
    if (!_choosebutton) {
        _choosebutton = [UIButton dd_buttonSystemButtonWithFrame:CGRectMake(0, 0, _orderImg2.width, RealValue_W(98)) NormalBackgroundImageString:@"" tapAction:^(UIButton *button) {
           
            [weakSelf choosePaypalWithButton:button];
        }];
    }
    return _choosebutton;
}
- (UILabel *)paypalMessageTitle
{
    if (!_paypalMessageTitle) {
        _paypalMessageTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), _paypalView.bottom , _orderImg2.width - RealValue_W(60), RealValue_W(80))];
        _paypalMessageTitle.textColor = [UIColor redColor];
        _paypalMessageTitle.font = AutoFont(12);
        _paypalMessageTitle.numberOfLines = 0;
        _paypalMessageTitle.text = Localized(@"OTC_order_msg");
        
    }
    return _paypalMessageTitle;
}
- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(RealValue_W(30), _orderImg2.height- RealValue_W(76 +40), RealValue_W(200), RealValue_W(76)) title:Localized(@"transaction_cancel") backgroundColor:RGBA(0, 191, 166, 0.08) titleColor:TABTITLECOLOR tapAction:^(UIButton *button) {
            [self cancelorder:button];
        }];
        _cancelButton.titleLabel.font = AutoFont(14);
        _cancelButton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_cancelButton cornerRadius:2];
    }
    return _cancelButton;
}
- (LcButton *)OKButton
{
    if (!_OKButton) {
        _OKButton = [[LcButton alloc] initWithFrame:CGRectMake(_cancelButton.right + RealValue_W(20), _cancelButton.mj_y, RealValue_W(420), RealValue_W(76))];
        _OKButton.backgroundColor = TABTITLECOLOR;
        [_OKButton setTitle:Localized(@"OTC_Marked_paid") forState:0];
        [_OKButton setTitleColor:WHITECOLOR forState:0];
        _OKButton.titleLabel.font = AutoFont(14);
        [_OKButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        _OKButton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_OKButton cornerRadius:2];
    }
    return _OKButton;
}
/*联系我们*/
- (UIButton *)usbutton
{
     WeakSelf
    if (!_usbutton)
       
        _usbutton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(0, UIScreenHeight - 40, 280, 30) title:@"" backgroundColor:CLEARCOLOR titleColor:WHITECOLOR tapAction:^(UIButton *button) {
            
            NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",weakSelf.phone];
            UIWebView * callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
            [self.view addSubview:callWebview];
            
            
        }];
    _usbutton.centerX = self.view.centerX;
    _usbutton.titleLabel.font = AutoFont(14);
    return _usbutton;
}
- (LcButton *)cell_OKButton
{
    if (!_cell_OKButton) {
        _cell_OKButton = [[LcButton alloc] initWithFrame:CGRectMake(RealValue_W(30), _orderImg2.height- RealValue_W(76 +40),_orderImg2.width -  RealValue_W(60), RealValue_W(76))];
        _cell_OKButton.backgroundColor = TABTITLECOLOR;
        [_cell_OKButton setTitle:Localized(@"C2C_Cancellation_order") forState:0];
        [_cell_OKButton setTitleColor:WHITECOLOR forState:0];
        _cell_OKButton.titleLabel.font = AutoFont(14);
        [_cell_OKButton addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
        _cell_OKButton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_cell_OKButton cornerRadius:2];
    }
    return _cell_OKButton;
}
@end
