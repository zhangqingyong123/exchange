//
//  OTC_BiPurchaseVC.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTC_BiPurchaseVC.h"
#import "OTCTableViewCell.h"
#import "tableViewFooterView.h"
#import "DropDownAlertVC.h"
#import "OTCDropView.h"
#import "OTCModel.h"
#import "LoginViewController.h"
#import "CheckVersionAlearView.h"
#import "PaymentSetVC.h"
#import "OTC_WaitingpaymentVC.h"
#import "IdentityAuthenticationViewController.h"
#import "CapitalPasswordViewController.h"
@interface OTC_BiPurchaseVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)tableViewFooterView *footerView;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (assign, nonatomic) NSUInteger         pages;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,assign)NSInteger  index;
@end

@implementation OTC_BiPurchaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    _index =10086;
    // 上拉加载
    WeakSelf
    WLRefreshGifHeader *header = [WLRefreshGifHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        [weakSelf getData];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getData];
    }];
    self.tableView.mj_footer = fooder;
}
#pragma mark----数据
- (void)refrech
{
    _pages = 0;
    [self getData];
}
- (void)getData
{
    WeakSelf
    [self GETWithHost:@"" path:getMerchant param:@{@"type":@"1",@"payMethod":@"",@"offset":@(self.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         [self.tableView.mj_header endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSArray *list = responseObject[@"result"][@"logs"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataArray addObjectsFromArray:[OTCModel mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataArray = [OTCModel mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
            [weakSelf.tableView reloadData];
            
        }else
        {
            [EXUnit showMessage:responseObject[@"message"]];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [self.tableView.mj_header endRefreshing];
    }];
    
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            WeakSelf
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        _tableView.tableFooterView = [[UIView alloc] init];
        [self.tableView addSubview:_workView];
    }else
    {
        _tableView.tableFooterView = self.footerView;
        [_workView dissmiss];
    }
}
#pragma mark----n懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -49 -64 -IPhoneTop -RealValue_W(88))];
       
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[OTCTableViewCell class] forCellReuseIdentifier:@"OTCTableViewCell"];
         _tableView.tableFooterView = self.footerView;
        
    }
    return _tableView;
}
- (tableViewFooterView *)footerView
{
    if (!_footerView) {
        _footerView = [[tableViewFooterView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 0)];
        _footerView.height = [_footerView getViewHeight];
    }
    return _footerView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==_index) {
         return RealValue_H(416);
    }
    return RealValue_H(210);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_H(20);
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(20))];
    view.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    
    return view;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OTCTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"OTCTableViewCell"];
    
    if (indexPath.row ==_index) {
        cell.tradeBgView.hidden = NO;
        cell.bottomView.mj_y = cell.tradeBgView.bottom;
    }else
    {
        cell.tradeBgView.hidden = YES;
        cell.bottomView.mj_y = cell.bgView.bottom;
    }
    cell.isSell = NO;
    cell.model = _dataArray[indexPath.row];
    [cell.buyButton add_BtnClickHandler:^(NSInteger tag) {
        
        [self buyOTCSymbolWithIndexPath:indexPath];
    }];
    WeakSelf
    [cell.cancelButton add_BtnClickHandler:^(NSInteger tag) {
        
        weakSelf.index =10086;
        [weakSelf.tableView reloadData];
    }];
    WeakObject(cell)
    [cell.okbutton add_BtnClickHandler:^(NSInteger tag) {
        
        [self tradeWithTableViewCell:weakcell indexPath:indexPath];
    }];
    return cell;
}
/*购买OTC*/
- (void)buyOTCSymbolWithIndexPath:(NSIndexPath *)indexPath
{
    _index = indexPath.row;
    [_tableView reloadData];
    
}
- (void)SetThePassword
{
    
    LoginViewController * loginView = [[LoginViewController alloc] init];
    [[EXUnit currentViewController] presentViewController:loginView animated:YES completion:nil];
}
- (void)tradeWithTableViewCell:(OTCTableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    
    [cell.numberField resignFirstResponder];
    [cell.priceField resignFirstResponder];
    if (![EXUserManager isLogin])
    {
        [self SetThePassword];
        return;
    }
    if ([[EXUnit getSite] isEqualToString:@"ZT"]) {
        if (![[EXUserManager personalData].country_code isEqualToString:@"+886"])
        {
            [EXUnit showMessage:Localized(@"OTC_Taiwan_user_message")];
            return;
        }
    }
   
    WeakSelf
    if ([AppDelegate shareAppdelegate].isBecomeActive) {
        if ([EXUnit isOpenTouchid] && ![EXUnit isOpenGesture]) {
            [YWFingerprintVerification fingerprintVerificationCallBack:^(NSError *error) {
                if(!error){
                    //验证成功，主线程处理UI
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        // 处理耗时操作的代码块...
                        
                        //通知主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf buyWithTableViewCell:cell indexPath:indexPath];
                        });
                        
                    });
                }else{
                    //验证成功，主线程处理UI
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        // 处理耗时操作的代码块...
                        
                        //通知主线程刷新
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [EXUserManager removeUserInfo];
                            [weakSelf SetThePassword];
                        });
                        
                    });
                    
                }
            }];
        }else if (![EXUnit isOpenTouchid] && [EXUnit isOpenGesture])
        {
            //验证手势密码
            [YWUnlockView showUnlockViewWithType:YWUnlockViewUnlock callBack:^(BOOL result) {
                if (result) {
                    [weakSelf buyWithTableViewCell:cell indexPath:indexPath];
                }else
                {
                    [EXUserManager removeUserInfo];
                    [weakSelf SetThePassword];
                }
                
                
            }];
            
        }else if (![EXUnit isOpenTouchid] && ![EXUnit isOpenGesture])
        {
            
            [weakSelf buyWithTableViewCell:cell indexPath:indexPath];
        }
    }else
    {
        [weakSelf buyWithTableViewCell:cell indexPath:indexPath];
    }
  
}
- (void)buyWithTableViewCell:(OTCTableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
     WeakSelf
    [AppDelegate shareAppdelegate].isBecomeActive = NO;
    if ([EXUserManager RealName].integerValue ==0) { //未认证
        [self popRealName:YES title:Localized(@"transaction_authenticated_title") message:Localized(@"transaction_authenticated_message")];
       
        
    }else if ([EXUserManager RealName].integerValue ==1 ||[EXUserManager RealName].integerValue ==4)//审核中
    {
        [EXUnit showMessage:Localized(@"C2C_under_review")];
 
    }else if ([EXUserManager RealName].integerValue ==3)//认证失败
    {
        [self popRealName:YES title:Localized(@"transaction_authenticated_title") message:Localized(@"transaction_authenticated_failed")];
 
    }else if ([EXUserManager RealName].integerValue ==2)//认证成功
    {
         OTCModel * model = _dataArray[indexPath.row];
        if ([NSString isEmptyString:cell.numberField.text] || cell.numberField.text.floatValue==0)
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"OTC_Rquantity_order_mgs") view:kWindow];
            return ;
        }
        if (cell.numberField.text.floatValue< model.limit_money_lower.floatValue || cell.numberField.text.floatValue > model.limit_money_higher.floatValue)
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:[NSString stringWithFormat:Localized(@"OTC_money_lower_message"),model.limit_money_lower,model.limit_money_higher] view:kWindow];
            return ;
        }
        
        if ([EXUserManager personalData].withdraw_password_set) {
            
            
            DropDownAlertVC * custom = [[DropDownAlertVC alloc] initWithCustomContentViewClass:@"OTCDropView" delegate:nil];
            custom.direction = FromBottom;
            [custom show];
            OTCDropView *aleatView = (OTCDropView *)custom.contentView;
            aleatView.cancelBlock = ^{
                [custom dismiss];
            };
            
            WeakObject(aleatView)
            [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
                
                
                if ([NSString isEmptyString:weakaleatView.capitalField.text])
                {
                    [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"Security_Password_funds_message") view:kWindow];
                    return ;
                }
                
                [weakSelf POSTWithHost:@"" path:otc_trade param:@{@"amount":cell.numberField.text,@"type":@"1",@"merchant_account_id":model.merchant_account_id,@"withdraw_password":weakaleatView.capitalField.text,@"pay_method":@""} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    
                    if ([responseObject[@"code"] integerValue]==0)//成功
                    {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:OTCPaypalNotificationse object:nil];
                        OTC_WaitingpaymentVC *  waitingpayment = [[OTC_WaitingpaymentVC alloc] init];
                        waitingpayment.type = [NSString stringWithFormat:@"%@",responseObject[@"result"][@"order"][@"type"]];
                        waitingpayment.order_no = responseObject[@"result"][@"order"][@"order_no"];
                        [self.navigationController pushViewController:waitingpayment animated:YES];
                        [custom dismiss];
                        [self refrech];
                        
                    }else
                    {
                        [EXUnit showMessage:responseObject[@"message"]];
                        
                    }
                    [weakaleatView.okButton stopAnimation];
                    
                } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    [weakaleatView.okButton stopAnimation];
                    
                }];
                
            }];
        }else
        {
            CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"Asset_capital_message")
                                                                                     titleStr:Localized(@"C2C_Reminder")
                                                                                      openUrl:@""
                                                                                      confirm:Localized(@"transaction_OK")
                                                                                       cancel:Localized(@"transaction_cancel")
                                                                                        state:2
                                                                                RechargeBlock:^
                                                {
                                                    
                                                    CapitalPasswordViewController *  Capitalpass  = [[CapitalPasswordViewController alloc] init];
                                                    Capitalpass.isEdit = NO;
                                                    Capitalpass.title = [EXUserManager personalData].withdraw_password_set?Localized(@"Security_fund_password"):Localized(@"Security_Set_funds_password");
                                                    [self.navigationController pushViewController:Capitalpass animated:YES];
                                                    
                                                }];
            aleartVC.animationStyle = LXASAnimationLeftShake;
            [aleartVC showLXAlertView];
            
        }
   
        
        
    }
    
    
}
- (void)popRealName:(BOOL)isRealName title:(NSString *)title message:(NSString *)message
{
    if (isRealName) {
        CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:message
                                                                                 titleStr:title
                                                                                  openUrl:@""
                                                                                  confirm:Localized(@"transaction_OK")
                                                                                   cancel:Localized(@"transaction_cancel")
                                                                                    state:2
                                                                            RechargeBlock:^
                                            {
                                                
                                                IdentityAuthenticationViewController * idACationVC = [[IdentityAuthenticationViewController alloc] init];
                                                idACationVC.title = Localized(@"my_identity_authentication");
                                                [[EXUnit currentViewController].navigationController pushViewController:idACationVC animated:YES];
                                                
                                                
                                            }];
        aleartVC.animationStyle = LXASAnimationTopShake;
        [aleartVC showLXAlertView];
    }else
    {
        CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:message
                                                                                 titleStr:title
                                                                                  openUrl:@""
                                                                                  confirm:Localized(@"transaction_OK")
                                                                                   cancel:Localized(@"transaction_cancel")
                                                                                    state:2
                                                                            RechargeBlock:^{
                                                                                
                                                                                PaymentSetVC * bindbank = [[PaymentSetVC alloc] init];
                                                                                bindbank.title = Localized(@"OTC_Payment_Settings");
                                                                                [self.navigationController pushViewController:bindbank animated:YES];
                                                                                
                                                                            }];
        aleartVC.animationStyle = LXASAnimationTopShake;
        [aleartVC showLXAlertView];
    }
}
@end
