//
//  OTC_CompletedOrderInfoVC.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTC_CompletedOrderInfoVC : BaseViewViewController
@property(nonatomic,strong)NSString * order_no;
@property(nonatomic,strong)NSString * type;//1 是买入 2是卖出
@end

NS_ASSUME_NONNULL_END
