//
//  OTC_CancelOrderVC.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTC_CancelOrderVC.h"

@interface OTC_CancelOrderVC ()
@property (nonatomic,strong)UIButton * returnbutton;
@property (nonatomic,strong)UILabel * stateLab;
@property (nonatomic,strong)UILabel * orderTimeLab;
@property (nonatomic,strong)UIImageView * orderImg1;
@property (nonatomic,strong)UILabel * priceTitle;
@property (nonatomic,strong)UILabel * totalPriceLab;
@property (nonatomic,strong)UIImageView * orderImg2;
@property (nonatomic,strong)UILabel * orderNumTitle;
@property (nonatomic,strong)UILabel * orderNum;
@property (nonatomic,strong)UILabel * unitpriceTitle;
@property (nonatomic,strong)UILabel * unitprice;
@property (nonatomic,strong)UILabel * numberTitle;
@property (nonatomic,strong)UILabel * number;
@property (nonatomic,strong)UILabel * remarksTitle;
@property (nonatomic,strong)UILabel * remarks;
@property (nonatomic,strong)UIButton * OKButton;
@property (nonatomic,strong)UIButton * usbutton;
@property (nonatomic,strong)NSString * phone;
@end

@implementation OTC_CancelOrderVC
- (void)viewWillAppear:(BOOL)animated
{
    [self statusBarStyleIsDark:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO]; //设置隐藏
    [super viewWillDisappear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self statusBarStyleIsDark:[ZBLocalized sharedInstance].StatusBarStyle];
    [self.navigationController setNavigationBarHidden:NO animated:NO]; //设置隐藏
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBA(45, 45, 62, 1);
    [self initOtherUI];
    [self getData];
    [self showAnimation];
}
- (void)showAnimation
{
    _orderImg1.hidden = YES;
    _orderImg2.hidden = YES;
    [self showLoadingAnimation];
}
- (void)stopAnimation
{
    _orderImg1.hidden = NO;
    _orderImg2.hidden = NO;
    [self stopLoadingAnimation];
}
- (void)getData
{
    WeakSelf
    if (![NSString isEmptyString:_order_no])
    {
        
        [self GETWithHost:@"" path:otc_detail param:@{@"orderNo":_order_no} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [weakSelf stopAnimation];
            if ([responseObject[@"code"] integerValue]==0)//成功
            {
                NSDictionary * dic = responseObject[@"result"];
                [self initButtonWithStatus:[dic[@"status"] integerValue]];

            
                CGFloat total = [dic[@"price"] floatValue]* [dic[@"amount"] floatValue];
                weakSelf.totalPriceLab.text = [NSString stringWithFormat:@"%@ %.2f",[EXUnit getZTPriceUnit],total];
            
                NSString * orderno = [NSString stringWithFormat:@"%@  ",weakSelf.order_no];
                weakSelf.orderNum.attributedText = [EXUnit getcontent:orderno image:@"otc_icon_copy" index:orderno.length imageframe:CGRectMake(0, -3, RealValue_W(32), RealValue_W(32))];
                
                weakSelf.unitprice.text = [NSString stringWithFormat:@"%@ %@",[EXUnit getZTPriceUnit],dic[@"price"]];
                weakSelf.number.text = [NSString stringWithFormat:@"%@ %@",dic[@"amount"],dic[@"otc_asset"]];
                weakSelf.remarks.text = [NSString stringWithFormat:@"%@",dic[@"remark"]];
                weakSelf.phone = [NSString stringWithFormat:@"%@",dic[@"phone"]];
                [weakSelf.usbutton setTitle:[NSString stringWithFormat:@"%@：%@",Localized(@"Authentication_us_message"), weakSelf.phone] forState:0];
            }
            
        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }
}
- (void)initButtonWithStatus:(NSInteger)status
{
    switch (status) {
        case 1:
            _stateLab.text = Localized(@"C2C_To_paid");
            
            break;
        case 2:
            _stateLab.text = Localized(@"C2C_Already_paid");
            _orderTimeLab.hidden = YES;
            break;
        case 3:
            _stateLab.text = Localized(@"OTC_Confirmed");
            break;
        case 4:
            
            _stateLab.text = Localized(@"C2C_cancelled");
            _orderTimeLab.hidden = YES;
             [_OKButton setTitle:Localized(@"C2C_cancelled") forState:0];
            break;
        case 5:
            
            _stateLab.text = Localized(@"Asset_in_hand");
            _orderTimeLab.hidden = YES;
            break;
        case 6:
            _stateLab.text = Localized(@"OTC_Expired");
            _orderTimeLab.hidden = YES;
             [_OKButton setTitle:Localized(@"OTC_Expired") forState:0];
            break;
        case 7:
            _stateLab.text = Localized(@"C2C_Completed");
            _OKButton.hidden = YES;
            break;
            
        default:
            break;
    }
}
/*设置其他的UI*/
- (void)initOtherUI
{
    [self.view addSubview:self.returnbutton];
    [self.view addSubview:self.stateLab];
    [self.view addSubview:self.orderTimeLab];
    [self.view addSubview:self.orderImg1];
    [_orderImg1 addSubview:self.priceTitle];
    [_orderImg1 addSubview:self.totalPriceLab];
    [self.view addSubview:self.orderImg2];
    [_orderImg2 addSubview:self.orderNumTitle];
    [_orderImg2 addSubview:self.orderNum];
    [_orderImg2 addSubview:self.unitpriceTitle];
    [_orderImg2 addSubview:self.unitprice];
    [_orderImg2 addSubview:self.numberTitle];
    [_orderImg2 addSubview:self.number];
    [_orderImg2 addSubview:self.remarksTitle];
    [_orderImg2 addSubview:self.remarks];
    [_orderImg2 addSubview:self.OKButton];
     [self.view addSubview:self.usbutton];
}
#pragma mark - 懒加载
- (UIButton *)returnbutton
{
    WeakSelf
    if (!_returnbutton) {
        _returnbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 50, 50)];
        [_returnbutton setImage:[UIImage imageNamed:@"title-back"] forState:0];
        [_returnbutton add_BtnClickHandler:^(NSInteger tag) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
        
        
    }
    return _returnbutton;
}
- (UILabel *)stateLab
{
    if (!_stateLab) {
        _stateLab = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), _returnbutton.bottom+5, UIScreenWidth - RealValue_W(120), 25)];
        _stateLab.textColor = WHITECOLOR;
        _stateLab.font = AutoBoldFont(25);
    }
    return _stateLab;
}
- (UILabel *)orderTimeLab
{
    if (!_orderTimeLab) {
        _orderTimeLab = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(60), _stateLab.bottom +10, UIScreenWidth - RealValue_W(120), 25)];
        _orderTimeLab.textColor = RGBA(255, 255, 255, 0.4);
        _orderTimeLab.font = AutoFont(12);
        
    }
    return _orderTimeLab;
}
- (UIImageView *)orderImg1
{
    if (!_orderImg1) {
        _orderImg1 = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_W(30), _orderTimeLab.bottom + 10, UIScreenWidth - RealValue_W(60), RealValue_W(186))];
        _orderImg1.image = [UIImage imageNamed:@"4otc_dd_bg01"];
    }
    return _orderImg1;
}
- (UILabel *)priceTitle
{
    if (!_priceTitle) {
        _priceTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(25), RealValue_W(38),  160, 18)];
        _priceTitle.text = Localized(@"OTC_Total_orders");
        _priceTitle.textColor = RGBA(170, 170, 170, 1);
        _priceTitle.font = AutoFont(12);
    }
    return _priceTitle;
}
- (UILabel *)totalPriceLab
{
    if (!_totalPriceLab) {
        _totalPriceLab = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(25),_priceTitle.bottom + RealValue_W(20),  200, 20)];

        _totalPriceLab.textColor = BLACKCOLOR;
        _totalPriceLab.font = AutoBoldFont(25);
    }
    return _totalPriceLab;
}
- (UIImageView *)orderImg2
{
    if (!_orderImg2) {
        _orderImg2 = [[UIImageView alloc] initWithFrame:CGRectMake(_orderImg1.mj_x, _orderImg1.bottom , UIScreenWidth - RealValue_W(60), RealValue_W(600))];
        _orderImg2.image = [UIImage imageNamed:@"4otc_dd_bg03"];
    }
    return _orderImg2;
}

- (UILabel *)orderNumTitle
{
    if (!_orderNumTitle) {
        _orderNumTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), RealValue_W(56), 80, RealValue_W(32))];
        _orderNumTitle.textColor = ColorStr(@"#AAAAAA");
        _orderNumTitle.font = AutoFont(13);
        _orderNumTitle.text = Localized(@"OTC_Order_numbe");
    }
    return _orderNumTitle;
}
- (UILabel *)orderNum
{
    if (!_orderNum) {
        _orderNum = [[UILabel alloc] initWithFrame:CGRectMake(_orderImg2.width - 280 - RealValue_W(26), 0, 280, RealValue_W(32))];
        _orderNum.textColor = BLACKCOLOR;
        _orderNum.font = AutoFont(12);
        _orderNum.textAlignment = NSTextAlignmentRight;
        _orderNum.centerY = _orderNumTitle.centerY;
    }
    return _orderNum;
}
- (UILabel *)unitpriceTitle
{
    if (!_unitpriceTitle) {
        _unitpriceTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _orderNum.bottom + RealValue_W(56), 80, RealValue_W(32))];
        _unitpriceTitle.textColor = ColorStr(@"#AAAAAA");
        _unitpriceTitle.font = AutoFont(13);
        _unitpriceTitle.text = Localized(@"OTC_price");
    }
    return _unitpriceTitle;
}
- (UILabel *)unitprice
{
    if (!_unitprice) {
        _unitprice = [[UILabel alloc] initWithFrame:CGRectMake(_orderImg2.width - 200 - RealValue_W(74), 0, 200, RealValue_W(32))];
        _unitprice.textColor = BLACKCOLOR;
        _unitprice.font = AutoFont(15);
        _unitprice.textAlignment = NSTextAlignmentRight;

        _unitprice.centerY = _unitpriceTitle.centerY;
    }
    return _unitprice;
}
- (UILabel *)numberTitle
{
    if (!_numberTitle) {
        _numberTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _unitpriceTitle.bottom + RealValue_W(56), 80, RealValue_W(32))];
        _numberTitle.textColor = ColorStr(@"#AAAAAA");
        _numberTitle.font = AutoFont(13);
        _numberTitle.text = Localized(@"hone_num");
    }
    return _numberTitle;
}
- (UILabel *)number
{
    if (!_number) {
        _number = [[UILabel alloc] initWithFrame:CGRectMake(_orderImg2.width - 200 - RealValue_W(74), 0, 200, RealValue_W(32))];
        _number.textColor = BLACKCOLOR;
        _number.font = AutoFont(15);
        _number.textAlignment = NSTextAlignmentRight;
        _number.centerY = _numberTitle.centerY;
    }
    return _number;
}
- (UILabel *)remarksTitle
{
    if (!_remarksTitle) {
        _remarksTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _numberTitle.bottom + RealValue_W(56), 120, RealValue_W(32))];
        _remarksTitle.textColor = ColorStr(@"#AAAAAA");
        _remarksTitle.font = AutoFont(13);
        _remarksTitle.text = Localized(@"address_Remarks");
    }
    return _remarksTitle;
}
- (UILabel *)remarks
{
    if (!_remarks) {
        _remarks = [[UILabel alloc] initWithFrame:CGRectMake(_orderImg2.width - 280 - RealValue_W(74), 0, 280, RealValue_W(32))];
        _remarks.textColor = BLACKCOLOR;
        _remarks.font = AutoFont(15);
        _remarks.textAlignment = NSTextAlignmentRight;
        _remarks.centerY = _remarksTitle.centerY;
    }
    return _remarks;
}
- (UIButton *)OKButton
{
    if (!_OKButton) {
        _OKButton = [[UIButton alloc] initWithFrame:CGRectMake(RealValue_W(28), self.orderImg2.height - RealValue_W(36 +76), self.orderImg2.width - RealValue_W(56), RealValue_W(76))];
       
        [_OKButton setTitleColor:ColorStr(@"#AAAAAA") forState:0];
        _OKButton.backgroundColor = RGBA(247, 247, 252, 1);
        
    }
    return _OKButton;
    
}
/*联系我们*/
- (UIButton *)usbutton
{
    WeakSelf
    if (!_usbutton)
        _usbutton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(0, UIScreenHeight - 45, 280, 30) title:@"" backgroundColor:CLEARCOLOR titleColor:WHITECOLOR tapAction:^(UIButton *button) {
            
            NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",weakSelf.phone];
            UIWebView * callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
            [self.view addSubview:callWebview];
            
        }];
    _usbutton.centerX = self.view.centerX;
    _usbutton.titleLabel.font = AutoFont(14);
    return _usbutton;
}
@end
