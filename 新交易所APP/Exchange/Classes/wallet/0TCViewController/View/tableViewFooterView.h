//
//  tableViewFooterView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tableViewFooterView : UIView
@property (nonatomic,strong)UIView * lightView;
@property (nonatomic,strong)UILabel * TStitle;
@property (nonatomic,strong)UILabel * contont;
- (CGFloat)getViewHeight;
@end
