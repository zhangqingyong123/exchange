//
//  OTCTableViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTCTableViewCell.h"

@implementation OTCTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(0), 0, RealValue_W(0))];
        
    }
    return self;
}
- (void)setIsSell:(BOOL)isSell
{
    if (isSell) {
        [_allbutton setTitleColor:RGBA(245, 74, 97, 1) forState:0];
        [_cancelButton setTitleColor:RGBA(245, 74, 97, 1) forState:0];
        _cancelButton.backgroundColor = RGBA(245, 74, 97, 0.08);
        _okbutton.backgroundColor = RGBA(245, 74, 97, 1);
        [self.buyButton setTitle:Localized(@"hone_sell") forState:0];
        [self.buyButton setTitleColor:[UIColor redColor] forState:0];
        self.buyButton.backgroundColor = RGBA(245, 74, 97, 0.08);
    }else
    {
        [_allbutton setTitleColor:TABTITLECOLOR forState:0];
        [_cancelButton setTitleColor:TABTITLECOLOR forState:0];
        _cancelButton.backgroundColor = RGBA(0, 191, 166, 0.08);
        _okbutton.backgroundColor = TABTITLECOLOR;
    
        _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
        [_buyButton setTitle:Localized(@"C2C_buy") forState:0];
        [_buyButton setTitleColor:TABTITLECOLOR forState:0];
    }
   
}
- (void)setModel:(OTCModel *)model
{
    _model = model;
    _userName.text = model.name;
    [_userName sizeToFit];
    _userName.width = _userName.width;
    _backimg.mj_x = _userName.right +RealValue_W(10);
    _alipayimg.mj_x = _backimg.right +RealValue_W(6);
    _weChatimg.mj_x = _alipayimg.right +RealValue_W(6);
    
  
    if ([model.pay_method isEqualToString:@"0,1,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _weChatimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _backimg.hidden = NO;
        _alipayimg.hidden = NO;
        _weChatimg.hidden = NO;
    }else if ([model.pay_method isEqualToString:@"0,1"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _weChatimg.hidden = YES;
        _backimg.hidden = NO;
        _alipayimg.hidden = NO;
    }else if ([model.pay_method isEqualToString:@"0,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _weChatimg.hidden = YES;
        _backimg.hidden = NO;
        _alipayimg.hidden = NO;
    }else if ([model.pay_method isEqualToString:@"0"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
        _backimg.hidden = NO;
    }else if ([model.pay_method isEqualToString:@"1"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
         _backimg.hidden = NO;
    }else if ([model.pay_method isEqualToString:@"1,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _weChatimg.hidden = YES;
         _backimg.hidden = NO;
        _alipayimg.hidden = NO;
    }else
    {
         _backimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
        _backimg.hidden = NO;
    }
     _priceLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@%@",Localized(@"OTC_price"),[EXUnit getZTPriceUnit], model.price] attributedString:Localized(@"OTC_price") color:MAINTITLECOLOR1 font:AutoFont(12)];
     _numberLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@ %@",Localized(@"hone_num"),model.amount,[AppDelegate shareAppdelegate].otc_asset] attributedString:Localized(@"hone_num") color:MAINTITLECOLOR1 font:AutoFont(12)];
     _quotaLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@~%@ %@",Localized(@"OTC_quota"),model.limit_money_lower, model.limit_money_higher, [AppDelegate shareAppdelegate].otc_asset] attributedString:Localized(@"OTC_quota") color:MAINTITLECOLOR1 font:AutoFont(12)];
    
    _cnyLable.text = [AppDelegate shareAppdelegate].otc_asset;
}
//数量
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >0 && ![NSString isEmptyString:textField.text])
    {
        _priceField.text = [NSString stringWithFormat:@"%0.2f",textField.text.floatValue *_model.price.floatValue];
    }else
    {
        _priceField.text = @"";
    }
}
//金额
- (void)textFieldPriceDidChange:(UITextField *)textField
{
    if (textField.text.length >0 && ![NSString isEmptyString:textField.text])
    {
        _numberField.text = [NSString stringWithFormat:@"%0.2f",textField.text.floatValue /_model.price.floatValue];
    }else
    {
        _numberField.text = @"";
    }
}
//全部数量
- (void)allnum
{
    _numberField.text =  [NSString stringWithFormat:@"%@",_model.amount];
    _priceField.text =  [NSString stringWithFormat:@"%0.2f",_model.amount.floatValue *_model.price.floatValue];
    
}
- (void)setOrderModel:(OTCOrderListModel *)orderModel
{
    _orderModel = orderModel;
    _tradeBgView.hidden = YES;
    _bottomView.mj_y = _bgView.bottom;
    _userName.text = orderModel.name;
    [_userName sizeToFit];
    _userName.width = _userName.width;
    _backimg.mj_x = _userName.right +RealValue_W(10);
    _alipayimg.mj_x = _backimg.right +RealValue_W(6);
    _weChatimg.mj_x = _alipayimg.right +RealValue_W(6);
    
    
    if ([orderModel.pay_method isEqualToString:@"0,1,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _weChatimg.image = [UIImage imageNamed:@"otc_icon_vx"];
    }else if ([orderModel.pay_method isEqualToString:@"0,1"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _weChatimg.hidden = YES;
    }else if ([orderModel.pay_method isEqualToString:@"0,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _weChatimg.hidden = YES;
    }else if ([orderModel.pay_method isEqualToString:@"0"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
    }else if ([orderModel.pay_method isEqualToString:@"1"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
    }else if ([orderModel.pay_method isEqualToString:@"1,2"])
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _weChatimg.hidden = YES;
    }else
    {
        _backimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _alipayimg.hidden = YES;
        _weChatimg.hidden = YES;
    }
    _priceLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@%@",Localized(@"OTC_price"),[EXUnit getZTPriceUnit],orderModel.price] attributedString:Localized(@"OTC_price") color:MAINTITLECOLOR1 font:AutoFont(12)];
    _numberLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@ %@",Localized(@"hone_num"),orderModel.amount,[AppDelegate shareAppdelegate].otc_asset] attributedString:Localized(@"hone_num") color:MAINTITLECOLOR1 font:AutoFont(12)];
    CGFloat  total = orderModel.price.floatValue * orderModel.amount.floatValue;
    _quotaLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %.2f %@",Localized(@"C2C_Total"), total,[AppDelegate shareAppdelegate].otc_currency] attributedString:Localized(@"C2C_Total") color:MAINTITLECOLOR1 font:AutoFont(12)];
    
    
    [self initButtonWithStatus:orderModel.status.integerValue];
    
    
}
- (void)initButtonWithStatus:(NSInteger)status
{
    switch (status) {
        case 1:
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = TABTITLECOLOR;
                [_buyButton setTitle:Localized(@"C2C_To_paid") forState:0];
                [_buyButton setTitleColor:WHITECOLOR forState:0];
            }else
            {
                [_buyButton setTitle:Localized(@"C2C_To_paid") forState:0];
                [_buyButton setTitleColor:[UIColor redColor] forState:0];
                _buyButton.backgroundColor = RGBA(245, 74, 97, 0.08);
            }
            
   
            break;
        case 2:
           
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"C2C_Already_paid") forState:0];
                [_buyButton setTitleColor:TABTITLECOLOR forState:0];
            }else
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"C2C_Already_paid") forState:0];
                [_buyButton setTitleColor:[UIColor redColor] forState:0];
            }
       
            break;
        case 3:
           
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"OTC_Beaten_currency") forState:0];
                [_buyButton setTitleColor:TABTITLECOLOR forState:0];
            }else
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"OTC_Paid") forState:0];
                [_buyButton setTitleColor:[UIColor redColor] forState:0];
            }
     
            break;
        case 4:
           
        
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = RGBA(178, 178, 197, 0.2);
                [_buyButton setTitle:Localized(@"C2C_cancelled") forState:0];
                [_buyButton setTitleColor:RGBA(170, 170, 170, 1) forState:0];
            }else
            {
                _buyButton.backgroundColor = RGBA(178, 178, 197, 0.2);
                [_buyButton setTitle:Localized(@"C2C_cancelled") forState:0];
                [_buyButton setTitleColor:RGBA(170, 170, 170, 1) forState:0];
            }
            break;
        case 5:
            
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"Asset_in_hand") forState:0];
                [_buyButton setTitleColor:TABTITLECOLOR forState:0];
            }else
            {
                _buyButton.backgroundColor = RGBA(178, 178, 197, 0.2);
                [_buyButton setTitle:Localized(@"Asset_in_hand") forState:0];
                [_buyButton setTitleColor:[UIColor redColor] forState:0];
            }
            
            break;
        case 6:
            _buyButton.backgroundColor = RGBA(178, 178, 197, 0.2);
            [_buyButton setTitle:Localized(@"OTC_Expired") forState:0];
            [_buyButton setTitleColor:RGBA(170, 170, 170, 1) forState:0];
            
            break;
        case 7:
            if (_orderModel.type.integerValue ==1)
            {
                _buyButton.backgroundColor = RGBA(0, 191, 166, 0.08);
                [_buyButton setTitle:Localized(@"C2C_Completed") forState:0];
                [_buyButton setTitleColor:TABTITLECOLOR forState:0];
                
            }else
            {
                _buyButton.backgroundColor = RGBA(245, 74, 97, 0.08);
                [_buyButton setTitle:Localized(@"C2C_Completed") forState:0];
                [_buyButton setTitleColor:[UIColor redColor] forState:0];
                
            }
            
            break;
            
        default:
            break;
    }
    _buyButton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_buyButton cornerRadius:4];
   
}
- (void)get_up
{
    
    [self.contentView addSubview:self.bgView];
    [_bgView addSubview:self.userImage];
    [_bgView addSubview:self.userName];
    [_bgView addSubview:self.backimg];
    [_bgView addSubview:self.alipayimg];
    [_bgView addSubview:self.weChatimg];
    [_bgView addSubview:self.priceLable];
    [_bgView addSubview:self.numberLable];
    [_bgView addSubview:self.quotaLable];
    [_bgView addSubview:self.buyButton];
    
    [self.contentView addSubview:self.bottomView];
    [self.contentView addSubview:self.tradeBgView];
    [_tradeBgView addSubview:self.numBgView];
    [_numBgView addSubview:self.numberField];
    
    [_numBgView addSubview:self.allbutton];
    [_numBgView addSubview:self.cnyLable];
 
    
    [_tradeBgView addSubview:self.icon];
    [_tradeBgView addSubview:self.priceBgView];
    [_priceBgView addSubview:self.priceField];
    [_priceBgView addSubview:self.NTLable];
    [_tradeBgView addSubview:self.cancelButton];
    [_tradeBgView addSubview:self.okbutton];
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(200))];
        _bgView.backgroundColor = TABLEVIEWLCOLOR;
    }
    return _bgView;
}
- (UIImageView *)userImage
{
    if (!_userImage) {
        _userImage = [[UIImageView alloc] initWithFrame:CGRectMake(RealValue_H(30), RealValue_H(30), RealValue_H(50), RealValue_H(50))];
        _userImage.image = [UIImage imageNamed:@"otc_user"];
    }
    return _userImage;
}
- (UILabel *)userName
{
    if (!_userName) {
        _userName = [[UILabel alloc] initWithFrame:CGRectMake(_userImage.right + RealValue_W(10), RealValue_H(36), RealValue_W(94), RealValue_W(34))];
        _userName.textColor = MAINTITLECOLOR;
        _userName.font = AutoBoldFont(18);
    }
    return _userName;
}
- (UIImageView *)backimg
{
    if (!_backimg) {
        _backimg = [[UIImageView alloc] initWithFrame:CGRectMake(_userName.right, 0, RealValue_H(28), RealValue_H(28))];
        _backimg.image = [UIImage imageNamed:@"otc_icon_card"];
        _backimg.centerY= _userName.centerY;
    }
    return _backimg;
}
- (UIImageView *)alipayimg
{
    if (!_alipayimg) {
        _alipayimg = [[UIImageView alloc] initWithFrame:CGRectMake(_backimg.right + RealValue_H(12), 0, RealValue_H(28), RealValue_H(28))];
        _alipayimg.image = [UIImage imageNamed:@"otc_icon_zhi"];
        _alipayimg.centerY= _userName.centerY;
    }
    return _alipayimg;
}
- (UIImageView *)weChatimg
{
    if (!_weChatimg) {
        _weChatimg = [[UIImageView alloc] initWithFrame:CGRectMake(_alipayimg.right + RealValue_H(12), 0, RealValue_H(28), RealValue_H(28))];
        _weChatimg.image = [UIImage imageNamed:@"otc_icon_vx"];
        _weChatimg.centerY= _userName.centerY;
    }
    return _weChatimg;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(30) - 240, 0, 240, RealValue_W(34))];
        _priceLable.textColor = MAINTITLECOLOR;
        _priceLable.textAlignment = NSTextAlignmentRight;
         _priceLable.font = AutoBoldFont(18);
       
       
        _priceLable.centerY = _userName.centerY;
    }
    return _priceLable;
}
- (UILabel *)numberLable
{
    if (!_numberLable) {
        _numberLable = [[UILabel alloc] initWithFrame:CGRectMake(_userImage.mj_x, _userImage.bottom + RealValue_W(20), 240, RealValue_W(32))];
        _numberLable.textColor = MAINTITLECOLOR;
        _numberLable.textAlignment = NSTextAlignmentLeft;
        _numberLable.font = AutoBoldFont(13);
       
        
        
    }
    return _numberLable;
}
- (UILabel *)quotaLable
{
    if (!_quotaLable) {
        _quotaLable = [[UILabel alloc] initWithFrame:CGRectMake(_userImage.mj_x, _numberLable.bottom + RealValue_W(4), 240, RealValue_W(32))];
        _quotaLable.textColor = MAINTITLECOLOR;
        _quotaLable.textAlignment = NSTextAlignmentLeft;
         _quotaLable.font = AutoBoldFont(13);
      
       
        
    }
    return _quotaLable;
}
- (UIButton *)buyButton
{
    if (!_buyButton) {
        _buyButton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth - RealValue_W(140) - RealValue_W(30), 0, RealValue_W(140), RealValue_W(50))];
        _buyButton.centerY = _quotaLable.centerY;
      
        _buyButton.titleLabel.font = AutoBoldFont(14);
    }
    return _buyButton;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, _bgView.bottom, UIScreenWidth, RealValue_H(10))];
        _bottomView.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    }
    return _bottomView;
}
- (UIView *)tradeBgView
{
    if (!_tradeBgView) {
        _tradeBgView = [[UIView alloc] initWithFrame:CGRectMake(0, _bgView.bottom, UIScreenWidth, RealValue_H(206))];
        _tradeBgView.backgroundColor = _bgView.backgroundColor;
    }
    return _tradeBgView;
}
- (UIView *)numBgView
{
    if (!_numBgView) {
        _numBgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(26),0 , RealValue_W(320), RealValue_H(66))];
        _numBgView.backgroundColor = TABLEVIEWLCOLOR;
        _numBgView.layer.borderWidth       =0.3;
        _numBgView.layer.cornerRadius      =2;
        _numBgView.layer.borderColor       =[RGBA(233, 233, 233, 1) CGColor];
    }
    return _numBgView;
}
- (UITextField *)numberField
{
    if (!_numberField) {
        _numberField = [[UITextField alloc] initWithFrame:CGRectMake(RealValue_W(10), 0, RealValue_W(170), _numBgView.height)];
        _numberField.placeholder = Localized(@"OTC_Input_quantity");
        _numberField.font = AutoBoldFont(14);
        _numberField.textColor = MAINTITLECOLOR;
        _numberField.tintColor = TABTITLECOLOR;
        //        _numberField.backgroundColor = [ZBLocalized sharedInstance].textfieldBgColor;
        [_numberField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        [_numberField setValue:AutoBoldFont(14)forKeyPath:@"_placeholderLabel.font"];
        _numberField.keyboardType = UIKeyboardTypeDecimalPad;
        [_numberField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
         _numberField.delegate = self;
    }
    return _numberField;
    
}
- (UIButton *)allbutton
{
    if (!_allbutton) {
        _allbutton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(_numBgView.width -40, 0, 40, _numBgView.height) title:Localized(@"transaction_all") backgroundColor:CLEARCOLOR titleColor:TABTITLECOLOR tapAction:^(UIButton *button) {
            [self allnum];
        }];
        
        _allbutton.titleLabel.font = AutoFont(11);
    }
    return _allbutton;
}
- (UILabel *)cnyLable
{
    if (!_cnyLable) {
        _cnyLable = [[UILabel alloc] initWithFrame:CGRectMake(_numberField.right, 0, 40, _numBgView.height)];
        _cnyLable.centerY =_allbutton.centerY;
        _cnyLable.textColor = MAINTITLECOLOR;
        _cnyLable.font = AutoBoldFont(10);
        _cnyLable.textAlignment = NSTextAlignmentCenter;
    }
    return _cnyLable;
}
- (UIImageView *)icon
{
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(_numBgView.right + RealValue_H(16), 0, RealValue_H(32), RealValue_H(32))];
        _icon.image = [UIImage imageNamed:@"otc_icon_qiehuan"];
        _icon.centerY= _numBgView.centerY;
    }
    return _icon;
}
- (UIView *)priceBgView
{
    if (!_priceBgView) {
        _priceBgView = [[UIView alloc] initWithFrame:CGRectMake(_icon.right + RealValue_W(16),0 , RealValue_W(320), RealValue_H(66))];
        _priceBgView.backgroundColor = TABLEVIEWLCOLOR;
        _priceBgView.layer.borderWidth       =0.3;
        _priceBgView.layer.cornerRadius      =2;
        _priceBgView.layer.borderColor       =[RGBA(233, 233, 233, 1) CGColor];
        _priceBgView.centerY = _numBgView.centerY;;
    }
    return _priceBgView;
}
- (UITextField *)priceField
{
    if (!_priceField) {
        _priceField = [[UITextField alloc] initWithFrame:CGRectMake(RealValue_W(10), 0, RealValue_W(240), _numBgView.height)];
        _priceField.placeholder = Localized(@"OTC_Input_amount");
        _priceField.font = AutoBoldFont(14);
        _priceField.textColor = MAINTITLECOLOR;
        _priceField.tintColor = TABTITLECOLOR;
        _priceField.delegate = self;
        [_priceField addTarget:self action:@selector(textFieldPriceDidChange:) forControlEvents:UIControlEventEditingChanged];
        [_priceField setValue:textFieldPCOLOR forKeyPath:@"_placeholderLabel.textColor"];
        [_priceField setValue:AutoBoldFont(14)forKeyPath:@"_placeholderLabel.font"];
        _priceField.keyboardType = UIKeyboardTypeDecimalPad;
        
    }
    return _priceField;
    
}
- (UILabel *)NTLable
{
    if (!_NTLable) {
        _NTLable = [[UILabel alloc] initWithFrame:CGRectMake(_priceBgView.width -35, 0, 30, _priceBgView.height)];
        _NTLable.centerY =_priceField.centerY;
        _NTLable.textColor = MAINTITLECOLOR;
        _NTLable.font = AutoBoldFont(10);
        _NTLable.textAlignment = NSTextAlignmentCenter;
        if ([[EXUnit getSite] isEqualToString:@"ZT"])
        {
            _NTLable.text = @"TWD";
        }else
        {
            _NTLable.text = @"¥";
        }
    }
    return _NTLable;
}
- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(_numBgView.mj_x, _numBgView.bottom + RealValue_W(34), RealValue_W(230), RealValue_W(66)) title:Localized(@"transaction_cancel") backgroundColor:RGBA(0, 191, 166, 0.08) titleColor:TABTITLECOLOR tapAction:^(UIButton *button) {
           
        }];
        _cancelButton.titleLabel.font = AutoFont(14);
        _cancelButton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_cancelButton cornerRadius:2];
    }
    return _cancelButton;
}
- (UIButton *)okbutton
{
    if (!_okbutton) {
        _okbutton = [[UIButton alloc] initWithFrame:CGRectMake(_cancelButton.right + RealValue_W(22), _cancelButton.mj_y, RealValue_W(440), RealValue_W(66))];
        _okbutton.backgroundColor = TABTITLECOLOR;
        [_okbutton setTitle:Localized(@"OTC_Place_order") forState:0];
        [_okbutton setTitleColor:WHITECOLOR forState:0];
        _okbutton.titleLabel.font = AutoFont(14);
        _okbutton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_okbutton cornerRadius:2];
    }
    return _okbutton;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //删除处理
    if ([string isEqualToString:@""]) {
        return YES;
    }
    //首位不能为.号
    if (range.location == 0 && [string isEqualToString:@"."]) {
        return NO;
    }
    return [self isRightInPutOfString:textField.text withInputString:string range:range textField:textField];
}


- (BOOL)isRightInPutOfString:(NSString *) string withInputString:(NSString *) inputString range:(NSRange) range textField:(UITextField *)textField{
    //判断只输出数字和.号
    NSString *passWordRegex = @"[0-9\\.]";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    if (![passWordPredicate evaluateWithObject:inputString]) {
        return NO;
    }
    
    //逻辑处理
    if ([string containsString:@"."]) {
        if ([inputString isEqualToString:@"."]) {
            return NO;
        }
        NSRange subRange = [string rangeOfString:@"."];
        if (textField == _priceField) {
            if (range.location - subRange.location > 2) {
                return NO;
            }
        }else if (textField ==_numberField)
        {
            if (range.location - subRange.location > 2) {
                return NO;
            }
        }
        
    }
    return YES;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
