//
//  tableViewFooterView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "tableViewFooterView.h"

@implementation tableViewFooterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self get_up];
        self.backgroundColor = TABLEVIEWLCOLOR;
    }
    return self;
}
- (CGFloat)getViewHeight;
{
    _lightView.height = _contont.height + 40;
    _lightView.backgroundColor = [ZBLocalized sharedInstance].C2CboxBgColor;
    _lightView.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_lightView cornerRadius:4];
    return _lightView.height +40;
}
- (void)get_up
{
    [self addSubview:self.lightView];
    [_lightView addSubview:self.TStitle];
    [_lightView addSubview:self.contont];
}
- (UIView *)lightView
{
    if (!_lightView) {
        _lightView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(26), UIScreenWidth - RealValue_W(52), 0)];
        _lightView.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    }
    return _lightView;
}
- (UILabel *)TStitle
{
    if (!_TStitle) {
        _TStitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(24), RealValue_W(28), 120, RealValue_H(24))];
        _TStitle.textColor = TABTITLECOLOR;
        _TStitle.text = Localized(@"OTC_Transaction_Notice");
        _TStitle.font = AutoFont(11);
    }
    return _TStitle;
}
- (UILabel *)contont
{
    if (!_contont) {
        _contont = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(24),_TStitle.bottom + RealValue_W(16), _lightView.width - RealValue_W(48), 0)];
        _contont.textColor = MAINTITLECOLOR1;
        _contont.text = Localized(@"OTC_Transaction_Notice_message");
        _contont.font = AutoFont(12);
        _contont.numberOfLines = 0;
        [_contont sizeToFit];
        _contont.height = _contont.height +10;
    }
    return _contont;
}
@end
