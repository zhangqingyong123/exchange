//
//  PaypalView.h
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaypalModel.h"
@interface PaypalView : UIView
/*0为银行卡支付 1为支付宝  2 为微信支付 */
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,strong)UILabel * payPalAccountNumTitle;
@property (nonatomic,strong)UILabel * payPalAccountNumLab;
@property (nonatomic,strong)UILabel * nameTitle;
@property (nonatomic,strong)UILabel * nameLab;
@property (nonatomic,strong)UIView * QRBgView;
@property (nonatomic,strong)UIImageView * QRimg;
@property (nonatomic,strong)UILabel * QRLab;

@property (nonatomic,strong)NSString * payPalAccountNumStr;
@property (nonatomic,strong)NSString * paynameStr;
@property (nonatomic,strong)NSString * nameStr;
@property (nonatomic,strong)NSString * bankStr;
@property (nonatomic,strong)NSString * remarsStr;
/*银行卡信息*/
@property (nonatomic,strong)UILabel * backTitle;
@property (nonatomic,strong)UILabel * backName;
@property (nonatomic,strong)UILabel * backinfo;

@property (nonatomic,strong)UILabel * payeeTitle;
@property (nonatomic,strong)UILabel * payeeName;
@property (nonatomic,strong)UILabel * backNumTitle;
@property (nonatomic,strong)UILabel * backNum;

@property (nonatomic,strong)UILabel * remarksTitle;
@property (nonatomic,strong)UILabel * remarks;
@property (nonatomic,strong)PModel * modle;
- (instancetype)initWithPaypalWithIndex:(NSInteger)index;
- (void)getPaypalWithDic:(NSDictionary *)dic;
- (void)getPaypalWithmodle:(PModel *)modle name:(NSString *)name;
@end

