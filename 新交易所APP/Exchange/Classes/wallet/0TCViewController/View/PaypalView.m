//
//  PaypalView.m
//  Exchange
//
//  Created by 张庆勇 on 2019/3/1.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "PaypalView.h"
#import "CheckVersionAlearView.h"
@implementation PaypalView
- (instancetype)initWithPaypalWithIndex:(NSInteger)index
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, RealValue_W(100), UIScreenWidth - RealValue_W(60), RealValue_W(470));
        _index = index;
        switch (_index) {
            case 0:
                [self bankUI];
                break;
            case 1:
                 [self alipayUI];
                 _payPalAccountNumTitle.text = Localized(@"OTC_Alipay_num");
                 _QRLab.text = Localized(@"OTC_Alipay_Preservation_icon");
                break;
            case 2:
                
                [self weChatUI];
                 _payPalAccountNumTitle.text = Localized(@"OTC_weChat_num");
                 _QRLab.text = Localized(@"OTC_weChat_Preservation_icon");
                break;
                
            default:
                break;
        }
        
    }
    return self;
}
- (void)getPaypalWithmodle:(PModel *)modle name:(NSString *)name
{
    _modle = modle;
    _payPalAccountNumStr = [NSString stringWithFormat:@"%@ ",modle.pay_method_name];
    _payPalAccountNumLab.attributedText = [EXUnit getcontent:_payPalAccountNumStr image:@"otc_icon_copy" index:_payPalAccountNumStr.length imageframe:CGRectMake(0, -2, RealValue_W(32), RealValue_W(32))];
    _paynameStr = [NSString stringWithFormat:@"%@ ",name];
     _nameLab.attributedText = [EXUnit getcontent:_paynameStr image:@"otc_icon_copy" index:_paynameStr.length imageframe:CGRectMake(0, -2, RealValue_W(32), RealValue_W(32))];
    [_QRimg sd_setImageWithURL:[NSURL URLWithString:modle.pay_method_no]];
}
-(void)payPalAccountNumLab:(UITapGestureRecognizer *)recognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _payPalAccountNumStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
    
}
-(void)nameLab:(UITapGestureRecognizer *)recognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _paynameStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
    
}
- (void)getPaypalWithDic:(NSDictionary *)dic
{
    _backName.text = dic[@"pay_method_name"];
    _backinfo.text = dic[@"pay_method_sub_name"];
    _nameStr = [NSString stringWithFormat:@"%@ ",dic[@"name"]];
    _payeeName.attributedText = [EXUnit getcontent:_nameStr image:@"otc_icon_copy" index:_nameStr.length imageframe:CGRectMake(0, -2, RealValue_W(32), RealValue_W(32))];
    _bankStr = [NSString stringWithFormat:@"%@ ",dic[@"pay_method_no"]];
    _backNum.attributedText = [EXUnit getcontent:_bankStr image:@"otc_icon_copy" index:_bankStr.length imageframe:CGRectMake(0, -2, RealValue_W(32), RealValue_W(32))];
    _remarsStr = [NSString stringWithFormat:@"%@ ",dic[@"user_id"]];
    _remarks.attributedText = [EXUnit getcontent:_remarsStr image:@"otc_icon_copy" index:_remarsStr.length imageframe:CGRectMake(0, -2, RealValue_W(32), RealValue_W(32))];
}
-(void)payeeName:(UITapGestureRecognizer *)recognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
     pasteboard.string = _nameStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
    
}
-(void)backNum:(UITapGestureRecognizer *)recognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _bankStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
    
}
-(void)remarks:(UITapGestureRecognizer *)recognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _remarsStr;
    [EXUnit showMessage:Localized(@"Copy_to_paste")];
    
}
#pragma mark 长按手势弹出警告视图确认
-(void)imglongTapClick:(UILongPressGestureRecognizer*)gesture

{
    
    if(gesture.state==UIGestureRecognizerStateBegan) 
    {
        
        CheckVersionAlearView * aleartVC = [[CheckVersionAlearView alloc] initWithmessage:Localized(@"Asset_Sure_album") titleStr:Localized(@"Asset_Save_picture") openUrl:@"" confirm:Localized(@"transaction_OK") cancel:Localized(@"transaction_cancel")   state:2 RechargeBlock:^{
            
            // 保存图片到相册
            UIImageWriteToSavedPhotosAlbum(self.QRimg.image,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),nil);
            
            
            
        }];
        
        aleartVC.animationStyle = LXASAnimationDefault;
        [aleartVC showLXAlertView];
        
        
        
    }
    
}
#pragma mark 保存图片后的回调
- (void)imageSavedToPhotosAlbum:(UIImage*)image didFinishSavingWithError:  (NSError*)error contextInfo:(id)contextInfo
{
    NSString*message =@"";
    
    if(!error) {
        
        message =Localized(@"Asset_Saved_album");
        
        [EXUnit showSVProgressHUD:message];
        
    }else
        
    {
        
        message = [error description];
        [EXUnit showMessage:Localized(@"album_message")];
    }
    
}
- (void)bankUI
{
    [self addSubview:self.backTitle];
    [self addSubview:self.backName];
    [self addSubview:self.backinfo];
    [self addSubview:self.payeeTitle];
    [self addSubview:self.payeeName];
    [self addSubview:self.backNumTitle];
    [self addSubview:self.backNum];
    [self addSubview:self.remarksTitle];
    [self addSubview:self.remarks];
}
- (UILabel *)backTitle
{
    if (!_backTitle) {
        _backTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(44), 120, RealValue_W(32))];
        _backTitle.textColor = ColorStr(@"#AAAAAA");
        _backTitle.font = AutoFont(13);
        _backTitle.text = Localized(@"OTC_bank_Info");
        
    }
    return _backTitle;
}
- (UILabel *)backName
{
    if (!_backName) {
        _backName = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 200 - RealValue_W(30), RealValue_W(44), 200, RealValue_W(32))];
        _backName.textColor = BLACKCOLOR;
        _backName.font = AutoBoldFont(15);
        _backName.textAlignment = NSTextAlignmentRight;
    }
    return _backName;
}
- (UILabel *)backinfo
{
    if (!_backinfo) {
        _backinfo = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28),_backName.bottom + RealValue_W(30), self.width - RealValue_W(56), RealValue_W(70))];
        _backinfo.textColor = BLACKCOLOR;
        _backinfo.font = AutoFont(12);
        _backinfo.textAlignment = NSTextAlignmentRight;
        
        _backinfo.backgroundColor = RGBA(247, 247, 252, 1);
        
    }
    return _backinfo;
}
- (UILabel *)payeeTitle
{
    if (!_payeeTitle) {
        _payeeTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _backinfo.bottom + RealValue_W(44), 120, RealValue_W(32))];
        _payeeTitle.textColor = ColorStr(@"#AAAAAA");
        _payeeTitle.font = AutoFont(13);
        _payeeTitle.text = Localized(@"OTC_Payee");
    }
    return _payeeTitle;
}
- (UILabel *)payeeName
{
    if (!_payeeName) {
        _payeeName = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 200 - RealValue_W(30), 0, 200, RealValue_W(32))];
        _payeeName.textColor = BLACKCOLOR;
        _payeeName.font = AutoFont(15);
        _payeeName.textAlignment = NSTextAlignmentRight;
        _payeeName.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(payeeName:)];
        
        [_payeeName addGestureRecognizer:labelTapGestureRecognizer];
        _payeeName.centerY = _payeeTitle.centerY;
    }
    return _payeeName;
}
- (UILabel *)backNumTitle
{
    if (!_backNumTitle) {
        _backNumTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _payeeTitle.bottom + RealValue_W(44), 120, RealValue_W(32))];
        _backNumTitle.textColor = ColorStr(@"#AAAAAA");
        _backNumTitle.font = AutoFont(13);
        _backNumTitle.text = Localized(@"OTC_bank_number");
    }
    return _backNumTitle;
}
- (UILabel *)backNum
{
    if (!_backNum) {
        _backNum = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 200 - RealValue_W(30), 0, 200, RealValue_W(32))];
        _backNum.textColor = BLACKCOLOR;
        _backNum.font = AutoFont(15);
        _backNum.textAlignment = NSTextAlignmentRight;
        _backNum.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backNum:)];
        
        [_backNum addGestureRecognizer:labelTapGestureRecognizer];
        _backNum.centerY = _backNumTitle.centerY;
    }
    return _backNum;
}
- (UILabel *)remarksTitle
{
    if (!_remarksTitle) {
        _remarksTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(28), _backNumTitle.bottom + RealValue_W(44), 120, RealValue_W(32))];
        _remarksTitle.textColor = ColorStr(@"#AAAAAA");
        _remarksTitle.font = AutoFont(13);
        _remarksTitle.text = Localized(@"OTC_Remarks_information");
    }
    return _remarksTitle;
}
- (UILabel *)remarks
{
    if (!_remarks) {
        _remarks = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 200 - RealValue_W(30), 0, 200, RealValue_W(32))];
        _remarks.textColor = BLACKCOLOR;
        _remarks.font = AutoFont(15);
        _remarks.textAlignment = NSTextAlignmentRight;
        _remarks.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(remarks:)];
        
        [_remarks addGestureRecognizer:labelTapGestureRecognizer];
        _remarks.centerY = _remarksTitle.centerY;
    }
    return _remarks;
}
- (void)weChatUI
{
    [self addSubview:self.payPalAccountNumTitle];
    [self addSubview:self.payPalAccountNumLab];
    [self addSubview:self.nameTitle];
    [self addSubview:self.nameLab];
    [self addSubview:self.QRBgView];
    [_QRBgView addSubview:self.QRimg];
    [_QRBgView addSubview:self.QRLab];
}
- (void)alipayUI
{
    [self addSubview:self.payPalAccountNumTitle];
    [self addSubview:self.payPalAccountNumLab];
    [self addSubview:self.nameTitle];
    [self addSubview:self.nameLab];
    [self addSubview:self.QRBgView];
    [_QRBgView addSubview:self.QRimg];
    [_QRBgView addSubview:self.QRLab];
}
- (UILabel *)payPalAccountNumTitle
{
    if (!_payPalAccountNumTitle) {
        _payPalAccountNumTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30), RealValue_W(44), 120, RealValue_W(32))];
        _payPalAccountNumTitle.textColor = ColorStr(@"#AAAAAA");
        _payPalAccountNumTitle.font = AutoFont(13);
       
        
    }
    return _payPalAccountNumTitle;
}
- (UILabel *)payPalAccountNumLab
{
    if (!_payPalAccountNumLab) {
        _payPalAccountNumLab = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 200 - RealValue_W(30), RealValue_W(44), 200, RealValue_W(32))];
        _payPalAccountNumLab.textColor = BLACKCOLOR;
        _payPalAccountNumLab.font = AutoFont(15);
        _payPalAccountNumLab.textAlignment = NSTextAlignmentRight;
        _payPalAccountNumLab.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(payPalAccountNumLab:)];
        
        [_payPalAccountNumLab addGestureRecognizer:labelTapGestureRecognizer];
        
    }
    return _payPalAccountNumLab;
}
- (UILabel *)nameTitle
{
    if (!_nameTitle) {
        _nameTitle = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(30),_payPalAccountNumTitle.bottom + RealValue_W(30), 120, RealValue_W(32))];
        _nameTitle.textColor = ColorStr(@"#AAAAAA");
        _nameTitle.font = AutoFont(13);
        _nameTitle.text = Localized(@"Identity_name");
        
    }
    return _nameTitle;
}
- (UILabel *)nameLab
{
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 120 -RealValue_W(30), RealValue_W(44), 120, RealValue_W(32))];
        _nameLab.textColor = BLACKCOLOR;
        _nameLab.font = AutoFont(15);
        _nameLab.textAlignment = NSTextAlignmentRight;
        _nameLab.centerY = _nameTitle.centerY;
        _nameLab.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(nameLab:)];
        
        [_nameLab addGestureRecognizer:labelTapGestureRecognizer];
    }
    return _nameLab;
}
- (UIView *)QRBgView
{
    if (!_QRBgView) {
        _QRBgView = [[UIView alloc] initWithFrame:CGRectMake(RealValue_W(30), _nameTitle.bottom +RealValue_W(44), self.width - RealValue_W(60), RealValue_W(280))];
        _QRBgView.backgroundColor = RGBA(247, 247, 252, 1);
    }
    return _QRBgView;
}
- (UIImageView *)QRimg
{
    if (!_QRimg) {
        _QRimg = [[UIImageView alloc] initWithFrame:CGRectMake(_QRBgView.width/2 -  RealValue_W(198)/2, RealValue_W(26), RealValue_W(198), RealValue_W(198))];
        //1.创建长按手势
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick:)];
        
        //2.开启人机交互
        _QRimg.userInteractionEnabled = YES;
        
        //3.添加手势
        [_QRimg addGestureRecognizer:longTap];
    }
    return _QRimg;
}
- (UILabel *)QRLab
{
    if (!_QRLab) {
        _QRLab = [[UILabel alloc] initWithFrame:CGRectMake(0, _QRimg.bottom, 220, 28)];
       
        _QRLab.textAlignment = NSTextAlignmentCenter;
        _QRLab.textColor = MAINTITLECOLOR1;
        _QRLab.font = AutoFont(11);
        _QRLab.centerX = _QRimg.centerX;
    }
    return _QRLab;
}

@end
