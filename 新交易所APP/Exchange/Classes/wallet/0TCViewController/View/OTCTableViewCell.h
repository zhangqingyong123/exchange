//
//  OTCTableViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTCModel.h"
@interface OTCTableViewCell : UITableViewCell <UITextFieldDelegate>
@property(nonatomic,strong)UIView * bgView;
@property(nonatomic,strong)UIImageView * userImage;
@property(nonatomic,strong)UILabel * userName;
@property(nonatomic,strong)UIImageView * backimg;
@property(nonatomic,strong)UIImageView * alipayimg;
@property(nonatomic,strong)UIImageView * weChatimg;
@property(nonatomic,strong)UILabel * priceLable;
@property(nonatomic,strong)UILabel * numberLable;
@property(nonatomic,strong)UILabel * quotaLable;
@property(nonatomic,strong)UIButton * buyButton;
@property(nonatomic,strong)UIView * bottomView;
@property(nonatomic,strong)UIView * tradeBgView;
@property(nonatomic,strong)UIView * numBgView;
@property(nonatomic,strong)UITextField *numberField;
@property(nonatomic,strong)UIButton *allbutton;
@property(nonatomic,strong)UILabel * cnyLable;
@property(nonatomic,strong)UIImageView * icon;
@property(nonatomic,strong)UIView * priceBgView;
@property(nonatomic,strong)UITextField *priceField;
@property(nonatomic,strong)UILabel * NTLable;
@property(nonatomic,strong)UIButton *cancelButton;
@property(nonatomic,strong)UIButton *okbutton;
@property(nonatomic,strong)OTCModel *  model;
@property(nonatomic,strong)OTCOrderListModel *  orderModel;
@property(nonatomic,assign)BOOL isSell;
@end

