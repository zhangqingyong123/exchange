//
//  OTC_CompletedOrderVC.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/28.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "OTC_CompletedOrderVC.h"
#import "OTCTableViewCell.h"
#import "OTC_WaitingpaymentVC.h"
#import "OTC_CompletedOrderInfoVC.h"
#import "OTC_CancelOrderVC.h"
#import "OTCModel.h"
@interface OTC_CompletedOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (assign, nonatomic) NSUInteger         pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation OTC_CompletedOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOtcList) name:OTCupdateNotificationse object:nil];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LoginSusess) name:LoginSusessNotificationse object:nil];
    // 上拉加载
    WeakSelf
    WLRefreshGifHeader *header = [WLRefreshGifHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        [weakSelf getData];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getData];
    }];
    self.tableView.mj_footer = fooder;
}
- (void)LoginSusess
{
    [self getOtcList];
}
- (void)getOtcList
{
    self.pages = 0;
    [self getData];
}
#pragma mark----数据
- (void)getData
{
    WeakSelf
    [self GETWithHost:@"" path:otc_trade param:@{@"status":@"7",@"offset":@(self.pages*10),@"limit":@(10)} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.tableView.mj_header endRefreshing];
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSArray *list = responseObject[@"result"][@"logs"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataArray addObjectsFromArray:[OTCOrderListModel mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataArray = [OTCOrderListModel mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
            [weakSelf.tableView reloadData];
            
        }else
        {
              [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"C2C_not_login")  view:kWindow];
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"C2C_not_login")  view:kWindow];
        [self.tableView.mj_header endRefreshing];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            WeakSelf
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma mark----n懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -49 -64 -IPhoneTop -RealValue_W(88))];
        
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[OTCTableViewCell class] forCellReuseIdentifier:@"OTCTableViewCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(210);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return RealValue_H(20);
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, RealValue_H(20))];
    view.backgroundColor = [ZBLocalized sharedInstance].BgViewLightColor;
    return view;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OTCTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"OTCTableViewCell"];
    
    cell.orderModel = _dataArray[indexPath.row];
  
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OTCOrderListModel * orderModel = _dataArray[indexPath.row];
    switch (orderModel.status.integerValue) {
        case 1: //待支付
        {
            OTC_WaitingpaymentVC *  waitingpayment = [[OTC_WaitingpaymentVC alloc] init];
            waitingpayment.order_no = orderModel.order_no;
            waitingpayment.type = [NSString stringWithFormat:@"%@",orderModel.type];
            [self.navigationController pushViewController:waitingpayment animated:YES];
            
        }
            break;
        case 2://已支付
        {
            OTC_CompletedOrderInfoVC * completedOrder = [[OTC_CompletedOrderInfoVC alloc] init];
            completedOrder.order_no = orderModel.order_no;
            [self.navigationController pushViewController:completedOrder animated:YES];
        }
            
            break;
        case 3://已确认
        {
            OTC_CompletedOrderInfoVC * completedOrder = [[OTC_CompletedOrderInfoVC alloc] init];
            completedOrder.order_no = orderModel.order_no;
            [self.navigationController pushViewController:completedOrder animated:YES];
        }
            
            break;
        case 4://已取消
        {
            OTC_CancelOrderVC * cancelOrderVC = [[OTC_CancelOrderVC alloc] init];
            cancelOrderVC.order_no = orderModel.order_no;
            [self.navigationController pushViewController:cancelOrderVC animated:YES];
        }
            
            break;
        case 5://处理中
        {
            OTC_CompletedOrderInfoVC * completedOrder = [[OTC_CompletedOrderInfoVC alloc] init];
            completedOrder.order_no = orderModel.order_no;
            [self.navigationController pushViewController:completedOrder animated:YES];
        }
            
            
            break;
        case 6://已过期
        {
            OTC_CancelOrderVC * cancelOrderVC = [[OTC_CancelOrderVC alloc] init];
            cancelOrderVC.order_no = orderModel.order_no;
            [self.navigationController pushViewController:cancelOrderVC animated:YES];
        }
            
            break;
        case 7://已完成
        {
            OTC_CompletedOrderInfoVC * completedOrder = [[OTC_CompletedOrderInfoVC alloc] init];
            completedOrder.order_no = orderModel.order_no;
            completedOrder.type = orderModel.type;
            [self.navigationController pushViewController:completedOrder animated:YES];
        }
            
            break;
            
        default:
            break;
    }
}
@end
