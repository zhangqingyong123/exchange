//
//  WOrderModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WOrderModle : NSObject
@property (nonatomic,strong)NSString * asset;
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * merchant_bank;
@property (nonatomic,strong)NSString * merchant_bank_id;
@property (nonatomic,strong)NSString * merchant_card_number;
@property (nonatomic,strong)NSString * merchant_name;
@property (nonatomic,strong)NSString * merchant_sub_bank;
@property (nonatomic,strong)NSString * order_no;
@property (nonatomic,strong)NSString * pay_method;
@property (nonatomic,strong)NSString * price;
@property (nonatomic,strong)NSString * status;
@property (nonatomic,strong)NSString * type;
@property (nonatomic,strong)NSString * user_bank_id;
@property (nonatomic,strong)NSString * remark;
@property (nonatomic,strong)NSString * expire_time;
@property (nonatomic,strong)NSString * contact_way;//联系方式
@property (nonatomic,strong)NSString * cancel_reason;
@end
@interface orderModle : NSObject
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * asset;
@property (nonatomic,strong)NSString * cancel_reason;
@property (nonatomic,strong)NSString * expire_time;
@property (nonatomic,strong)NSString * fee;

@end
