//
//  WaitForPaymentOrederViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "WOrderModle.h"
#import "TimeLable.h"
@interface WaitForPaymentOrederViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *currency;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable1;
@property (weak, nonatomic) IBOutlet UILabel *titleLable2;
@property (weak, nonatomic) IBOutlet UILabel *titleLable3;

@property (weak, nonatomic) IBOutlet UILabel *bankmodetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *nametitleLable;
@property (weak, nonatomic) IBOutlet UILabel *bankNametitleLable;
@property (weak, nonatomic) IBOutlet UILabel *classtitleLable;
@property (weak, nonatomic) IBOutlet UILabel *banktitleLable;
@property (weak, nonatomic) IBOutlet UILabel *remakeLable;
@property (weak, nonatomic) IBOutlet UILabel *timetitleLable;

@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *unitPrice;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *payment;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

- (IBAction)copyName:(UIButton *)sender;
- (IBAction)Alreadybutton:(UIButton *)sender;
- (IBAction)cancelbutton:(UIButton *)sender;
- (IBAction)remarks:(UIButton *)sender;
- (IBAction)bankNumber:(UIButton *)sender;
- (IBAction)capybranch:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet TimeLable *time;
@property (weak, nonatomic) IBOutlet UILabel *bank;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *bankNumber;
@property (weak, nonatomic) IBOutlet UILabel *Branch;
@property (weak, nonatomic) IBOutlet UILabel *Remarks;
@property (weak, nonatomic) IBOutlet UIButton *Alreadybutton;
@property (weak, nonatomic) IBOutlet UIButton *cancelbutton;

@property (weak, nonatomic) IBOutlet UIButton *copyname;
@property (weak, nonatomic) IBOutlet UIButton *remarksbutton;
@property (weak, nonatomic) IBOutlet UIButton *bankNumberbutton;
@property (weak, nonatomic) IBOutlet UIButton *capybranch;
@property (nonatomic,strong)WOrderModle * modle;
@property (nonatomic,strong)NSDictionary * dic;
@end
