//
//  WOrderInfoViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "WOrderModle.h"
@interface WOrderInfoViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *currency;
@property (weak, nonatomic) IBOutlet UILabel *titleLable1;
@property (weak, nonatomic) IBOutlet UILabel *titleLable2;
@property (weak, nonatomic) IBOutlet UILabel *titleLable3;

@property (weak, nonatomic) IBOutlet UILabel *orderTimetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *ordernumbertitleLable;
@property (weak, nonatomic) IBOutlet UILabel *NametitleLable;
@property (weak, nonatomic) IBOutlet UILabel *PaymenttitleLable;
@property (weak, nonatomic) IBOutlet UILabel *AmounttitleLable;
@property (weak, nonatomic) IBOutlet UILabel *timetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *releasetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *remakeLable;
@property (weak, nonatomic) IBOutlet UIButton *copybutton;
@property (weak, nonatomic) IBOutlet UILabel *phoneTitle;
@property (weak, nonatomic) IBOutlet UIButton *copyphoneButton;
@property (weak, nonatomic) IBOutlet UIButton *copyname;

@property (weak, nonatomic) IBOutlet UIButton *cardTitle;
- (IBAction)copyPhone:(UIButton *)sender;
- (IBAction)copyback:(UIButton *)sender;
- (IBAction)copyname:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *backNumber;
@property (weak, nonatomic) IBOutlet UILabel *backNumberTitle;


@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *unitPrice;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *payment;
@property (weak, nonatomic) IBOutlet UILabel *amountpayment;
@property (weak, nonatomic) IBOutlet UILabel *transactiontime;
@property (weak, nonatomic) IBOutlet UILabel *outTime;
@property (weak, nonatomic) IBOutlet UILabel *remake;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (nonatomic,strong)WOrderModle * modle;
@end
