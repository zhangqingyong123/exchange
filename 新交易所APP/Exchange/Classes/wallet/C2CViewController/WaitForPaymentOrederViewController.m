//
//  WaitForPaymentOrederViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "WaitForPaymentOrederViewController.h"

@interface WaitForPaymentOrederViewController ()

@end

@implementation WaitForPaymentOrederViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = MainBGCOLOR;
    self.titleLable.textColor = MAINTITLECOLOR;
    self.titleLable.text = Localized(@"C2C_Please_pay");
    self.titleLable1.text = Localized(@"C2C_following_baccount");
    self.titleLable2.text = Localized(@"C2C_Transaction_unit_price");
    self.titleLable3.text = Localized(@"C2C_Volume_of_transactions");
    self.bankmodetitleLable.text = Localized(@"C2C_Payment_method");
    self.nametitleLable.text = Localized(@"C2C_Payee");
    self.bankNametitleLable.text = Localized(@"C2C_bank");
    self.classtitleLable.text = Localized(@"C2C_Branch");
    self.banktitleLable.text = Localized(@"C2C_Bank_card_number");
    self.remakeLable.text = Localized(@"C2C_Transfer_notes");
    
    self.timetitleLable.text = Localized(@"C2C_Order_validity_period");
    
    [self.remarksbutton setTitle:Localized(@"copy") forState:0];
    [self.bankNumberbutton setTitle:Localized(@"copy") forState:0];
    [self.capybranch setTitle:Localized(@"copy") forState:0];
    [self.copyname setTitle:Localized(@"copy") forState:0];
    [self.Alreadybutton setTitle:Localized(@"C2C_Already_paid") forState:0];
    [self.cancelbutton setTitle:Localized(@"C2C_Cancellation_order") forState:0];
    
    self.currency.textColor = MAINTITLECOLOR1;
    self.amount.textColor = MAINTITLECOLOR;
    self.unitPrice.textColor = MAINTITLECOLOR;
    self.number.textColor = MAINTITLECOLOR;
    self.bank.textColor = MAINTITLECOLOR;
    self.name.textColor = MAINTITLECOLOR;
    self.payment.textColor = MAINTITLECOLOR;
    self.time.textColor = MAINTITLECOLOR;
    self.bankNumber.textColor = MAINTITLECOLOR;
    self.bank.textColor = MAINTITLECOLOR;
    self.Branch.textColor = MAINTITLECOLOR;
    self.Remarks.textColor = MAINTITLECOLOR;
   
    
    NSString * color = [[ZBLocalized sharedInstance] AppbBGColor];
    if ([NSString isEmptyString:color] ||[color isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        self.bgImage.image = [UIImage imageNamed:@"img_fukuan_bg"];
        
    }else if ([color isEqualToString:WHITE_COLOR])//app风格为白色
    {
        self.bgImage.image = [UIImage imageNamed:@"img_fukuan_bg_1"];
    }
      [[NSNotificationCenter defaultCenter]postNotificationName:updateTradeNotificationse object:nil];
    if (_dic) {
         [self getData];
    }else
    {
        [self getData1];
    }
   [self dateTimeDifferenceWithStartTime:[EXUnit getLocalDateFormateUTCDate:_modle.expire_time] endTime:[EXUnit getNowTimeymd]];
    

    
}
- (void )dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = start - end;
    int second = (int)value %60;//秒
    int minute = (int)value /60%60;
    int house = (int)value / (24 *3600)%3600;
    int day = (int)value / (24 *3600);
    NSString *str;
    if (day > 0) {
        str = [NSString stringWithFormat:@"%d:%d:%d:%d",day,house,minute,second];
    }else if (day==0 && house >0) {
        str = [NSString stringWithFormat:@"%d:%d:%d",house,minute,second];
    }else if (day==0 && house==0 && minute>0) {
        
        str = [NSString stringWithFormat:@"00:%d:%d",minute,second];
        
    }else if (day==0 && house==0 && minute ==0 &&second >0){
        
        str = [NSString stringWithFormat:@"00:00:%d",second];
       
    }else if (day==0 && house==0 && minute ==0 &&second ==0)
    {
        _time.text = Localized(@"Current_elegationendTime");
        return;
    }else
    {
        _time.hidden = YES;
    }
    if (day>0)
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _time.day = [array[0] integerValue];
        _time.hour = [array[1] integerValue];
        _time.minute = [array[2] integerValue];
        _time.second = [array[3] integerValue];
        _time.textColor = [UIColor redColor];
    }else
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _time.hour = [array[0] integerValue];
        _time.minute = [array[1] integerValue];
        _time.second = [array[2] integerValue];
        _time.textColor = [UIColor redColor];
    }
   

   
}
- (void)getData1
{
    KViewRadius(_cancelbutton, 2);
    KViewRadius(_Alreadybutton, 2);
    _currency.text = [NSString stringWithFormat:Localized(@"C2C_pay_in_time"),_modle.asset];
    _amount.text = [NSString stringWithFormat:@"¥ %.2f",_modle.amount.floatValue *_modle.price.floatValue];
    _unitPrice.text =[NSString stringWithFormat:@"%@",_modle.price];
    _number.text =[NSString stringWithFormat:@"%@",_modle.amount];
    _payment.text = [EXUserManager userInfo].id;
    _name.text = _modle.merchant_name;
    _bank.text = _modle.merchant_bank;
    _Branch.text = _modle.merchant_sub_bank;
    _bankNumber.text = _modle.merchant_card_number;
    _Remarks.text = [NSString stringWithFormat:@"%@",[EXUserManager personalData].id];;
}
- (void)getData
{
    _currency.text = [NSString stringWithFormat:Localized(@"C2C_pay_in_time"),_modle.asset];
    _amount.text = [NSString stringWithFormat:@"¥ %.2f",_modle.amount.floatValue *_modle.price.floatValue];
    _unitPrice.text =[NSString stringWithFormat:@"%@",_modle.price];
    _number.text =[NSString stringWithFormat:@"%@",_modle.amount];
    _payment.text = Localized(@"C2C_Bank_payment");
    _name.text = _dic[@"name"];
    _bank.text = [NSString isEmptyString:_dic[@"bank"]]?@"":_dic[@"bank"];
    _Branch.text = [NSString isEmptyString:_dic[@"sub_bank"]]?@"":_dic[@"sub_bank"];
    _bankNumber.text = [NSString isEmptyString:_dic[@"card_number"]]?@"":_dic[@"card_number"];
    _Remarks.text = [NSString stringWithFormat:@"%@",[EXUserManager personalData].id];;
    
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)Alreadybutton:(UIButton *)sender {
    [self updateTradeWithaction:@"confirm"];
}

- (IBAction)cancelbutton:(UIButton *)sender {
    [self updateTradeWithaction:@"cancel"];
}
- (void)updateTradeWithaction:(NSString *)action
{
    WeakSelf
    [self POSTWithHost:@"" path:updateTrade param:@{@"id":_modle.ID,@"action":action} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
             [[NSNotificationCenter defaultCenter]postNotificationName:updateTradeNotificationse object:nil];
             [self.navigationController popViewControllerAnimated:YES];
             [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"C2C_success") view:kWindow];
        }else
        {
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (IBAction)remarks:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.Remarks.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)bankNumber:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.bankNumber.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)capybranch:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.Branch.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
- (IBAction)copyName:(UIButton *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.name.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
@end
