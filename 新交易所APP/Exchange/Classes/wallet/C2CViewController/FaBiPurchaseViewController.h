//
//  FaBiPurchaseViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "LcButton.h"
@interface FaBiPurchaseViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *rightLable1;
@property (weak, nonatomic) IBOutlet UILabel *rightLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable1;
@property (weak, nonatomic) IBOutlet UILabel *titleLable2;
@property (weak, nonatomic) IBOutlet UILabel *titleLable3;
@property (weak, nonatomic) IBOutlet UILabel *titleLable4;
@property (weak, nonatomic) IBOutlet UILabel *messageLable;
@property (weak, nonatomic) IBOutlet UILabel *tipsLable;
@property (weak, nonatomic) IBOutlet UILabel *tipsContontLable;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet UIView *line3;
@property (weak, nonatomic) IBOutlet UILabel *assetlable;
@property (weak, nonatomic) IBOutlet LcButton *buybutton;
@property (weak, nonatomic) IBOutlet UILabel *CNY_Price;
@property (weak, nonatomic) IBOutlet UITextField *CNT_numberField;
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UILabel *bankLable;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)buyButton:(LcButton *)sender;
- (IBAction)ChooseButton:(UIButton *)sender;

@end
