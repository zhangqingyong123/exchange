//
//  CompletedOrderViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CompletedOrderViewController.h"
#import "OrderTableViewCell.h"
#import "WOrderInfoViewController.h"
@interface CompletedOrderViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * dataAry;
@property (nonatomic,strong)UITableView * tableView;
@property (assign, nonatomic) NSUInteger     pages;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation CompletedOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    // 上拉加载
    WeakSelf
    WLRefreshGifHeader *header = [WLRefreshGifHeader headerWithRefreshingBlock:^{
        weakSelf.pages = 0;
        [weakSelf getData];
        
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
    EXRefrechFootview *fooder = [EXRefrechFootview footerWithRefreshingBlock:^{
        weakSelf.pages++;
        [weakSelf getData];
    }];
    self.tableView.mj_footer = fooder;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update) name:updateTradeNotificationse object:nil];
   
}
- (void)update
{
    _pages = 0;
    [self getData];
}
#pragma mark----数据
- (void)getData
{
    WeakSelf
    [HomeViewModel getTradeListWithDic:@{@"status":@(3),@"offset":@(weakSelf.pages*10),@"limit":@(10)} url:gettrade callBack:^(BOOL isSuccess, id callBack) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if (isSuccess) {
            NSArray *list = callBack[@"result"];
            if (weakSelf.pages > 0)
            {
                [weakSelf.dataAry addObjectsFromArray:[WOrderModle mj_objectArrayWithKeyValuesArray:list]];
                if (list.count<10)
                {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else
            {
                weakSelf.dataAry = [WOrderModle mj_objectArrayWithKeyValuesArray:list];
                if (list.count<10) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }
            [weakSelf.tableView reloadData];
            [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:NO];
        }else
        {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [weakSelf axcBasePopUpWarningAlertViewWithMessage:Localized(@"C2C_not_login") view:kWindow];
            if ([EXUserManager isLogin]) {
                [self placeholderViewWithFrame:weakSelf.tableView.frame NoNetwork:YES];
            }
        }
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (_dataAry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        if (NoNetwork) {
            WeakSelf
            _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
                [weakSelf getData];
            };
        }
        
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -49 -64 -IPhoneTop -RealValue_W(88))];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[OrderTableViewCell class] forCellReuseIdentifier:@"OrderTableViewCell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataAry
{
    if (!_dataAry) {
        _dataAry = [[NSMutableArray alloc]init];
    }
    return _dataAry;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_H(229);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"OrderTableViewCell"];
    cell.modle = _dataAry[indexPath.row];
    cell.typebutton.text = Localized(@"C2C_Completed");
    CGSize highsize = [cell.typebutton sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
    cell.typebutton.height = 16;
    cell.typebutton.width =  highsize.width +6;
    cell.typebutton.right = cell.toalLable.right;
    cell.typebutton.centerY = cell.purchaseLable.centerY;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WOrderModle * modle = _dataAry[indexPath.row];
    WOrderInfoViewController * orderVC = [[WOrderInfoViewController alloc] init];
    orderVC.modle = modle;
    [self.navigationController pushViewController:orderVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
