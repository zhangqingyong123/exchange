//
//  WOrderInfoViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "WOrderInfoViewController.h"

@interface WOrderInfoViewController ()

@end

@implementation WOrderInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBGCOLOR;
    
    self.currency.textColor = MAINTITLECOLOR1;
    switch (_modle.status.integerValue) {
        case 1:
       
            self.titleLable.text = Localized(@"C2C_Unpaid");
             self.titleLable.textColor = MAINTITLECOLOR;
            break;
        case 2:
            self.titleLable.text = Localized(@"C2C_Already_paid");
             self.titleLable.textColor = MAINTITLECOLOR;
            break;
        case 3:
            self.titleLable.text = Localized(@"C2C_Order_completed");
            self.titleLable.textColor = MAINTITLECOLOR;
            break;
        case 4:
        {
            if (_modle.cancel_reason.integerValue ==1)
            {
         
                 self.titleLable.text = Localized(@"Current_shoudelegation");
            }else if (_modle.cancel_reason.integerValue ==2)
            {
                self.titleLable.text = Localized(@"Current_elegationendTime");
              
            }else
            {

                 self.titleLable.text = Localized(@"Current_delegation");
            }
             self.titleLable.textColor = [UIColor redColor];
            
        }
            
            break;
        case 5:
            self.titleLable.text = Localized(@"C2C_Order_completed");
            self.titleLable.textColor = MAINTITLECOLOR;
            break;
        case -1:
            self.titleLable.text = Localized(@"C2C_Order_completed");
            self.titleLable.textColor = MAINTITLECOLOR;
            break;
            
        default:
            break;
    }
    

    self.titleLable1.text = Localized(@"C2C_Total");
    self.titleLable2.text = Localized(@"C2C_Transaction_unit_price");
    self.titleLable3.text = Localized(@"C2C_Volume_of_transactions");
    self.orderTimetitleLable.text = Localized(@"C2C_Order_time");
    self.ordernumbertitleLable.text = Localized(@"C2C_Order_number");
    self.NametitleLable.text = Localized(@"C2C_seller");
    self.PaymenttitleLable.text = Localized(@"C2C_Payment_method");
    self.AmounttitleLable.text = Localized(@"C2C_Amount_of_payment");
    self.timetitleLable.text = Localized(@"C2C_Time_of_payment");
    self.releasetitleLable.text = Localized(@"C2C_Time_of_currency");
    self.remakeLable.text = Localized(@"address_Remarks");
    self.phoneTitle.text = Localized(@"Contact_information");
    [self.copybutton setTitle:Localized(@"copy") forState:0];
    [self.copyphoneButton setTitle:Localized(@"copy") forState:0];
    [self.copyname setTitle:Localized(@"copy") forState:0];
    [self.cardTitle setTitle:Localized(@"copy") forState:0];
    self.backNumberTitle.text = Localized(@"C2C_Bank_card_number");
    self.amount.textColor = MAINTITLECOLOR;
    self.unitPrice.textColor = MAINTITLECOLOR;
    self.number.textColor = MAINTITLECOLOR;
    self.time.textColor = MAINTITLECOLOR;
    self.orderNumber.textColor = MAINTITLECOLOR;
    self.name.textColor = MAINTITLECOLOR;
    self.payment.textColor = MAINTITLECOLOR;
    self.amountpayment.textColor = MAINTITLECOLOR;
    self.transactiontime.textColor = MAINTITLECOLOR;
    self.outTime.textColor = MAINTITLECOLOR;
    self.remake.textColor = MAINTITLECOLOR;
    self.phone.textColor = MAINTITLECOLOR;
    self.backNumber.textColor = MAINTITLECOLOR;
    NSString * color = [[ZBLocalized sharedInstance] AppbBGColor];
    if ([NSString isEmptyString:color] ||[color isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        self.bgImage.image = [UIImage imageNamed:@"img_complete_bg"];
        
    }else if ([color isEqualToString:WHITE_COLOR])//app风格为白色
    {
        self.bgImage.image = [UIImage imageNamed:@"img_complete_bg_1"];
    }
    [self getdata];
    
}
- (void)getdata
{
    
    _currency.text = [NSString stringWithFormat:@"%@ %@",(_modle.type.integerValue ==1)? Localized(@"C2C_buy"):Localized(@"C2C_sell"),_modle.asset];
    _amount.text = [NSString stringWithFormat:@"¥ %.2f",_modle.amount.doubleValue *_modle.price.doubleValue];
    _unitPrice.text =[NSString stringWithFormat:@"%@",_modle.price];
    _number.text =[NSString stringWithFormat:@"%@",_modle.amount];
    _time.text = [NSString stringWithFormat:@"%@",[EXUnit getLocalDateFormateUTCDate:_modle.CreatedAt]];
    _orderNumber.text = _modle.order_no;
    _name.text = _modle.merchant_name;
    _payment.text = Localized(@"C2C_Bank_payment");
    _amountpayment.text = _amount.text;
    _transactiontime.text = [NSString stringWithFormat:@"%@",[EXUnit getLocalDateFormateUTCDate:_modle.UpdatedAt]];
    _outTime.text = [NSString stringWithFormat:@"%@",[EXUnit getLocalDateFormateUTCDate:_modle.UpdatedAt]];
    _remake.text = [NSString stringWithFormat:@"%@",[EXUserManager personalData].id];
    _phone.text = [NSString isEmptyString:_modle.contact_way]?@"":_modle.contact_way;
     _backNumber.text = _modle.merchant_card_number;
}
- (IBAction)copyButton:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string =  _orderNumber.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)copyPhone:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.phone.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)copyback:(UIButton *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.backNumber.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
- (IBAction)copyname:(UIButton *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.name.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
@end
