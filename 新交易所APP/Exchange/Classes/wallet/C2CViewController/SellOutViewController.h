//
//  SellOutViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "LcButton.h"
@interface SellOutViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *rightLable1;
@property (weak, nonatomic) IBOutlet UILabel *rightLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable1;
@property (weak, nonatomic) IBOutlet UILabel *titleLable2;
@property (weak, nonatomic) IBOutlet UILabel *titleLable3;
@property (weak, nonatomic) IBOutlet UILabel *titleLable4;
@property (weak, nonatomic) IBOutlet UILabel *messageLable;
@property (weak, nonatomic) IBOutlet UILabel *tipsLable;
@property (weak, nonatomic) IBOutlet UILabel *tipsContontLable;
@property (weak, nonatomic) IBOutlet UILabel *assetLable;
@property (weak, nonatomic) IBOutlet UILabel *sellOutPrice;
@property (weak, nonatomic) IBOutlet UITextField *sellOutNumber;
@property (weak, nonatomic) IBOutlet UITextField *sellOutToal;
@property (weak, nonatomic) IBOutlet UILabel *bankLable;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet UIView *line3;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)sellOutbutton:(LcButton *)sender;
@property (weak, nonatomic) IBOutlet LcButton *sellOutbutton;
- (IBAction)Choosebutton:(UIButton *)sender;

@end
