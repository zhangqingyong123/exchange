//
//  CellOrderinfoViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CellOrderinfoViewController.h"

@interface CellOrderinfoViewController ()

@end

@implementation CellOrderinfoViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.bgView.backgroundColor = MainBGCOLOR;
    
    self.titleLable.textColor = MAINTITLECOLOR;
    self.titleLable.text = Localized(@"C2C_Sell_sure");
    self.titleContontlable.textColor = MAINTITLECOLOR1;
    self.titleContontlable.text = Localized(@"C2C_transfer");
    self.titleLable1.text = Localized(@"C2C_Total");
    self.titleLable2.text = Localized(@"C2C_Sell_price");
    self.titleLable3.text = Localized(@"C2C_Sell_num");
    self.bankmodetitleLable.text = Localized(@"C2C_Payment_method");
    self.nametitleLable.text = Localized(@"C2C_Payee");
    self.bankNametitleLable.text = Localized(@"C2C_bank");
    self.classtitleLable.text = Localized(@"C2C_Branch");
    self.banktitleLable.text = Localized(@"C2C_Bank_card_number");
    self.timetitleLable.text = Localized(@"C2C_Order_validity_period");
    self.remakeLable.text = Localized(@"address_Remarks");
    self.phoneTitle.text = Localized(@"Contact_information");
    [self.okbutton setTitle:Localized(@"transaction_OK") forState:0];
    [self.copyphone setTitle:Localized(@"copy") forState:0];
    [self.copybank setTitle:Localized(@"copy") forState:0];
    [self.copyid setTitle:Localized(@"copy") forState:0];
    [self.copyname setTitle:Localized(@"copy") forState:0];
    self.totalPrice.textColor = MAINTITLECOLOR;
    self.sellPrice.textColor = MAINTITLECOLOR;
    self.cellNum.textColor = MAINTITLECOLOR;
    self.bank.textColor = MAINTITLECOLOR;
    self.name.textColor = MAINTITLECOLOR;
    self.bankname.textColor = MAINTITLECOLOR;
    self.classbank.textColor = MAINTITLECOLOR;
    self.banknumber.textColor = MAINTITLECOLOR;
    self.remake.textColor = MAINTITLECOLOR;
    self.phone.textColor = MAINTITLECOLOR;
    self.timetitleLable.text = Localized(@"C2C_Order_validity_period");

    
    NSString * color = [[ZBLocalized sharedInstance] AppbBGColor];
    if ([NSString isEmptyString:color] ||[color isEqualToString:BLACK_COLOR]) //app风格为默认色
    {
        self.bgImageview.image = [UIImage imageNamed:@"img_fukuan_bg"];
        
    }else if ([color isEqualToString:WHITE_COLOR])//app风格为白色
    {
        self.bgImageview.image = [UIImage imageNamed:@"img_fukuan_bg_1"];
    }
    if (_dic) {
        [self getData];
    }else
    {
        [self getData1];
    }
//   [self dateTimeDifferenceWithStartTime:[EXUnit getLocalDateFormateUTCDate:_modle.expire_time] endTime:[EXUnit getNowTimeymd]];
    
}
- (void )dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = start - end;
    int second = (int)value %60;//秒
    int minute = (int)value /60%60;
    int house = (int)value / (24 *3600)%3600;
    int day = (int)value / (24 *3600);
    NSString *str;
    if (day > 0) {
        str = [NSString stringWithFormat:@"%d:%d:%d:%d",day,house,minute,second];
    }else if (day==0 && house >0) {
        str = [NSString stringWithFormat:@"%d:%d:%d",house,minute,second];
    }else if (day==0 && house==0 && minute>0) {
        
        str = [NSString stringWithFormat:@"00:%d:%d",minute,second];
        
    }else if (day==0 && house==0 && minute ==0 &&second >0){
        
        str = [NSString stringWithFormat:@"00:00:%d",second];
        
    }else if (day==0 && house==0 && minute ==0 &&second ==0)
    {
        _time.text = Localized(@"Current_elegationendTime");
        return;
    }else
    {
        _time.hidden = YES;
        _titleLable.hidden = YES;
    }
    if (day>0)
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _time.day = [array[0] integerValue];
        _time.hour = [array[1] integerValue];
        _time.minute = [array[2] integerValue];
        _time.second = [array[3] integerValue];
        _time.textColor = [UIColor redColor];
    }else
    {
        NSArray *array = [str componentsSeparatedByString:@":"]; //从字符A中分隔成2个元素的数组
        _time.hour = [array[0] integerValue];
        _time.minute = [array[1] integerValue];
        _time.second = [array[2] integerValue];
        _time.textColor = [UIColor redColor];
    }
    
    
    
}
- (void)getData1
{
    
    KViewRadius(_okbutton, 2);
    _totalPrice.text = [NSString stringWithFormat:@"¥ %.2f",_modle.amount.floatValue *_modle.price.floatValue];
    _cellNum.text = [NSString stringWithFormat:@"%@",_modle.amount];
    _sellPrice.text = [NSString stringWithFormat:@"%@",_modle.price];
    _bank.text = Localized(@"C2C_Bank_payment");
    _name.text = _modle.merchant_name;
    _bankname.text = [NSString isEmptyString:_modle.merchant_bank]?@"":_modle.merchant_bank;
    _classbank.text = [NSString isEmptyString: _modle.merchant_sub_bank]?@"": _modle.merchant_sub_bank;
    _banknumber.text = [NSString isEmptyString:_modle.merchant_card_number]?@"":_modle.merchant_card_number;
    _remake.text = [NSString stringWithFormat:@"%@",[EXUserManager personalData].id];;
    _phone.text = [NSString isEmptyString:_modle.contact_way]?@"":_modle.contact_way;
}
- (void)getData
{
    KViewRadius(_okbutton, 2);
    _totalPrice.text = [NSString stringWithFormat:@"¥ %.2f",_modle.amount.floatValue *_modle.price.floatValue];
    _cellNum.text = [NSString stringWithFormat:@"%@",_modle.amount];
    _sellPrice.text = [NSString stringWithFormat:@"%@",_modle.price];
    _bank.text = Localized(@"C2C_Bank_payment");
    _name.text = _dic[@"name"];
    _bankname.text = [NSString isEmptyString:_dic[@"bank"]]?@"":_dic[@"bank"];
    _classbank.text = [NSString isEmptyString:_dic[@"sub_bank"]]?@"":_dic[@"sub_bank"];
    _banknumber.text = [NSString isEmptyString:_dic[@"card_number"]]?@"":_dic[@"card_number"];
    _remake.text = [NSString stringWithFormat:@"%@",[EXUserManager personalData].id];
    _phone.text = [NSString isEmptyString:_dic[@"contact_way"]]?@"":_dic[@"contact_way"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)okbutton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)copyNameClick:(UIButton *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.name.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)copyClassbank:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.classbank.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)copyBankNum:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.banknumber.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}

- (IBAction)copyPhone:(UIButton *)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.phone.text;
    [self axcBasePopUpWarningAlertViewWithMessage:Localized(@"Copy_to_paste") view:self.view];
}
@end
