//
//  CellOrderinfoViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "WOrderModle.h"
#import "TimeLable.h"
@interface CellOrderinfoViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *titleContontlable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable1;
@property (weak, nonatomic) IBOutlet UILabel *titleLable2;
@property (weak, nonatomic) IBOutlet UILabel *titleLable3;

@property (weak, nonatomic) IBOutlet UILabel *bankmodetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *nametitleLable;
@property (weak, nonatomic) IBOutlet UILabel *bankNametitleLable;
@property (weak, nonatomic) IBOutlet UILabel *classtitleLable;
@property (weak, nonatomic) IBOutlet UILabel *banktitleLable;
@property (weak, nonatomic) IBOutlet UILabel *timetitleLable;
@property (weak, nonatomic) IBOutlet UILabel *remakeLable;
@property (weak, nonatomic) IBOutlet TimeLable *time;
@property (weak, nonatomic) IBOutlet UILabel *phoneTitle;

@property (weak, nonatomic) IBOutlet UILabel *totalPrice;
@property (weak, nonatomic) IBOutlet UILabel *sellPrice;
@property (weak, nonatomic) IBOutlet UILabel *cellNum;
@property (weak, nonatomic) IBOutlet UILabel *bank;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *bankname;
@property (weak, nonatomic) IBOutlet UILabel *classbank;
@property (weak, nonatomic) IBOutlet UILabel *banknumber;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageview;
@property (weak, nonatomic) IBOutlet UILabel *remake;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UIButton *copyphone;
@property (weak, nonatomic) IBOutlet UIButton *copybank;
@property (weak, nonatomic) IBOutlet UIButton *copyid;
@property (weak, nonatomic) IBOutlet UIButton *copyname;
- (IBAction)okbutton:(UIButton *)sender;
- (IBAction)copyNameClick:(UIButton *)sender;

- (IBAction)copyClassbank:(UIButton *)sender;
- (IBAction)copyBankNum:(UIButton *)sender;
- (IBAction)copyPhone:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *okbutton;
@property (nonatomic,strong)WOrderModle * modle;
@property (nonatomic,strong)NSDictionary * dic;
@end
