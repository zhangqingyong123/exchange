//
//  SubmitOrderViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/7.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface SubmitOrderViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UILabel *classlable;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
- (IBAction)choosebutton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
