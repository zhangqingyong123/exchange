//
//  WalletViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/12.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "WalletViewController.h"
#import "FaBiPurchaseViewController.h"
#import "SellOutViewController.h"
#import "UnfinishedOrderViewController.h"
#import "CompletedOrderViewController.h"
#import "LoginViewController.h"
#import "MDMultipleSegmentView.h"
#import "FaBiPageView.h"
#import "JGPopView.h"
#import "OTC_BiPurchaseVC.h"
#import "OTC_SellOutVC.h"
#import "OTC_UnfinishedOrderVC.h"
#import "OTC_CompletedOrderVC.h"
@interface WalletViewController ()<MDMultipleSegmentViewDeletegate,selectIndexPathDelegate>
@property(nonatomic,strong) FaBiPageView *C2CcollectView;
@property(nonatomic,strong) FaBiPageView *OTCcollectView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) UIButton *chooseButton;
@end
@implementation WalletViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[AppDelegate shareAppdelegate].privilages isEqualToString:@"4,8"]) {
        [self.dataArray addObject:@"C2C"];
        [self.dataArray addObject:@"OTC"];
        [self.view addSubview:self.OTCcollectView];
        [self.view addSubview:self.C2CcollectView];
    }else if ([[AppDelegate shareAppdelegate].privilages isEqualToString:@"4"])
    {
        [self.dataArray addObject:@"C2C"];
        [self.view addSubview:self.C2CcollectView];
    }else if ([[AppDelegate shareAppdelegate].privilages isEqualToString:@"8"])
    {
        [self.dataArray addObject:@"OTC"];
        [self.view addSubview:self.OTCcollectView];
    }else
    {
        [self.dataArray addObject:@"C2C"];
        [self.view addSubview:self.C2CcollectView];
    }
    self.navigationItem.titleView =self.chooseButton;

   
//     self.navigationItem.rightBarButtonItem = [UIBarButtonItem RightitemWithImageNamed:@"" title:@"提交工单" target:self action:@selector(subit)];
    if ([[EXUnit getSite] isEqualToString:@"ZG"]) {
        [self axcBaseAddRightBtnWithImage:[UIImage imageNamed:@"b_us"]];
    }
    
}
- (UIButton *)chooseButton
{
    if (!_chooseButton) {
        _chooseButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 140, 30)];
        [_chooseButton setTitle:self.dataArray[0] forState:0];
        _chooseButton.titleLabel.font = AutoBoldFont(18);
        [_chooseButton setTitleColor:MAINTITLECOLOR forState:0];
        [_chooseButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        if (_dataArray.count >1)
        {
            [_chooseButton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].paymenticonDowm] forState:0];
            [_chooseButton centerHorizontallyImageAndTextWithPadding:-60];
        }
       
    }
    return _chooseButton;
}
- (void)buttonClick:(UIButton *)button
{
    if (self.dataArray.count >1)
    {
        CGPoint point = CGPointMake(UIScreenWidth/2 + 20,kStatusBarAndNavigationBarHeight);
        CGFloat with = RealValue_W(160);
        JGPopView *view2 = [[JGPopView alloc] initWithOrigin:point Width:with Height:_dataArray.count * RealValue_W(80) Type:JGTypeOfUpRight Color:TRABSACTIONColor];
        view2.dataArray = _dataArray;
        view2.row_height = 40;
        view2.delegate = self;
        [view2 popView];
        [kWindow addSubview:view2];
    }
   
}
- (void)selectIndexPathRow:(NSInteger)index {
    
    [_chooseButton setTitle:self.dataArray[index] forState:0];
    WeakSelf
    if ([self.dataArray[index] isEqualToString:@"OTC"])
    {
        [UIView animateWithDuration:1.2 animations:^{
            if ([[EXUnit getSite] isEqualToString:@"ZT"]) {
                [EXUnit showMessage:Localized(@"OTC_Taiwan_user_message")];
            }
            weakSelf.C2CcollectView.hidden = YES;
            weakSelf.OTCcollectView.hidden = NO;
        }];
    }else if ([self.dataArray[index] isEqualToString:@"C2C"])
    {
        [UIView animateWithDuration:1.2 animations:^{
            weakSelf.C2CcollectView.hidden = NO;
            weakSelf.OTCcollectView.hidden = YES;
        }]; 
    }
}

// 联系客服
- (void)axcBaseClickBaseRightImageBtn:(UIButton *)sender{
    QYSource *source = [[QYSource alloc] init];
    source.title = @"ZG.com";
    source.urlString = @"home-notice";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
    sessionViewController.sessionTitle = @"ZG.com";
    sessionViewController.source = source;
    sessionViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sessionViewController animated:YES];
    
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
        
    }
    return _dataArray;
}
- (FaBiPageView *)OTCcollectView
{
    if (!_OTCcollectView) {
        _OTCcollectView = [[FaBiPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight- RealValue_H(88))
                                                   withTitles:@[Localized(@"C2C_buy"),Localized(@"C2C_sell"),Localized(@"C2C_Incomplete_order"),Localized(@"C2C_Completed_order")]
                                          withViewControllers:@[@"OTC_BiPurchaseVC",@"OTC_SellOutVC",@"OTC_UnfinishedOrderVC",@"OTC_CompletedOrderVC"]
                                               withParameters:nil];
        
        _OTCcollectView.selectedColor = WHITECOLOR;
        _OTCcollectView.unselectedColor = maintitleLCOLOR ;
        _OTCcollectView.backgroundColor = MAINBLACKCOLOR;
        
        
    }
    return _OTCcollectView;
}
- (FaBiPageView *)C2CcollectView {
    
    if (!_C2CcollectView) {
        _C2CcollectView = [[FaBiPageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight- RealValue_H(88))
                                             withTitles:@[Localized(@"C2C_buy"),Localized(@"C2C_sell"),Localized(@"C2C_Incomplete_order"),Localized(@"C2C_Completed_order")]
                                    withViewControllers:@[@"FaBiPurchaseViewController",@"SellOutViewController",@"UnfinishedOrderViewController",@"CompletedOrderViewController"]
                                         withParameters:nil];
        
        _C2CcollectView.selectedColor = WHITECOLOR;
        _C2CcollectView.unselectedColor = maintitleLCOLOR ;
        _C2CcollectView.backgroundColor = MAINBLACKCOLOR;
    }
    return _C2CcollectView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
