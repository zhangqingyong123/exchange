//
//  OrderTableViewCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOrderModle.h"
@interface OrderTableViewCell : UITableViewCell
/*买入*/
@property (nonatomic,strong)UILabel *purchaseLable;
/*状态*/
@property (nonatomic,strong)UILabel *typebutton;
/*价格*/
@property (nonatomic,strong)UILabel *priceLable;
/*数量*/
@property (nonatomic,strong)UILabel *numberLable;
/*总金额*/
@property (nonatomic,strong)UILabel *toalLable;
@property (nonatomic,strong)WOrderModle *modle;
@end
