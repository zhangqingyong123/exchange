//
//  OrderTableViewCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "OrderTableViewCell.h"
#define titleWith (UIScreenWidth-RealValue_W(40))/3
@implementation OrderTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(0), 0, RealValue_W(0))];
        
    }
    return self;
}
- (void)setModle:(WOrderModle *)modle
{
    if (modle.type.integerValue ==1) {
        _purchaseLable.textColor =  TABTITLECOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@%@ %@",Localized(@"C2C_buy"),modle.asset,[EXUnit getLocalDateFormateUTCDate:modle.CreatedAt]]
                                                      attributedString:[EXUnit getLocalDateFormateUTCDate:modle.CreatedAt]
                                                                 color:MAINTITLECOLOR font:AutoFont(11)];
    }else
    {
        _purchaseLable.textColor =  DIEECOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@%@ %@",Localized(@"C2C_sell"),modle.asset,[EXUnit getLocalDateFormateUTCDate:modle.CreatedAt]]
                                                      attributedString:[EXUnit getLocalDateFormateUTCDate:modle.CreatedAt]
                                                                 color:MAINTITLECOLOR1 font:AutoFont(11)];
    }
    _priceLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"hone_price"),[EXUserManager getValuationMethod],modle.price]
                                                    attributedString:modle.price
                                                               color:MAINTITLECOLOR
                                                                font:AutoFont(15)];
    _numberLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"hone_num"),modle.asset,modle.amount]
                                                     attributedString:modle.amount
                                                                color:MAINTITLECOLOR
                                                                 font:AutoFont(15)];
    _numberLable.textAlignment = NSTextAlignmentCenter;
    
    _toalLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"C2C_Actual_transaction"),[EXUserManager getValuationMethod],modle.amount]
                                                   attributedString:modle.amount
                                                              color:MAINTITLECOLOR
                                                               font:AutoFont(15)];
    _toalLable.textAlignment = NSTextAlignmentRight;
    switch (modle.status.integerValue) {
        case 1:
            _typebutton.text = Localized(@"C2C_Unpaid");
            break;
        case 2:
             _typebutton.text = Localized(@"C2C_Already_paid");
            break;
        case 3:
            _typebutton.text = Localized(@"C2C_Have_done_deal");
            break;
        case 4:
        {
            if (modle.cancel_reason.integerValue ==1)
            {
                _typebutton.text = Localized(@"Current_shoudelegation");
            }else if (modle.cancel_reason.integerValue ==2)
            {
                  _typebutton.text = Localized(@"Current_elegationendTime");
                
            }else
            {
                 _typebutton.text = Localized(@"Current_delegation");
                
            }
            
        }
            
            break;
        case 5:
            _typebutton.text = Localized(@"C2C_Audit");
          
            break;
        case -1:
            _typebutton.text = Localized(@"C2C_Hang_the_air");
           
            break;
            
        default:
            break;
    }
    
    CGSize highsize = [_typebutton sizeThatFits:CGSizeMake(1000, MAXFLOAT)];
    _typebutton.height = 16;
    _typebutton.width =  highsize.width +6;
    _typebutton.right = _toalLable.right;
    _typebutton.centerY = _purchaseLable.centerY;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.purchaseLable];
    
    [self.contentView addSubview:self.priceLable];
    [self.contentView addSubview:self.numberLable];
    [self.contentView addSubview:self.toalLable];
    [self.contentView addSubview:self.typebutton];
}
-(UILabel *)purchaseLable
{
    if (!_purchaseLable) {
        _purchaseLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_W(42),RealValue_W(600), RealValue_W(34))];
        
        _purchaseLable.font = AutoFont(16);
        
    }
    return _purchaseLable;
    
}
-(UILabel *)typebutton
{
    if (!_typebutton) {
        _typebutton = [[UILabel alloc] init];
        _typebutton.font = AutoFont(11);
        _typebutton.textColor = WHITECOLOR;
        _typebutton.backgroundColor = DIEECOLOR;
        _typebutton.layer.masksToBounds     =YES;
        _typebutton.layer.cornerRadius      =2;
        _typebutton.textAlignment = NSTextAlignmentCenter;
    }
    return _typebutton;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20),_purchaseLable.bottom + RealValue_W(38),titleWith, RealValue_W(90))];
        _priceLable.textColor =  MAINTITLECOLOR1;
        _priceLable.font = AutoFont(12);
        _priceLable.numberOfLines = 0;
        
    }
    return _priceLable;
    
}
- (UILabel *)numberLable
{
    if (!_numberLable) {
        _numberLable = [[UILabel alloc] initWithFrame:CGRectMake(_priceLable.right,_priceLable.mj_y,titleWith, RealValue_W(90))];
        _numberLable.textColor =  MAINTITLECOLOR1;
        _numberLable.font = AutoFont(12);
        
        _numberLable.numberOfLines = 0;
        
    }
    return _numberLable;
    
}
- (UILabel *)toalLable
{
    if (!_toalLable) {
        _toalLable = [[UILabel alloc] initWithFrame:CGRectMake(_numberLable.right,_priceLable.mj_y,titleWith, RealValue_W(90))];
        _toalLable.textColor =  MAINTITLECOLOR1;
        _toalLable.font = AutoFont(12);
        
        _toalLable.numberOfLines = 0;
        
    }
    return _toalLable;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
