//
//  CountryCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "CountryCell.h"

@implementation CountryCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        
        [self setSeparatorInset:UIEdgeInsetsMake(0, RealValue_W(20), 0, RealValue_W(20))];
        [self.contentView addSubview:self.titlelable];
        [self.contentView addSubview:self.rightlable];
    }
    return self;
    
}
- (void)setModel:(CountryModle *)model
{

    if ([[EXUnit  isLocalizable]  hasPrefix:@"zh-Hant"] || [[EXUnit  isLocalizable]  hasPrefix:@"zh-Hans"] ) {
        
       self.titlelable.text = [NSString stringWithFormat:@"%@",model.name_cn];
        self.rightlable.text = [NSString stringWithFormat:@"%@",model.area_code];
        
    }else
    {
        self.titlelable.text = [NSString stringWithFormat:@"%@",model.name_en];
        self.rightlable.text = [NSString stringWithFormat:@"%@",model.area_code];
    }
    if (_index == model.ID.integerValue)
    {
        self.titlelable.textColor = TABTITLECOLOR;
        self.rightlable.textColor = TABTITLECOLOR;
    }else
    {
        self.titlelable.textColor = MAINTITLECOLOR;
        self.rightlable.textColor = MAINTITLECOLOR1;
    }
    
   
}
- (UILabel *)titlelable
{
    if (!_titlelable) {
        _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 240, 50)];
        _titlelable.font = AutoBoldFont(14);
        _titlelable.textColor = MAINTITLECOLOR;
        _titlelable.textAlignment = NSTextAlignmentLeft;
    }
    return _titlelable;
}
- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth -15 -140, 0, 140, 50)];
        _rightlable.font = AutoBoldFont(14);
        _rightlable.textColor = MAINTITLECOLOR1;
        _rightlable.textAlignment = NSTextAlignmentRight;
    }
    return _rightlable;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
