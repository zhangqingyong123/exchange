//
//  CountryCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/12/25.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryModle.h"
@interface CountryCell : UITableViewCell
@property (nonatomic,strong)UILabel * titlelable;
@property (nonatomic,strong)UILabel * rightlable;
@property (nonatomic,strong)CountryModle * model;
@property (nonatomic,assign)NSInteger  index;
@end

