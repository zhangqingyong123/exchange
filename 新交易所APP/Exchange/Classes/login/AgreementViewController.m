//
//  AgreementViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "AgreementViewController.h"
#import "TGWebProgressLayer.h"
@interface AgreementViewController ()
@property (nonatomic, strong)TGWebProgressLayer *webProgressLayer;
@end

@implementation AgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
    self.bgimage.backgroundColor = TABLEVIEWLCOLOR;
    self.titleLable.textColor = MAINTITLECOLOR;
    self.titleLable.text = Localized(@"register_Agreementinfo");
    self.titleL.textColor =MAINTITLECOLOR;
    [self.backbutton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].loginbackicon] forState:0];
    _textView.editable = NO;
    
    _textView.showsVerticalScrollIndicator = NO;
  
    [self getMessageInfo];
    self.webProgressLayer = [[TGWebProgressLayer alloc] init];
    self.webProgressLayer.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight - 2, UIScreenWidth, 2);
    self.webProgressLayer.strokeColor =  TABTITLECOLOR.CGColor;
    [self.view.layer addSublayer:self.webProgressLayer];
    [self.webProgressLayer tg_startLoad];
}
#pragma mark----数据
- (void)getMessageInfo
{
    WeakSelf
    NSString * str = [NSString stringWithFormat:@"%@/user-agreement",about];
    [self GETWithHost:@"" path:str param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
             NSString * result = responseObject[@"result"][@"content"];

            //验证成功，主线程处理UI
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                // 处理耗时操作的代码块...
                 NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[result dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                //通知主线程刷新
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    weakSelf.textView.attributedText = attrStr;
                    weakSelf.textView.textColor = RGBA(114, 145, 161, 1);
                     [self.webProgressLayer tg_finishedLoadWithError:nil];
                });
                
            });
        }else
        {
            [self axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
        }
        
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {  
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClick:(UIButton *)sender {
    CATransition *animation = [CATransition animation];
    animation.duration = 1.0;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"rippleEffect";
    //animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:animation forKey:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
