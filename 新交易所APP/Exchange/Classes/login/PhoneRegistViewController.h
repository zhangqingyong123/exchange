//
//  PhoneRegistViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "QCCountdownButton.h"
#import "LcButton.h"
typedef enum : NSUInteger {
    RegistPhoneType = 0,//注册
    passWordPhoneType = 1,  //找回密码
}PhoneType;

@interface PhoneRegistViewController : BaseViewViewController
// 第一步
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *lineView1;

@property (weak, nonatomic) IBOutlet UILabel *cityLable;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;

@property (weak, nonatomic) IBOutlet UILabel *loginlable;
@property (weak, nonatomic) IBOutlet LcButton *nextButton;
@property (nonatomic,readwrite)PhoneType phoneType;
- (IBAction)nextButton:(LcButton *)sender;
- (IBAction)citybutton:(UIButton *)sender;

//第二部注册
@property (strong, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UIView *lineView2;
@property (weak, nonatomic) IBOutlet UIView *nextlineView;
@property (weak, nonatomic) IBOutlet UIView *nextlineView1;
@property (weak, nonatomic) IBOutlet UIView *nextlineView2;
@property (weak, nonatomic) IBOutlet UIView *nextlineView3;
@property (weak, nonatomic) IBOutlet UIButton *openButton;
@property (strong, nonatomic) UITextView * agreementTextView;
@property (weak, nonatomic) IBOutlet UIImageView *iconCodeImg;
@property (weak, nonatomic) IBOutlet UITextField *iconCode;
@property (weak, nonatomic) IBOutlet UITextField *verificationField;
@property (weak, nonatomic) IBOutlet UITextField *agginPWField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *InvitationField;
@property (weak, nonatomic) IBOutlet QCCountdownButton *verificationCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *nextLoginlable;
@property (weak, nonatomic) IBOutlet LcButton *registButton;
- (IBAction)verificationButton:(QCCountdownButton *)sender;
- (IBAction)registButton:(LcButton *)sender;
- (IBAction)openbutton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@end
