//
//  LoginViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/18.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "LcButton.h"
@interface LoginViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UIView *lineACO;
@property (weak, nonatomic) IBOutlet UIView *linePW;
@property (weak, nonatomic) IBOutlet UIButton *forgetPwbutton;

@property (weak, nonatomic) IBOutlet UIButton *openPassword;
@property (weak, nonatomic) IBOutlet UITextField *PhoneField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet LcButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *registerLable;
@property (weak, nonatomic) IBOutlet UIButton *returnbutton;
@property (weak, nonatomic) IBOutlet UIButton *icon;
- (IBAction)forgetPwButton:(UIButton *)sender;
- (IBAction)deletebutton:(UIButton *)sender;
- (IBAction)openPassword:(UIButton *)sender;
- (IBAction)returnHome:(UIButton *)sender;

@end
