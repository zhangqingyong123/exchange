//
//  CountryListViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2019/2/15.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryModle.h"
@interface CountryListsViewController : UIViewController
@property(nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,assign)NSInteger  index;
@property(copy,nonatomic)void(^selectIndexPathRow)(CountryModle * modle); //退出
@end

