//
//  ForgetThePasswordViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface ForgetThePasswordViewController : BaseViewViewController
- (IBAction)back:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;

- (IBAction)deleteButton:(UIButton *)sender;
- (IBAction)nextButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextbutton;

@end
