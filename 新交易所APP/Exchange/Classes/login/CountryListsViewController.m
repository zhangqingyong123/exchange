//
//  CountryListViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2019/2/15.
//  Copyright © 2019年 张庆勇. All rights reserved.
//

#import "CountryListsViewController.h"
#import "CountryCell.h"
#import "AJSearchBar.h"
@interface CountryListsViewController ()<UITableViewDelegate,UITableViewDataSource,SearchDelegate>
/*! 搜索框 */
@property(nonatomic,strong)AJSearchBar *searchBar;
@property(nonatomic,strong)UIView *searchbgView;
@property (nonatomic,strong)UIButton * cancelbutton;
@property (nonatomic,strong)UITableView * tableview;

@property(nonatomic,strong)NSMutableArray * searchDataArray;

@property (nonatomic,assign) BOOL  isSearch;
@property (nonatomic,assign) BOOL  isChinaese;
@property (nonatomic,strong) NoNetworkView * workView;

@end

@implementation CountryListsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[EXUnit  isLocalizable]  hasPrefix:@"zh-Hant"] || [[EXUnit  isLocalizable]  hasPrefix:@"zh-Hans"] ) {
        
        _isChinaese = YES;
        NSArray * array =  [self countryNamepaixuWith:self.dataArray isChinaese:_isChinaese];
        NSMutableArray * muarray = [NSMutableArray new];
        for (CountryModle * modle in array)
        {
            if (modle.ID.integerValue ==44)
            {
                [muarray insertObject:modle atIndex:0];

            }else
            {
                [muarray addObject:modle];
            }
        }
        self.dataArray  = muarray;
    }else
    {
        _isChinaese = NO;
        NSArray * array =  [self countryNamepaixuWith:self.dataArray isChinaese:_isChinaese];
        NSMutableArray * muarray = [NSMutableArray new];
        for (CountryModle * modle in array)
        {
            if (modle.ID.integerValue ==44)
            {
                [muarray insertObject:modle atIndex:0];
                
            }else
            {
                [muarray addObject:modle];
            }
        }
        self.dataArray  = muarray;
    }
    [self.view addSubview:self.searchbgView];
    [_searchbgView addSubview:self.searchBar];
    [self.view addSubview:self.tableview];
}
//国家名字根据拼音的字母排序  ps：排序适用于所有类型
- (NSMutableArray *)countryNamepaixuWith:(NSMutableArray *)array isChinaese:(BOOL)isChinaese{
    [array sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        CountryModle * modle1 = obj1;
        CountryModle * modle2 = obj2;
        NSString *string1;
        NSString *string2;
        if (isChinaese) {
            string1 = [EXUnit huoqushouzimuWithString:modle1.pinyin];
            string2 = [EXUnit huoqushouzimuWithString:modle2.pinyin];;
        }else
        {
            string1 = [EXUnit huoqushouzimuWithString:modle1.name_en];
            string2 = [EXUnit huoqushouzimuWithString:modle2.name_en];;
        }
        return [string1 compare:string2];
    }];
    
    return array;
}
#pragma mark---懒加载
- (UIView *)searchbgView
{
    if (!_searchbgView) {
        _searchbgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, kStatusBarAndNavigationBarHeight)];
        _searchbgView.backgroundColor = TABLEVIEWLCOLOR;
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(0 , kStatusBarAndNavigationBarHeight-1)];
        [bezierPath addLineToPoint:CGPointMake(UIScreenWidth , kStatusBarAndNavigationBarHeight-1)];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = CELLCOLOR.CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [_searchbgView.layer addSublayer:shapeLayer];
        
    }
    return _searchbgView;
}
- (AJSearchBar *)searchBar{
    if (!_searchBar) {
        _searchBar = [[AJSearchBar alloc]initWithFrame:CGRectMake(15,IPhoneTop + 20, _searchbgView.width -30, 40)];
        _searchBar.SearchDelegate = self;
        _searchBar.placeholderLabel.text = Localized(@"choose_country_message");
       
        _searchBar.isChina = YES;
        if (_isChinaese) {
            _searchBar.textField.keyboardType = UIKeyboardTypeNamePhonePad;
        }
    }
    return _searchBar;
}
- (NSMutableArray *)searchDataArray
{
    if (!_searchDataArray) {
        _searchDataArray = [[NSMutableArray alloc] init];
    }
    return _searchDataArray;
}
- (UITableView *)tableview
{
    if (!_tableview) {
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight)];
        _tableview.tableFooterView = [UIView new];
        _tableview.separatorColor = CLEARCOLOR;
        _tableview.backgroundColor = TABLEVIEWLCOLOR;
        _tableview.delegate = self;
        _tableview.dataSource=self;
        _tableview.showsVerticalScrollIndicator = NO;
        [_tableview registerClass:[CountryCell class] forCellReuseIdentifier:@"CountryCell"];
        
    }
    return _tableview;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _isSearch?_searchDataArray.count : _dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"CountryCell"];

    cell.index = _index;
    cell.model = _isSearch?_searchDataArray[indexPath.row] : _dataArray[indexPath.row];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     CountryModle * modle = _isSearch?_searchDataArray[indexPath.row] : _dataArray[indexPath.row];
    if (self.selectIndexPathRow) {
        self.selectIndexPathRow(modle);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark --- SearchDelegate
/*! 新增历史搜索 */
- (void)searchWithStr:(NSString *)text{
    
  
    
    [_searchDataArray removeAllObjects];
    NSString * resultStr;
    if(text && text.length>0) {
        
        resultStr = [text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[text substringToIndex:1] capitalizedString]];
        
    }
    for (CountryModle * modle in self.dataArray) {
        if (_isChinaese)
        {
            if ([modle.name_cn containsString:text]) {
                [self.searchDataArray addObject:modle];
            }
        }else
        {
            if ([modle.name_en containsString:resultStr])
            {
                [self.searchDataArray addObject:modle];
            }
           
        }
        
    }
    _isSearch = YES;
    [self.tableview reloadData];
    [self placeholderViewWithFrame:self.tableview.frame title:Localized(@"search_no_symbol")];
}
- (void)placeholderViewWithFrame:(CGRect)frame title:(NSString *)title
{
    if (_searchDataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:_tableview.frame NoNetwork:NO];
        [_tableview addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
    
}
- (void)searchWithcancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
