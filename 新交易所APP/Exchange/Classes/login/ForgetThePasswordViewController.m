//
//  ForgetThePasswordViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/8/8.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "ForgetThePasswordViewController.h"

@interface ForgetThePasswordViewController ()

@end

@implementation ForgetThePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_phoneTextfield setValue:textFieldPCOLOR  forKeyPath:@"_placeholderLabel.textColor"];
    [_phoneTextfield setValue:SystemBlodFont(13) forKeyPath:@"_placeholderLabel.font"];
     _phoneTextfield.tintColor = WHITECOLOR;
    KViewRadius(self.nextbutton, 2);
    RACSignal*phone=[_phoneTextfield.rac_textSignal map:^id(NSString* value) {
        return  @(value.length>5);
    }];
    WeakSelf
    [phone subscribeNext:^(NSNumber* x) {
        if ([x boolValue]) {
            weakSelf.nextbutton.enabled=YES;
            weakSelf.nextbutton.alpha = 1;
        }else{
            weakSelf.nextbutton.enabled=NO;
            weakSelf.nextbutton.alpha = 0.5;
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)back:(UIButton *)sender {
    
    CATransition *myTra = [CATransition animation];
    myTra.duration = 0.5;
    myTra.type = kCATransitionMoveIn;
    [self.view.superview.layer addAnimation:myTra forKey:nil];
    
    //水波动效果
    CATransition *transition = [CATransition animation];
    
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 1.5f;
    transition.type = @"rippleEffect";
    
    [self.view.window.layer addAnimation:transition forKey:@"rippleEffect"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)deleteButton:(UIButton *)sender {
     self.phoneTextfield.text = nil;
}

- (IBAction)nextButton:(UIButton *)sender {
    [_phoneTextfield resignFirstResponder];
//    CustomAlertVC * custom = [[CustomAlertVC alloc] initWithCustomContentViewClass:@"BindCustonAlert" delegate:nil];
//    custom.direction = FromBottom;
//    [custom show];
//    BindCustonAlert *aleatView = (BindCustonAlert *)custom.contentView;
//    aleatView.type = @"4";
//    aleatView.cancelBlock = ^{
//        [custom dismiss];
//    };
//    WeakSelf
//    WeakObject(aleatView)
//    [aleatView.okButton add_BtnClickHandler:^(NSInteger tag) {
//        NSDictionary * param = @{@"phone":weakaleatView.phoneField.text,
//                                 @"sms_code":weakSelf.phoneVerification.text,
//                                 @"email_code":[NSString isEmptyString:weakaleatView.emailField.text]?@"":weakaleatView.emailField.text,
//                                 @"country_id":weakSelf.country_id,
//                                 @"two_step_code":[NSString isEmptyString:weakaleatView.gooldField.text]?@"":weakaleatView.gooldField.text};
//        
//        [weakSelf POSTWithHost:@"" path:bindPhone param:param cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//            if ([responseObject[@"code"] integerValue]==0)//成功
//            {
//                [EXUserManager savephone:@"1"];
//                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
//                [custom dismiss];
//                [self.navigationController popViewControllerAnimated:YES];
//                
//            }else
//            {
//                [weakSelf axcBasePopUpWarningAlertViewWithMessage:responseObject[@"message"] view:kWindow];
//            }
//            
//        } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            
//        }];
//        
//    }];
    
}
@end
