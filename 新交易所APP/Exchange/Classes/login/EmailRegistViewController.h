//
//  EmailRegistViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "QCCountdownButton.h"
#import "LcButton.h"
typedef enum : NSUInteger {
    RegistEmailType = 0,//注册
    passWordEmailType = 1,  //找回密码
}EmailType;

@interface EmailRegistViewController : BaseViewViewController
// 第一步
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet LcButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *lineview;
@property (weak, nonatomic) IBOutlet UILabel *loginlable;
- (IBAction)nextButton:(LcButton *)sender;
// 第二步
@property (strong, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UIView *nextlineView;
@property (weak, nonatomic) IBOutlet UIView *nextlineView1;
@property (weak, nonatomic) IBOutlet UIView *nextlineView2;
@property (weak, nonatomic) IBOutlet UIView *nextlineView3;
@property (weak, nonatomic) IBOutlet UIView *nextlineView4;
@property (weak, nonatomic) IBOutlet UIButton *openButton;
@property (strong, nonatomic) UITextView * agreementTextView;

@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *agginPWField;
@property (weak, nonatomic) IBOutlet UITextField *verificationField;
@property (weak, nonatomic) IBOutlet UITextField *InvitationField;
@property (weak, nonatomic) IBOutlet UITextField *iconCodeField;
@property (weak, nonatomic) IBOutlet UIImageView *iconCodeImage;
@property (weak, nonatomic) IBOutlet UILabel *nextLoginlable;
@property (weak, nonatomic) IBOutlet LcButton *registButton;
@property (weak, nonatomic) IBOutlet QCCountdownButton *verificationCodeBtn;

@property (nonatomic,readwrite)EmailType emailType;
- (IBAction)verificationButton:(QCCountdownButton *)sender;
- (IBAction)registButton:(UIButton *)sender;
- (IBAction)openbutton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;


@end
