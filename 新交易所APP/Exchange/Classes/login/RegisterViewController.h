//
//  RegisterViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"

@interface RegisterViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (nonatomic,assign)BOOL isRegister;
@property (weak, nonatomic) IBOutlet UILabel *titlelable;
@property (weak, nonatomic) IBOutlet UIButton *returnbutton;

- (IBAction)returnButton:(UIButton *)sender;

@end
