//
//  CountryModle.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
/*
 CreatedAt = "0001-01-01T00:00:00Z";
 ID = 39;
 UpdatedAt = "0001-01-01T00:00:00Z";
 "area_code" = "+238";
 code = "";
 "is_show" = 0;
 "name_cn" = "\U4f5b\U5f97\U89d2";
 "name_en" = "Cape Verde";
 */

#import <Foundation/Foundation.h>

@interface CountryModle : NSObject
@property (nonatomic,strong)NSString * CreatedAt;
@property (nonatomic,strong)NSString * ID;
@property (nonatomic,strong)NSString * UpdatedAt;
@property (nonatomic,strong)NSString * area_code;
@property (nonatomic,strong)NSString * is_show;
@property (nonatomic,strong)NSString * name_cn;
@property (nonatomic,strong)NSString * name_en;
@property (nonatomic,strong)NSString * pinyin;
@property (nonatomic,strong)NSString * country_code; //澳大利亚是AU
@end
