//
//  RegisterViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/19.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterPageView.h"

@interface RegisterViewController ()
@property(nonatomic,strong) RegisterPageView *pageView;
@property(nonatomic,assign) CGFloat pageView_y;
@property(nonatomic,strong) NSArray * titles;
@property(nonatomic,strong) NSArray * Controllers;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_returnbutton setImage:[UIImage imageNamed:[ZBLocalized sharedInstance].loginbackicon] forState:0];
    _titlelable.textColor = MAINTITLECOLOR;
    if (_isRegister) {
        _titlelable.text = @"";
        _pageView_y = kStatusBarAndNavigationBarHeight;
    }else
    {
       _titlelable.text = Localized(@"Forget_password");
        _pageView_y = kStatusBarAndNavigationBarHeight +54;
    }
    _titlelable.font = AutoBoldFont(24);
    _bgImageView.backgroundColor = TABLEVIEWLCOLOR;
    [self.view addSubview:[self test1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (RegisterPageView *)test1 {
    
    if ([NSString isEmptyString:[AppDelegate shareAppdelegate].default_register_method]) {
        [AppDelegate shareAppdelegate].default_register_method = @"1";
    }
    if ([AppDelegate shareAppdelegate].default_register_method.integerValue ==1)
    {
        _titles = _isRegister? @[Localized(@"login_Mobile_registration"),Localized(@"login_Mailbox_registration")]:@[Localized(@"login_Mobile_account_number"),Localized(@"login_Mailbox_account")];
        _Controllers = @[@"PhoneRegistViewController",@"EmailRegistViewController"];
        
    }else if ([AppDelegate shareAppdelegate].default_register_method.integerValue ==2)
    {
        
        _titles = _isRegister? @[Localized(@"login_Mailbox_registration"),Localized(@"login_Mobile_registration")]:@[Localized(@"login_Mailbox_account"),Localized(@"login_Mobile_account_number")];
        _Controllers = @[@"EmailRegistViewController",@"PhoneRegistViewController"];
        
        
    }else if ([AppDelegate shareAppdelegate].default_register_method.integerValue ==3)
    {
        
        
        _titles = _isRegister? @[Localized(@"login_Mailbox_registration")]:@[Localized(@"login_Mailbox_account")];
        _Controllers = @[@"EmailRegistViewController"];
        
        
    }else if ([AppDelegate shareAppdelegate].default_register_method.integerValue ==4)
    {
        
        
        _titles = _isRegister? @[Localized(@"login_Mobile_registration")]:@[Localized(@"login_Mobile_account_number")];
        _Controllers = @[@"PhoneRegistViewController"];
    }else
    {
        
    }
    
    _pageView = [[RegisterPageView alloc] initWithFrame:CGRectMake(0,_pageView_y, UIScreenWidth, UIScreenHeight-RealValue_W(142))
                                             withTitles:_titles
                                    withViewControllers:_Controllers
                                         withParameters:nil];
    
    _pageView.selectedColor = TABTITLECOLOR;
    _pageView.unselectedColor = maintitleLCOLOR ;
    _pageView.backgroundColor =TABLEVIEWLCOLOR;
    _pageView.isRegist = _isRegister;
    return _pageView;
   
}

- (IBAction)returnButton:(UIButton *)sender {
    
    CATransition *myTra = [CATransition animation];
    myTra.duration = 0.5;
    myTra.type = kCATransitionMoveIn;
    [self.view.superview.layer addAnimation:myTra forKey:nil];
    
    //水波动效果
    CATransition *transition = [CATransition animation];
    
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 1.5f;
    transition.type = @"rippleEffect";
    
    [self.view.window.layer addAnimation:transition forKey:@"rippleEffect"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
