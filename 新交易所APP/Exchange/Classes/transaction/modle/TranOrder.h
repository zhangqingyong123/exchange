//
//  TranOrder.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/31.
//  Copyright © 2018年 张庆勇. All rights reserved.
/*
 "deal_money": "0",
 "maker_fee": "0.002",
 "amount": "0.003",
 "id": 41192,
 "taker_fee": "0.002",
 "price": "1.002",
 "market": "ZT_CNT",
 "type": 1,
 "source": "122.233.191.168",
 "side": 2,
 "user": 5,
 "deal_stock": "0",
 "ctime": 1533022616.895427,
 "deal_fee": "0",
 "left": "0.003",
 "mtime": 1533022616.895427
 */

#import <Foundation/Foundation.h>

@interface TranOrder : NSObject
@property (nonatomic,strong)NSString * deal_money;
@property (nonatomic,strong)NSString * maker_fee;
@property (nonatomic,strong)NSString * amount;
@property (nonatomic,strong)NSString * id;
@property (nonatomic,strong)NSString * taker_fee;
@property (nonatomic,strong)NSString * price;
@property (nonatomic,strong)NSString * market;
@property (nonatomic,strong)NSString * type;
@property (nonatomic,strong)NSString * source;
@property (nonatomic,strong)NSString * side;
@property (nonatomic,strong)NSString * user;
@property (nonatomic,strong)NSString * deal_stock;
@property (nonatomic,strong)NSString * ctime;
@property (nonatomic,strong)NSString * deal_fee;
@property (nonatomic,strong)NSString * left;
@property (nonatomic,strong)NSString * mtime;
@end
