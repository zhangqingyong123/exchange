//
//  TransactionCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "TransactionCell.h"
#define titleWith (UIScreenWidth-RealValue_W(40))/3
@implementation TransactionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}
- (void)get_up
{
    [self.contentView addSubview:self.purchaseLable];
    [self.contentView addSubview:self.revokebutton];
    [self.contentView addSubview:self.priceLable];
    [self.contentView addSubview:self.numberLable];
    [self.contentView addSubview:self.transactionNumbLable];
}
- (void)setOrderModle:(TranOrder *)orderModle
{
    _orderModle = orderModle;
    NSArray *namearray = [self.orderModle.market componentsSeparatedByString:@"_"];
    if (orderModle.side.integerValue ==2)
    {
        _purchaseLable.textColor = ZHANGCOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@",Localized(@"hone_Purchase"), [EXUnit getTimeFromTimestamp:_orderModle.ctime]]
                                                      attributedString:[EXUnit getTimeFromTimestamp:_orderModle.ctime]
                                                                 color:RGBA(63, 92, 107, 1) font:AutoFont(11)];
    }else
    {
        _purchaseLable.textColor =  DIEECOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@",Localized(@"C2C_sell"),[EXUnit getTimeFromTimestamp:_orderModle.ctime]]
                                                      attributedString:[EXUnit getTimeFromTimestamp:_orderModle.ctime]
                                                                 color:RGBA(63, 92, 107, 1) font:AutoFont(11)];
    }
    NSString * price = [self valWith:orderModle.price];
    _priceLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"Current_entrustment_price"), namearray[1], price]
                                                    attributedString:price
                                                               color:MAINTITLECOLOR
                                                                font:AutoFont(15)];
    NSString * amount = [self valWith:orderModle.amount];
    _numberLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"hone_num"),namearray[0],amount]
                                                     attributedString:amount
                                                                color:MAINTITLECOLOR
                                                                 font:AutoFont(15)];
    _numberLable.textAlignment = NSTextAlignmentCenter;
    NSString * str = [self valWith:orderModle.deal_money];
//    NSString * deal_money = [EXUnit asksOrbadsformatternumber:_homeMoel.quote_asset_precision.intValue assess:str];
    _transactionNumbLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"C2C_Actual_transaction"),namearray[1],str]
                                                              attributedString:str
                                                                         color:MAINTITLECOLOR
                                                                          font:AutoFont(15)];
    _transactionNumbLable.textAlignment = NSTextAlignmentRight;
    
}
- (NSString *)valWith:(NSString *)val
{
    NSRange eSite = [val rangeOfString:@"e"];
    if (eSite.length ==0)
    {
        return val;
    }
    double fund = [[val substringWithRange:NSMakeRange(0, eSite.location)] doubleValue];  //把E前面的数截取下来当底数
    double top = [[val substringFromIndex:eSite.location + 1] doubleValue];   //把E后面的数截取下来当指数
    if (fund > -0.000001 && fund < +0.000001)
    {
        fund = 10;
    }
    
    //    double result = fund * pow(10.0, top);
    NSDecimalNumber *number1 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",fund]];
    NSDecimalNumber *num;
    if (top<0)
    {
        top = -top;
        num = [number1 decimalNumberByRaisingToPower:top];// number1的2次方
        
        double  var = pow(10, top);
        //方式1：不进行四舍五入
        
        NSDecimalNumber *number2 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",var]];
        
        NSDecimalNumber *numer = [num decimalNumberByDividingBy:number2];
        NSLog(@"num===%@",numer);
        return [NSString stringWithFormat:@"%@",numer];
    }else
    {
        num = [number1 decimalNumberByRaisingToPower:top];// number1的2次方
    }
    
    return [NSString stringWithFormat:@"%@",num];
    
}
- (void)setHiustModle:(TranOrder *)hiustModle
{
    _hiustModle = hiustModle;
    NSArray *namearray = [self.hiustModle.market componentsSeparatedByString:@"_"];
    if (_hiustModle.side.integerValue ==2)
    {
        _purchaseLable.textColor = ZHANGCOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@",Localized(@"hone_Purchase"), [EXUnit getTimeFromTimestamp:_hiustModle.ctime]]
                                                      attributedString:[EXUnit getTimeFromTimestamp:_hiustModle.ctime]
                                                                 color:RGBA(63, 92, 107, 1) font:AutoFont(11)];
    }else
    {
        _purchaseLable.textColor =  DIEECOLOR;
        _purchaseLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ %@",Localized(@"C2C_sell"),[EXUnit getTimeFromTimestamp:_hiustModle.ctime]]
                                                      attributedString:[EXUnit getTimeFromTimestamp:_hiustModle.ctime]
                                                                 color:RGBA(63, 92, 107, 1) font:AutoFont(11)];
    }
    
    
    NSString * price;
    if ([_hiustModle.deal_money isEqualToString:@"0"])
    {
         price=@"0";//均价
    }else
    {
        NSDecimalNumber*jianfa1 = [NSDecimalNumber decimalNumberWithString:_hiustModle.deal_money];
        NSDecimalNumber*jianfa2 = [NSDecimalNumber decimalNumberWithString:_hiustModle.deal_stock];
        NSDecimalNumber *chufa = [jianfa1 decimalNumberByDividingBy:jianfa2];
         price=[NSString stringWithFormat:@"%@",chufa];//均价
    }
   
    
   
    _priceLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"C2C_average_price"),namearray[1], price]
                                                    attributedString:price
                                                               color:MAINTITLECOLOR
                                                                font:AutoFont(15)];
    _numberLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"C2C_average_num"),namearray[0],hiustModle.deal_stock]
                                                     attributedString:hiustModle.deal_stock
                                                                color:MAINTITLECOLOR
                                                                 font:AutoFont(15)];
    _numberLable.textAlignment = NSTextAlignmentCenter;
    NSString * str = [self valWith:hiustModle.deal_money];
//    NSString * deal_money = [EXUnit asksOrbadsformatternumber:_homeMoel.quote_asset_precision.intValue assess:str];
    _transactionNumbLable.attributedText = [EXUnit finderattributedLinesString:[NSString stringWithFormat:@"%@(%@)\n%@",Localized(@"C2C_average_Total"),namearray[1],str]
                                                              attributedString:str
                                                                         color:MAINTITLECOLOR
                                                                          font:AutoFont(15)];
    _transactionNumbLable.textAlignment = NSTextAlignmentRight;
    
}
//防止小数误差丢失
- (NSDecimalNumber *)decimalNumber:(double)num {
    NSString *numString = [NSString stringWithFormat:@"%lf", num];
    return [NSDecimalNumber decimalNumberWithString:numString];
}
- (UILabel *)purchaseLable
{
    if (!_purchaseLable) {
        _purchaseLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20), RealValue_W(25),RealValue_W(320), RealValue_W(34))];
        
        _purchaseLable.font = AutoFont(16);
        
        
    }
    return _purchaseLable;
    
}
-(UIButton *)revokebutton
{
    if (!_revokebutton) {
        _revokebutton = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(UIScreenWidth - RealValue_W(20) -RealValue_W(100), 0, RealValue_W(80), RealValue_W(32)) title:Localized(@"transaction_Revoke") backgroundColor:DIEECOLOR titleColor:RGBA(245, 74, 97, 1) tapAction:^(UIButton *button) {
            
        }];
        _revokebutton.titleLabel.font = AutoFont(8);
        _revokebutton.centerY = _purchaseLable.centerY;
        _revokebutton.backgroundColor = RGBA(245, 74, 97, 0.2);
        
        _revokebutton.layer.mask = [EXUnit ShapeLayerWithViewCGRect:_revokebutton cornerRadius:2];
    }
    return _revokebutton;
}
- (UILabel *)priceLable
{
    if (!_priceLable) {
        _priceLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(20),_purchaseLable.bottom + RealValue_W(18),titleWith, RealValue_W(90))];
        _priceLable.textColor =  RGBA(63, 92, 107, 1);
        _priceLable.font = AutoFont(12);
        _priceLable.numberOfLines = 0;
        
    }
    return _priceLable;
    
}
- (UILabel *)numberLable
{
    if (!_numberLable) {
        _numberLable = [[UILabel alloc] initWithFrame:CGRectMake(_priceLable.right,_priceLable.mj_y,titleWith, RealValue_W(90))];
        _numberLable.textColor =  RGBA(63, 92, 107, 1);
        _numberLable.font = AutoFont(12);
        
        _numberLable.numberOfLines = 0;
        
    }
    return _numberLable;
    
}
- (UILabel *)transactionNumbLable
{
    if (!_transactionNumbLable) {
        _transactionNumbLable = [[UILabel alloc] initWithFrame:CGRectMake(_numberLable.right,_priceLable.mj_y,titleWith, RealValue_W(90))];
        _transactionNumbLable.textColor =  RGBA(63, 92, 107, 1);
        _transactionNumbLable.font = AutoFont(12);
        
        _transactionNumbLable.numberOfLines = 0;
        
    }
    return _transactionNumbLable;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
@implementation NewTransactionCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}
- (void)get_up
{
    [self.contentView addSubview:self.leftlable];
    [self.contentView addSubview:self.rightlable];
    
}
-(void) labelTouchUpInside:(UITapGestureRecognizer *)recognizer{
    
    UILabel *label=(UILabel*)recognizer.view;
    if (self.getPriceblock) {
        self.getPriceblock(label.text);
    }
    
}
- (UILabel *)leftlable
{
    if (!_leftlable) {
        _leftlable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,(UIScreenWidth - RealValue_W(416) -15)/2, 24)];
        _leftlable.textColor =  MAINTITLECOLOR;
        _leftlable.font = AutoBoldFont(12);
        
        
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouchUpInside:)];
        _leftlable.userInteractionEnabled=YES;
        [_leftlable addGestureRecognizer:labelTapGestureRecognizer];
    }
    return _leftlable;
}

- (UILabel *)rightlable
{
    if (!_rightlable) {
        _rightlable = [[UILabel alloc] initWithFrame:CGRectMake((UIScreenWidth - RealValue_W(416) -15)/2 , 0,(UIScreenWidth - RealValue_W(416) -15)/2, 24)];
        _rightlable.textColor =  MAINTITLECOLOR1;
        _rightlable.font = AutoFont(12);
        
        _rightlable.textAlignment = NSTextAlignmentRight;
        
    }
    return _rightlable;
}

@end
