//
//  TransactionHeadView.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCHStepSizeSlider.h"
#import "HomeModle.h"
@interface TransactionHeadView : UIView
@property (nonatomic,strong)UIButton * addbutton;
@property (nonatomic,strong)UIButton * reduceButton;
@property (nonatomic,strong)UIButton * chooseType;
@property (nonatomic,strong)UILabel * pricetitle;
@property (nonatomic,strong)UILabel * numbertitle;
@property (nonatomic, strong)UITableView * ToptableView;
@property (nonatomic, strong)UITableView * bottombleView;
@property (nonatomic, strong) NSArray * dataArray;
@property (nonatomic,strong)UITextField * priceField;
@property (nonatomic,strong)UILabel * priceLable;
@property (nonatomic,strong)UILabel * rangeLable;
@property (nonatomic,strong)UITextField * numField;
@property (nonatomic,strong)UILabel * priceValue;
@property (nonatomic,strong)UILabel * numLabValue;
@property (nonatomic,strong)UIButton * buyButton;
@property (nonatomic,strong)UIButton * chooseButton;
@property (nonatomic,strong)UILabel * sellnumLabValue;
@property (nonatomic,strong)HomeModle * modle;
@property (nonatomic,strong)NSString  * balance;
@property (nonatomic,strong)NSString  * buybalance;
@property (nonatomic,strong)NSString  * priceText;
@property (nonatomic,strong)UILabel * ATurnover;
@property (nonatomic,assign)BOOL  isSell;
@property(nonatomic,strong) UIButton * purchaseButton; //购买
@property(nonatomic,strong) UIButton * sellButton; //购买
@property (copy, nonatomic) void(^buySuccess)(BOOL isSuccess);
@property (copy, nonatomic) void(^buttonBlock)(UIButton * button);
@property (copy, nonatomic) void(^askAndbidBlock)(UIButton * button);
@property (copy, nonatomic) void(^priceBlock)(NSString * price);
/*获取币价深度和降度
 *asks 深度
 *bids
 *rangeIndex 数组的后几位
 */
- (void)getDepthDataWithasks:(NSArray *)asks
                        bids:(NSArray *)bids
                  rangeIndex:(NSInteger)rangeIndex;
- (void)refushUIdataWithModle:(HomeModle *)modle;
@end
