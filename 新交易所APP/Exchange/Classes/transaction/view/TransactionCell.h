//
//  TransactionCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TranOrder.h"
@interface TransactionCell : UITableViewCell
/*买入*/
@property (nonatomic,strong)UILabel *purchaseLable;
/*撤销*/
@property (nonatomic,strong)UIButton *revokebutton;
/*价格*/
@property (nonatomic,strong)UILabel *priceLable;
/*数量*/
@property (nonatomic,strong)UILabel *numberLable;
/*实际成交*/
@property (nonatomic,strong)UILabel *transactionNumbLable;

@property (nonatomic,strong)HomeModle *homeMoel;
@property (nonatomic,strong)TranOrder *orderModle;
@property (nonatomic,strong)TranOrder *hiustModle;
@end
@interface NewTransactionCell : UITableViewCell
@property (nonatomic,strong)UILabel *leftlable;
@property (nonatomic,strong)UILabel *rightlable;
@property (copy, nonatomic) void(^getPriceblock)(NSString * price);
@end
