//
//  LeftBCell.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModle.h"
@interface LeftBCell : UITableViewCell
/*类型*/
@property (nonatomic,strong)UILabel *BTCLable;
/*增量*/
@property (nonatomic,strong)UILabel *incrementLable;
/*幅度*/
@property (nonatomic,strong)UILabel *rangeLable;

@property (nonatomic,strong)HomeModle * modle;
@end
