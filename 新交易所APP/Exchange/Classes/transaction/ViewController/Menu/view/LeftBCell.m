//
//  LeftBCell.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "LeftBCell.h"

@implementation LeftBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = TABLEVIEWLCOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self get_up];
    }
    return self;
}
- (void)setModle:(HomeModle *)modle
{
    _modle = modle;
    NSArray *array = [modle.name componentsSeparatedByString:@"_"];
     int precision = modle.quote_asset_precision.intValue;
    _BTCLable.attributedText = [EXUnit finderattributedString:[NSString stringWithFormat:@"%@ / %@",array[0],array[1]]
                                             attributedString:[NSString stringWithFormat:@"/ %@",array[1]]
                                                        color:MAINTITLECOLOR1
                                                         font:AutoFont(14)];
    
    float _x = modle.IncreaseDegree.floatValue;
    if (!isnan(_x)) {
        _rangeLable.text = [NSString stringWithFormat:@"%.2f%%",modle.IncreaseDegree.floatValue];
    }else
    {
        _rangeLable.text = @"0.00%";
    }
    
    if (modle.last.floatValue - modle.open.floatValue >=0) {
        _incrementLable.textColor =ZHANGCOLOR;
        _rangeLable.textColor = ZHANGCOLOR;
      
    }else
    {
        _incrementLable.textColor =DIEECOLOR;
        _rangeLable.textColor = DIEECOLOR;
      
    }
     _incrementLable.text = [NSString stringWithFormat:@"%@",modle.last];
     _incrementLable.text = [NSString stringWithFormat:@"%@",[EXUnit formatternumber:precision assess:modle.last]];
}
- (void)get_up
{
    [self.contentView addSubview:self.BTCLable];
 
    [self.contentView addSubview:self.rangeLable];
    [self.contentView addSubview:self.incrementLable];
 
}
- (UILabel *)BTCLable
{
    if (!_BTCLable) {
        _BTCLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_W(16), 0,RealValue_W(240), RealValue_W(100))];
        _BTCLable.textColor =  MAINTITLECOLOR;
        _BTCLable.font = AutoFont(16);
        _BTCLable.numberOfLines = 0;
       
    }
    return _BTCLable;
}
- (UILabel *)rangeLable
{
    if (!_rangeLable) {
        _rangeLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth *0.75 - RealValue_W(16) -RealValue_W(160)
                                                                    ,0,
                                                                    RealValue_W(160),
                                                                    RealValue_W(26))];
        _rangeLable.textColor =  DIEECOLOR;
        _rangeLable.font = AutoFont(14);
     
        _rangeLable.centerY = _BTCLable.centerY;
        _rangeLable.textAlignment = NSTextAlignmentRight;
        
    }
    return _rangeLable;
}
- (UILabel *)incrementLable
{
    if (!_incrementLable) {
        _incrementLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth *0.75 /2 - 60
                                                                ,0,
                                                                140,
                                                                RealValue_W(26))];
        _incrementLable.textColor =  ZHANGCOLOR;
        _incrementLable.font = AutoFont(14);
      
        _incrementLable.centerY = _BTCLable.centerY;
      
        _incrementLable.textAlignment = NSTextAlignmentCenter;
        
    }
    return _incrementLable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
