//
//  LeftCollectionViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/30.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "LeftCollectionViewController.h"
#import "LeftBCell.h"
#import "Y_StockChartViewController.h"
#import "AppDelegate.h"
#import "CWLateralSlideAnimator.h"
#import "UIViewController+CWLateralSlide.h"
@interface LeftCollectionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataAay;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation LeftCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
    [self.view addSubview:self.tableView];
    [self reconnect];
}
- (void)reconnect
{
    NSMutableArray * array = [ EXUnit getCollectionData];
    for (HomeModle * modle in self.dataArray)
    {
        if ([array containsObject:modle.name]) {
          
            if (self.ID==10086) {
                [self.dataAay addObject:modle];
            }else
            {
                if (modle.classes.count > 0)
                {
                    for (NSDictionary * dic in modle.classes) {
                        if ([dic[@"symbol_class_id"] integerValue] ==self.ID)
                        {
                            [self.dataAay addObject:modle];
                        }
                    }
                }
               
            }
        }
    }
    if (self.dataAay.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth*0.75, UIScreenHeight- RealValue_H(88) - IPhoneTop -RealValue_W(116)) NoNetwork:NO];
        WeakSelf
        _workView.buttonclickeBlock = ^(UIButton *button) { //重新加载
            [weakSelf reconnect];
        };
        [self.tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
   
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  UIScreenWidth*0.75 ,
                                                                  UIScreenHeight- RealValue_H(88) - IPhoneTop -RealValue_W(136) - IPhoneBottom)
                                                 style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[LeftBCell class] forCellReuseIdentifier:@"LeftBCell"];
        
        
    }
    return _tableView;
}
- (NSMutableArray *)dataAay
{
    if (!_dataAay) {
        _dataAay = [[NSMutableArray alloc]init];
    }
    return _dataAay;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(100);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataAay.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftBCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"LeftBCell"];
    cell.modle = self.dataAay[indexPath.row];
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     HomeModle *modle = self.dataAay[indexPath.row];
     [AppDelegate shareAppdelegate].modle = modle;
     [self dismissViewControllerAnimated:YES completion:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:leftSymbolNotificationse object:modle];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
