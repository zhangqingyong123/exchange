//
//  LeftViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
typedef NS_ENUM(NSUInteger,DrawerType) {
    DrawerDefaultLeft = 1, // 默认动画，左侧划出
    DrawerDefaultRight,    // 默认动画，右侧滑出
    DrawerTypeMaskLeft,    // 遮盖动画，左侧划出
    DrawerTypeMaskRight    // 遮盖动画，右侧滑出
};
@interface LeftViewController : BaseViewViewController
@property (nonatomic,assign) DrawerType drawerType; // 抽屉类型
@end
