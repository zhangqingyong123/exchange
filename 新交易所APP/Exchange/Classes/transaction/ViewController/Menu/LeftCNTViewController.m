//
//  LeftCNTViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/30.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "LeftCNTViewController.h"
#import "LeftBCell.h"
#import "Y_StockChartViewController.h"
#import "AppDelegate.h"
#import "UIViewController+CWLateralSlide.h"
#import "CWInteractiveTransition.h"

@interface LeftCNTViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@end

@implementation LeftCNTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLEVIEWLCOLOR;
    [self.view addSubview:self.tableView];
    [self reconnect];
}
- (void)reconnect
{
    NSArray * array = [NSArray bg_arrayWithName:FMDBName_Symbol];
    self.dataArray = [NSMutableArray arrayWithArray:array];
    for (HomeModle * mode in self.dataArray) {
        if ([self.title isEqualToString:mode.quote_asset]) {
            if (self.ID==10086) {
                
                [self.dataSource addObject:mode];
                
            }else
            {
                if (mode.classes.count > 0)
                {
                    for (NSDictionary * dic in mode.classes) {
                        if ([dic[@"symbol_class_id"] integerValue] ==self.ID)
                        {
                            [self.dataSource addObject:mode];
                        }
                    }
                }
            }
        }
    }
    
    
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  UIScreenWidth*0.75,
                                                                  UIScreenHeight- RealValue_H(88) - IPhoneTop -RealValue_W(136) - IPhoneBottom)
                                                 style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CELLCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = TABLEVIEWLCOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[LeftBCell class] forCellReuseIdentifier:@"LeftBCell"];
        
        
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return _dataSource;
}
#pragma  mark -----tableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RealValue_W(100);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftBCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"LeftBCell"];
    cell.modle = self.dataSource[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    HomeModle *modle = self.dataSource[indexPath.row];
    [AppDelegate shareAppdelegate].modle = modle;
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:leftSymbolNotificationse object:modle];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
