//
//  LeftViewController.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "LeftViewController.h"
#import "SMCustomSegment.h"
#import "SymbolClass.h"
#import "MDMultipleSegmentView.h"
#import "MDFlipCollectionView.h"
#import "LeftCollectionViewController.h"
#import "LeftCNTViewController.h"
@interface LeftViewController ()<SMCustomSegmentDelegate,MDMultipleSegmentViewDeletegate,MDFlipCollectionViewDelegate>
@property(nonatomic,strong) SMCustomSegment *segment;
@property(nonatomic,strong) MDMultipleSegmentView *segView;
@property(nonatomic,strong) MDFlipCollectionView *collectView;
@property(nonatomic,strong)NSMutableArray * titles;
@property(nonatomic,strong)NSMutableArray * ViewControllers;
@property(nonatomic,strong)NSMutableArray * dataArray;
@property (nonatomic,strong) NoNetworkView * workView;
@property(nonatomic,strong)NSMutableArray * symbolArray; //板块数组
@property(nonatomic,strong)NSMutableArray * symbolNameArray; //板块名数组
@property(nonatomic,strong)UILabel * titleLable; //板块数组
@end

@implementation LeftViewController
- (void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.titles addObject:Localized(@"quotation_selection")];
    [self.symbolNameArray addObject:Localized(@"transaction_all")];
    [self.ViewControllers addObject:[[LeftCollectionViewController alloc]init]];
    NSArray * dataArray = [NSArray bg_arrayWithName:FMDBName_Symbol];
    self.dataArray = [NSMutableArray arrayWithArray:dataArray];
    
    if ( ![AppDelegate shareAppdelegate].isNoWork) {
         [EXUnit showMessage:Localized(@"transaction_Network")];
    }else
    {
        [self getSymbolClass];
    }
    [self.view addSubview:self.titleLable];
}
- (void)getSymbolClass
{
     WeakSelf
    [HomeViewModel getSymbolClassWithDic:@{} url:symbolClass callBack:^(BOOL isSuccess, id callBack) {
        if (isSuccess) {
            weakSelf.symbolArray = callBack;
            for (SymbolClass * symbol in weakSelf.symbolArray) {
                [self.symbolNameArray addObject:symbol.name];
            }
            if (self.symbolNameArray.count>1) {
                [self initSegment];
            }
            [self getData];
        }
    }];
}
- (void)getdata
{
    NSMutableArray * restrs = [[NSMutableArray alloc] init];
    for (HomeModle * modle in  self.dataArray)
    {
        [restrs addObject:modle.quote_asset];
        
    }
    for (NSString *str in restrs) {
        if (![self.titles containsObject:str]) {
            [self.titles addObject:str];
            [self.ViewControllers addObject:[[LeftCNTViewController alloc]init]];
            
        }
    }
    
   [self initTest:10086];

}
- (void)initSegment
{
    _segment =  [[SMCustomSegment alloc] initWithFrame:CGRectMake((UIScreenWidth*0.75 - self.symbolNameArray.count*64)/2,IPhoneTop+ RealValue_W(60), self.symbolNameArray.count*64, 28) titleArray:self.symbolNameArray];
    _segment.selectIndex = 0;
    _segment.delegate = self;
    _segment.normalBackgroundColor = MAINBLACKCOLOR;
    _segment.selectBackgroundColor = [ZBLocalized sharedInstance].TabBgColor;
    _segment.titleNormalColor = MAINTITLECOLOR1;
    _segment.titleSelectColor = [UIColor whiteColor];
    _segment.normalTitleFont = 12;
    _segment.selectTitleFont = 12;
    _segment.borderWidth = 0.5;
    [self.view addSubview:self.segment];//导航筛选
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake((UIScreenWidth*0.75 - 120)/2, IPhoneTop+ RealValue_W(60), 120, RealValue_W(60))];
        _titleLable.text = Localized(@"Asset_whole");
        _titleLable.font = AutoBoldFont(18);
        _titleLable.textColor = MAINTITLECOLOR;
        _titleLable.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLable;
}
- (void)customSegmentSelectIndex:(NSInteger)selectIndex
{
    [_titles removeAllObjects];
    [_ViewControllers removeAllObjects];
    [self.titles addObject:Localized(@"quotation_selection")];
    [self.ViewControllers addObject:[[LeftCollectionViewController alloc]init]];
    if (selectIndex ==0) {
        
        [self getData];
    }else
    {
        
        SymbolClass * symbol = _symbolArray[selectIndex-1];
        [self getDataClass:symbol];
    }
    
}
- (void)getData
{
    NSMutableArray * restrs = [[NSMutableArray alloc] init];
    for (HomeModle * modle in  self.dataArray)
    {
        [restrs addObject:modle.quote_asset];
    }
    for (NSString *str in restrs) {
        if (![self.titles containsObject:str]) {
            [self.titles addObject:str];
            [self.ViewControllers addObject:[[LeftCNTViewController alloc]init]];
        }
    }
    [self initTest:10086];
    
}
- (void)getDataClass:(SymbolClass *)Symbol
{
    NSMutableArray * restrs = [[NSMutableArray alloc] init];
    for (HomeModle * modle in  self.dataArray)
    {
        if (modle.classes.count > 0)
        {
            
            for (NSDictionary * dic in modle.classes) {
                if ([dic[@"symbol_class_id"] integerValue] ==Symbol.ID.integerValue)
                {
                    [restrs addObject:modle.quote_asset];
                }
            }
            
        }
    }
    for (NSString *str in restrs) {
        if (![self.titles containsObject:str]) {
            [self.titles addObject:str];
            [self.ViewControllers addObject:[[LeftCNTViewController alloc]init]];
        }
    }
    if (self.titles.count>1) {
        [self initTest:Symbol.ID.integerValue];
    }else
    {
        [self removePageView];
    } 
}
// 移除CurrencyPageView
- (void)removePageView
{
    for (UIView *view in self.view.subviews) {
        if (view.tag == 10086 || view.tag == 10011) {
            [UIView animateWithDuration:0.2 animations:^{
                view.hidden = YES;
                [view removeFromSuperview];
            }];
            
        }
    }
}
- (void )initTest:(NSInteger)ID {
     [self removePageView];
    CGFloat segViewWidth;
    NSInteger pages;
    if (_titles.count>3) {
        segViewWidth = UIScreenWidth *0.75;
        pages = 3;
    }else
    {
        segViewWidth = _titles.count * UIScreenWidth *0.75/3;
        pages = _titles.count;
    }
    _segView = [[MDMultipleSegmentView alloc] initWithFrame:CGRectMake(0, _segment.bottom +10, segViewWidth, RealValue_H(88)) ItemsPerPage:pages Type:1];
    _segView.delegate =  self;
    _segView.items = _titles;
    _segView.titleNormalColor = MAINTITLECOLOR1;
    _segView.titleSelectColor = MAINTITLECOLOR;
    _segView.mj_x = UIScreenWidth *0.75/2 -segViewWidth/2;
    if (_titles.count>1)
    {
        [_segView selectIndex:1];
    }else
    {
        [_segView selectIndex:0];
    }
    
    [self.view addSubview:_segView];
    
    _collectView = [[MDFlipCollectionView alloc] initWithFrame:CGRectMake(0,
                                                                          _segView.bottom,
                                                                          UIScreenWidth*0.75,
                                                                          UIScreenHeight- RealValue_H(88)  - IPhoneBottom -IPhoneTop -10- _segment.height) withArray:_ViewControllers];
    _collectView.delegate = self;
    
    if (_titles.count>1)
    {
        [_collectView selectIndex:1];
    }else
    {
        [_collectView selectIndex:0];
    }
    _collectView.titles = _titles;
    _collectView.sourceArray = _dataArray;
    if (self.symbolNameArray.count>1) {
        _collectView.isSymbolClass = YES;
        _segView.mj_y = _titleLable.bottom +5;
        _collectView.mj_y = _segView.bottom;
        _collectView.height = UIScreenHeight- RealValue_H(88)  - IPhoneBottom -IPhoneTop -10- _segment.height;
    }else
    {
        _segView.mj_y = _titleLable.bottom +10;
        _collectView.mj_y = _segView.bottom;
        _collectView.height = UIScreenHeight- RealValue_H(88)  - IPhoneBottom -IPhoneTop -10- _titleLable.height - _titleLable.mj_y;
    }
    _collectView.ID = ID;
    [self.view addSubview:_collectView];
    _segView.tag = 10086;
    _collectView.tag = 10011;
}
- (void)changeSegmentAtIndex:(NSInteger)index
{
    [_collectView selectIndex:index];
}


- (void)flipToIndex:(NSInteger)index
{
    [_segView selectIndex:index];
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
- (NSMutableArray *)titles
{
    if (!_titles) {
        _titles = [[NSMutableArray alloc] init];
    }
    return _titles;
}

- (NSMutableArray *)ViewControllers
{
    if (!_ViewControllers) {
        _ViewControllers = [[NSMutableArray alloc] init];
    }
    return _ViewControllers;
}
- (NSMutableArray *)symbolArray
{
    if (!_symbolArray) {
        _symbolArray = [[NSMutableArray alloc] init];
    }
    return _symbolArray;
}
- (NSMutableArray *)symbolNameArray
{
    if (!_symbolNameArray) {
        _symbolNameArray = [[NSMutableArray alloc] init];
    }
    return _symbolNameArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
