//
//  PurchaseViewController.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/13.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "BaseViewViewController.h"
#import "HomeModle.h"
@interface PurchaseViewController : BaseViewViewController
@property (nonatomic,strong)HomeModle * modle;
@property (nonatomic,assign)NSString * name;
@property (nonatomic,assign)NSInteger index;
@end
