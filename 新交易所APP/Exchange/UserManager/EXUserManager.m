//
//  EXUserManager.m
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import "EXUserManager.h"
#import "LCAFRequestHost.h"
#import "HomeModle.h"
@implementation EXUserManager
DEFINE_SINGLETON_FOR_CLASS(EXUserManager)
//保存计价方式
+ (void)saveValuationMethod:(NSString *)valuationMethod;
{
    C_UserDefaults_Set(@"valuationMethod", valuationMethod);
}
//获取计价方式
+ (NSString *)getValuationMethod;
{
    NSString *str = C_UserDefaults_Get(@"valuationMethod");
    if ([NSString isEmptyString:str]) {
        
        NSString * language =  [ZBLocalized sharedInstance].currentLanguage;
        if ([[EXUnit getSite]isEqualToString:@"BitAladdin"]) {
            return @"USD";
        }
        if([language hasPrefix:@"zh-Hans"]){
           
            str = @"CNY";
        }else
        {
            str = @"USD";
        }
       
    }
    return str;
}
//保存打开z资产的状态
+ (void)saveOpenAssetsState:(NSString *)State;
{
    C_UserDefaults_Set(@"State", State);
}
//获取打开z资产的状态
+ (NSString *)getState;
{
    NSString *str = C_UserDefaults_Get(@"State");
    if ([NSString isEmptyString:str]) {
        str = @"1";
    }
    return str;
}
//用来判断是否登录如果token失效需要移除变量
+ (void)savelogin:(NSString *)islogin
{
    C_UserDefaults_Set(@"islogin", islogin);
}
//获取islogin变量判断是否登录
+ (NSString *)getIslogin;
{
    NSString *str = C_UserDefaults_Get(@"islogin");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
    //保持默认语言
+ (void)saveDefaultLanguage:(NSString *)language
{
     C_UserDefaults_Set(@"language", language);
}
//获取默认语言
+ (NSString *)getLanguage;
{
    NSString *str = C_UserDefaults_Get(@"language");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断是否实名认证
+ (void)saveRealName:(NSString *)RealName
{
    C_UserDefaults_Set(@"RealName", RealName);
}
//获取实名认证
+ (NSString *)RealName
{
    NSString *str = C_UserDefaults_Get(@"RealName");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断是否实名认证
+ (void)saveauthenticated:(NSString *)authenticatedName
{
    C_UserDefaults_Set(@"authenticatedName", authenticatedName);
}
//获取实名认证
+ (NSString *)authenticatedName
{
    NSString *str = C_UserDefaults_Get(@"authenticatedName");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断手机是否验证
+ (void)savephone:(NSString *)phone
{
    C_UserDefaults_Set(@"phone", phone);
}
//获取手机验证状态
+ (NSString *)phone;
{
    NSString *str = C_UserDefaults_Get(@"phone");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断邮箱是否验证
+ (void)saveemail:(NSString *)email
{
    C_UserDefaults_Set(@"email", email);
}
//获取邮箱验证状态
+ (NSString *)email;
{
    NSString *str = C_UserDefaults_Get(@"email");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断谷歌是否验证
+ (void)saveGoole:(NSString *)goole
{
    C_UserDefaults_Set(@"goole", goole);
}
//获取谷歌证状态
+ (NSString *)goole;
{
    NSString *str = C_UserDefaults_Get(@"goole");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断是否开启touchid
+ (void)saveIsTouchid:(NSString *)Touchid
{
     C_UserDefaults_Set(@"Touchid", Touchid);
}
//获取touchid
+ (NSString *)Touchid
{
    NSString *str = C_UserDefaults_Get(@"Touchid");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//用来判断是否开启Gesture
+ (void)saveIsGesture:(NSString *)Gesture
{
    C_UserDefaults_Set(@"Gesture", Gesture);
}
//获取Gesture
+ (NSString *)Gesture
{
    NSString *str = C_UserDefaults_Get(@"Gesture");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//保持eth价格
+ (void)saveETHPrice:(NSString *)price
{
    C_UserDefaults_Set(@"ETHPrice", price);
}
//获取ETH
+ (NSString *)getETHPrice;
{
    NSString *str = C_UserDefaults_Get(@"ETHPrice");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//保持CNZ价格
+ (void)saveCNZPrice:(NSString *)price
{
    C_UserDefaults_Set(@"CNZPrice", price);
}
//获取CNZ
+ (NSString *)getCNZHPrice;
{
    NSString *str = C_UserDefaults_Get(@"CNZPrice");
    if ([NSString isEmptyString:str]) {
        str = @"";
    }
    return str;
}
//保存个人信息
+ (void)saveUserInfo:(EXUser *)userInfo;
{
    [userInfo saveModelWithPath:@"EXUser"];
}
//获取绑定信息
+ (void)getbindInfo:(SuccessHandle)successBlock
{
    EXUserManager *manager = [EXUserManager sharedEXUserManager];
    WeakObject(manager)
    [weakmanager GETWithHost:@"" path:security param:@{} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"code"] integerValue]==0)//成功
        {
            NSArray * result = responseObject[@"result"];
            [self savephone:@"0"];
            [self saveemail:@"0"];
            [self saveGoole:@"0"];
            for (NSDictionary * dic in result) {
              
                if ([dic[@"type"] integerValue] ==1)
                {
                    [self savephone:@"1"];
                }else if ([dic[@"type"] integerValue] ==0)
                {
                    [self saveemail:@"1"];
                }else if ([dic[@"type"] integerValue] ==2)
                {
                    [self saveGoole:@"1"];
                }
            }
            if (successBlock) {
                successBlock(YES);
            }
        }else
        {
            [EXUnit showMessage:responseObject[@"message"]];
          
        }
        
        if (successBlock) {
            successBlock(NO);
        }
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (successBlock) {
            successBlock(NO);
        }
    }];
}
//获取个人信息
+ (EXUser *)userInfo;
{
    return [EXUser getModelWithPath:@"EXUser"];
}

+ (Userinfo *)personalData
{
    return [Userinfo getModelWithPath:@"Userinfo"];
}
+ (void)savepersonalData:(Userinfo *)personalData
{
    [personalData saveModelWithPath:@"Userinfo"];
}

//获取汇率接口
+ (Currency *)currency
{
    return [Currency getModelWithPath:@"currency"];
}
//保存汇率接口
+ (void)saveCurrencyData:(Currency *)currency
{
    [currency saveModelWithPath:@"currency"];
}
//检测版本更新
+ (void)getVersion:(VersionInfo)versionInfo;
{
    NSString * Version = [EXUnit getVersion];
    NSArray *array = [Version componentsSeparatedByString:@"."];
   
    [self POSTWithHost:@"" path:upgrade param:@{@"app_endpoint":@"iOS",@"version_id":array[0],@"version_major":array[1],@"version_minor":array[2]} cache:NO completed:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue]==0)
        {
            if (versionInfo) {
                versionInfo(responseObject[@"result"]);
            }
            
        }else
        {
            if (versionInfo) {
                versionInfo(nil);
            }
            [EXUnit showMessage:responseObject[@"errorMsg"]];
        }
        
    } error:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (versionInfo) {
            versionInfo(nil);
        }
        
    }];
    
    
}
//删除个人信息
+ (void)removeUserInfo
{
   
    if ([[EXUnit getSite] isEqualToString:@"ZG"])
    {
        [[QYSDK sharedSDK] logout:^{
            [EXUser removeFileByFileName:@"EXUser"];
            [EXUser removeFileByFileName:@"Userinfo"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"islogin"];
            
            [[[[LCAFRequestHost sharedLCAFRequestHost] requestManager] operationQueue] cancelAllOperations];
        }];
    }else
    {
        [EXUser removeFileByFileName:@"EXUser"];
        [EXUser removeFileByFileName:@"Userinfo"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"islogin"];
        
        [[[[LCAFRequestHost sharedLCAFRequestHost] requestManager] operationQueue] cancelAllOperations];
    }
   
 
}
//是否登录就是判断token是否有效
+ (BOOL)isLogin
{
    if ([NSString isEmptyString:[EXUserManager userInfo].token])
    {
        return NO;
    }else
    {
        return YES;
    }
//  return [[EXUserManager getIslogin] isEqualToString:@"1"]? YES:NO;
}
@end
