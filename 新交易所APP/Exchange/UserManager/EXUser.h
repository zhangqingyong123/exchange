//
//  EXUser.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EXCoding.h"
@interface EXUser : EXCoding
@property (strong, nonatomic) NSString  * token;//用户的token，根据token获取用户信息
@property (strong, nonatomic) NSString  * bank_card_bound;
@property (strong, nonatomic) NSString  * country_code;
@property (strong, nonatomic) NSString  * email;
@property (strong, nonatomic) NSString  * email_bound;
@property (strong, nonatomic) NSString  * id;
@property (strong, nonatomic) NSString  * identity_authenticated;
@property (strong, nonatomic) NSString  * identity_status;
@property (strong, nonatomic) NSString  * last_ip;
@property (strong, nonatomic) NSString  * last_login;
@property (strong, nonatomic) NSString  * phone;
@property (strong, nonatomic) NSString  * phone_bound;
@property (strong, nonatomic) NSString  * token_expire;
@property (strong, nonatomic) NSString  * two_step_set;
@property (strong, nonatomic) NSString  * withdraw_password_set;
@end
@interface Userinfo : EXCoding
@property (assign, nonatomic) BOOL   bank_card_bound; //是否绑定银行卡
@property (strong, nonatomic) NSString  * country_code;
@property (strong, nonatomic) NSString  * email;
@property (assign, nonatomic) BOOL   email_bound; //是否绑定邮箱
@property (assign, nonatomic) BOOL   identity_authenticated; //是否绑定生份证
@property (strong, nonatomic) NSString  * id;
@property (strong, nonatomic) NSString  * identity_status;
@property (strong, nonatomic) NSString  * last_ip;
@property (strong, nonatomic) NSString  * last_login;
@property (strong, nonatomic) NSString  * phone;
@property (assign, nonatomic) BOOL   phone_bound; //是否绑定手机
@property (assign, nonatomic) BOOL   two_step_set; //是否绑定谷歌
@property (assign, nonatomic) BOOL   withdraw_password_set; //是否绑定资金
@end
//汇率
@interface Currency : EXCoding
@property (strong, nonatomic) NSString  * BTC_USD;
@property (strong, nonatomic) NSString  * USD_CNY;
@property (strong, nonatomic) NSString  * USD_HKD;
@property (strong, nonatomic) NSString  * USD_JPY;
@property (strong, nonatomic) NSString  * USD_TWD;
@end
