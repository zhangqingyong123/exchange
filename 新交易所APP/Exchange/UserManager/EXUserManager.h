//
//  EXUserManager.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/11.
//  Copyright © 2018年 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXUser.h"
@interface EXUserManager : NSObject
typedef void(^SuccessHandle)(BOOL isSuccess);
typedef void(^VersionInfo)(NSDictionary * dic);

/*
 *@brief用来判断是否登录如果token失效需要移除变量
 *@brief islogin ==1 表示已经登陆不需登陆
 */
+ (void)savelogin:(NSString *)islogin;

//保持默认语言
+ (void)saveDefaultLanguage:(NSString *)language;
//获取默认语言
+ (NSString *)getLanguage;
    
//获取实名认证
+ (NSString *)authenticatedName;
//用来判断是否实名认证
+ (void)saveauthenticated:(NSString *)authenticatedName;
//保持eth价格
+ (void)saveETHPrice:(NSString *)price;
//获取islogin变量判断是否登录
+ (NSString *)getETHPrice;
//保存打开z资产的状态
+ (void)saveOpenAssetsState:(NSString *)State;
//获取打开z资产的状态
+ (NSString *)getState;
//保存计价方式
+ (void)saveValuationMethod:(NSString *)valuationMethod;
//获取计价方式
+ (NSString *)getValuationMethod;
//保持CNZ价格
+ (void)saveCNZPrice:(NSString *)price;
//获取CNZ
+ (NSString *)getCNZHPrice;

//用来判断是否实名认证
+ (void)saveRealName:(NSString *)RealName;
//获取实名认证
+ (NSString *)RealName;
//用来判断手机是否验证
+ (void)savephone:(NSString *)phone;
//获取手机验证状态
+ (NSString *)phone;
//用来判断邮箱是否验证
+ (void)saveemail:(NSString *)email;
//获取邮箱验证状态
+ (NSString *)email;
//用来判断谷歌是否验证
+ (void)saveGoole:(NSString *)goole;
//获取谷歌证状态
+ (NSString *)goole;

//用来判断是否开启touchid
+ (void)saveIsTouchid:(NSString *)Touchid;
//获取touchid
+ (NSString *)Touchid;
//用来判断是否开启Gesture
+ (void)saveIsGesture:(NSString *)Gesture;
//获取Gesture
+ (NSString *)Gesture;
//获取绑定信息
+ (void)getbindInfo:(SuccessHandle)successBlock;
/*
 * @brief 获取个人信息
 * @return userInfo
 */
+ (EXUser *)userInfo;


/*
 * @brief 保存个人信息
 * @return userInfo
 */
+ (void)saveUserInfo:(EXUser *)userInfo;


/*
 * @brief 获取个人中心的信息
 * @return Userinfo
 */
+ (Userinfo *)personalData;

//检测版本更新
+ (void)getVersion:(VersionInfo)versionInfo;
/*
 * @brief 保存个人信息
 * @return userInfo
 */
+ (void)savepersonalData:(Userinfo *)personalData;


//获取汇率接口
+ (Currency *)currency;
//保存汇率接口
+ (void)saveCurrencyData:(Currency *)currency;
/**
 *@brief 删除个人信息信息
 */
+ (void)removeUserInfo;

/*
 *是否登录就是判断token是否有效或者ID是否存在
 */
+ (BOOL)isLogin;

////保存币种最高收益的币
//+ (void)saveCurrency:(HomeModle *)currency;
////获取个人信息
//+ (HomeModle *)currency;
@end
