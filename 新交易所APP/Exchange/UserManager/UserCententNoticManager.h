//
//  UserCententNoticManager.h
//  Exchange
//
//  Created by 张庆勇 on 2018/7/24.
//  Copyright © 2018年 张庆勇. All rights reserved.
#import <Foundation/Foundation.h>
static NSString *const   SetThePasswordNotificationse = @"SetThePasswordNotificationse";// 设置密码通知
static NSString *const   LoginSusessNotificationse = @"loginSusessNotificationse";// 登录成功后通知
static NSString *const   WebSoketNotificationse = @"WebSoketNotificationse";//重连
static NSString *const   VerificationSusessNotificationse = @"VerificationSusessNotificationse";// 认证成功g后通知
static NSString *const   CollectionNotificationse = @"CollectionNotificationse";// 收藏通知
static NSString *const   ExitLogonNotificationse = @"ExitLogonNotificationse";// 自动退出登录通知
static NSString *const   ManualExitLogonNotificationse = @"ManualExitLogonNotificationse";// 手动退出登录通知
static NSString *const   SetSecurityNotificationse = @"SetSecurityNotificationse";// 设置安全中心后通知
static NSString *const   bindbnakNotificationse = @"bindbnakNotificationse";// 绑定银行卡成功后
static NSString *const   updateTradeNotificationse = @"updateTradeNotificationse";// 更改c2c交易状态
static NSString *const   updateSymbolNotificationse = @"updateSymbolNotificationse";// 更新最新交易对
static NSString *const   SubimtVerificationDataNotificationse = @"SubimtVerificationDataNotificationse";// 验证资料
static NSString *const   allowRotationNotificationse = @"allowRotationNotificationse";// 反转通知
static NSString *const   leftSymbolNotificationse = @"leftSymbolNotificationse";// 测滑栏选中通知
static NSString *const   skinNotificationse = @"skinNotificationse";// 皮肤切肤通知
static NSString *const   ValuationMethodNotificationse = @"ValuationMethodNotificationse";// 更换汇率通知
static NSString *const   OTCPaypalNotificationse = @"OTCPaypalNotificationse";//otc 下单通知
static NSString *const   OTCupdateNotificationse = @"OTCupdateNotificationse";//otc 确认user/otc/update
static NSString *const   scrollViewDidEndNotificationse = @"scrollViewDidEndNotificationse";//scrollView结束滑动之后
static NSString *const   scrollViewbeginNotificationse = @"scrollViewbeginNotificationse";//scrollView开始滑动
@interface UserCententNoticManager : NSObject

@end
